import { defineConfig } from 'vitepress'
import { withMermaid } from "vitepress-plugin-mermaid";

// https://vitepress.dev/reference/site-config
export default withMermaid({
  title: "420-1N1",
  description: "1n1 introduction à la programmation",
  outDir: "../public",
  base: "/1N1_24/",
  mermaid: {
    // refer https://mermaid.js.org/config/setup/modules/mermaidAPI.html#mermaidapi-configuration-defaults for options
  },
  // optionally set additional config for plugin itself with MermaidPluginConfig
  mermaidPlugin: {
    class: "mermaid my-class", // set additional css classes for parent container 
  },
  markdown: {
    container: {
      tipLabel: 'Information',
      warningLabel: 'Attention',
      dangerLabel: 'Danger',
      infoLabel: 'Information',
      detailsLabel: 'Solution'
    }
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    docFooter: {
      prev: "Page précédente",
      next: "Page suivante"
    },
    outline:{
      label: "Sur cette page"
    },
    search: {
      provider: 'local'
    },
    nav: [
      { text: 'Accueil', link: '/' },
      
    ],

    sidebar: [
      {
        text: 'Travaux Pratiques',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/TPs/index' },
          { text: 'TP1', link: '/TPs/TP1' },
          { text: 'TP2', link: '/TPs/TP2' },
          { text: 'TP3', link: '/TPs/TP3' },
          


        ]
      },
      {
        text: 'Premier cours',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/intro/introduction' },
          { text: 'Le clavier', link: '/intro/touches' },
          { text: 'Plagiat', link: '/intro/plagiat' },

          { text: 'cmd', link: '/intro/cmd' },
          { text: 'Installation des logiciels', link: '/intro/installation_logiciels' },
          { text: 'Configuration initiale Intellij', link: '/intro/ij_config_initiale' },
          { text: 'Récupération du TP1', link: '/intro/recuperationTP1' },
        ]
      },
      {
        text: 'Algorithmie',
        collapsed: true,
        items: [
          { text: '1) Introduction', link: '/Flowgorithm/introduction' },
          { text: 'Console', link: '/Flowgorithm/console' },

        ]
      },
      {
        text: 'Introduction à Java',
        collapsed: true,
        items: [
          
          //{ text: 'Premier exemple', link: '/Java/Introduction/premier_exemple' },
          { text: '2) La programmation', link: '/Java/Introduction/programmation' },
          { text: 'Intelligence Artificielle', link: '/Java/Introduction/AI' },
          { text: 'Commentaire de code', link: '/Java/Introduction/commentaire' },
          { text: 'Variable', link: '/Java/Introduction/variable' },
          { text: 'Console', link: '/Java/Introduction/console' },
          { text: 'Menu', link: '/Java/Introduction/menu' },
          { text: 'Opérateurs mathématique', link: '/Java/Introduction/op_math' },
          { text: 'Booléen et Conditions', link: '/Java/Introduction/condition' },
          { text: 'String', link: '/Java/Introduction/string' },
          { text: 'Char', link: '/Java/Introduction/char' },
          { text: 'Type primitif vs Objet', link: '/Java/Introduction/primitif_vs_objet' },
          { text: '3) Boucle', link: '/Java/Introduction/boucle' },
          { text: 'Entrées/Sortie', link: '/Java/Introduction/entre_sortie' },
          { text: 'Math', link: '/Java/Introduction/math' },
          { text: '4) Quiz sur le plagiat', link: '/Java/quizplagiat' },
          { text: '4) Fonctions', link: '/Java/Introduction/fonction' },
          { text: 'Tableau', link: '/Java/Introduction/tableau' },
          { text: 'Tableau multi dimensions', link: '/Java/Introduction/tableaumulti' },

          { text: 'for each', link: '/Java/Introduction/foreach' },
          { text: 'Valeur vs références', link: '/Java/Introduction/valeur_vs_reference' },
          { text: 'Arrays', link: '/Java/Introduction/arrays' },
          { text: '5) Pseudo code', link: '/Java/Introduction/pseudo_code' },
          { text: 'Switch', link: '/Java/Introduction/switch' },
          { text: 'Types primitifs', link: '/Java/Introduction/type_primitif' },
          { text: '6) Énoncé du TP2', link: '/Java/Introduction/enonceTP2' },
          { text: 'Debug', link: '/Java/Introduction/debug' },
          { text: 'Exceptions', link: '/Java/Introduction/exceptions' },
          { text: 'Révision 1', link: '/Java/Introduction/revision' },

        ]
      },
      {
        text: 'Tests',
        collapsed: true,
        items: [
          { text: '8) Tests fonctionnels', link: '/Test/fonctionnel' },
        ]
      },
      {
        text: 'Greenfoot',
        collapsed: true,
        items: [
          { text: '8) Introduction', link: '/Greenfoot/introduction' },
          { text: 'Demo fusée', link: '/Greenfoot/demo_fusee' },

        ]
      },
      
      {
        text: 'Listes',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/Liste/introduction' },
        ]
      },  
      {
        text: 'Pour le TP3',
        collapsed: true,
        items: [
          { text: 'Introduction', link: '/Demo_TP3/introduction' },

          { text: 'Création du personnage', link: '/Demo_TP3/etape1' },
          { text: 'Gestion des collisions', link: '/Demo_TP3/etape2' },
          { text: 'Création d\'un niveau', link: '/Demo_TP3/etape3' },
          { text: 'Création d\'un ennemi', link: '/Demo_TP3/etape4' },
          { text: 'Tir d\'un missile', link: '/Demo_TP3/etape5' },
          { text: 'Capture d\'un objet', link: '/Demo_TP3/etape6' },
          { text: 'Affichage des points', link: '/Demo_TP3/etape7' },
          { text: 'Algorithmes utiles', link: '/Demo_TP3/etape8' },
          { text: 'Planification', link: '/Demo_TP3/planification' },


        ]
      },
      {
        text: 'Retour sur les tests fonctionnels',
        collapsed: true,
        items: [
          { text: 'Exercices tests fonctionnels', link: '/Test/exercices_tests_fonctionnels' },

          { text: 'Avec Greenfoot', link: '/Greenfoot/test_fonctionnel' },
        ]
      },
      {
        text: 'Intro à l\'orienté objet (OO)',
        collapsed: true,
        items: [
          { text: '9) Introduction', link: '/OO/introduction' },
          { text: 'Classe', link: '/OO/classe' },
          { text: 'Instance', link: '/OO/instance' },
          { text: 'Constructeur', link: '/OO/constructeur' },
          { text: 'Accès', link: '/OO/modificateur_acces' },
          { text: 'Getter Setter', link: '/OO/encapsulation' },
          { text: 'Héritage', link: '/OO/heritage' },
          { text: 'Ramasse miettes', link: '/OO/ramasse_miette' },
          { text: 'Exercice', link: '/OO/exercice' },


        ]
      },
      {
        text: 'Révision',
        collapsed: true,
        items: [
          { text: 'Exercices', link: '/Revision/exercices' },
        ]
      },             
    ],

    socialLinks: [
      { icon: 'facebook', link: 'https://www.facebook.com/groups/ancien.informatique.drummond' }
    ], 

    
  }
})
