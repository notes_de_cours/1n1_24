# Exercices de révision

Pour ces exercices, vous devez vous créer un menu à l'aide de la commande `switch case`. Vous devez donc ajouter une entrée à votre menu pour chaque exercice au fur et à mesure que vous les faites. 

:::details Solution du menu

```java
public static void main(String[] args) {
        int choix=0;
        do {
            Console.afficher("1) fonction 1");
            Console.afficher("2) fonction 2");
            // ajoutez le code pour les autres fonctions...

            Console.afficher("0) sortir");
            choix = Console.demanderEntier();
            switch (choix) {
                case (0):
                    break;
                case (1):
                    //fonction1();
                    break;
                case (2):
                    //fonction2();
                    break;
                // ajoutez le code pour les autres fonctions
                default:
                    Console.afficher("mauvais choix");
            }
        } while(choix != 0);
    }
```

:::

## 1 

Ajoutez une fonction qui 

* génère un nombre entier aléatoire entre 1 et 10
* (1) demande un entier 
* affiche si l'entier généré est plus petit, plus grand ou égal au nombre entré.
* s'il n'était pas égal, retourne à (1)
* s'il était égal, sort de la boucle et termine la fonction.


:::details pseudocode


```java
private static void fonction1() {
        // générer un nombre aléatoire entre 1 et 10
        // tant que la bonne valeur n'est pas entrée
        //    demander une valeur à l'usager
        //    afficher un message indiquant si le nombre généré est
        //       plus petit, plus grand ou égal au nombre entré
        final int MAX = 10;
        final int MIN = 1;
        // votre code ici 
    }
```

:::

:::details

```java
private static void fonction1() {
        // générer un nombre aléatoire entre 1 et 10
        // tant que la bonne valeur n'est pas entrée
        //    demander une valeur à l'usager
        //    afficher un message indiquant si le nombre généré est
        //       plus petit, plus grand ou égal au nombre entré
        final int MAX = 10;
        final int MIN = 1;
        int nombre = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        int reponse;
        do {
            Console.afficher("devinez quel nombre entier a été généré (entre " + MIN + " et " + MAX + ")");
            reponse = Console.demanderEntier();
            if (nombre > reponse) {
                Console.afficher("le nombre généré est plus grand que votre réponse");
            } else if (nombre < reponse) {
                Console.afficher("le nombre généré est plus petit que votre réponse");
            } else {
                Console.afficher("Bravo vous avez trouvé ");
            }
        } while(nombre != reponse);
    }
```

:::

## 2

Ajoutez une fonction qui 

* demande un entier entre 1 et 9 (inclusivement) (faire la validation et redemander le nombre s'il n'est pas bon)
* génère la table de multiplication pour cet entier (de 1 à 9)

ex: l'usager entre 7
le résultat sera:  
1 x 7 = 7   
2 x 7 = 14    
....    
9 x 7 = 63    

:::details pseudocode

```java
private static void fonction2() {
        // demander un nombre entre 1 et 9 inclusivement
        // redemander le nombre tant qu'il ne sera pas bon (entre 1 et 9)
        // boucler de 1 à 9
        //   afficher le compteur de la boucle multiplié par le nombre
        //      (faire un bel affichage, ex: 3 x 1 = 3)
 
    }
```

:::

:::details

```java
private static void fonction2() {
        // demander un nombre entre 1 et 9 inclusivement
        // redemander le nombre tant qu'il ne sera pas bon (entre 1 et 9)
        // boucler de 1 à 9
        //   afficher le compteur de la boucle multiplié par le nombre
        //      (faire un bel affichage , ex: 3 x 1 = 3)
        int nombre;
        do {
            Console.afficher("entrez une valeur entre 1 et 9");
            valeur = Console.demanderEntier();
            if (valeur < 1 || valeur > 9) {
                Console.afficher("mauvaise valeur");
            } else {
                pasBon = false;
            }
        }while(pasBon);

        for (int i = 1; i < 10; i++) {
            Console.afficher(nombre +" x "+ i +" = "+nombre*i);
        }
    }
```

:::


## 3

Ajoutez une fonction qui 

* génère 20 nombres entiers aléatoires entre 1 et 1000 dans un tableau
* affiche ce tableau sur une ligne 
* trouve le plus grand nombre et l'affiche
* trouve le plus petit nombre et l'affiche
* calcule la moyenne de ces nombres (moyenne = somme / 20)


:::details pseudocode

```java
    public static void fonction3() {
        // générer 20 nombres aléatoires dans une boucle
        // initialiser lPlusGrand comme étant la valeur MIN
        // initialiser lPlusPetit comme étant la valeur MAX
        // boucler pour chacun des éléments
        //    si la valeur du tableau à cette position est plus grande que lPlusGrand
        //        mettre cette valeur dans lPlusGrand
        //    si la valeur du tableau à cette position est plus petite que lPlusPetit
        //        mettre cette valeur dans lPlusPetit
        //    cumuler la somme des éléments
        // afficher les résultats (la moyenne est la somme divisée par le nombre d'éléments (en double).

        final int MAX = 1000;
        final int MIN = 1;
        final int NBR_ELEMENT  = 20;
        //votre code ici
        Console.afficher("le tableau en string: "+lTableauEnString);
        Console.afficher("la valeur la plus grande: "+lPlusGrand);
        Console.afficher("la valeur la plus petite: "+lPlusPetit);
        Console.afficher("la moyenne est de "+ lMoyenne);
    }
```

:::

:::details

```java
    public static void fonction3() {
        // générer 20 nombres aléatoires dans une boucle
        // initialiser lPlusGrand comme étant la valeur MIN
        // initialiser lPlusPetit comme étant la valeur MAX
        // boucler pour chacun des éléments
        //    si la valeur du tableau à cette position est plus grande que lPlusGrand
        //        mettre cette valeur dans lPlusGrand
        //    si la valeur du tableau à cette position est plus petite que lPlusPetit
        //        mettre cette valeur dans lPlusPetit
        //    cumuler la somme des éléments
        // afficher les résultats (la moyenne est la somme divisée par le nombre d'éléments (en double).

        final int MAX = 1000;
        final int MIN = 1;
        final int NBR_ELEMENT  = 20;
        int[] tableau = new int[NBR_ELEMENT];
        for (int i = 0; i < NBR_ELEMENT; i++) {
            tableau[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        int lPlusGrand = MIN;
        int lPlusPetit = MAX;
        int lTotal = 0;
        
        for (int i = 0; i < NBR_ELEMENT; i++) {
            if(tableau[i] > lPlusGrand){
                lPlusGrand = tableau[i];
            }
            if(tableau[i] < lPlusPetit){
                lPlusPetit = tableau[i];
            }
            lTotal += tableau[i];
        }
        String lTableauEnString = Arrays.toString(tableau);
        double lMoyenne =(lTotal*1.0)/NBR_ELEMENT);
        Console.afficher("le tableau en string: "+lTableauEnString);
        Console.afficher("la valeur la plus grande: "+lPlusGrand);
        Console.afficher("la valeur la plus petite: "+lPlusPetit);
        Console.afficher("la moyenne est de "+ lMoyenne );
    }
```

:::

## 4

Ajoutez une fonction qui simule le montant à rembourser lors d'un achat et d'un paiement fait en argent comptant (oui oui, ca existe encore des gens qui paye comptant!!)
Nous utiliserons des montants sans les cents (cennes en bon Québécois). 

* génère un montant entier aléatoire entre 10 et 100 (le total des achats)
* demande à l'usager d'entrer un montant entier supérieur à ce montant (le paiement)
* calculer le nombre minimal de 1,2,5,10,20, 100$ à remettre (les coupures Canadiennes)

Ex: achat 3$
paiement 20$
remise:  1x10,1x5, 1x2  

Dans cet exemple, 3x5 n'est pas une bonne réponse, et encore moins 17x1

:::details pseudocode

```java
    public static void fonction4() {
        final int MAX = 100;
        final int MIN = 10;

        // générer le montant d'achatTotal aléatoirement
        // demander le montant du paiement tant qu'il est plus petit que le montant d'achatTotal
        // calculer le montant restant à payer à partir de l'achatTotal et du paiement
        // créer un tableau représentant le montant des coupures (20,10,5,2,1)
        // créer un variable pour représenter le type de coupure à traiter (servira comme index dans lCoupures) (typeDeCoupure)
        // tant que le restant n'est pas 0
        //    le nombre de billets à afficher est le montant restant, divisé par le montant de la coupure  (lCoupures[typeDeCoupure])
        //    si le nombre de billets pour cette coupure est plus grand que 0
        //       soustraire du montant restant le nombre de billets multiplié par le montant pour ce type de coupure
        //       afficher le nombre de billets et le montant pour cette coupure
        //    incrémenter le type de coupure
        //
        
         int[] lCoupures = {100, 20, 10, 5, 2, 1};
        // votre code ici
        
    }
```

:::

:::details 

```java
    public static void fonction4() {
        final int MAX = 100;
        final int MIN = 10;

        // générer le montant d'achatTotal aléatoirement
        // demander le montant du paiement tant qu'il est plus petit que le montant d'achatTotal
        // calculer le montant restant à payer à partir de l'achatTotal et du paiement
        // créer un tableau représentant le montant des coupures (20,10,5,2,1)
        // créer un variable pour représenter le type de coupure à traiter (servira comme index dans lCoupures) (typeDeCoupure)
        // tant que le restant n'est pas 0
        //    le nombre de billets à afficher est le montant restant, divisé par le montant de la coupure  (lCoupures[typeDeCoupure])
        //    si le nombre de billets pour cette coupure est plus grand que 0
        //       soustraire du montant restant le nombre de billets multiplié par le montant pour ce type de coupure
        //       afficher le nombre de billets et le montant pour cette coupure
        //    incrémenter le type de coupure
        //

        int lAchatTotal = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        int lPaiement = 0;
        do {
            Console.afficher("Entrez le paiement pour l'achat de $"+lAchatTotal);
            lPaiement = Console.demanderEntier();
        } while(lPaiement < lAchatTotal);

        int lRestant = lPaiement-lAchatTotal;

        int[] lCoupures = {100, 20, 10, 5, 2, 1};

        int lTypeCoupure = 0;
        if(lRestant == 0) {
            Console.afficher("Le montant a été payé au complet");
        } else {
            Console.afficher("Vous pouvez payer avec les coupures suivantes : ");
        }
        while (lRestant != 0) {
            int lNombreBillet = lRestant / lCoupures[lTypeCoupure];
            if(lNombreBillet > 0 ) {
                lRestant = lRestant - lNombreBillet*lCoupures[lTypeCoupure];
                Console.afficher(lNombreBillet+"x"+lCoupures[lTypeCoupure]);
            }
            lTypeCoupure++;
        }
    }
```

:::

## 5

Ajoutez une fonction qui calcule le nombre de secondes dans une période. 

La fonction doit demander un nombre de jours, d'heures, de minutes, et de secondes

Elle affiche le nombre de seconde pour cette période.

Ex: j = 2, h = 5, m = 34, s = 2

le résultat est : 192842


:::details pseudocode

```java
    public static void fonction5() {
        // créez des constantes pour représenter:
        //    le nombre de secondes dans une minute
        //    le nombre de secondes dans une heure (vous pouvez vous servir de la constant que vous venez de définir, une heure c'est 60 minutes)
        //    le nombre de secondes dans une journée (encore ici, utilisez la constante pour les heures (un jour c'est 24 heures)
        // demandez les nombres
        // faire le calcul

        // votre code ici
    }
```

:::

:::details 

```java
    public static void fonction5() {
        // créez des constantes pour représenter:
        //    le nombre de secondes dans une minute
        //    le nombre de secondes dans une heure (vous pouvez vous servir de la constant que vous venez de définir, une heure c'est 60 minutes)
        //    le nombre de secondes dans une journée (encore ici, utilisez la constante pour les heures (un jour c'est 24 heures)
        // demandez les nombres
        // faire le calcul

        final int MINUTE_EN_SECONDE = 60;
        final int HEURE_EN_SECONDE = MINUTE_EN_SECONDE * 60;
        final int JOUR_EN_SECONDE = HEURE_EN_SECONDE * 24;

        Console.afficher("Combien de jours :");
        int j = Console.demanderEntier();
        Console.afficher("Combien d'heures :");
        int h = Console.demanderEntier();
        Console.afficher("Combien de minutes :");
        int m = Console.demanderEntier();
        Console.afficher("Combien de secondes :");
        int s = Console.demanderEntier();

        Console.afficher("Le résultat est:"+(j*JOUR_EN_SECONDE+h*HEURE_EN_SECONDE+m*MINUTE_EN_SECONDE+s));
    }
```

:::

## 6

Ajoutez une fonction qui génère 1000 nombres aléatoires entre  0 et 9 (inclusivement)

Calculez la fréquence des 0, des 1, des 2 ... des 9 générés. 

Affichez cettte fréquence. 

:::details pseudocode

```java
    public static void fonction6() {
        // créer un tableau de 10 entiers (la fréquence pour chacun des nombres de 0 à 9)
        // faire une boucle de 1000 répétitions
        //   générer un nombre aléatoire entre 0 et 9 (inclusivement)
        //   ajouter 1 à la fréquence pour ce nombre
        // faire une boucle pour afficher les 10 valeurs récoltées. 
        final int MAX = 9;
        final int MIN = 0;
        final int NBR_ELEMENT  = 1000;
        // votre code
    }
```

:::

:::details 

```java
    public static void fonction6() {
        // créer un tableau de 10 entiers (la fréquence pour chacun des nombres de 0 à 9)
        // faire une boucle de 1000 répétitions
        //   générer un nombre aléatoire entre 0 et 9 (inclusivement)
        //   ajouter 1 à la fréquence pour ce nombre
        // faire une boucle pour afficher les 10 valeurs récoltées. 
        final int MAX = 9;
        final int MIN = 0;
        final int NBR_ELEMENT  = 1000;
        int[] lFrequence = new int[10];
        for (int i = 0; i < NBR_ELEMENT; i++) {
            int lNombre = (int) (Math.random()*(MAX-MIN+1)) + MIN;
            lFrequence[lNombre]++;
        }
        for (int i = 0; i < 9; i++) {
            Console.afficher("fréquence des "+i+" = "+lFrequence[i]);
        }
    }
```

:::

## 7

Ajouter une fonction qui calcule la fréquence des caractères dans une chaine de caractères fournie par l'usager. 

Les caractères acceptables sont les caractères ascii de base (code 32 à 127). 

Note: un caractère (char) peut être comparé à un entier. Dans ce cas, c'est le code ascii du char qui sera utilisé comme valeur. 
Par exemple, si j'ai le caractère 'c', son code ascii est 99. La comparaison suivante donnera donc true: if('c'==99)

:::details pseudocode

```java
    public static void fonction7() {
        // créer un tableau pour contenir les fréquences.
        //    Il y a 128 caractères
        //    mais les 32 premiers sont des caractères de contrôles
        //    et le caractère 127 est aussi impossible à écrire
        //    Le tableau devrait donc contenir moins que 128 entrées... à vous de le calculer
        // Demandez la chaine de caractères à analyser
        // Pour chacun des caractères de la chaine
        //   si le caractère est acceptable (ex: é, à ne sont pas acceptables car leur code ascii est  > 128)
        //          ajoutez 1 à la fréquece de ce caractère.
        //          (attention: votre tableau n'a pas 128 position, il faut donc soustraire 32 du code ascii)
        // Pour chacune des valeurs dans le tableau de fréquences
        //   si la fréquence pour cette position n'est pas 0
        //      affichez la fréquence
        //        (note: pour convertir un entier (la position dans le tableau) en son caractère correspondant:
        //              Character.toString((char) (position + 32))
        //        (ex:  fréquence pour 'a' = 3 )


        final int PREMIER_BON_CARACTERE = 32;
        final int DERNIER_BON_CARACTERE = 127;
        final int NOMBRE_CARACTERES_VALIDES = DERNIER_BON_CARACTERE-PREMIER_BON_CARACTERE;
        int[] lFrequence = new int[NOMBRE_CARACTERES_VALIDES];
        Console.afficher("Entrez une longue chaine de caractères : ");
        String lChaine = Console.demanderTexte();
        // votre code 
    }
```

:::


:::details 

```java
    public static void fonction7() {
        // créer un tableau pour contenir les fréquences.
        //    Il y a 128 caractères
        //    mais les 32 premiers sont des caractères de contrôles
        //    et le caractère 127 est aussi impossible à écrire
        //    Le tableau devrait donc contenir moins que 128 entrées... à vous de le calculer
        // Demandez la chaine de caractères à analyser
        // Pour chacun des caractères de la chaine
        //   si le caractère est acceptable (ex: é, à ne sont pas acceptables car leur code ascii est  > 128)
        //          ajoutez 1 à la fréquece de ce caractère.
        //          (attention: votre tableau n'a pas 128 position, il faut donc soustraire 32 du code ascii)
        // Pour chacune des valeurs dans le tableau de fréquences
        //   si la fréquence pour cette position n'est pas 0
        //      affichez la fréquence
        //        (note: pour convertir un entier (la position dans le tableau) en son caractère correspondant:
        //              Character.toString((char) (position + 32))
        //        (ex:  fréquence pour 'a' = 3


        final int PREMIER_BON_CARACTERE = 32;
        final int DERNIER_BON_CARACTERE = 127;
        final int NOMBRE_CARACTERES_VALIDES = DERNIER_BON_CARACTERE-PREMIER_BON_CARACTERE;
        int[] lFrequence = new int[NOMBRE_CARACTERES_VALIDES];
        Console.afficher("Entrez une longue chaine de caractères : ");
        String lChaine = Console.demanderTexte();

        for (int i = 0; i < lChaine.length(); i++) {
            char c = lChaine.charAt(i);
            if(c>= PREMIER_BON_CARACTERE && c < DERNIER_BON_CARACTERE) { //ATTENTION ici c'est >= et < car le caractère 32 est bon, mais 127 n'est pas bon. 
                lFrequence[c - PREMIER_BON_CARACTERE]++;
            }
        }

        for (int i = 0; i < NOMBRE_CARACTERES_VALIDES; i++) {
            if (lFrequence[i] != 0) {
                Console.afficher("fréquence pour '" + Character.toString((char) (i + PREMIER_BON_CARACTERE)) + "' = " + lFrequence[i]);
            }
        }
    }
```

:::

## 8

Ajoutez une fonction qui demande un nombre N et qui trouve les N premiers nombres premiers. 

:::details pseudocode

```java
    public static void fonction8() {
        // demandez une valeur entière qui sera la taille du tableau
        // initialisez le tableau d'entiers de la taille demandée
        // faire une boucle de 0 à la taille du tableau.
        //   incrémenter le nombreATester et boucler tant qu'il n'est pas un nombre premier
        //      (note: créez vous une fonction qui vérifie si un nombre est premier ou non)
        //      ( vous avez déjà fait une fonction qui fait ca.)
        //   une fois que vous avez trouvé un nombre premier, ajoutez-le dans le tableau
        // une fois le tableau rempli, affichez-le (vous pouvez utiliser Arrays.toString())

        int lNombreATester=1;
        int[] nombresPremier;

        Console.afficher("entrez un nombre");
        int lNombreElements = Console.demanderEntier();
        nombresPremier = new int[lNombreElements];
        // votre code ici 
        Console.afficher(Arrays.toString(nombresPremier));
    }

    // et ajoutez la fonction pour déterminer si un nombre est entier
```

:::

:::details 


```java
   public static void fonction8() {
        // demandez une valeur entière qui sera la taille du tableau
        // initialisez le tableau d'entiers de la taille demandée
        // faire une boucle de 0 à la taille du tableau.
        //   incrémenter le nombreATester et boucler tant qu'il ne sera pas un nombre premier
        //      (note: créez vous une fonction qui vérifie si un nombre est premier ou non)
        //      ( vous avez déjà fait une fonction qui fait ca.)
        //   une fois que vous avez trouvé un nombre premier, ajoutez-le dans le tableau
        // une fois le tableau rempli, affichez-le (vous pouvez utiliser Arrays.toString())

        int lNombreATester=1;
        int[] nombresPremier;

        Console.afficher("entrez un nombre");
        int lNombreElements = Console.demanderEntier();
        nombresPremier = new int[lNombreElements];

        for (int i = 0; i < lNombreElements; i++) {
            do {
                lNombreATester++;
            } while(!estNombrePremier(lNombreATester));
            nombresPremier[i] = lNombreATester;
        }
        Console.afficher(Arrays.toString(nombresPremier));
    }

    public static boolean estNombrePremier(int n) {// solution générée par chatgpt :)
        boolean estPremier = true;
        if (n <= 1) {
            estPremier = false; // Les nombres négatifs, 0 et 1 ne sont pas premiers
        } else if (n <= 3) {
            estPremier = true; // 2 et 3 sont premiers
        } else if (n % 2 == 0 || n % 3 == 0) {
            estPremier =  false; // Les multiples de 2 ou de 3 ne sont pas premiers
        } else {
            // Vérification pour les autres nombres
            for (int i = 5; i * i <= n; i += 6) {
                if (n % i == 0 || n % (i + 2) == 0) {
                    estPremier = false;
                }
            }
        }

        return estPremier;
    }

```

:::

## 9

Ajoutez une fonction qui génère 100 nombres aléatoires dans un tableau, et qui recopie les valeurs paires dans un second tableau et les impaires dans un troisième. 

Affichez les trois tableaux à la fin. 


:::details pseudocode


```java
    public static void fonction9() {
        // initialiser un compteur pour la position dans le tableau pair, et un pour le tableau impair
        // boucler pour toutes les valeurs du tableau d'entiers générés.
        //    si la valeur dans le tableau généré est pair
        //        l'ajouter au tableau des valeurs pairs
        //        et incrémenter le compteur pour la position dans le tableau pair
        //    sinon
        //        faire la même chose, mais pour le tableau impair. 
        // afficher les 3 tableaux. 
        
        final int MAX = 1000;
        final int MIN = 1;
        final int NBR_ELEMENT  = 100;
        int[] tableau = new int[NBR_ELEMENT];
        int[] tableauPair = new int[NBR_ELEMENT];
        int[] tableauImpair = new int[NBR_ELEMENT];
        for (int i = 0; i < NBR_ELEMENT; i++) {
            tableau[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        // votre code
        Console.afficher(Arrays.toString(tableau));
        Console.afficher(Arrays.toString(tableauPair));
        Console.afficher(Arrays.toString(tableauImpair));
    }
```
:::

:::details


```java
        public static void fonction9() {
        // initialiser un compteur pour la position dans le tableau pair, et un pour le tableau impair
        // boucler pour toutes les valeurs du tableau d'entiers générés.
        //    si la valeur dans le tableau généré est pair
        //        l'ajouter au tableau des valeurs pairs
        //        et incrémenter le compteur pour la position dans le tableau pair
        //    sinon
        //        faire la même chose, mais pour le tableau impair. 
        // afficher les 3 tableaux. 
        
        final int MAX = 1000;
        final int MIN = 1;
        final int NBR_ELEMENT  = 100;
        int[] tableau = new int[NBR_ELEMENT];
        int[] tableauPair = new int[NBR_ELEMENT];
        int[] tableauImpair = new int[NBR_ELEMENT];
        for (int i = 0; i < NBR_ELEMENT; i++) {
            tableau[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        int posPair = 0;
        int posImpair = 0;
        for (int i = 0; i < NBR_ELEMENT; i++) {
            if(tableau[i]%2 == 0) {
                tableauPair[posPair] = tableau[i];
                posPair++;
            } else {
                tableauImpair[posImpair] = tableau[i];
                posImpair++;
            }
        }
        Console.afficher(Arrays.toString(tableau));
        Console.afficher(Arrays.toString(tableauPair));
        Console.afficher(Arrays.toString(tableauImpair));
    }
```

:::


## 10

Ajoutez une fonction qui fusionne 2 tableaux  de nombres entiers aléatoires. 
Ces 2 tableaux sont triés. 

Le code pour générer les 2 tableaux triés vous est donné. 

Vous devez écrire le code qui prend ces 2 tableaux et les fusionne en un troisième tout en gardant le tri. 

Ex:  
t1 = [1, 2, 3, 4, 5]
t2 = [3, 3, 4, 6]

le résultat t3 sera donc: [1, 2, 3, 3, 3, 4, 4, 5, 6]

les cas à prévoir:

* le premier tableau est le plus court
* le deuxième tableau est le plus court
* les dernières données du premier tableau sont les plus grandes
* les dernières données du deuxième tableau sont les plus grandes

Les tableaux ne seront pas vides. Ils contiendront entre 3 et 20 éléments. 



:::details pseudocode


```java
package ca.cegepdrummond;

import java.util.Arrays;
import java.util.Scanner;

public class Programme {


    public static void main(String[] args) {

        final int MAX = 20;
        final int MIN = 3;
        int nbr_element1  = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        int nbr_element2  = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        int[] tableau1 = new int[nbr_element1];
        int[] tableau2 = new int[nbr_element2];
        int[] tableauTrie = new int[nbr_element1+nbr_element2];
        for (int i = 0; i < nbr_element1; i++) {
            tableau1[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        for (int i = 0; i < nbr_element2; i++) {
            tableau2[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        Arrays.sort(tableau1);
        Arrays.sort(tableau2);
        Console.afficher(Arrays.toString(tableau1));
        Console.afficher(Arrays.toString(tableau2));

        // pour la longueur de tableauTrie
        //    si rendu à la fin de tableau1
        //       transférer dans tableauTrie le prochain élément de tableau2 afin de le vider
        //       incrémenter la position dans tableau2
        //    sinon si rendu à la fin de tableau2
        //       transférer dans tableauTrie le prochain élément de tableau1 afin de le vider
        //       incrémenter la position dans tableau1
        //    sinon si le prochain élément de tableau1 est plus grand que le prochain élément de tableau2
        //       transférer dans tableauTrie le prochain élément de tableau2
        //       incrémenter la position dans tableau2
        //    sinon 
        //       transférer dans tableauTrie le prochain élément de tableau1 
        //       incrémenter la position dans tableau1
        
        // votre code ici 
        
        Console.afficher(Arrays.toString(tableauTrie));
    }
}
```

:::

:::details


```java
package ca.cegepdrummond;

import java.util.Arrays;
import java.util.Scanner;

public class Programme {


    public static void main(String[] args) {

        final int MAX = 20;
        final int MIN = 3;
        int nbr_element1  = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        int nbr_element2  = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        int[] tableau1 = new int[nbr_element1];
        int[] tableau2 = new int[nbr_element2];
        int[] tableauTrie = new int[nbr_element1+nbr_element2];
        for (int i = 0; i < nbr_element1; i++) {
            tableau1[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        for (int i = 0; i < nbr_element2; i++) {
            tableau2[i] = (int) (Math.random()*(MAX-MIN+1)) + MIN;
        }
        Arrays.sort(tableau1);
        Arrays.sort(tableau2);
        Console.afficher(Arrays.toString(tableau1));
        Console.afficher(Arrays.toString(tableau2));

        // pour la longueur de tableauTrie
        //    si rendu à la fin de tableau1
        //       transférer dans tableauTrie le prochain élément de tableau2 afin de le vider
        //       incrémenter la position dans tableau2
        //    sinon si rendu à la fin de tableau2
        //       transférer dans tableauTrie le prochain élément de tableau1 afin de le vider
        //       incrémenter la position dans tableau1
        //    sinon si le prochain élément de tableau1 est plus grand que le prochain élément de tableau2
        //       transférer dans tableauTrie le prochain élément de tableau2
        //       incrémenter la position dans tableau2
        //    sinon 
        //       transférer dans tableauTrie le prochain élément de tableau1 
        //       incrémenter la position dans tableau1

        int posTableau1 = 0;
        int posTableau2 = 0;
        int petit;
        for (int i = 0; i < tableauTrie.length; i++) {
            if(posTableau1==tableau1.length) { // rendu à la fin de tableau 1, on vide tableau2
                petit = tableau2[posTableau2];
                posTableau2++;
            } else if(posTableau2==tableau2.length) { // rendu à la fin de tableau 2, on vide tableau1
                petit = tableau1[posTableau1];
                posTableau1++;
            } else if(tableau1[posTableau1]>tableau2[posTableau2]){
               petit = tableau2[posTableau2];
               posTableau2++;
            } else {
                petit = tableau1[posTableau1];
                posTableau1++;
            }
            tableauTrie[i]=petit;
        }
        Console.afficher(Arrays.toString(tableauTrie));
    }
}
```

:::

## 11

**Pour cet exercice, vous devez créer un nouveau projet.** 

Créez le programme répondant aux critères suivants:

* doit contenir 2 classes: **Classe1** et **Classe2**
* **Classe1** a un attribut de type **int** appelé **nombre**
* **Classe1** doit avoir un **constructeur** prenant un **int** en argument et qui met **nombre** à la valeur de cet argument
* **Classe1** doit avoir un second **constructeur** n'ayant aucun argument et qui appel le premier **constructeur** en fournissant la valeur 10
* **Classe2** est une sous-classe de **Classe1**
* **Classe2** doit avoir un constructeur prenant un **double** en argument et qui appel le super-constructeur (celui de **Classe1**) en lui fournissant la valeur entière du double reçu en argument (cast). 
* le programme principale crée un objet de type **Classe1** en lui envoyant une valeur entière
* le programme principale crée un objet de type **Classe1** en ne lui envoyant pas de valeur en argument
* le programme principale crée un objet de type **Classe2** en lui envoyant **PI** en argument
* le programme principale affiche la valeur stockée dans chacun des 3 objets
* vous devez respecter les critères d'encapsulation (getter, setter, variable private)



:::details


**Programme principale**

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        Classe1 obj1 = new Classe1(3);
        Classe1 obj2 = new Classe1();
        Classe2 obj3 = new Classe2(Math.PI);

        System.out.println(obj1.getValeur());
        System.out.println(obj2.getValeur());
        System.out.println(obj3.getValeur());

    }
}
```

**Classe1**

```java
package ca.cegepdrummond;

public class Classe1 {
    private int nombre;

    public Classe1(int aValeur) {
        nombre = aValeur;
    }

    public Classe1() {
        this(10);
    }

    public int getValeur() {
        return nombre;
    }
    
}

```

**Classe2**

```java
package ca.cegepdrummond;

public class Classe2 extends Classe1 {
    public Classe2(double aValeur) {
        super((int)aValeur);
    }
}

```
:::