# String

## Type String

Le type String est un objet (nous allons revenir sur le concept d'objet d'ici peu) contenant une liste de caractères.

Exemple

```java
"Bonjour à tous!"
```

Contient 15 caractères incluant les espaces.

## Création d'une String

Pour créer une String, il suffit d'écrire du texte entre 2 guillemets doubles.

```java
String exemple = "Voici un exemple";
Console.afficher(exemple);
```

<!-- Puisqu'une String est un objet, on pourrait l'instancier comme avec `Scanner`.

```java
String instance = new String("Une instance");
```

En réalité, `"une string"` est un raccourci pour écrire `new String("une string")`.

-->

## Sauts de ligne

On peut ajouter des sauts de ligne à l'intérieur d'une même String.

```java
String sautDeLigne = "Voici un exemple\navec saut de ligne...";
Console.afficher(sautDeLigne);
```

Résultat:

> Voici un exemple
>
> avec saut de ligne...

Il ne faut pas abuser de `\n`. Par exemple, cette facon de faire un menu n'est pas acceptable:

```java
String menu = "1) choix 1\n2) choix 2\n3)choix 3\n4) choix4\n5)choix 5\n6)choix 6\n0) sortir";
```

car c'est illisible. 


:::danger
L'utilisation abusive de **\n** ne sera pas tolérée et vous fera perdre des points.
:::

## Taille 

Une String peut avoir de 0 à 2 147 483 647  caractères.

```java
String vide = "";
```

Puisqu'une String est un objet, on peut aussi créer la variable, mais ne pas initialiser l'objet.

```java
String chaineNonInitialisee = null;
```

`null` est un mot réservé qui indique que l'objet n'est pas initialisé encore.

`""` représente une String initialisée, mais vide. Donc de longueur 0.



## Méthodes d'une String

* `length()` : retourne le nombre de caractères dans la String

```java
String s = "test";
int len = s.length(); // len = 4
```

:::warning

Si vous écrivez `chaineNonInitialisee.length();` vous aurez une erreur, car on ne peut appeler une fonction sur un objet null. 

Alors que `vide.length()` retournera 0. 
:::

* `charAt(int index)` : retourne le caractère à la position de `index` en commençant à 0

```java
String s = "test";
char premier = s.charAt(0); // t
char troisieme = s.charAt(2); // s
char dernier = s.charAt(s.length() - 1); // t
```

* `isEmpty()` : retourne vrai si la chaîne de caractères est vide, sinon retourne faux
:::warning

Si vous écrivez `chaineNonInitialisee.isEmpty();` vous aurez une erreur, car on ne peut appeler une fonction sur un objet null. 

Pour vérifier si une String est initialisée, il faut utiliser:  `chaine == null`
:::

* `toUpperCase()` : retourne la chaîne de caractères en majuscule
* `toLowerCase()` : retourne la chaîne de caractères en minuscule
* `startsWith(String prefix)` : retourne vrai si la chaîne de caractères débute par `prefix`
* `contains(CharSequence s)` : retourne vrai si `s` (String ou char) se trouve dans la chaîne de caractères
* `substring(int indexDebut, int indexFin)` : retourne une nouvelle chaîne de caractères contenant les caractères de indexDebut jusqu'à indexFin exclusivement (c.à.d. que le caractère à indexFin ne sera pas retourné)
* `indexOf(String sousChaine)` : retourne l'index (débutant à 0) du début de `sousChaine` dans la chaîne de caractères
* `replace(CharSequence ancien, CharSequence nouveau)` : retourne une nouvelle chaîne de caractères en remplacant `ancien` (String ou char) par `nouveau` (String ou char)

```java
String texte = "Ceci est une simple string";
boolean vide = texte.isEmpty(); // false
String texteEnMajuscule = texte.toUpperCase(); // "CECI EST UNE SIMPLE STRING"
boolean debuteParCeci = texteEnMajuscule.startsWith("CECI"); // true
String pasSiSimple = texte.substring(texte.indexOf("simple"), texte.length()); // "simple string"
String suppressionDesEspaces = texteEnMajuscule.replace(" ", ""); // "CECIESTUNESIMPLESTRING"
```

* `concat(String unAutreTexte)` : retourne une nouvelle chaîne de caractères contenant l'ancien contenu auquel est ajouté le contenu de unAutreTexte
  * Il est aussi possible d'utiliser le signe `+` (ex.: `"string 1" + " string 2"`)

```java
String prenom = "Stéphane";
String nom = "Janvier";

String nomComplet = prenom + " " + nom; // "Stéphane Janvier"
String nomComplet2 = prenom.concat(" ").concat(nom); // "Stéphane Janvier"
```

## Problème de conversion automatique lors du Concat

Java convertit automatiquement le type de données au fur et à mesure.

```java
String test = 10 + 20 + "allo"; // "30allo"
```

Ici, Java calcule, selon la priorité des opérations, `10 + 20` (calcul arithmétique) d'abord et `30 + "allo"` (concaténation) ensuite.

```java
String test2 = "allo" + 10 + 20; // "allo1020"
```

Ici, Java calcule, selon la priorité des opérations, `"allo" + 10` (concaténation) d'abord et `"allo10" + 20` (concaténation) ensuite.

```java
String test3 = "allo" + 10 * 20; // "allo200"
```

Ici, Java calcule, selon la priorité des opérations, `10 * 20` (calcul arithmétique) d'abord et `"allo" + 200` (concaténation) ensuite.

:::tip
Si vous avez remarqué, dans Flowgorithm, l'opérateur de concaténation est `&`. Ce problème d'addition vs concaténation ne s'applique donc pas. 
:::

## Transformer les String en int

Il est fréquent de demander à l'utilisateur d'entrer un entier au clavier. Afin de parer à toutes éventualité, il est courant de faire l'entrée de données en utilisant une string, de vérifier qu'elle ne contient que des nombres, et d'en faire la convertion par la suite. Voici un exemple :

```java
public class Programme {
    public static void main(String[] args) {
        String texte = "33";
        int nombre = Integer.parseInt(texte);
    }
}
```
:::warning Attention: 
s'il y a autre chose que des nombres dans la string, vous aurez une erreur. 
Nous verrons plus tard qu'il est possible *d'attraper* ce type d'erreur afin de la traiter. 
:::

## Comparer une String correctement

:::warning 
Une String étant un objet, **vous ne devez pas** comparer 2 String avec `==` ou `!=`. (il y'a des exceptions, mais nous n'en aurons pas besoin pour ce cours)

Une exception que nous avons déjà vue est la comparaison avec null: `chaine == null` 
:::

Vous devez utiliser l'une de ces 2 méthodes afin de faire la comparaison entre 2 String :

* `equals(String other)`
* `equalsIgnoreCase(String other)`

```java
String a = "test";
String b = "test2";

String c = "test";
String d = "TEST2";

boolean b1 = a.equals(b); // false
boolean b2 = a.equals(c); // true

boolean b3 = b.equals(c); // false
boolean b4 = b.equalsIgnoreCase(d); // true
```

## Échappement pour les caractères spéciaux

Il existe plusieurs caractères spéciaux dans une String en Java :

* `"` : débute ou termine une String
* `\"` : affiche un guillemet double
* `\n` : saut de ligne
* `\r` : retour de chariot (pour Windows le saut de ligne s'écrit `\r\n`, pour Linux, c'est seulement `\n`)
* `\t` : tabulation
* `\\`: affiche le \
* ...

Si on doit écrire ces caractères dans notre String, on doit échapper le caractère en utilisant le symbole `\`.

```java
String test = "Voici différents caractères à échapper en Java : \\n, \\r, \\t, \", etc.";
// Voici différents caractères à échapper en Java : \n, \r, \t, ", etc.
```


# Exercices

* Écrivez un programme qui demande un mot à l'usager.
* Affichez la longeur de ce mot.
* Si la longueur est plus grande que 5 caractères, afficher le mot en majuscules.  
* Si ce mot contient la lettre `c` affichez les premiers caractères jusqu'au `c` inclusivement. 
* Demandez un deuxième mot à l'usager. 
* Affichez le mot le plus court. 
* Comparez les deux mots et affichez si c'est le même mot ou pas. 
  

:::details

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("Entrez le premier mot: ");
        String mot = Console.demanderTexte();
        int longueur = mot.length();
        if(longueur > 5 ) {
            String motEnMajuscule = mot.toUpperCase();
            Console.afficher("Le mot en majuscule: "+ motEnMajuscule);
        }
        if(mot.contains("c")) {
            Console.afficher("le mot jusqu'au c: " + mot.substring(0,mot.indexOf("c")+1));
        } else {
            Console.afficher("le mot ne contient pas de c");
        }

        Console.afficher("Entrez le deuxième mot: ");
        String mot2 = Console.demanderTexte();
        if(mot.length() < mot2.length()) {
            Console.afficher("le premier mot (" + mot +") est le plus court");
        } else if(mot.length() > mot2.length()) {
            Console.afficher("le deuxième mot (" + mot2 +") est le plus court");
        } else {
            Console.afficher("ils ont la même longueur");
        }

        if(mot.equals(mot2) ) {
            Console.afficher("les deux mots sont identiques");
        } else {
            Console.afficher("les deux mots sont différents");
        }
    }
}

```

:::