# Révision de Java

Pour ces exercices, nous allons revoir les notions vues jusqu'à maintenant. 

Le pseudocode vous est fourni afin de vous aider. Libre à vous de trouver d'autres solutions. 

## Exercice 1: Manipulation de String

Cet exercice vous fait pratiquer:
1) fonction avec paramètres
2) les retours de fonctions
3) les tableaux
4) la manipulation de caractères
   
Voici le code principal ainsi que le pseudocode pour les fonctions.

À vous de compléter le code

:::details Énoncé avec pseudocode

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("Entrez un texte ");
        String lTexte = Console.demanderTexte();
        int nombreDeLettre = compterLettre(lTexte);
        Console.afficher("Il y'a "+nombreDeLettre+" lettre(s) dans ce mot");
        String chameau = transformerEnChameau(lTexte);
        Console.afficher("Le texte en chameau :"+chameau);
        int[] codesAscii = convertirEnValeurAscii(lTexte);
        Console.afficher("Les valeurs ascii pour ce texte sont: "+ Arrays.toString(codesAscii));
    }

    /**
     * Cette fonction retourne un tableau contenant le code ascii pour chaque
     * caractère de la String en argument
     * Notez que ce code ne fonctionne que pour des caractères contenus dans le code ascii.
     * @param aTexte la String à analyser
     * @return un tableau contenant le code ascii de chaque caractère.
     */
    private static int[] convertirEnValeurAscii(String aTexte) {
        // boucler sur les caractères de aTexte
        //    ajouter le code ascii du caractère à la position x dans resultat à la position x
        // une fois tous les caractères traités, retourner resultat
        
        // vous pouvez trouver la table des caractères ascii sur https://www.asciitable.com/

        // astuce: pour convertir un caractère en son code ascii, il suffit de le convertir en int
        //         à l'aide de l'opérateur de cast.
        // exemple: si j'ai le caractère 'a', et que j'écris 
        //  int x =   (int) 'a'
        // j'obtiens son code ascii qui est 97 

        int[] resultat = new int[aTexte.length()];
        // votre code ici 
        return resultat;
    }

    /**
     * Cette fonction prend une chaine de caractère en argument et retourne cette
     * même chaine en mettant les caractères à la position impaire en majuscule, et les autres
     * en minuscule
     * Exemple: Bonjour
     * devient: bOnJoUr
     * @param aTexte le texte à convertir
     * @return le texte mis en super camelCase
     */
    private static String transformerEnChameau(String aTexte) {
        // convertir aTexte en minuscule
        // boucler sur les caractères de aTexte en minuscule
        //    déclarer un caractère appelé c qui sera égale au caractère de aTexte en minuscule à la position de la boucle
        //    si la position est impaire (vous vous souvenez de modulo ?)
        //        mettre ce caractère en majuscule
        //    ajouter le caractère à la fin de résultat
        // retourner resultat une fois tous les caractères traités
        String resultat = "";
        // votre code ici
        return resultat;
    }

    /**
     * Cette fonction compte les lettres dans un mot. Les chiffres ne sont pas comptés
     * @param aTexte le mot à analyser
     * @return le nombre de lettre dans le mot.
     */
    private static int compterLettre(String aTexte) {
        // boucler sur les caractères de aTexte
        //    si un caractère est une lettre, incrémenter le compteur
        // une fois tous les caractères analysés, retourner le compteur.
        int lCompteur = 0;
        //votre code ici 
        return lCompteur;
    }

}


```

:::

:::details

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("Entrez un texte ");
        String lTexte = Console.demanderTexte();
        int nombreDeLettre = compterLettre(lTexte);
        Console.afficher("Il y'a "+nombreDeLettre+" lettre(s) dans ce mot");
        String chameau = transformerEnChameau(lTexte);
        Console.afficher("Le texte en chameau :"+chameau);
        int[] codesAscii = convertirEnValeurAscii(lTexte);
        Console.afficher("Les valeurs ascii pour ce texte sont: "+ Arrays.toString(codesAscii));
    }

    /**
     * Cette fonction retourne un tableau contenant le code ascii pour chaque
     * caractère de la String en argument
     * Notez que ce code ne fonctionne que pour des caractères contenus dans le code ascii.
     * @param aTexte la String à analyser
     * @return un tableau contenant le code ascii de chaque caractère.
     */
    private static int[] convertirEnValeurAscii(String aTexte) {
        // boucler sur les caractères de aTexte
        //    ajouter le code ascii du caractère à la position x dans resultat à la position x
        // une fois tous les caractères traités, retourner resultat

        // vous pouvez trouver la table des caractères ascii sur https://www.asciitable.com/

        // astuce: pour convertir un caractère en son code ascii, il suffit de le convertir en int
        //         à l'aide de l'opérateur de cast.
        // exemple: si j'ai le caractère 'a', et que j'écris 
        //  int x =   (int) 'a'
        // j'obtiens son code ascii qui est 97 
        int[] resultat = new int[aTexte.length()];
        for (int i = 0; i < aTexte.length(); i++) {
            resultat[i] = (int) aTexte.charAt(i);
        }
        return resultat;
    }

    /**
     * Cette fonction prend une chaine de caractère en argument et retourne cette
     * même chaine en mettant les caractères à la position impaire en majuscule, et les autres
     * en minuscule
     * Exemple: Bonjour
     * devient: bOnJoUr
     * @param aTexte le texte à convertir
     * @return le texte mis en super camelCase
     */
    private static String transformerEnChameau(String aTexte) {
        // convertir aTexte en minuscule
        // boucler sur les caractères de aTexte en minuscule
        //    déclarer un caractère appelé c qui sera égale au caractère de aTexte en minuscule à la position de la boucle
        //    si la position est impaire (vous vous souvenez de modulo ?) (le premier caractère est à la position 0)
        //        mettre ce caractère en majuscule
        //    ajouter le caractère à la fin de résultat
        // retourner resultat une fois tous les caractères traités
        String resultat = "";
        String enMinuscule = aTexte.toLowerCase();
        for (int i = 0; i < enMinuscule.length(); i++) {
            char c = enMinuscule.charAt(i);
            if(i%2 != 0) {
                c = Character.toUpperCase(c);
            }
            resultat += c;
        }
        return resultat;
    }

    /**
     * Cette fonction compte les lettres dans un mot. Les chiffres ne sont pas comptés
     * @param aTexte le mot à analyser
     * @return le nombre de lettre dans le mot.
     */
    private static int compterLettre(String aTexte) {
        // boucler sur les caractères de aTexte
        //    si un caractère est une lettre, incrémenter le compteur
        // une fois tous les caractères analysés, retourner le compteur.
        int lCompteur = 0;
        for (int i = 0; i < aTexte.length(); i++) {
            if (Character.isLetter(aTexte.charAt(i))) {
                lCompteur++;
            }
        }
        return lCompteur;
    }

}


```

:::

## Exercice 2: Probabilités

Cet exercice vous fait pratiquer:
1) fonction avec paramètres et retour de tableau
2) boucle for, while, do while
3) boolean
4) Arrays et tableau
5) Math.random()
  

Pour cette exercice vous devez faire un générateur de loto. 

* Demandez la taille de la loto (le nombre de chiffres par combinaison (par exemple, le 6/49 a 6 chiffres (pas de complémentaire)))
* Demandez la valeur maximale pour un nombre généré (par exemple le 6/49 a 49 comme valeur max)
* Demandez à l'usager d'entrer sa combinaison
  * faire une boucle pour demander *taille* nombre
  * vérifiez que chacun de ces nombres doit être entre 1 et la valeur maximale (inclusivement)
  * vérifiez que le même nombre n'est pas sélectionné deux fois
    * pour ce faire, à chacune des entrées, vous devez la comparer avec les entrées précédentes
    * si une sélection a déjà été entrée, vous devez redemander cette sélection
* Triez la combinaison entrée 
  * indice: dans Arrays, nous avons vu une fonction permettant de trier un tableau
* (1) Générez une combinaison gagnante
  * générez *taille* nombres aléatoires entre 1 et *valeur maximale*
  * vérifiez que le même nombre n'est pas généré 2 fois. 
    * tant que le nombre a déjà été généré, vous devez regénérer un autre nombre pour cette position 
* Triez la combinaison gagnante
* Vérifiez si vous gagnez
* Si non, retournez à la génération de la combinaison gagnante (étape (1))
* Si oui, affichez combien d'essais ont été nécessaires pour gagner. 
  * vous devez donc avoir un compteur du nombre de combinaisons générées.

Note: je vous recommande d'afficher la combinaison gagnante au début afin de vérifier que les nombres font du sens (nous avons vue une fonction dans Arrays permettant d'afficher un tableau comme une String)

Allez vous vous achetez un billet de 6/49 après ca ?!?!? :smile:

:::details Énoncé avec le pseudocode

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("taille: ");
        int lTaille = Console.demanderEntier();
        int lValeurMax;
        do {
            Console.afficher("Valeur maximale :");
            lValeurMax = Console.demanderEntier();
        } while (lTaille>lValeurMax);
        int[] lChoix = acheterBillet(lTaille, lValeurMax);
        Console.afficher("votre billet: "+Arrays.toString(lChoix));
        int lCompteur = 0;
        int[] loto;
        do {
            lCompteur++;
            loto = genererCombinaisonGagnante(lTaille, 1, lValeurMax);
        } while (!Arrays.equals(lChoix, loto));
        Console.afficher(lCompteur + " " + Arrays.toString(loto));
    }

    /**
     * Génère une combinaison de loto
     * @param lTaille   le nombre de nombres aléatoires à générer
     * @param lValeurMin la valeur minimale pour les nombres aléatoires
     * @param lValeurMax la valeur maximale pour les nombres aléatoires
     * @return un tableau d'entiers de taille lTaille composé de nombre aléatoire entre lValeurMin et lValeurMax (inclusivement)
     */
    private static int[] genererCombinaisonGagnante(int lTaille, int lValeurMin, int lValeurMax) {
        int[] resultatLoto = new int[lTaille];
        // boucle pour générer lTaille nombres aléatoire
        //   déclarer un booléen appelé "pasBon" et le mettre à vrai
        //   tant que pasBon est vrai
        //     générer un nombre aléatoire entre lValeurMin et lValeurMax (inclusivement)
        //     mettre pasBon à faux
        //     boucle pour tous les nombres générés jusqu'à maintenant
        //        si le nombre qui vient d'être généré est égale à un nombre généré précédemment
        //            pasBon devient vrai
        // une fois tous les nombres générés, trier resultatLoto
        // retourner resultatLoto

        // votre code 

        return resultatLoto;
    }

    /**
     * Permet à l'usager de choisir une combinaison
     *
     * @param lTaille le nombre de numéro à choisir
     * @param lValeurMax la valeur maximale pour un numéro (le minimum est 1
     * @return un tableau d'entiers triés contenant les numéros choisis
     */
    private static int[] acheterBillet(int lTaille, int lValeurMax) {
        int[] lCombinaison = new int[lTaille];
        // boucle pour demander lTaille numéro
        //   déclarer un booléen appelé "pasBon" et le mettre à vrai
        //   tant que pasBon est vrai
        //     demander un nombre entre 1 et lValeurMax (inclusivement)
        //     mettre pasBon à faux
        //     boucle pour tous les numéros entrés jusqu'à maintenant
        //        si le numéro qui vient d'être choisit est égale à un numéro choisit précédemment
        //            pasBon devient vrai
        // une fois tous les numéros entrés, trier lCombinaison
        // retourner lCombinaison
        
        //votre code

        return lCombinaison;
    }

}


```

:::

:::details

```java
    package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("taille: ");
        int lTaille = Console.demanderEntier();
        int lValeurMax;
        do {
            Console.afficher("Valeur maximale :");
            lValeurMax = Console.demanderEntier();
        } while (lTaille>lValeurMax);
        int[] lChoix = acheterBillet(lTaille, lValeurMax);
        Console.afficher("votre billet: "+Arrays.toString(lChoix));
        int lCompteur = 0;
        int[] loto;
        do {
            lCompteur++;
            loto = genererCombinaisonGagnante(lTaille, 1, lValeurMax);
        } while (!Arrays.equals(lChoix, loto));
        Console.afficher(lCompteur + " " + Arrays.toString(loto));
    }

    /**
     * Génère une combinaison de loto
     * @param lTaille   le nombre de nombres aléatoires à générer
     * @param lValeurMin la valeur minimale pour les nombres aléatoires
     * @param lValeurMax la valeur maximale pour les nombres aléatoires
     * @return un tableau d'entiers de taille lTaille composé de nombre aléatoire entre lValeurMin et lValeurMax (inclusivement)
     */
    private static int[] genererCombinaisonGagnante(int lTaille, int lValeurMin, int lValeurMax) {
        int[] resultatLoto = new int[lTaille];
        // boucle pour générer lTaille nombres aléatoire
        //   déclarer un booléen appelé "pasBon" et le mettre à vrai
        //   tant que pasBon est vrai
        //     générer un nombre aléatoire entre lValeurMin et lValeurMax (inclusivement)
        //     mettre pasBon à faux
        //     boucle pour tous les nombres générés jusqu'à maintenant
        //        si le nombre qui vient d'être généré est égale à un nombre généré précédemment
        //            pasBon devient vrai
        // une fois tous les nombres générés, trier resultatLoto
        // retourner resultatLoto

        for (int i = 0; i < lTaille; i++) {
            boolean pasBon;
            //génère un nombre et vérifie qu'il n'a pas déjà été généré
            do {
                resultatLoto[i] = (int) (Math.random() * (lValeurMax -  lValeurMin + 1 )) + lValeurMin;
                pasBon = false;
                for (int j = 0; j < i; j++) {
                    if (resultatLoto[i] == resultatLoto[j]) {
                        pasBon = true;
                    }
                }
            } while (pasBon);
        }
        Arrays.sort(resultatLoto);
        return resultatLoto;
    }

    /**
     * Permet à l'usager de choisir une combinaison
     *
     * @param lTaille le nombre de numéro à choisir
     * @param lValeurMax la valeur maximale pour un numéro (le minimum est 1
     * @return un tableau d'entiers triés contenant les numéros choisis
     */
    private static int[] acheterBillet(int lTaille, int lValeurMax) {
        int[] lCombinaison = new int[lTaille];
        // boucle pour demander lTaille numéro
        //   déclarer un booléen appelé "pasBon" et le mettre à vrai
        //   tant que pasBon est vrai
        //     demander un nombre entre 1 et lValeurMax (inclusivement)
        //     mettre pasBon à faux
        //     boucle pour tous les numéros entrés jusqu'à maintenant
        //        si le numéro qui vient d'être choisit est égale à un numéro choisit précédemment
        //            pasBon devient vrai
        // une fois tous les numéros entrés, trier lCombinaison
        // retourner lCombinaison
        for (int i = 0; i < lCombinaison.length; i++) {
            do {
                boolean pasBon;
                do {
                    pasBon = false;
                    Console.afficher("entrez le nombre pour la sélection " + (i + 1));
                    lCombinaison[i] = Console.demanderEntier();
                    for (int j = 0; j < i; j++) {
                        if (lCombinaison[i] == lCombinaison[j]) {
                            pasBon = true;
                        }
                    }
                } while (pasBon);
            } while (lCombinaison[i] < 1 || lCombinaison[i] > lValeurMax);
        }
        Arrays.sort(lCombinaison);
        return lCombinaison;
    }

}


```

:::