# La classe Arrays

Pour nous faciliter les manipulations des tableaux, la classe `Arrays` nous offre une panoplie de fonctions.

:::warning
Notez ici que Arrays s'écrit avec un A majuscule et un s à la fin.  (C'est donc une classe)
:::

Pour l'utiliser, vous devez d'abord importer la classe.

```java
import java.util.Arrays;
```

Ensuite, vous pouvez l'utiliser comme suit :

```java
double[] resultatsExamen = {75.5, 99.0, 80.0};

String arrayEnString = Arrays.toString(resultatsExamen); // "[75.5, 99.0, 80.0]"

Arrays.sort(resultatsExamen);
System.out.println(Arrays.toString(resultatsExamen)); // [75.5, 80.0, 99.0]

double[] resultatsExamenGroupe2 = {100.0, 100.0};
double[] resultatsExamenGroupe3 = {100.0, 100.0};
System.out.println(Arrays.equals(resultatsExamen, resultatsExamenGroupe2)); // false
System.out.println(Arrays.equals(resultatsExamenGroupe2, resultatsExamenGroupe3)); // true

Arrays.fill(resultatsExamen, 0, 2, 100.0);
Arrays.fill(resultatsExamen, 2, 3, 50.0); // 2e index = fin exclusivement
System.out.println(Arrays.toString(resultatsExamen)); // [100.0, 100.0, 50.0]
```

Il existe bien d'autres fonctionnalités, voir la documentation.

https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html


## Exercice

Le but de cet exercice est de générer 2 tableaux de nombres aléatoires et de vérifier si ces 2 tableaux sont égaux. En d'autres mots, on simule un tirage de loto. Le premier tableau étant les chiffres que vous choisissez, le deuxième étant le tirage. Cela vous donnera une idée des probabilités de gagner selon le nombre de valeurs sur le billet. 

1) Créez un programme qui demande un nombre entier (appelons le `taille`)
2) Créez deux tableaux d'entiers de longueur `taille`
3) Remplissez ces 2 tableaux avec des nombres aléatoires entre 1 et 3 inclusivement.  (n'oubliez pas d'utiliser une constante :smile:)
4) Triez les 2 tableaux (utilisez les fonctionnalités de Arrays)
5) Affichez les 2 tableaux sur une même ligne (utilisez les fonctionnalités de Arrays)
6) Comparez les 2 tableaux:
    1) s'ils sont égaux vous pouvez arrêter le programme
    1) s'ils sont différents, retournez à l'étape 3 afin de générer un autre tirage.  

Une fois que vous aurez réussi ce code, modifiez le pour
* qu'une fois que le programme a trouvé 2 tableaux identiques, au lieu d'arrêter, il boucle au début et demande une nouvelle taille;
* si la taille 0 est demandée, le programme se termine immédiatement; 
* ajoutez un compteur du nombre d'essais et affichez le au début de la ligne affichant le contenu des tableaux. 


:::details

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        int taille;
        final int VALEUR_MAX = 4;
        do {
            Console.afficher("taille ?");
            taille = Console.demanderEntier();
            if(taille != 0) {
                int[] tableau1 = new int[taille];
                int[] tableau2 = new int[taille];
                boolean trouve = false;
                int compteur = 0;
                do {
                    for (int i = 0; i < taille; i++) {
                        tableau1[i] = (int) Math.floor(Math.random() * VALEUR_MAX) + 1;
                        tableau2[i] = (int) Math.floor(Math.random() * VALEUR_MAX) + 1;
                    }
                    Arrays.sort(tableau1);
                    Arrays.sort(tableau2);
                    if (Arrays.equals(tableau1, tableau2)) {
                        trouve = true;
                    }
                    compteur++;
                    Console.afficher("essai :" + compteur + " " + Arrays.toString(tableau1) + " - " + Arrays.toString(tableau2));
                } while (!trouve);
            }
        } while (taille != 0);
    }
}

```

:::

:::tip
Si vous utilisez 49 comme étant la valeur maximum, et 6 comme étant le nombre de valeurs, vous venez de simuler un tirage de 6/49. 

Si vous trouvez des valeurs égales en moins de 10 essais, allez vous acheter un billet ;)
:::