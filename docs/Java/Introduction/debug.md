# Debugger

Plus on programme, plus on génère de bug!

Il faut donc connaitre les outils utiles pour régler ces bugs. 

Créez un nouveau projet et copiez ce code (Remplacez le code de Programme au complet )

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        int a = 10;
        String b = "allo";
        Console.afficher(b);
        fonction1(9);
        fonction1(a);
        for(int i = 0; i < 10; i++) {
            Console.afficher(i);
        }
    }

    public static void fonction1(int a) {
        Console.afficher(a);
    }
}

```

À la ligne 5, ajoutez un point d'arrêt (breakpoint) en cliquant juste à côté du 5 dans la marge à gauche. Vous devriez obtenir un cercle rouge. 

Maintenant démarrez le programme en mode débug en appuyant sur le bug vert en haut de l'écran. 

![img](/debug1.png)

Vous devriez obtenir cet écran

![img](/debug3.png)

1) permet de passer du debugger à la console
2) exécute la ligne actuelle sans entrer dans la fonction (si c'est une fonction)
3) exécute la ligne actuelle en entrant dans la fonction (si c'est une fonction)
4) sort de la fonction (se rend jusqu'au prochain return ou la fin de la fonction)
5) exécute jusqu'au curseur
6) continue l'exécution (jusqu'au prochain point d'arrêt)
7) arrête l'exécution
8) affiche les points d'arrêts
9) mets les points d'arrêts inactifs
10) les variables (au niveau actuel de la pile d'exécution)
11) permet d'évaluer une expression
12) la pile d'exécution

Notez que la valeur actuelle des variables est affichée directement dans le code aussi

![img](/debug4.png)


