# Opérateurs mathématiques de base

* Addition (`+`)
  * `int test = 1 + 1; // test = 2`
* Soustraction (`-`)
  * `int test = 1 - 1; // test = 0`
* Multiplication (`*`)
  * `int test = 2 * 3; // test = 6`
* Division entière (`/`)
  * `int test = 2 / 3; // test = 0`
  * Attention à ne pas diviser par zéro !!!! :-O
  * Pour une division normale, il doit avoir un réel dans l'équation
    * double test  = 10 / 3.0; // test = 3.333333, car 3.0 est un réel
    * Ce qui donne un réel en retour
* Modulo (`%`)
  * `int test = 2 % 3; // test = 2`
  * Permet d'avoir le reste de la division entière
  * `int test = 20 % 3; // test = 2, car 20/3=6 et 6*3=18 et 20-18=2.`

La préséance fonctionne comme en mathématique.

Voir https://introcs.cs.princeton.edu/java/11precedence

Utilisez les parenthèses si vous devez prioriser une partie de l'équation ou si vous n'êtes pas certain de la préséance des opérateurs utilisés.

## Exercices

1) Écrivez un programme qui demande 2 entiers
1) Additionnez ces 2 entiers et affichez le résultat à l'aide de Console
1) Divisez le premier entier par le second et affichez le résultat. Quel type de variable devez-vous utiliser? Est-ce que le résultat est celui que vous vous attendiez (essayez avec 3 et 5)?
1) Divisez le premier par 2 fois le second et affichez le résultat.  
1) Affichez le modulo de second par le premier (n'utilisez pas de variable pour mettre le résultat du modulo, faites le calcul directement lors de l'affichage). 

Essayez avec les valeurs 3 et 5. 


:::details

```java:line-numbers
    int a;
        int b;
        Console.afficher("Entrez un entier :");
        a=Console.demanderEntier();
        Console.afficher("Entrez un 2e entier :");
        b=Console.demanderEntier();
        Console.afficher("2) a+b = " +(a+b));
        Console.afficher("3) a / b = " + (a/b));
        
        double d = a/(b*1.0); // 4)
        Console.afficher("5) premier divisé par 2 fois le second =" + a/(2.0*b));
        Console.afficher("6) modulo: " + b%a);

```
:::



Que remarquez pour le point 3 ?
:::details

Si, comme dans la solution proposée, vous avez simplement utilisé **a/b**, le résultat affiché n'aura pas de décimales. Par exemple, si vous entrez 3 et 5, le résultat sera 0 . Si vous entrez 10 et 3, le résultat sera 3.

Pourquoi?

Le résultat de la division dépend du type des opérandes. Ici, nous avons 2 entiers. Le résultat est donc arrondi à l'entier le plus près. 

Il y a plusieurs facons de forcer la division à retourner un nombre à virgule. La plus simple, remplacez **a/b** par **a/(b*1.0)**

Notez que dans la solution proposée pour le point 5, au lieu de multiplier par 2, la multiplication utilise 2.0. 
:::

Pour la réponse 2, pourquoi doit-on mettre le a+b entre parenthèse? 

:::details
Si on ne mets pas de (), alors l'évaluation se fera de gauche à droite. La valeur de **a** sera donc concaténée au texte "2) a+b = ". Si on essaie avec 5 et 3, ca donnera "2) a+b = 5", qui sera ensuite concaténé à 3, ce qui donnera "2) a+b = 53"
:::

Pour la réponse 6, pourquoi il n'est pas nécessaire de mettre les () autour de b%a ?

:::details
L'opérateur % à précédence sur +. Le modulo se fait donc avant la concaténation. 
:::

