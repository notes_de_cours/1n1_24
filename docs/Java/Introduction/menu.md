# La classe `Menu`

Tout comme la classe **Console**, la classe **Menu** fait partie de la librairie de base pour le cours.

Elle permet de simplifier la conception et la gestion d'un menu. 

Exemple:

```java
public static void main(String[] args) {
    Menu menu = new Menu("Menu principal");
    menu.ajouterOption("Option 1", Programme::option1);
    menu.ajouterOption("Option 2", Programme::option2);
    menu.executer();
}

public static void option1(){
    Console.afficher("option1");
}

public static void option2(){
    Console.afficher("option2");
}

```

:::warning ATTENTION : Vous devez utiliser la bonne classe Menu

Lorsque vous inscrivez le mot Menu, faites attention à ce que suggère Intellij.

Vous devez choisir le menu de ca.cegepdrummond

![img](/BonMenu.png)

Si par erreur vous avez choisi celle de java.io, vous obtiendrez un include supplémentaire 

![img](/MauvaisMenu.png)

Si c'est le cas, vous n'avez qu'à reculer votre curseur sur le mot Menu, effacer le dernier `u` , et le réécrire, mais cette fois choisir ca.cegepdrummond. 
:::


## Fonctionnement


La première ligne permet de créer le menu. 

La deuxième permet d'ajouter une option au menu. Les paramètres sont le texte de l'option du menu qui sera affiché, suivit de la fonction à appeler lorsque cette option sera choisie (nous verrons les fonctions sous peu). 

La syntaxe peut sembler un peu bizarre. Vous devez mettre le nom de la classe dans laquelle les options sont (habituellement, ce sera la classe Programme), suivit de `::` suivit du nom de la fonction. 

La première option aura l'indice 1. 

Par défaut, l'option 0 pour quitter sera ajoutée automatiquement. Si vous désirez remplacer le mot `Quitter` par un autre, vous pouvez remplacer la création du menu par cette ligne:

```
Menu menu = new Menu("Menu principal", "Votre texte pour quitter");
```

La dernière ligne sert à démarrer le menu.

## Sous menu

Vous pouvez avoir des sous-menus dans les fonctions. Par exemple, changez option1() par:

```java
public static void option1(){
    Console.afficher("option1");
    Menu menu = new Menu("Menu secondaire");
    menu.ajouterOption("Option 1.1", Programme::option11);
    menu.ajouterOption("Option 1.2", Programme::option12);
    menu.executer();
}

 public static void option11(){
        Console.afficher("option1.1");
    }
    public static void option12(){
        Console.afficher("option1.2");
    }

```

Vous pouvez voir plus d'exemples de son utilisation dans le code du TP1. 


## Exercice

* Écrivez un programme qui affiche le menu d'un restaurant
  * Repas du midi
  * Repas du soir
* Pour le repas du midi, vous aurez les options suivantes
  * Soupe
  * Poulet
* Pour celui du soir, vous aurez les options suivantes
  * Lasagne
  * Spaghetti
* Pour chacun des choix de menu, vous devez afficher que ce choix a été choisi. Par exemple, si je prends Repas du midi/Soupe, ca affichera `Vous avez pris la soupe du midi`

:::details Solution
```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Menu menuResto = new Menu("Resto chez Benou");
        menuResto.ajouterOption("Repas du midi", Programme::afficherMenuMidi);
        menuResto.ajouterOption("Repas du soir", Programme::afficherMenuSoir);
        menuResto.executer();
    }

    public static void afficherMenuMidi() {
        Menu menuMidi = new Menu("Choix du Midi");
        menuMidi.ajouterOption("Soupe", Programme::acheterSoupe);
        menuMidi.ajouterOption("Poulet", Programme::acheterPoulet);
        menuMidi.executer();
    }

    public static void afficherMenuSoir() {
        Menu menuSoir = new Menu("Choix du Soir");
        menuSoir.ajouterOption("Lasagne", Programme::acheterLasagne);
        menuSoir.ajouterOption("Spaghetti", Programme::acheterSpaghetti);
        menuSoir.executer();
    }

    public static void acheterSoupe() {
        Console.afficher("Vous avez pris la soupe");
    }
    public static void acheterPoulet() {
        Console.afficher("Vous avez pris le poulet");
    }
    public static void acheterLasagne() {
        Console.afficher("Vous avez pris la Lasagne");
    }
    public static void acheterSpaghetti() {
        Console.afficher("Vous avez pris le spaghetti");
    }
}
```
:::