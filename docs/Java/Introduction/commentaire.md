# Les commentaires

Texte qui sera ignoré lors de la compilation du programme.

Voici un exemple de commentaire:

```java
// Ceci est un exemple de commentaire sur une ligne

/*
   Ceci est un exemple de commentaire
   multi ligne
*/
```


Il existe 3 types de commentaires

* Commentaires de fin de ligne (les 2 premières lignes)
* Commentaires multilignes (les 4 dernières)
* Documentation Java  (que nous verrons ci-bas)
* 
Permet de :

* Expliquer du code plus complexe
* Documenter le code pour faciliter la maintenance
* Désactiver une ligne de code temporairement
* Écrire un commentaire avec le mot TODO pour vous souvenir d'une chose à faire
* Etc.


## Commentaire de fin de ligne

Permet de mettre en commentaire le texte qui suit les `backslash` `//`

Exemple

```java
Console.afficher("Ceci n'est pas en commentaire"); // ceci est un commentaire
// Cette ligne ne contient qu'un commentaire
// Console.afficher("Je ne suis pas exécuté");
```

La dernière ligne semble être une ligne de code (Nous verrons Console.afficher() bientôt), mais étant donné qu'elle commence par //, ce code ne sera pas exécuté. 

## Commentaire multilignes

Commentaire pouvant être sur plusieurs lignes entre le symbole `/*` et le symbole `*/` le plus près.

Exemple

```java
/*
Je suis un commentaire sur plusieurs lignes */
/* Peut quand même être sur la même ligne */
```

## Documentation Java

Commentaire ayant une syntaxe particulière permettant de définir le contenu d'une fonction, d'une classe, etc. Cela permet de générer éventuellement la documentation de votre code.

Dans les IDE modernes tel qu'Intellij, simplement écrire `/**` sur la ligne précédent une fonction, et le bloc sera généré automatiquement. 

Exemple

<<< @/public/code/src/ca/cegepdrummond/Programme.java#commentaire_fonction {java}


Seule la **documentation Java de fonction et de classe** est obligatoire afin d'expliquer ce que fait une fonction ou une classe, et ce que font les paramètres.  

Un commentaire à l'intérieur du code, par exemple lors de la définition des variables, doit ajouter de l'information au programme. Ça ne sert à rien de documenter la variable **age** en indiquant que c'est l'âge. Si c'est l'âge d'adoption, ne pas mettre un commentaire indiquant que c'est l'âge d'adoption, c'est mieux de changer le nom de la variable pour **ageAdoption**. 

Les commentaires doivent être utilisés quand il y a quelque chose de "bizarre" dans le code, ou quand la logique est difficile à comprendre. Mais c'est encore mieux de changer le code pour simplifier la logique (ex.: envoyer la partie complexe dans une fonction ayant un nom significatif).

Nous reviendrons sur les commentaires plus tard. 