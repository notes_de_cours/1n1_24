# Gestion des exceptions

## Introduction

Il existe plusieurs types d'erreurs: 

* Erreurs de compilation
  * Votre programme ne pourra pas compiler sans corriger ces erreurs
  * C'est le rouge dans IntelliJ
* Erreurs de logique ou de comportement
  * Le programme ne fait pas ce qu'on attendait de lui
  * On peut voir ces erreurs en testant notre programme et en vérifiant s'il fait bien ce qu'il a à faire
  * Nous allons revoir cette partie dans les tests fonctionnels
* Erreurs d'exécution (exceptions)
  * Erreurs qui fait fermer le programme (division par zéro, sortir d'un tableau, variable nul...)
  * Vous avez habituellement un message d'erreur dans la console à ce moment-là
  * On peut attraper ces exceptions et déterminer ce que l'on fait lorsque cette exception arrive
  * Si on ne le fait pas, nous allons voir l'erreur dans la console

Cette section sera dédiée à la gestion des exceptions et à l'interprétation du message d'erreur.

## Liste des exceptions dans Java

https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html


## Exemples d'une exception


### Dépassement de tableau

```java
public class Programme {
    public static void main(String[] args) {
	    double[] notes = {};
        System.out.println("La première valeur est " + notes[0]);
    }
}
```

Exception :

```
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Array index out of range: 0
	at ca.cegepdrummond.Programme.main(Programme.java:4)
```

### Division par zéro

```java
public class Programme {
    public static void main(String[] args) {
        int pointesDeTarte = 16;
        int nbAmis = 0;
        int nbPointesParAmi = pointesDeTarte/nbAmis;
        System.out.println("Chaque ami peut avoir " + nbPointesParAmi + " pointes de tarte");
    }
}
```

Exception :

```
Exception in thread "main" java.lang.ArithmeticException: division par zéro
	at ca.cegepdrummond.Programme.main(Programme.java:5)
```

### Accéder à un attribut ou une méthode d'un objet null

```java
public class Programme {
    public static void main(String[] args) {
        String texteARemplir = null;
        System.out.println("Vous avez écrit " + texteARemplir.length() + " caractères");
    }
}
```

Exception :

```
Exception in thread "main" java.lang.NullPointerException: Cannot invoke "java.lang.String.length()" because "texteARemplir" is null
	at ca.cegepdrummond.Programme.main(Programme.java:4)
```

### Scanner nextInt avec du texte

```java
public class Programme {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Inscrivez un nombre :");
        int nombre = scanner.nextInt();

        System.out.println("Vous avez inscrit le nombre : " + nombre);
    }
}
```

Console + Exception :

```
Inscrivez un nombre :
allo
Exception in thread "main" java.util.InputMismatchException
	at java.base/java.util.Scanner.throwFor(Scanner.java:939)
	at java.base/java.util.Scanner.next(Scanner.java:1594)
	at java.base/java.util.Scanner.nextInt(Scanner.java:2258)
	at java.base/java.util.Scanner.nextInt(Scanner.java:2212)
	at ca.cegepdrummond.Programme.main(Programme.java:6)
```

## Comment lire le message d'erreur

Prenons l'exemple suivant :

```java
public class Programme {
    public static void main(String[] args) {
        fonctionEnErreur();
    }

    private static void fonctionEnErreur() {
        String texteARemplir = null;
        System.out.println("Vous avez écrit " + texteARemplir.length() + " caractères");
    }
}
```

Nous allons obtenir le message d'erreur suivant :

```
Exception in thread "main" java.lang.NullPointerException: Cannot invoke "java.lang.String.length()" because "texteARemplir" is null
	at ca.cegepdrummond.Programme.fonctionEnErreur(Programme.java:8)
	at ca.cegepdrummond.Programme.main(Programme.java:3)
```

Analysons chacune des parties de ce message d'erreur :

| Partie du message d'erreur                                   | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `Exception in thread "main"`                                 | Spécifie dans quel processus parallèle l'erreur c'est produite. Vous allez voir les threads plus tard dans un autre cours. Pour l'instant ce sera toujours "main" |
| `java.lang.NullPointerException`                             | Type de l'exception qui s'est produite. Vous pouvez aller voir la documentation de cette exception par la suite pour mieux comprendre votre erreur.  (ex.: https://docs.oracle.com/javase/7/docs/api/java/lang/NullPointerException.html) |
| `Cannot invoke "java.lang.String.length()" because "texteARemplir" is null` | Message d'erreur. Lorsqu'une exception est lancée, celui qui lance l'exception peut accompagner l'exception d'un message pour préciser la raison de l'erreur. Vous pouvez chercher ce message d'erreur sur internet pour mieux comprendre votre erreur. Par contre, vous devrez enlever ce qui est propre à votre code. (ex.: enlever "texteARemplir") |
| `at ca.cegepdrummond.Programme.fonctionEnErreur(Programme.java:10)`<br/>`at ca.cegepdrummond.Programme.main(Programme.java:5)` | Pile d'exécution. Cela vous spécifie à quel endroit a eu lieu l'exception. De plus précis au moins précis. La première ligne est l'endroit exacte où s'est produit l'erreur. Ensuite, vous avez quelle ligne a appelé la fonction en erreur et ainsi de suite, jusqu'au "main".<br /><br />Dans l'ordre, vous avez d'abord le "package", le nom de la classe, le nom de la méthode en erreur, le fichier où se trouve l'erreur et le numéro de la ligne en erreur. |

## Try, catch

Une exception peut être prévisible et donc il est normal de vouloir l'attraper, l'interpréter et la gérer.

Pour cela, nous devons utiliser l'instruction `try` pour essayer le code et l'instruction `catch` pour attraper l'erreur si elle se produit.

Exemple :

```java
package ca.cegepdrummond;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Programme {
    public static void main(String[] args) {
        int nombre = lireNombreMinMax(0, 10);

        System.out.println("Vous avez inscrit le nombre : " + nombre);
    }

    private static int lireNombreMinMax(int min, int max) {
        Scanner scanner = new Scanner(System.in);
        int nombre = min-1;
        do {
            try {
                System.out.println("Inscrivez un nombre de " + min + " à " + max + " :");
                nombre = scanner.nextInt();
            } catch (InputMismatchException erreur) {
                System.err.println("Vous devez écrire un nombre!");
                scanner.next();
            }
        } while(nombre < min || nombre > max);
        return nombre;
    }
}
```

Ici, on essaie de lire un `int`.

Si ce n'est pas un `int`, on aura une erreur de type `InputMismatchException ` qui sera attrapée par le `catch`.

Si l'erreur se produit, on affiche un message d'erreur et on fait avancer le scanner d'un mot (le scanner n'avance pas si ce n'est pas un `int` et vous aurez l'erreur à l'infini).

S'il n'y a pas d'erreur, on continue le code normalement sans aller dans le `catch`. 



On doit absolument mettre le code qui peut être en erreur dans le `try`. Ainsi, ensuite, on peut attraper l'erreur le `catch` si l'erreur se produit. 

## Différents types d'exception

On peut gérer plusieurs types d'exception pour un même code.

Exemple :

```java
public class Programme {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean valide = false;

        while (!valide) {
            try {
                System.out.println("Entrez le dividende : ");
                int dividende = scanner.nextInt();

                System.out.println("Entrez le diviseur : ");
                int diviseur = scanner.nextInt();

                int resultat = dividende / diviseur;
                System.out.println("Voici le quotient : " + dividende / diviseur);
                System.out.println("Voici le reste : " + dividende % diviseur);
                valide = true;
            } catch (InputMismatchException erreur) {
                System.err.println("Vous devez entrez des nombres entiers de " + Integer.MIN_VALUE + " à " + Integer.MAX_VALUE + "!");
                scanner.next();
            } catch (ArithmeticException erreur) {
                System.err.println("Vous ne pouvez pas faire une division par zéro!");
            } 
        }
    }
}
```

Ici, on peut attraper 2 types d'erreurs dans le code que l'on essaie.

Notre but ici, c'est que si l'une des 2 erreurs se produit, on recommence le tout.

Dépendamment du type d'erreur qui sera lancée, l'un des `catch` va s'exécuter.

## Attraper toutes les exceptions

Pour attraper toutes les exceptions, vous pouvez attraper les exceptions de type `Exception`. Cette classe est la mère de toutes les exceptions.

Exemple : 

```java
public class Programme {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean valide = false;

        while (!valide) {
            try {
                System.out.println("Entrez le dividende : ");
                int dividende = scanner.nextInt();

                System.out.println("Entrez le diviseur : ");
                int diviseur = scanner.nextInt();

                int resultat = dividende / diviseur;
                System.out.println("Voici le quotient : " + dividende / diviseur);
                System.out.println("Voici le reste : " + dividende % diviseur);
                valide = true;
            } catch (InputMismatchException erreur) {
                System.err.println("Vous devez entrer des nombres entiers de " + Integer.MIN_VALUE + " à " + Integer.MAX_VALUE + "!");
                scanner.next();
            } catch (ArithmeticException erreur) {
                System.err.println("Vous ne pouvez pas faire une division par zéro!");
            } catch (Exception erreur) {
                System.err.println("Une erreur inconnue s'est produite!");
            }
        }
    }
}
```

## Informations dans l'exception

Dans une exception, il y a 3 informations essentielles :

* Le type de l'exception
* Le message
* La pile d'exécution

Exemple pour les afficher :

```java
public class Programme {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean valide = false;

        while (!valide) {
            try {
                System.out.println("Entrez le dividende : ");
                int dividende = scanner.nextInt();

                System.out.println("Entrez le diviseur : ");
                int diviseur = scanner.nextInt();

                int resultat = dividende / diviseur;
                System.out.println("Voici le quotient : " + dividende / diviseur);
                System.out.println("Voici le reste : " + dividende % diviseur);
                valide = true;
            } catch (InputMismatchException erreur) {
                System.err.println("Vous devez entrer des nombres entiers de " + Integer.MIN_VALUE + " à " + Integer.MAX_VALUE + "!");
                scanner.next();
            } catch (ArithmeticException erreur) {
                System.err.println("Vous ne pouvez pas faire une division par zéro!");
            } catch (Exception erreur) {
                System.err.println("Une erreur inconnue s'est produite!");
                System.err.println("Voici les informations de l'erreur : ");
                System.err.println("type : " + erreur.getClass().getName());
                System.err.println("message : " + erreur.getMessage());
                System.err.println("pile d'exécution :");
                for (StackTraceElement element : erreur.getStackTrace()) {
                    System.err.println(element);
                }
            }
        }
    }
}
```

> Pour afficher les 3 informations de l'erreur, vous pouvez aussi utiliser une fonctionnalité de `Exception` qui permet de faire cela.
>
> ```java
> catch (Exception erreur) {
>  System.err.println("Une erreur inconnue s'est produite!");
>  System.err.println("Voici les informations de l'erreur : ");
>  erreur.printStackTrace();
> }
> ```

## Lancer ou relancer une exception

On peut gérer l'exception et la relancer pour quelle soit attrapée plus haut grâce à l'instruction `throw`

Exemple : 

```java
public class Programme {
    public static void main(String[] args) {
        try {
            division(3,0);
        } catch (Exception erreur) {
            System.err.println("Une erreur s'est produite dans le main");
            erreur.printStackTrace();
        }
    }

    private static int division(int dividende, int diviseur) {
        try {
            return dividende / diviseur;
        } catch (Exception erreur) {
            System.err.println("Une erreur s'est produite dans la fonction division");
            throw erreur;
        }
    }
}
```

On peut aussi décider de lancer nos propres exception.

Exemple :

```java
public class Programme {
    public static void main(String[] args) {
        try {
            division(3,0);
        } catch (Exception erreur) {
            System.err.println("Une erreur s'est produite dans le main");
            erreur.printStackTrace();
        }
    }

    private static int division(int dividende, int diviseur) {
        if (diviseur == 0) {
            throw new IllegalArgumentException("Vous ne pouvez pas diviser par zéro");
        }

        return dividende / diviseur;
    }
}
```

## Throws

On peut spécifier dans la signature de la fonction que cette fonction peut lancée certains types d'exceptions avec `throws`.

Exemple : 

```java
public class Programme {
    public static void main(String[] args) {
        try {
            division(3,0);
        } catch (Exception erreur) {
            System.err.println("Une erreur s'est produite dans le main");
            erreur.printStackTrace();
        }
    }

    private static int division(int dividende, int diviseur) throws IllegalArgumentException {
        if (diviseur == 0) {
            throw new IllegalArgumentException("Vous ne pouvez pas diviser par zéro");
        }

        return dividende / diviseur;
    }
}
```

> Si une fonction peut lancée une exception de type `RuntimeException` (et ses enfants) ou une `Error` (et ses enfants), vous devez absolument gérer cette erreur lorsque vous utilisez cette fonction. Vous pouvez la gérer en utilisant un `try` `catch` ou en définissant `throws` dans la signature de votre fonction.

## Finally

`finally` permet d'exécuter du code même si vous lancé une exception.

Exemple : 

```java
public class Programme {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            int[] tableau = {1, 2, 3};

            System.out.println("Donnez l'index du tableau pour voir sa valeur : ");
            int index = scanner.nextInt();
            System.out.println("La valeur est : " + tableau[index]);
        } catch (Exception erreur) {
            System.err.println("Une erreur s'est produite...");
            throw erreur;
        } finally {
            System.out.println("Action exécutée malgré tout :)");
        }
        System.out.println("Programme terminé");
    }
}
```
