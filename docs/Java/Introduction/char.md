# char

## Qu'est-ce qu'un char ?

Un `char` représente un seul caractère de n'importe quelle langue. En réalité, une `String` est une liste de `char`.

Lorsque l'on veut créer un `char`, on utilise les guillemets simples (ex.: `'a'`).


## Échappement du Char

Pour représenter le guillemet simple, on utilise la même technique que pour l'échappement dans une String.

```java
char guillemetSimple = '\'';
```

## Fonctions

Voici quelques méthodes pouvant être exécutées sur un caractère. Ces fonctions proviennent de la classe `Character`. (Nous verrons plusieurs classe utilitaires dans ce genre durant la session)

* `Character.isLetter(char c)` : Permet de vérifier si le Char est une lettre
* `Character.isDigit(char c)` : Permet de vérifier si le Char est un chiffre
* `Character.isUpperCase(char c)` : Permet de vérifier si le Char est une majuscule
* `Character.isLowerCase(char c)` : Permet de vérifier si le Char est une minuscule
* `Character.toUpperCase(char c)` : Retourne le Char en majuscule
* `Character.toLowerCase(char c)` : Retourne le Char en minuscule
* `Character.toString(char c)` :  Retourne le Char en String

Par contre, puisque Char est un type primitif, ces fonctions ne sont pas accessibles par le Char directement. On doit utiliser la classe `Character`.

```java
Console.afficher(Character.isLetter('c'));
```

Il est fréquent d'analyser une String caractères par caractères. Il est possible d'extraire chaque caractère à l'aide de la fonction `charAt(index)` que nous avons vu précédemment. 

Voici un exemple:

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("Entrez un mot: ");
        String mot = Console.demanderTexte();
        int longueur = mot.length();
        for (int i = 0; i < longueur; i++) {
            char c = mot.charAt(i);
            if(Character.isLetter(c)) {
                Console.afficher("Le caractère à la position " + i + " est une lettre");
            } else if(Character.isDigit(c)) {
                Console.afficher("Le caractère à la position " + i + " est un chiffre");
            } else {
                Console.afficher("le caractère à la position " + i + " n'est ni une lettre, ni un chiffre");
            }
            if(Character.isUpperCase(c)) {
                Console.afficher("Le caractère à la position " + i + " est une majuscule");
            } else if(Character.isLowerCase(c)) {
                Console.afficher("Le caractère à la position " + i + " est une minuscule");
            } else {
                Console.afficher("le caractère à la position " + i + " n'est ni en majuscule, ni en minuscule");
            }
        }

        for (int i = 0; i < longueur; i++) {
            char c = mot.charAt(i);
            Console.afficher(Character.toUpperCase(c));
        }
    }
}
```

Essayez ce code avec la chaine **abCD12!@**

Comme vous pouvez le voir, certain caractère ne sont ni des lettres ni des chiffres, ni même en majuscule ou minuscule. 

:::tip
Nous verrons la boucle `for` d'ici peu
:::
