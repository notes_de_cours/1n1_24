# Entrées/Sorties (avancées)

Un programme en mode console à souvent cette structure:

1. Lecture des entrées de l'utilisateur (System.in)
2. Traitement de l'information
3. Écriture du résultat (System.out)

# Afficher en console

 Presque tous les langages de programmation fournissent au programmeur un ensemble de fonctionnalité dans le but d'interagir avec le système d'exploitation ou de simplifier la programmation.

 Java nous fournit une fonctionnalité permettant d'inscrire du texte dans la console

 Cette fonctionnalité se trouve dans System.out et elle s'appelle `println`.

 Comme si vous naviguiez dans une arborescence de dossiers, println se trouve dans System et ensuite dans out.

:::warning
 Dans le cadre du cours  (et des exercices) nous vous avons fournie une alternative maison: `Console.afficher()`.   
Cette alternative ne sera pas toujours disponible. Il est donc important de connaitre et de comprendre la solution classique. 
:::

On peut afficher :

* du texte
  * `System.out.println("Bonjour");`
* des entiers
  * `System.out.println(1);`
* des réels
  * `System.out.println(1.2);`
* des booléens (vrai ou faux)
  * `System.out.println(true);`
* Une ligne vide
  * `System.out.println();`
* même des objets (nous y reviendrons)

`print`, son frère (ou sa soeur ... ou son adelphe (pour être politically correct :smile:), permet d'afficher un texte sans sauter une ligne à la fin.

Ainsi,
```java
System.out.print("Bonjour ");
System.out.println("à tous!");
```

Affiche:

```
Bonjour à tous!

```

Une utilisation fréquente du print est l'affichage dans une boucle for.

```java
for(int i = 0;  i<10; i++) {
  System.out.print(i);
}
System.out.println(); // pour faire le retour de ligne à la fin
```

Essayez de faire la même chose avec Console ... 

Parfois les bibliothèques de fonctions ne fournissent pas exactement ce qu'on veut. Il est alors utile de connaitre la base afin d'avoir plein contrôle. 

# Les entrées au clavier

:::warning
Les fonctionnalités offertes par Java afin de faire l'entrée de donnée ne sont pas *facile* à utiliser. Il peut parfois être difficile de lire certaines séquences d'entrées (ex: un int, suivit d'un texte). C'est pourquoi nous vous avons fournis `Console`.     
Il est recommandé d'utiliser `Console` pour faire les exercices. 
:::

## System.in

De la même façon que `System.out` servait à *sortir* de l'information du système (sur l'écran), `System.in` sert pour entrer de l'information dans le système (à partir du clavier)

## Scanner  

La classe [Scanner](https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html#Scanner-java.io.InputStream-) simplifie l'utilisation de `System.in` en offrant quelques fonctionnalités plus avancées.  

On doit d'abord importer Scanner avec l'instruction suivante

```java
import java.util.Scanner; // sous la ligne package
```

Ensuite, vous devez créer un `Scanner` à l'intérieur de votre `main`.

```java
Scanner clavier = new Scanner(System.in);
```


Scanner offre les fonctionnalités suivantes:

`nextLine()`    lit toute une ligne jusqu'au saut de ligne (équivalent à Console.demanderTexte())    
`nextInt()`     lit un entier jusqu'au prochain caractère qui n'est pas un entier (Console.demanderEntier())    
`nextDouble()`  lit un double jusqu'au prochain caractère qui n'est pas un double (Console.demanderNombreAVirgule())    
`next()`        lit un mot jusqu'au prochain espace ou prochain saut de ligne (pas d'équivalent avec Console)


Voici un  exemple d'un programme utilisant le scanner:

```java
    Scanner clavier = new Scanner(System.in);
    System.out.print("Entrez un  texte: ");
    String ligne = clavier.nextLine(); 
    System.out.print("Entrez un int: ");
    int num = clavier.nextInt(); 
    System.out.print("Entrez un double: ");
    double d = clavier.nextDouble(); 
    System.out.print("Entrez plusieurs mots séparées par des espaces: ");
    String mot = clavier.next(); 
    System.out.println("le premier mot est :"+mot);
```

:::warning
`nextDouble()` utilise une variable d'environnement de Windows afin de savoir si le séparateur décimale est la virgule ou le point pour l'entrée de données. Il est donc possible que vous ayez à utiliser une virgule.  
Il est à noter que Console.demanderNombreAVirgule() utilise toujours le point comme séparateur décimale
:::

:::tip 
Vous pouvez forcer le système à utiliser le point comme séparateur décimale en changeant le Locale utiliser par défaut:    
Scanner clavier = new Scanner(System.in).useLocale(Locale.US);    
:::

Voici une entrée valide pour le code plus haut :

```
Bonjour tout le monde!
123
123.01
Bonjour tout le monde
```

Notez que seul Bonjour est affiché à la fin. La fonction next() lit les caractères jusqu'à la fin de la ligne ou jusqu'au prochain espace. 


Bizarrement, vous pourriez aussi utiliser ceci :

```
Bonjour tout le monde!
123 123.01 Bonjour
```

L'espace est aussi un séparateur pour les nombres. C'est pour cela que l'on peut entrer plusieurs données sur la même ligne (sans faire Entrée)

## Problème avec scanner

Essayez ce code :
```java
  System.out.print("Entrez un int: ");
  int num = clavier.nextInt();
  System.out.print("Entrez un  texte: ");
  String ligne = clavier.nextLine();
  System.out.println("Le texte: " + ligne);
```

Que se passe-t-il?

Le problème est que nextInt() ne "mange" pas le \n quand vous faites entrée après votre entier.

Le \n est alors "mangé" par le nextLine(), ce qui donne une ligne vide. 

Afin de régler ce problème, il est nécessaire de faire un nextLine() à la suite sur nextInt() afin de passer le \n.

Par contre, si vous aviez entrez l'entier suivit du texte sur la même ligne, alors ce texte serait perdu... il n'y a pas de monde parfait ;)

:::tip
C'est pour cette complexité que nous vous recommandons d'utiliser `Console` dans le cadre de ce cours
:::


## Gestion des erreurs

Si vous entrez une chaine de caractères comme réponse pour nextInt() ou nextDouble(), vous obtiendrez une erreur et le programme s'arrêtera. 

Il est possible de gérer cette erreur ... ce que nous verrons dans la section Exceptions.

:::tip
Pour l'instant, il est recommandé d'utiliser les fonctionnalités fournies par `Console` afin d'éviter ce genre de problème. 
:::




