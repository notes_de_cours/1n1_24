# Classe Math

La classe Math contient un ensemble de fonctions permettant d'effectuer plusieurs opérations mathématiques complexes.

En voici quelques exemples:

* `Math.abs(nombre)` : Retourne la valeur absolue d'un nombre (int, double, float, long)

```java
int test = Math.abs(-3); // 3
```

* `Math.sqrt(double nombre)` : Retourne la racine carré de nombre
* `Math.cbrt(double nombre)` : Retourne la racine cubique de nombre
* `Math.pow(double a, double b)` : Retourne `a` à la puissance `b` 

```
double a = Math.sqrt(144); // 12
double b = Math.cbrt(8); // 2
double c = Math.pow(2,4); // 16
```

* `Math.min(a, b)` :  Retourne la valeur minimum entre a et b (int, double, float, long)
* `Math.max(a, b)` : Retourne la valeur maximum entre a et b (int, double, float, long)

```java
int d = Math.min(1, 2); // 1
int e = Math.max(1, 2); // 2
```

* `Math.toRadians(double angDeg)` : Retourne la valeur en radians de angDeg en degrés 
* `Math.sin(double nombre)` : Retourne sin(nombre), nombre est en radian
* `Math.cos(double nombre)` : Retourne cos(nombre), nombre est en radian

```java
double f = Math.toRadians(180); // PI
double g = Math.sin(Math.toRadians(30)); // 0.5
double h = Math.cos(Math.toRadians(60)); // 0.5
```

* `Math.floor(double nombre)` : Retourne nombre arrondi à l'unité inférieure
* `Math.ceil(double nombre)` : Retourne nombre arrondi à l'unité supérieure
* `Math.round(double nombre)` : Retourne nombre arrondi à l'unité la plus près

```java
double i = Math.floor(0.5); // 0.0
double j = Math.ceil(0.4); // 1.0
double k = Math.round(0.5); // 1.0
```

* `Math.random()` : Retourne un nombre double au hasard de 0.0 à 1.0 (exclusivement, c.à.d. que la valeur 1.0 ne sera jamais retournée)


Pour générer une valeur double aléatoire dans un intervalle inclusif de 1 à 10 (c.à.d. que 10 peut être généré)
```java
double aleatoireUnAdix = Math.floor(Math.random() * 10)+1 // nombre au hasard de 1 à 10
```

* `Math.PI` : Constante représentant le nombre pi

## Cast

Pour convertir un double ou un float en int, il faut faire un cast. 

```java
int x  = (int) 3.0; // x -> 3
```

Pour générer une valeur entière aléatoire dans un intervalle inclusivement  (c.à.d. que la valeur MAX peut être retournée) :

```java
final int MIN = 5;
final int MAX = 10;

int nombre = (int) (Math.random()*(MAX-MIN+1)) + MIN;
```


## Exercice

1) Écrivez un programme qui demande un nombre double à l'usager
2) Affichez la racine carrée de ce nombre. 
3) Affichez ce nombre arrondi à l'unité supérieure. 
4) Multipliez ce nombre par pi (3.1415...) et affichez le résultat.
5) Additionnez ce nombre à pi et affichez le résultat.
6) Générez et affichez un nombre entier aléatoire entre 10 et 20 inclusivement. 
7) Multipliez le nombre original par le nombre aléatoire que vous venez de générer et affichez le résultat. 

Minimisez le nombre de ligne. Lorsqu'on demande de faire une opération mathématique et d'afficher le résultat, faites l'opération à même le Console.afficher(). Ne créez pas une variable temporaire pour entreposer le résultat. 
:::details

```java
package ca.cegepdrummond;
public class Programme {
    public static void main(String[] args) {

        double nombre;
        nombre = Console.demanderNombreAVirgule();
        Console.afficher("Racine carrée : " + Math.sqrt(nombre));
        Console.afficher("Arrondi a l'unite superieur :"+Math.ceil(nombre));
        double nombreAleatoire = Math.floor(Math.random()*11)+10; // 10 à 20, c'est 11 nombres ;)
        Console.afficher("nombre aleatoire :"+nombreAleatoire);
        Console.afficher("Multiplication :"+nombre*nombreAleatoire);
    }
}
```
:::

Question: est-ce le point 5 affiche le bon résultat?

Si non, pourquoi?
:::details
Si vous avez utilisez la même technique que dans la solution proposée, rappelez-vous que l'addition se fait de gauche à droite. 

La valeur de nombre sera concaténée à la chaine de caractère au lieu d'être additionnée à PI.
:::