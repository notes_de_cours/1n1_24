# Les conditions

## Type booléen

Peut prendre 2 valeurs : `true` ou `false`.

Peut être utilisé lorsqu'il y a 2 états opposés :

* le magasin est ouvert (ouvert - `true`, fermé - `false`)
* l'avion vole (oui - `true`, non - `false`)
* une option est active (oui - `true`, non - `false`)
* Etc.

## Opérateurs de comparaison

On peut comparer 2 valeurs et obtenir un booléen.

* `==` (est égal à) (ATTENTION: les 2 symboles "=" sont pour la comparaison, alors que l'assignation n'en requiert qu'un)
* `!=` (n'est pas égal à)
* `>` (est plus grand que)
* `>=` (est plus grand ou égal à)
* `<` (est plus petit que)
* `<=` (est plus petit ou égal à)

Exemple

```java
int a = 1;
int b = 2;
int c = 3;
int d = 4;

Console.afficher(a < b);
Console.afficher(a == b);
Console.afficher(a != b);
Console.afficher(a+b+c > d);
```



## if, else, else if

On peut contrôler le flux du programme selon le résultat d'une expression booléenne.

:::danger ne pas copier
```java
if (expression) {
    // fait quelque chose
}
```
:::

Si l'expression est vraie, on exécute le code à l'intérieur.

```java
int age = Console.demanderEntier();
if (age >= 18) {
    Console.afficher("Tu es majeur.");
}
// autres instructions
```

Qui correspond à l'algorithme suivant:

![if1](/if1.png)

On peut aussi exécuter un bloc d'instructions si l'expression est fausse.

```java
int age = Console.demanderEntier();
if (age >= 18) {
    Console.afficher("Tu es majeur.");
} else {
    Console.afficher("Tu es mineur.");
}
// autres instructions
```
Qui correspond à l'algorithme suivant:

![if2](/if2.png)

On peut aussi mettre une autre condition si la première expression est fausse.

```java
int age = Console.demanderEntier();
if (age >= 18) {
    Console.afficher("Tu es majeur.");
} else if (age >= 12) {
    Console.afficher("Tu es adolescent.");
} else if (age >= 2) {
    Console.afficher("Tu es un enfant.");
} else {
    Console.afficher("Tu es un bébé.");
}
```
Qui correspond à l'algorithme suivant:

![if3](/if3.png)

## Bloc de codes

Les `{}` représentent les limites d'un bloc de codes.

:::danger Attention

Si vous déclarez une variable à l'intérieur d'un bloc de codes, celle-ci ne sera pas accessible à l'extérieur de ce bloc.
:::

:::danger ne pas utiliser
```java
int temperatureEau = Console.demanderEntier();
if (temperatureEau <= 0) {
    String etatEau = "glace"; // [!code highlight]
} else if (temperatureEau >= 100) {
    etatEau = "vapeur"; // variable inconnue  // [!code error]
} else {
    etatEau = "liquide"; // variable inconnue // [!code error]
}

Console.afficher(etatEau); // variable inconnue // [!code error]
```
:::

Correction

```java
int temperatureEau = Console.demanderEntier();
String etatEau; // [!code highlight]
if (temperatureEau <= 0) {
    etatEau = "glace";
} else if (temperatureEau >= 100) {
    etatEau = "vapeur";
} else {
    etatEau = "liquide";
}

Console.afficher(etatEau);
```

## Conditions imbriquées

On peut aussi mettre une condition dans une condition

```java
int age;
Console.afficher("Quel est ton age ?");
age = Console.demanderEntier();

if( age >= 18 ) {
    Console.afficher("Tu es majeur");
    if (age >= 65) {
        Console.afficher(" et tu es à la retraite");
    } else {
        Console.afficher(" et tu travails encore");
    }
} else {
    Console.afficher("Tu es mineur");
}


```

Qui correspond à l'algorithme suivant:

![if4](/if4.png)

## Tester un booléen

On peut écrire directement un booléen comme expression a tester


```java
boolean estUnHomme = Console.demanderOuiNon();
if (estUnHomme) {
    Console.afficher("Bonjour Monsieur");
}
// est équivalent à 
if (estUnHomme == true) {
    Console.afficher("Bonjour Monsieur");
}
```

Si je veux vérifier si la valeur est fausse

```java
if (!estUnHomme) {
    ....
}
// est équivalent à 

if (estUnHomme == false) {
    ...
}
```





## Opérateurs booléens


* **NOT** est un opérateur unaire qui inverse la valeur booléenne. En Java, on utilise le symbole `!`

```java
public class Programme {
    public static void main(String[] args) {
        Console.afficher("Entrez le nombre 0");
        int valeur = Console.demanderEntier();
       	if( !(valeur == 0) ) { // [!code highlight]
	        Console.afficher("le nombre n'est pas 0");
        } else {
            Console.afficher("Bravo!");
        }
    }
} 
```

:::tip 
La ligne surlignée peut aussi s'écrire 
```java
if(valeur != 0) {
```
:::
* **AND** est un opérateur binaire qui retourne vrai si les deux opérandes sont vrais. Sinon, il retourne faux. En Java, on utilise le symbole `&&`.

|valeur 1|valeur 2|résultat|
|-|-|-|
|true|true|true|
|true|false|false|
|false|true|false|
|false|false|false|


```java
public class Programme {
    /**
     *  Detecte si un nombre entré au clavier est un multiple de 3 ET de 5
     */
    public static void main(String[] args) {
        Console.afficher("Entrez un nombre :");
        int valeur = Console.demanderEntier();
       	if( (valeur % 5 == 0) && (valeur % 3 == 0) ) {
	        Console.afficher("le nombre est un multiple de 3 et de 5");
        } else {
            Console.afficher("le nombre n'est pas un multiple de 3 et de 5");
        }
    }
} 

```

* **OR** est un opérateur binaire qui retourne vrai si au moins un des opérandes est vraie. Sinon, il retourne faux. En Java, on utilise le symbole `||`.

|valeur 1|valeur 2|résultat|
|-|-|-|
|true|true|true|
|true|false|true|
|false|true|true|
|false|false|false|

```java
import java.util.Scanner;
public class Programme {
    /**
     *  Detecte si un nombre entré au clavier est un multiple de 3 OU de 5
     */
    public static void main(String[] args) {
        Console.afficher("Entrez un nombre :");
        int valeur = Console.demanderEntier();
       	if( (valeur % 5 == 0) || (valeur % 3 == 0) ) {
	        Console.afficher("le nombre est un multiple de 3 ou de 5");
        } else {
            Console.afficher("le nombre n'est pas un multiple de 3 ni de 5");
        }
    }
} 
```

* **XOR** (ou exclusif) est un opérateur binaire qui retourne vrai si les opérandes sont différents. Sinon, il retourne faux. En Java, on utilise le symbole `^`. (Il n'est pas souvent utilisé)

|valeur 1|valeur 2|résultat|
|-|-|-|
|true|true|false|
|true|false|true|
|false|true|true|
|false|false|false|

```java
import java.util.Scanner;
public class Programme {
    /**
     *  Detecte si un nombre entré au clavier est un multiple de 3 OU de 5 mais pas les deux
     */
    public static void main(String[] args) {
        Console.afficher("Entrez un nombre :");
        int valeur = Console.demanderEntier();
       	if( (valeur % 5 == 0) ^ (valeur % 3 == 0) ) {
	        Console.afficher("le nombre est un multiple de 3 ou de 5 mais pas des deux");
        } else {
            Console.afficher("le nombre est soit un multiple de 3 et de 5, soit ni de 3 ni de 5");
        }
    }
} 
```

## Exercice 1

* Écrivez un programme qui demande 3 entiers en utilisant Console. 
* Vérifiez l'ordre de ces 3 entiers. 
  * S'ils sont tous égaux, écrivez `Égaux`
  * S'ils sont en ordre croissant, écrivez `Croissant` (note: il peut y en avoir 2 d'égaux)
  * S'ils sont en ordre décroissant, écrivez `Décroissant`
  * S'ils s'ont en ordre quelconque, indiquez l'ordre du premier vs deuxième, et l'ordre du deuxième vs troisième. 
    * Par exemple: `le premier est plus grand que le deuxième, et le deuxième est plus petit que le troisième`, ou `le premier est plus petit que le deuxième, et le deuxième est plus grand que le troisième`

Commencez par en faire l'algorithme. 

:::details Solution de l'algorithme
![conditioncroissance](/conditioncroissance.png)
:::

:::details

```java
    int a,b,c;
    a=Console.demanderEntier();
    b=Console.demanderEntier();
    c=Console.demanderEntier();

    if(a==b && b==c) {
        Console.afficher("Égaux");
    } else if (a<=b && b<=c ) {
        Console.afficher("Croissant");
    } else if (a>=b && b>=c) {
        Console.afficher("Décroissant");
    } else if(a>=b) {
        Console.afficher("le premier est plus grand que le deuxième, et le deuxième est plus petit que le troisième");
    } else {
        Console.afficher("le premier est plus petit que le deuxième, et le deuxième est plus grand que le troisième");
    }
       
```

:::


## Exercice 2 

Écrivez un programme qui demande une année (un entier entre 1 et 9999, faire la validation) et qui détermine si c'est une années bissextile.

Une année est bissextile si elle est divisible par 4 sans être divisible par 100, ou si elle est divisible par 400. Par exemple: 1600, 1604, 2000, 2004 sont bissextiles, mais 1601, 1800, 2001 ne le sont pas.

:::tip
Pour savoir si un nombre est divisible par un autre, l'opérateur modulo `%` est très utile 
:::

Commencez par en faire l'algorithme.

:::details Solution de l'algorithme

Comme la majorité des problèmes, il y a plusieurs solutions. En voici 2:

![bissextile1](/bissextile1.png)

![bissextile2](/bissextile2.png)

:::

:::details Solution du code
```java
package ca.cegepdrummond;
public class Programme {
    public static void main(String[] args) {
        int annee;
        boolean bissextile = false;
        do {
            Console.afficher("Entrez une année entre l'an 1 et l'an 9999 ?");
            annee = Console.demanderEntier();
        } while (annee < 1 || annee > 9999);

        if (annee % 4 == 0) {
            if (annee % 400 == 0) {
                bissextile = true;
            } else if (annee % 100 != 0) {
                bissextile = true;
            }
        }
        if(bissextile) {
            Console.afficher("Bissextile");
        } else {
            Console.afficher("Non Bissextile");
        }
    }

}
```

ou encore

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        int annee;
        boolean bissextile;
        do {
            Console.afficher("Entrez une année entre l'an 1 et l'an 9999 ?");
            annee = Console.demanderEntier();
        } while (annee < 1 || annee > 9999);

        if (annee % 4 == 0 && ((annee % 400 == 0) || (annee % 100 != 0)) ) {
            bissextile = true;
        } else{
            bissextile = false;
        }
        if (bissextile) {
            Console.afficher("Bissextile");
        } else {
            Console.afficher("Non Bissextile");
        }
    }
}
```
:::


:::info
Il est fréquent de concevoir un algorithme pour ensuite en trouver un qui est plus simple ou plus court. L'important c'est qu'il soit facile à comprendre. Vous écrivez du code 1 fois, mais vous le lirez des dizaines ou même centaines de fois, et surtout, d'autres personnes le liront. Il est donc important que ce code soit *simple* à comprendre. N'hésitez pas à repenser votre code si vous le trouvez difficile à comprendre.

Dans ce cours, l'important c'est que votre code fonctionne. Avec l'expérience, vous aurez plus de facilité à écrire du *beau* code. 

:::

## Priorité des opérations (du plus prioritaire au moins prioritaire)

* `!` - NOT
* `^` - XOR
* `&&` - AND
* `||` - OR

Quel est le résultat de `!!true || true` ?

Quel est le résultat de `!(true || true)` ?

L'opérateur `!` est donc évalué avant le `||`. 

Comme pour les opérations arithmétiques, on peut utiliser les parenthèses pour prioriser les opérations.

```java
boolean a = true;
boolean b = false;
boolean c = true;
boolean d = (a || b) && (c ^ a); // false
```

## Raccourcis

Il est possible dans certains cas de déterminer le résultat d'une opération booléen en regardant seulement le premier opérande.

**OR**

Si le premier opérande est vrai, il y a au moins un opérande à vrai, donc le résultat est vrai.

```java
boolean a = true;
boolean b = false;
boolean c = true;
boolean d = a || (b && a || a && c); // étant donné que a est true, pas besoin de calculer le contenue de la parenthèse
```

**AND**

Si le premier opérande est faux, les deux opérandes ne sont pas vrais, donc le résultat est faux.

```java
boolean a = true;
boolean b = false;
boolean c = true;
boolean d = b && (a || b && b ^ c); //étant donné que b est false, pas besoin de calculer le contenue de la parenthèse
```

Java applique ce principe et n'exécutera pas le code du second opérande s'il connait déjà la réponse.

Ce truc est souvent utilisé pour simplifier des tests. Ici, si valeur2 est à 0, cela provoquerai une division par zéro. Mais étant donnée que le if le vérifi en premier, la division ne sera pas effectuée si valeur2 est à 0. 

```java
int valeur = Console.demanderEntier();
int valeur2 = Console.demanderEntier();
if(valeur2!=0 && valeur/valeur2 == 1) {
    Console.afficher("la division a fonctionnée");
} else if(valeur2 == 0) {
    Console.afficher("0 ne peut être utilisé dans la division");
}
```

