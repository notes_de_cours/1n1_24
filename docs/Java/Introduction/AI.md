# Attention aux Intelligences Artificielles (IA). 

Avec la venue de chatGPT et des autres IA, il est rendu beaucoup trop facile de générer les travaux que nous vous demandons en première année. 

Mais est-ce que vous désirez apprendre à programmer, ou apprendre à utiliser chatGPT???

Le cégep a établit des niveaux d'utilisation de l'IA dans les cours:

:::info Niveaux
* Niveau 1 : L’étudiant rédige un texte, du code ou produit un travail sans consulter l’IA ou internet.
* Niveau 2 : L'étudiant rédige un texte, du code ou produit un travail sans consulter l'IA.
* Niveau 3 : L’étudiant consulte l’IA pour une tempête d’idées et rédige un texte, du code ou produit un travail à partir des idées générées par l’IA.
* Niveau 4 : L'étudiant rédige les idées principales de son texte, de son code ou de son travail. Il demande à l'IA de créer un brouillon et de suggérer des pistes d'amélioration.
* Niveau 5 : L’étudiant demande à l’IA de générer plusieurs parties de texte, de code ou de son travail. Il assemble ensuite ces parties, les édite et remet le travail à son enseignant.
* Niveau 6 : L'étudiant fait écrire le texte, le code ou produit son travail par l'IA. Il relit, ajuste le texte et le remet à son enseignant.
* Niveau 7 : L’étudiant demande à l’IA d’écrire le texte, le code ou de produire un travail et fait un copier-coller qu’il remet à l’enseignant.


:::

Dans le cadre de ce cours, le niveau suivant sera accepté pour les travaux: 

:::info Niveau 2
L'étudiant rédige un texte, du code ou produit un travail sans consulter l'IA.
:::

:::danger AVERTISSEMENT
Si l'enseignant soupconne moindrement l'utilisation d'une IA à un niveau supérieur dans votre travail, vous devrez répondre à une série de questions très détaillées portant sur votre programme. Si vous ne pouvez répondre à ces questions, l'enseignant présumera que ce n'est pas votre oeuvre, **vous obtiendrez automatiquement la note 0 ainsi qu'une accusation de plagiat**
:::

Les conséquences d'un plagiat sont graves. La première fois, vous serez interpelé par la direction afin de vous faire rappeler la gravité de l'acte. 

La deuxième fois, vous risquez une **expulsion du Cégep**. 

Cette deuxième fois n'a pas besoin d'être dans le même cours... votre dossier vous suit!!!. 



