# Variables

## Qu'est-ce qu'une variable

Une variable est un espace réservé pour stocker une valeur d'un type particulier : une chaîne de caractères, un nombre ou autre chose.

Chaque variable a un nom pour la distinguer des autres.

:::info
 Comme en mathématique, lorsqu'on écrit `x = 12` et qu'on l'utilise après dans la formule `f(x) = x + 30`
:::

## Déclaration d'une variable

Avant de commencer à utiliser une variable vous devez la déclarer pour allouer l'espace nécessaire en mémoire.

Vous devez déclarer la variable comme ceci :

```java
TypeDeLaVariable nomDeLaVariable;
```

:::warning Attention:
Vous devez déclarer la variable une seule fois, mais vous pouvez changer sa valeur plusieurs fois.
:::

## Nom d'une variable

Le nom distingue la variable des autres.

Le nom doit respecter certaines règles :

* Le nom ne doit **pas** **commencer** par un **chiffre**
* Le nom **peut** contenir
  * des caractères de **A à Z** (**majuscule** et **minuscule**, pas d'accent)
  * des **chiffres**
  * signe de **dollar** (`$`)
  * le symbole de **soulignement** (`_`)
* Par convention, le nom **doit**
  * **commencer** par une **lettre**
  * utiliser le standard **camelCase**
    * première lettre du premier mot en minuscule
    * première lettre des autres mots en majuscule


* Et la règle la plus importante: Le nom doit être **significatif**


> Exemple 
>
> ```java
> int e;
> ```
>
> versus
>
> ```java
> int nombreEtudiants;
> ```
>
> à la limite
>
> ```java
> int nbEtudiants;
> ```

* Le nom doit être de préférence **court** (sauf si cela le rend moins significatif)
* Le nom ne doit **pas** être un **mot réservé** à Java
  * voir https://www.javatpoint.com/java-keywords
* Le nom doit être en **français** :)
* Le nom est **sensible à la casse** (majuscule et minuscule)
  * En fait, Test et test sont 2 variables différentes aux yeux de Java
  * Java est sensible à la casse, tout simplement

## Type d'une variable

Le type de variable détermine les opérations possibles sur la variable et quelles valeurs peuvent y être stockées.

Plusieurs types existent en java :

* Types primitifs
  * Entiers
    * byte
    * **char**
    * short
    * **int**
    * long
  * Réels
    * float
    * **double**
  * Booléens
    * **boolean**
* Types objets
  * Chaîne de caractères
    * **String**
  * Plusieurs autres
    * Nous y reviendrons

Pour l'instant nous utiliserons seulement char, int, double, boolean, String.



## Portée d'une variable

Lorsque nous verrons les blocs de code et plus particulièrement les fonctions, vous verrez qu'un même nom peut être utilisé à plusieurs endroits et ne pas représenter la même valeur. Nous appelons cela la portée de la variable (scope en anglais). 

Ne soyez donc pas surpris de voir plusieurs fois une variable être déclarée avec le même nom à l'intérieur de plusieurs fonctions. 

Il sera très important de comprendre ce principe. Nous y reviendrons lorsque nous parlerons des fonctions.

## Assignation d'une variable

Pour assigner une valeur à une variable, il suffit d'inscrire le nom de la variable, suivit de l'opérateur `=`, suivit d'une valeur ou d'une expression dont le résultat sera stocké dans la variable.

Exemple

```java
nbEtudiants = 30;
```

On peut aussi assigner une variable en la déclarant.

```java
int nbEtudiants = 30;
```

Exemple pour les différents types

```java
char premiereLettreMonNom = 'S';
int nbEtudiants = 30;
double moyenneEtudiants = 89.9;
boolean programmationCool = true;
String proverbe = "C'est en programmant qu'on devient programmeur";
```

Exemple d'assignations non valides

```java
char charCEstUnCharacterePasDeux = 'ab'; // erreur, un char ne peut conteneur qu'un caractère.
int nbEtudiants = "30"; // pas de guillemets pour un int
double moyenneEtudiants = 89,9; // en Java, le délimiteur décimal est le point "." et non la virgule
boolean programmationCool = True; // Java est sensible à la casse, il faut utiliser true
string proverbe = "C'est en programmant..."; // String est une classe et s'écrit donc avec une majuscule (nous y reviendrons plus tard)
```


## Utilisation d'une variable

Pour utiliser une variable, il suffit d'inscrire son nom.

Exemple

```java
int nbEtudiants = 30;
Console.afficher(nbEtudiants);
```


## Constantes

Les constantes sont une catégorie spéciale de variables. 

Une constante, comme son nom l'indique, ne changera pas de valeur. Elles sont souvent utilisées pour représenter des nombres `magiques`.

Exemple:

Dans un programme vous devez représenter un dé. Vous devez donc représenter le nombre de faces qu'a ce dé. Vous aurez donc le réflexe d'inscrire 6 quand vous devez référer au nombre de faces. Par exemple, vous devez générer un nombre aléatoire représentant la valeur du dé. Vous allez donc écrire quelque chose comme (nous verrons Math.random() un peu plus tard):

```java
valeur = Math.floor(Math.random()*6)+1;
```

S'il y a plusieurs références au nombre de faces, vous vous retrouverez plusieurs fois avec le nombre 6 dans votre code. Mais 6 peut aussi représenter autre chose dans votre jeu (le nombre de joueur, la valeur maximum pour une caractéristique du jeu, etc.). 

Si jamais on change les règles du jeu et que maintenant le dé peut avoir 9-10-12... faces, qu'avez-vous à changer dans le code? 

Vous aurez à trouver tous les 6 et les changer pour la nouvelle valeur... mais une minute, la valeur 6 peut aussi représenter autre chose!! Vous devez donc comprendre à quoi sert ce 6 et décider de le changer ou non.

La solution: une constante.

En java une constante se déclare comme suit:

```java
final int NOMBRE_DE_FACES = 6;
```

et dans le code, elle s'utilise comme une variable:

```java
valeur = Math.floor(Math.random()*NOMBRE_DE_FACES)+1);
```

Maintenant, si votre programme utilise un dés à 9 faces, vous n'avez qu'à changer la constante; évitant ainsi beaucoup de lecture de code, et la création de bug difficiles à trouver.

Comme indiqué, une constante ne peut changer de valeur. On  lui donne une valeur lors de sa création, et cette valeur ne changera plus. Il est donc impossible d'écrire:
```java 
NOMBRE_DE_FACES = 9
```

Afin de différencier les constantes des autres variables, il est courant de leur donner un nom en majuscules séparées par des soulignés `_`

Un autre avantage des constantes est qu'elles rendent le code plus facile à lire. Dans l'exemple précédent, quand il y avait un 6 dans le random, en lisant le code on se demanderait que représente ce 6. Il aurait fallu mettre un commentaire pour indiquer que c'est le nombre de faces. Losqu'un constante est utilisée, le code s'auto-documente.

Maintenant que vous comprenez l'utilisation des constantes, vous **DEVEZ** les utiliser. 

:::danger AVERTISSEMENT
Si vous utilisez des nombres magiques dans votre code au lieu d'y mettre une constante, vous perdrez des points.
:::

