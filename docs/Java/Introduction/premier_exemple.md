# Exemple complet de code

Vous trouverez dans cet exemple plusieurs éléments que nous allons vous expliquer dans les prochains cours. 
:::info
Ne vous occupez pas des lignes avec **// #region** et **// #endregion**, elle sont là pour notre documentation
:::

Créez un nouveau projet, et copiez ce code dans Programme.java

:::details Exemple de code

<<< @/public/code/src/ca/cegepdrummond/Programme.java {1,4 java:line-numbers}
:::




