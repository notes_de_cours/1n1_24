# Boucles

Les boucles sont nécessaire afin de répéter des opérations. Nous en avons vu un exemple dans la section pour les `char`. La boucle permettait de parcourir la chaine de caractères. 

Il existe plusieurs types de boucles. Chaque type a son utilité et doit être utilisé dans les bonnes circonstances. 


## while

C'est la boucle la plus générale. Elle peut remplacer tous les autres types. 

Permet de répéter un bloc de codes tant qu'une condition est vraie.

```java
while (condition) {
    // bloc de code à répéter
}
```

Vous devez vous assurer qu'il est possible que la condition devienne fausse. Sinon, vous aurez une boucle infinie.

:::danger À ne pas faire
```java
int x = 10;
while (x < 20 ) {
    // Je ne pourrai jamais sortir de ce bloc de codes si je ne donne 
    // pas une nouvelle valeur à la variable x. 
}
```


:::

Voici un exemple pour afficher les nombres de 1 à 5.

```java
int i = 1;
while (i <= 5) {
    Console.afficher(i);
    i = i + 1;
}
```

Ce qui correspond à l'algorithme suivant:

![while1](/while1.png)


:::tip
À moins d'avoir un nom plus significatif, le standard (pour plusieurs langages de programmation) veut que les variables **i, j et k** soient utilisées pour représenter l'itérateur (variable qui incrémente dans le but de progresser dans une boucle) (i d'abord, j si i est déjà pris, k si i et j sont pris).
:::

## do while

Fait la même chose que `while`. Par contre, il exécute une première fois le bloc de codes avant de vérifier la condition. Dans le cas du `while`, si la condition est fausse dès le départ, le bloc de codes n'est jamais exécuté. Le `do while` est moins utilisé que le `while`. Par contre, il est souvent utilisé pour les menus. 

Il est à utiliser si vous savez que la boucle doit toujours être exécutée au moins une fois. 

Exemple

```java
int choix;
do {
    Console.afficher("Inscrivez n'importe quel nombre au-dessus de 0: ");
    choix = Console.demanderEntier();
    Console.afficher(choix);
} while (choix > 0);

Console.afficher("C'est terminé!");
```



La même boucle, mais en utilisant un `while`

```java
int choix = 1;  // doit initialiser la valeur juste pour entrer dans la boucle. Ce n'est pas 
                // toujours facile, ou même possible. 
while (choix > 0) {
    Console.afficher("Inscrivez n'importe quel nombre au-dessus de 0: ");
    choix = Console.demanderEntier();
    Console.afficher(choix);
}

Console.afficher("C'est terminé!");
```

Comme vous pouvez le voir, avec la boucle `while`, il est nécessaire d'ajuster la variable choix afin que la boucle s'exécute au moins une fois. 

Voici la comparaison de ces 2 boucles. 

| dowhile | while |
|-------|---------|
| ![dowhile1](/dowhile1.png) |  ![while2](/while2.png) |

## Incrémentation et décrémentation d'un nombre

Pour une question de rapidité et de simplicité, il existe un raccourci pour incrémenter et décrémenter un nombre.

Il y a d'autres raccourcis, mais nous ne verrons que celui-ci pour l'instant. 

```java
int i = 0;
i++; // équivalent de i = i + 1;
i--; // équivalent de i = i - 1;
```

## for

Il arrive souvent de vouloir faire une boucle pour un certain nombre de fois. `for` permet de faire une boucle pour un nombre précis d'itérations d'une manière plus structurée que le `while`.

```java
for (initialisation; condition; modification) {
    // bloc de codes
}
```

* initialisation : initialise l'itérateur
* condition : vérifie l'itérateur (même condition que le `while`)
* modification : modifie l'itérateur (habituellement l'incrémente)

Voici un exemple pour afficher les nombres de 1 à 5.

```java
for (int i = 1; i <= 5; i++) {
    Console.afficher(i);
}
```


L'itérateur peut être déclaré plus haut

```java
int i = 0;
for (i = 1; i <= 5; i++) {
    Console.afficher(i);
}
```

Ce qui correspond à l'algorithme suivant:

![for1](/for1.png)



On peut faire une boucle imbriquée.

Exemple d'une table de multiplication de 1 à 9.

```java
String ligne;
for (int i = 1; i < 10; i++) {
    ligne = "";
    for (int j = 1; j < 10; j++) {
        ligne = ligne + (i * j) + "\t";
    }
    Console.afficher(ligne);
}
```

Ce qui correspond (à peu près) à l'algorithme suivant:

![formultiplication](/for_multiplication.png)
Résultat :

```
1   2   3   4   5   6   7   8   9  
2   4   6   8   10  12  14  16  18  
3   6   9   12  15  18  21  24  27  
4   8   12  16  20  24  28  32  36  
5   10  15  20  25  30  35  40  45  
6   12  18  24  30  36  42  48  54  
7   14  21  28  35  42  49  56  63  
8   16  24  32  40  48  56  64  72  
9   18  27  36  45  54  63  72  81
```

## Comment convertir une boucle for en boucle while

Toute boucle for peut être convertie en boucle while. 

Si on regarde la syntaxe du for:

```
for(initialisation;condition; incrémentation) {
    instruction
}
```

Elle est équivalente à un while écrit de cette facon

```
initialisation
while(condition) {
    instruction
    incrémentation
}
```

Par exemple:

```
for(int i = 10; i< 100; i+2) {
    faire qqchose;
}
```

est équivalent à:

```
int i = 10;
while(i<100) {
    faire qqchose;
     i+2;
}
```


## break et continue

Dans le cadre de ce cours (et du DEC en général) il vous est interdit d'utiliser ces deux commandes: break ou continue.

Je ne vous explique même pas à quoi elles servent. Elles ont leur utilité, mais elles peuvent surtout causer des bugs très difficiles à voir dans certains scénarios.

:::danger
 **IMPORTANT**: vous perdrez des points automatiquement si vous les utilisez. :warning:
:::

# Exercice

* Écrivez un programme qui demande 10 nombres et qui en fait la somme. 
  * Faites en une version avec une boucle `for`
  * et une version avec une boucle `while`. 

Afin de mieux comprendre, commencez par écrire l'algorithme dans Flowgorithm. 

:::details Solution de l'algorithme avec **for**
![for10total](/for10total.png)
:::
:::details Solution de l'algorithme avec **while**
![for10total](/while10total.png)
:::

:::details Solution en java

Solution avec for
```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        int nombre;
        int somme = 0;
        for (int i = 1; i <= 10; i++) {
            if(i==1) {
                Console.afficher("Entrez le 1ier nombre :");
            } else {
                Console.afficher("Entrez le " + i + "e nombre :");
            }
            nombre = Console.demanderEntier();
            somme = somme + nombre;
        }
        Console.afficher("La somme est : "+ somme);

    }
}
```

Solution avec while
```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        int nombre;
        int somme = 0;
        int i = 1;
        while( i <= 10) {
            if(i==1) {
                Console.afficher("Entrez le 1ier nombre :");
            } else {
                Console.afficher("Entrez le " + i + "e nombre :");
            }
            nombre = Console.demanderEntier();
            somme = somme + nombre;
            i++;
        }
        Console.afficher("La somme est : "+ somme);

    }
}
```

:::