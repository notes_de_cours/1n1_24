# Tableau multi-dimensions

Un tableau peut avoir plusieurs dimensions. Par exemple, pour représenter un jeu 2D, nous utiliserons un tableau bidimensionnel (2 dimensions), pour un jeu 3D, un tableau à 3 dimensions. 

En réalité, un tableau multidimensionnel est un tableau de tableaux. 

## exemple:

```java
//création 
int[][] t = new int[3][4];

//assignation de valeurs
t[0][1] = 3;
t[2][3] = 1;

//affichage
for(int ligne = 0; ligne < 3; ligne++) {
	for(int col = 0; col < 4; col++ ) {
		System.out.print(t[ligne][col]);
	}
	System.out.println();
}
```

résultat:

```
0300
0000
0001
```

La valeur par défaut est mise dans chaque cellule (pour int, c'est 0) 

## Structure 

|         | colonne 1 | colonne 2 | colonne 3 | colonne 4 |
| ------- | --------- | --------- | --------- | --------- |
| ligne 1 | `t[0][0]` | `t[0][1]` | `t[0][2]` | `t[0][3]` |
| ligne 2 | `t[1][0]` | `t[1][1]` | `t[1][2]` | `t[1][3]` |
| ligne 3 | `t[2][0]` | `t[2][1]` | `t[2][2]` | `t[2][3]` |

## Initialisation

Il est possible d'initialiser le tableau lors de sa création

```java
 //création et assignation de valeurs
 int[][] t = {
 	{0,3},
 	{},
 	{},
 	{0,0,0,1}
 };
 //affichage
 for(int ligne = 0; ligne < t.length; ligne++) {
 	for(int col = 0; col < t[ligne].length; col++ ) {
 		System.out.print(t[ligne][col]);
 	}
 	System.out.println();
 }
 
 
 affichera
03


0001
```

Un tableau initialisé de la sorte n'aura pas nécessairement les mêmes dimensions pour chacune des lignes. (contrairement à C/C++)

Remarquez que la boucle utilise `t.length` et `t[ligne].length`

