# Fonctions

## Fonction en mathématique

> Entité mathématique dont la valeur de la variable dépendante dépend des valeurs d'une ou plusieurs variables indépendantes, de façon qu'une valeur unique de la variable dépendante corresponde à chaque combinaison permise de valeurs des variables indépendantes. 
[GDT]



f(x) = x ^ 2 + 1 

g(w,y) = f(w) + y + 2 

h(x, y) = f(x) + g(x,y) + 3


## Fonction en programmation

En programmation, une fonction est une séquence d'instructions regroupées pour effectuer une opération.

En Java, une fonction est toujours située dans une classe.

L'équivalent des fonctions ci-haut en java est:

```java
private static int f(int x) {
    return Math.pow(x,2) + 1;
}

private static int g(int w, int y) {
    return f(w) + y + 2;
}

private static int h(int x, int y) {
    return f(x) + g(x,y) + 3;
}
```

## Pourquoi écrire des fonctions ?

* décomposer un programme en petits sous-programmes
* réutiliser les fonctions plusieurs fois au lieu de toujours réécrire le code
* beaucoup plus facile à modifier et à maintenir qu'un programme écrit entièrement dans la fonction `main`

Vous utilisez déjà plusieurs fonctions (ou méthodes (nous y reviendrons)) : méthodes de String, méthodes de Scanner, fonctions de System.out, fonctions de Math, etc.

## Contenu de la définition d'une fonction

1. Nom
2. Modificateurs d'accès (public, static, etc.) (optionnel)
3. Type de retour
4. Paramètres (optionnel)
5. Exceptions possibles (optionnel, si aucune exception possible)
6. Bloc de codes

Nous allons nous concentrer sur 1, 3, 4 et 6.

Pour l'élément 2, dans le cadre de ce cours, toujours utiliser `public static`.

## Exemple de définition d'une fonction

```java
public static int additionner(int a, int b) {
    return a + b;
}
```

* `public static` modificateurs d'accès
* `int` type de retour
* `additionner` nom de la fonction
* `int a, int b` paramètres
* `return` mot clé pour spécifier ce que la fonction retourne

Utilisation

```java
int resultat = additionner(1, 2); // 3
```

## Nommer une fonction

Ce sont les mêmes règles que les variables :

* camelCase (toutes les lettres en minuscules, sauf le première lettre de chaque mot à partir du 2e mot)
* peut contenir des chiffres, des lettres, `$` et `_`
* ne peut pas commencer par un chiffre
* ne doit pas être un mot réservé

Par convention, le premier mot doit être un verbe suivi d'un adjectif ou d'un nom.

```java
additionner();
obtenirValeur();
calculerNombreEleves();
trouverUtilisateurParNom();
...
```

## Type de retour

Une fonction peut retourner une valeur ou non.

Le type de retour d'une fonction peut être n'importe quel type qui existe.

Si la fonction ne retourne aucune valeur, utilisez le type `void`.

```java
public static void methode() {
    // bloc de codes
}
```

Exemple

```java
public static void afficherNomComplet(String prenom, String nom) {
    System.out.println(prenom + " " + nom);
}
```

## Paramètres d'une fonction

Les paramètres servent à envoyer une ou des valeurs à la fonction. 

Si on regarde la fonction `additionner()` ci-haut, elle retourne une valeur différente selon les valeurs passées dans `a` et `b`.

Les variables définies en paramètres ne sont visibles que dans la fonction (voir Portée des variables ci-bas). 

Il est courant de mettre le préfixe `a` devant le nom d'un paramètre. Ce `a` indique que c'est un `argument`. 

De la même façon, il est courant de préfixer par un `l` les variables `locales` à une fonction. 

Exemple:

```java
public static int additionner(int aPremierNombre, int aDeuxiemeNombre) {
    int lResultat = 0;
    lResultat = aPremierNombre + aDeuxiemeNombre;
    return lResultat;
}
```

Cette pratique vous permettra de mieux comprendre la provenance d'une variable dans votre code, et en simplifie la lecture. Nous vous recommendons donc de l'utiliser. 

## Attributs de classe

Nous avons vu les variables locales (préfix `l`) et les variables en paramètres (préfix `a`). 

Java étant un langage orienté objet, il en existe un troisième: les `attributs`. En Java, on utilise la terminologie `variable d'instance`, ou encore `membre`. Nous n'utiliserons pas de préfix pour les attributs.

Un attribut est déclaré à l'intérieur de la classe, mais à l'extérieur des fonctions. 

```java
public class Programme {
    private static int attribut;
    
    public static void main(String[] args) {
        attribut = 1;
        faireQqChose();
        System.out.println(attribut); // affichera 2
    }
    
    private static void faireQqChose() {
        attribut = 2;
    }
}
```

Un attribut est donc accessible partout dans la classe. Il est recommandé de le déclarer `private` afin de restreindre son accessibilité (nous y reviendrons plus tard). Étant donné que nous n'utiliserons pas vraiment l'approche objet dans un premier temps, nos programme devrait avoir qu'une seule classe. 

Nous verrons plus tard l'utilisation des attributs. Pour l'instant, vous ne devriez pas vraiment en utiliser. 


## Variable globale

Si vous utilisez la technique des préfixes `a` et `l` dans une fonction, alors TOUTES les variables se trouvant dans celle-ci devraient commencer par `a` ou `l`. Si vous vous retrouvez avec une variable qui n'est pas préfixée, c'est que cette variable a été définie à l'extérieur de la fonction. C'est donc une `variable globale`. 

Il y a une exception: les attributs que nous venons de voir. Étant donné que nous ne faisons pas de l'OO pour l'instant (jusqu'à la partie greenfoot), vous ne devriez pas en avoir pour l'instant. Je vais donc considérer les attributs comme étant des `globales` et donc les interdire. 

 En java, il est *difficile* de créer une vraie globale. Je ne vous montrerai même pas comment le faire. 
:::warning
Vous ne devriez jamais avoir de globale dans votre code. Leur utilisation est dangereuse et peut causer des bugs difficiles à localiser. 
:::
:::danger IMPORTANT
Dans le cadre de ce cours (et potentiellement pour le reste de vos études), vous perdrez des points si vous avec une globale dans votre code. 
 (ou si, pour la première partie du cours, vous utilisez un attribut au lieu d'utiliser des paramètres) 
:::

## Retour d'une fonction

`return` permet de retourner une valeur à l'appelant de la fonction.

Il existe 2 écoles de pensées en regard avec l'utilisation du return:

1) Il ne doit y avoir qu'un et un seul return à la fin de la fonction;
2) Il peut y avoir plusieurs return afin de traiter des cas simples au début de la fonction et ainsi simplifier la fonction. 

Dans tous les cas, la fonction termine son exécution après le return.

```java
public static int diviserPar2(int nombre, int nbFois) {
    if (nbFois == 0) { // ça ne sert a rien de faire la boucle dans le cas de 0
        return nombre;
    }
    
    for (int i = 0; i < nbFois; i++) {
        nombre = nombre / 2;
    }
    
    return nombre;
}
```

## Portée des variables

Lorsqu'une variable est définie, elle est visible seulement dans une partie du code. On nomme cela la **portée** de la variable. 

Un même nom de variable peut être utilisé à plusieurs endroits dans le code et ne pas représenter la même valeur. 

```java
public static void fct1() {
   int x = 1;
   fct2();
   System.out.println(x);  // affichera 1
   ...
}

public static void fct2() {
   int x = 2;
   ...
}
```

Ce code est tout à fait valide. Les deux variables x sont complètement indépendantes. Changer la valeur de l'une ne changera pas la valeur de l'autre. C'est pourquoi la valeur de x dans fct1 ne change pas  même si on appelle fct2 qui a son propre x. 

De la même façon, le nom d'un paramètre de fonction n'a aucun lien avec les variables dans la fonction appelante. 

```java
public static void fct3() {
  int x = 1;
  fct4(x);
  System.out.println(x); // affichera 1
}

public static void fct4(int x) {
  x = 2;
  ...
}
```

Lors de l'appel de la fct4, c'est la valeur 1 qui est envoyée, et non pas x (passage par valeur vs par référence) (Nous verrons que ce n'est pas toujours le cas, mais pour l'instant nous passons la valeur)

Il est courant que le nom de la variable que nous envoyons à la fonction et le nom de l'argument qui la reçoit soient les mêmes. Par exemple, nous aurons une fonction qui demande le **nom** de la personne, l'entrepose dans une variable appelée `nom` et qui envoie cette variable à une fonction qui fait quelque chose avec ce **nom**. Cette fonction reçoit un **nom**, donc l'argument sera appelé `nom`. Mais rappelez-vous que ces deux variables ne sont pas la même chose. Il y a une **copie de la valeur** qui se fait automatiquement. 

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        String nom = "";
        Console.afficher("Quel est votre nom ?");
        nom = Console.demanderTexte(); 
        faireQqchoseAvecLeNom(nom); // [!code highlight]

    }

    private static void faireQqchoseAvecLeNom(String nom) { // [!code highlight]
        //... fait quelque chose avec l'argument nom. 
    }
}
```

## Documentation d'une fonction

Quand vous définissez une fonction, vous devez **absolument** la documenter ainsi :

```java
/**
 * Donne la somme de 2 entiers (décrire le pourquoi de la fonction)
 * 
 * @param a Premier entier à additionner (description du premier paramètre)
 * @param b Deuxième entier à additionner (description du 2e paramètre)
 * @return La somme de a et b (description de ce qui est retourné)
 */
public static int additionner(int a, int b) {
    return a + b;
}
```

## Limites d'une fonction

Afin de garder vos fonctions simples et faciles à comprendre, vous devriez respecter ces limites :

* 3 paramètres au maximum
* 10 lignes dans le bloc de codes maximum
  * Si vous devez en faire plus, regroupez vos instructions et faites une autre fonction
  * [7 ± 2 items dans notre mémoire de travail](https://en.wikipedia.org/wiki/The_Magical_Number_Seven,_Plus_or_Minus_Two)
* Une fonction doit faire 1 et 1 seule chose
  * cette "chose" peut être complexe et devrait être découpée en sous-fonction
  * si, pour expliquer la fonction, vous devez faire une phrase avec plusieurs ET/OU, alors votre fonction est peut-être trop complexe. 

:::tip
Ces limites ne sont pas des lois. Commencez par écrire votre fonction, et ensuite pensez à l'optimiser. Plus vous aurez de l'expérience, plus ca sera un réflexe. 
:::

## Exercice 1

* Écrivez un programme qui demande 2 entiers à l'usager. 
* Envoyez le premier des 2 entiers à une fonction qui affichera la racine carrée de ce nombre. 
* Envoyez les deux entiers à une fonction qui retournera le premier élevé à la puissance du deuxième. Affichez le résultat dans la fonction main(). 

Commencez par écrire l'algorithme avec Flowgorithm

:::details Solution algorithmique
Principale
![apowb](/apowb.png)

CalculerRacineCarre
![calculerRacineCarre](/calculerRacineCarre.png)

CalculerAPowB
![calculerApowB](/calculerApowB.png)

:::

:::details Solution en Java

```java
package ca.cegepdrummond;
public class Programme {
    public static void main(String[] args) {
        int a,b;
        Console.afficher("premier nombre entier: ");
        a= Console.demanderEntier();
        Console.afficher("deuxième nombre entier: ");
        b= Console.demanderEntier();
        calculerRacineCarree(a);
        double resultat = calculerApowB(a,b);
        Console.afficher(""+a+" à la puissance "+b+" => "+resultat);
    }
    public static void calculerRacineCarree(int aNombre1) {
        Console.afficher("la racine carrée de "+aNombre1+ " est "+Math.sqrt(aNombre1));
    }
    public static double calculerApowB(int aNombre1, int aNombre2) {
        return Math.pow(aNombre1,aNombre2);
    }
}

```

Notez que **calculerApowB()** est de type double car Math.pow() retourne un double en Java, alors que cette fonction retourne un entier dans Flowgorithm.

:::


## Exercice 2

* Écrivez un programme qui demande un montant à l'usager
* Envoyez ce montant à une fonction qui retourne la tps pour ce montant.
* Envoyez ce montant à une fonction qui retourne la tvq pour ce montant. 
* Affichez le montant, suivit de la tps, suivit de la tvq, suivit du total montant+tps+tvq. 

Pour savoir comment calculer la tps et tvq: 

https://www.calculconversion.com/calcul-taxes-tps-tvq.html

:::details
```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
       double montant;
       Console.afficher("Quel est le montant à payer ?");
       montant = Console.demanderNombreAVirgule();
       double tps = calculerTPS(montant);
       double tvq = calculerTVQ(montant);
       Console.afficher("Montant facturé : "+montant);
       Console.afficher("Montant TPS : "+tps);
       Console.afficher("Montant TVQ : "+tvq);
       Console.afficher("Total : "+(montant+tps+tvq));

    }

    public static double calculerTPS(double montant) {
        return montant * 5 /100;
    }

    public static double calculerTVQ(double montant) {
        return montant * 9.975 / 100;
    }
}
```

:::