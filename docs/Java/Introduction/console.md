# La classe `Console`

La classe **Console** fait partie de la librairie de base pour le cours que vous avez installé plus tôt.

La "console" est le nom que l'on donne à l'interface textuelle. 

Elle permet de simplifier l'utilisation de la console  et de mieux vous concentrer sur la logique de votre code.


Par exemple :

```java
// avec Console
Console.afficher("Bonjour!");

// sans Console
System.out.println("Bonjour!");
```

```java
//avec Console
Console.afficher("Quel est votre nom ?");
String nom = Console.demanderTexte();
Console.afficher("Votre nom est " + nom);

// sans Console
Scanner scanner = new Scanner(System.in);
System.out.println("Quel est votre nom ?");
String nom = scanner.nextLine();
System.out.println("Votre nom est " + nom);
```

:::warning ATTENTION : Vous devez utiliser la bonne classe Console

Lorsque vous inscrivez le mot Console, faites attention à ce que suggère Intellij.


Vous devez choisir la console de ca.cegepdrummond

![img](/BonneConsole.png)

Si par erreur vous avez choisi celle de java.io, vous obtiendrez un include supplémentaire 

![img](/MauvaiseConsole.png)

Si c'est le cas, vous n'avez qu'à reculer votre curseur sur le mot Console, effacer le dernier `e` , et le réécrire, mais cette fois choisir ca.cegepdrummond. 

:::

## Fonctionnalités de Console :

```java
// Afficher du texte, un nombre entier, un nombre à virgule, un booléen ou un caractère
Console.afficher("Allo");
Console.afficher(2);
Console.afficher(2.0);
Console.afficher(true);
Console.afficher('a');

// Pour demander du texte à l'utilisateur
String texte = Console.demanderTexte();

// Pour demander un nombre entier à l'utilisateur
int a = Console.demanderEntier();

// Pour demander un nombre à virgule à l'utilisateur
double s = Console.demanderNombreAVirgule();

// Pour demander oui ou non à l'utilisateur
boolean oui = Console.demanderOuiNon();

// Pour afficher le texte "Appuyez sur une touche pour continuer..." et d'attendre que l'utilisateur appuie sur "ENTER" pour continuer le code.
Console.appuyezSurUneTouchePourContinuer();
```

En classe, nous ferons plusieurs exemples concrets de l'utilisation de la console.

Voici quelques exemples:

```java
int a = 10;
String nomDesProfs = "Benoit et Jonathan";
Console.afficher("La valeur de a : " + a);
Console.afficher("Vos professeurs : " + nomDesProfs);

Console.afficher("Entez une nouvelle valeur pour a : ");
a = Console.demanderEntier();
Console.afficher("La nouvelle valeur de a : " + a);


``` 

Comme vous pouvez le voir, il est possible de concaténer (mettre à la suite) du texte et une variable en utilisation l'opérateur **+**. 

## Exercice

* Écrivez un programme qui demande d'inscrire votre age et qui l'affiche.
* Utilisez Console. 
* L'age est un nombre entier. 

Vous devez donc créer un nouveau projet dans Intellij, et utiliser la template créée dans le premier cours afin d'intégrer Console dans le projet. 

:::details Solution

Dans Programme.java:

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("Inscrire votre age: ");
        int age = Console.demanderEntier();
        Console.afficher("Vous avez entré : " + age);
    }
}
```

:::