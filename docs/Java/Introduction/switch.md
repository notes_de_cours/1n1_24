# Switch

Lorsque nous devons vérifier plusieurs fois la même valeur comme suit :

```java
char reponse = 'a';
if (reponse == 'a') {
    System.out.println("Bonne réponse!!!");
} else if (reponse == 'b') {
    System.out.println("Presque la bonne réponse!");
} else {
    System.out.println("Vraiment pas la bonne réponse...");
}
```

On peut utiliser l'instruction `switch` comme suit :

```java
char reponse = 'a';
switch (reponse) {
    case 'a':
        System.out.println("Bonne réponse!!!");
        break;
    case 'b':
        System.out.println("Presque la bonne réponse!");
        break;
    default:
        System.out.println("Vraiment pas la bonne réponse...");
}
```

Description des éléments :

* variable entre parenthèses du switch
  * Valeur à tester
* case x:
  * Si la valeur est égale à x, fait cela
* break
  * Fin du bloc de codes (sinon exécute les autres cases)
* default
  * Si aucun cas n'est valide, exécute default


:::danger Avertissement 
L'instruction switch case est le seul endroit ou le break est accepté. Si vous l'utilisez ailleurs, vous perdrez des points. 
:::


## Exercice 1

* Créez un programme pour gérer un menu de restaurant.
* Vous ne devez PAS utiliser la classe Menu qui vous a été fournie en début de session.
* Affichez le menu suivant:
```
"Menu du resto chez Benou"
"1) menu du déjeuner"   
"2) menu du midi"     
"3) menu du souper"    
"4) payer"       
"0) sortir"
```

Pour le menu du déjeuner, affichez les choix suivants:
```
"1) déjeuner 1 ($8.75)"
"2) déjeuner 2 ($9.00)"
"0) sortir"  
```

Pour le menu du midi:

```
"1) diner 1 ($18.75)"
"2) diner 2 ($16.00)"
"0) sortir"
```

Pour le menu du souper:
```
"1) souper 1 ($25.99)"
"2) souper 2 ($39.00)"
"0) sortir"
```


Vous devez gérer ces menus à l'aide de switch case. 

La gestion de chaque menu doit être dans une fonction (donc 1 fonctions par repas).

Il doit être possible de prendre plusieurs choix dans chacun des menus. Le total doit être calculé, et retourné au menu principal afin de payer.
Ex: dans le menu déjeuner, je prends le déjeuner 1  et 2 déjeuner 2 pour un total de 8.75$ + 2*9$ = 26.75$

Lors du paiement, affichez le montant à payer, et remettre le total à zéro.

Si l'usager fait un choix invalide, vous devez lui indiquer (default).



:::details

```java:line-numbers
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        int lChoix = 0;
        double lPrix = 0;
      do{
          Console.afficher("Menu du resto chez Benou");
          Console.afficher("1) menu du déjeuner");
          Console.afficher("2) menu du midi");
          Console.afficher("3) menu du souper");
          Console.afficher("4) payer");
          Console.afficher("0) sortir");
          lChoix = Console.demanderEntier();
          switch(lChoix) {
              case 0:
                  break;
              case 1:
                  lPrix += menuDejeuner();
                  break;
              case 2:
                  lPrix += menuDiner();
                  break;
              case 3:
                  lPrix += menuSouper();
                  break;
              case 4:
                  payer(lPrix);
                  lPrix= 0;
                  break;
              default:
                  Console.afficher("faites un choix entre 1, 2, et 3");
                  break;
          }
      } while(lChoix != 0);
    }

    public static double menuDejeuner() {
        int lChoix = 0;
        double lPrix = 0;
        do {
            Console.afficher("1) déjeuner 1 ($8.75)");
            Console.afficher("2) déjeuner 2 ($9.00)");
            Console.afficher("0) sortir");
            lChoix = Console.demanderEntier();
            switch(lChoix) {
                case 0:
                    break;
                case 1:
                    lPrix+=8.75;
                    break;
                case 2:
                    lPrix+=9.00;
                    break;
                default:
                    Console.afficher("faites un choix entre 1 et 2 ");
                    break;
            }
        } while (lChoix != 0);
        return lPrix;

    }

    public static double menuDiner() {
        int lChoix = 0;
        double lPrix = 0;
        do {
            Console.afficher("1) diner 1 ($18.75)");
            Console.afficher("2) diner 2 ($16.00)");
            Console.afficher("0) sortir");
            lChoix = Console.demanderEntier();
            switch(lChoix) {
                case 0:
                    break;
                case 1:
                    lPrix+=8.75;  // <<<<<<<<<  notez l'erreur ici ... vive le copier coller // [!code highlight]
                    break;
                case 2:
                    lPrix+=9.00;  // <<<<<<<<<  notez l'erreur ici ... vive le copier coller // [!code highlight]
                    break;
                default:
                    Console.afficher("faites un choix entre 1 et 2 ");
                    break;
            }
        } while (lChoix != 0);
        return lPrix;
    }

    public static double menuSouper() {
        int lChoix = 0;
        double lPrix = 0;
        do {
            Console.afficher("1) souper 1 ($25.99)");
            Console.afficher("2) souper 2 ($39.00)");
            Console.afficher("0) sortir");
            lChoix = Console.demanderEntier();
            switch(lChoix) {
                case 0:
                    break;
                case 1:
                    lPrix+=25.99;
                    break;
                case 2:
                    lPrix+=39;
                    break;
                default:
                    Console.afficher("faites un choix entre 1 et 2 ");
                    break;
            }
        } while (lChoix != 0);
        return lPrix;
    }

    public static void payer(double aPrix) {
        Console.afficher("Votre total est de "+aPrix);
        Console.afficher("merci!");
    }

}


```

:::


   
      




## Correction de l'erreur du menu midi 

À la ligne 76 et 79, vous pouvez voir une erreur de copier/coller. Je n'ai pas changé le montant lorsque j'ai copié les chiffres du matin vers le midi. Ce genre d'erreur est difficile à voir. 

Vive les nombres magiques!!!  Le fait que j'ai utilisé directement les montants à plusieurs endroits dans le code fait en sorte que je dois faire attention à la signification de ces nombres. 

Une correction possible de ce code serait d'utiliser des constantes:

```java
public static double menuDiner() {
        final double DINER1 = 18.75; // [!code highlight]
        final double DINER2 = 16.00; // [!code highlight]
        int lChoix = 0;
        double lPrix = 0;
        do {
            Console.afficher("1) diner 1 ("+DINER1+")");// [!code highlight]
            Console.afficher("2) diner 2 ("+DINER2+")");// [!code highlight]
            Console.afficher("0) sortir");
            lChoix = Console.demanderEntier();
            switch(lChoix) {
                case 0:
                    break;
                case 1:
                    lPrix+=DINER1;// [!code highlight]
                    break;
                case 2:
                    lPrix+=DINER2;// [!code highlight]
                    break;
                default:
                    Console.afficher("faites un choix entre 1 et 2 ");
                    break;
            }
        } while (lChoix != 0);
        return lPrix;
    }
```

De cette façon , si je veux changer un prix, je n'ai qu'à le changer à une seule place. 


## Format de nombre

Si vous essayez ce code, vous remarquerez que le 16.00 s'affiche 16.0. 

Pour régler ce problème il faut formater le nombre avec le formateur de la classe String. 



Vous devez donc utiliser une syntaxe comme celle-ci:

```java
Console.afficher("2) diner 2 ("+String.format(Locale.US,"%,.2f",DINER2)+")");
```

Le formatage de nombre est souvent compliqué en programmation. Je ne vous demanderai pas de le faire dans le cadre de ce cours. 

Pour en savoir plus, googler `java String format`

Un site donnant quelques exemples *simples* 

https://mkyong.com/java/how-to-format-a-double-in-java/


## Amélioration du code

La solution proposée comprends 3 fonctions qui se resemblent beaucoup. 

Je vous propose ici une solution qui élimine ces fonctions et les remplace par l'utilisation de tableau pour l'affichage du texte des menus ainsi que des prix. 

Bien que cette solution soit un peu plus complexe d'un point de vue algorithmique, elle est beaucoup plus flexible. En effet, l'ajout d'une option à un menu se fait très facilement en ajoutant tout simplement une entrée dans les 2 tableaux. 
De plus, tout changement de format pour l'affichage pourra se faire à un seul endroit. 

Vous n'avez pas à produire ce genre de code dans ce cours, mais si vous vous sentez capable, n'hésitez pas à le faire car ca vous sera demandé dans les futurs cours.  

:::details

```java
package ca.cegepdrummond;

import java.util.Arrays;
import java.util.Locale;

public class Programme {
    public static void main(String[] args) {
        int choix;
        double prixTotal = 0;
        do {
            Console.afficher("Menu du resto chez Benou");
            Console.afficher("1) menu du déjeuner");
            Console.afficher("2) menu du midi");
            Console.afficher("3) menu du souper");
            Console.afficher("4) payer");
            Console.afficher("0) sortir");
            choix = Console.demanderEntier();
            switch (choix) {
                case 0:
                    break;
                case 1:
                    prixTotal = prixTotal + dejeuner();
                    break;
                case 2:
                    prixTotal += diner();
                    break;
                case 3:
                    prixTotal += souper();
                    break;
                case 4:
                    payer(prixTotal);
                    prixTotal = 0;
                    break;
                default:
                    Console.afficher("Entrez une valeur entre 1 et 4");
                    break;
            }
        } while (choix != 0);
    }

    public static void payer(double aPrix) {
        Console.afficher("Le prix total est de "+aPrix);

    }
    public static double dejeuner() {
        String[] lMenu = new String[2];
        lMenu[0] = "déjeuner 1";
        lMenu[1] ="déjeuner 2";
        double[] lPrix = new double[2];
        lPrix[0] =8.75;
        lPrix[1] = 9;
        return afficherMenu(lMenu, lPrix);
    }

    public static double diner() {
        String[] lMenu = new String[2];
        lMenu[0] = "diner 1";
        lMenu[1] ="diner 2";
        double[] lPrix = new double[2];
        lPrix[0] = 18.75;
        lPrix[1] = 16;
        return afficherMenu(lMenu, lPrix);
    }

    public static double souper() {
        String[] lMenu = new String[2];
        lMenu[0] = "souper 1";
        lMenu[1] ="souper 2";
        double[] lPrix = new double[2];
        lPrix[0] = 25.99;
        lPrix[1] = 39;
        return afficherMenu(lMenu, lPrix);
    }

    public static double afficherMenu(String[] aLesMenus, double[] aLesPrix) {
        double lPrix = 0;
        int lChoix = 0;
        do {
            for(int i = 0; i < aLesMenus.length; i++) {
                Console.afficher(i+1+") "+aLesMenus[i]+" ($"+aLesPrix[i]+")");
            }
            Console.afficher("0) Sortir");
            lChoix = Console.demanderEntier();
            if(lChoix>aLesMenu.length || lChoix < 0) {
                    Console.afficher("Valeur entre 0 et "+aLesMenu.length);
                } else {
                    lPrix = lPrix + aLesPrix[lChoix-1];
                }
        } while (lChoix !=0);
        return lPrix;
    }
}
```

:::

## Erreur!!!

Avec le code précédent, essayer de prendre un déjeuner 1 suivit d'un souper 1, et payer ... que remarquez vous ?

À quoi est dû cette erreur?

:::details

C'est une erreur d'arrondissement. 

L'utilisation de nombre à virgule n'est pas recommandée lorsqu'il est question de manipuler des montants d'argents. Il est recommandé de prendre des entiers et de les diviser par 100 lors de l'affichage. Vous devriez voir d'oû provient cette erreur de calcul dans votre cours de math. 
:::