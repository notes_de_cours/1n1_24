# Les types primitifs vs les Objets

Nous venons de voir le type `String` et le type `char`. 

Comme vous pouvez le remarquer, c'est **String** avec un `S` majuscule et **char** avec un `c` minuscule. Ce standard de nommage permet de différencier les objets des types primitifs. 

Un type primitifs est une valeur qui est représentée directement dans la mémoire de l'ordinateur. Nous avons vu plusieurs types numérique (int, float) ainsi que le type char (qui est aussi considéré comme un type numérique car en réalité, c'est un entier qui est entreposé afin de représenter le caractère). 

Un objet est une structure plus complexe en mémoire.  Nous avons vu le type String qui représente une série de char. Les objets sont souvent un regroupement de plusieurs éléments composés de valeurs de type primitif, ou d'autres objets. Vous verrez plus en détails en deuxième session comment créer et manipuler des objets.

Vous verrez souvent le mot `classe` utilisé pour parler d'un objet. En réalité, un objet est une `instance` d'une classe. Une classe est un générateur d'objet. (je suis un instance de la classe humain) 

Pour ce cours, nous manipulerons surtout le type String. 

Une différence majeure entre l'utilisation d'une variable de type primitif et un objet est la façon dont les fonctions sont appliquées. 

## Type primitif

Nous avons vu que pour le type primitif char, la définition se fait comme ceci: 

```
    char c;
```

Qu'on lui assigne une valeur comme cela:

```
    c = 'a';
```

et qu'ensuite on utilise cette variable comme argument d'une fonction:

```
    char r = Character.toUpperCase(c);
```

## Objet

Pour un objet, vous avez peut-être remarqué que c'est un peu différent.

La définition de la variable est semblable:

```
    String s;
```

L'assignation d'une valeur est aussi semblable:

```
    s = "allo"
```

Mais son utilisation diffère. Au lieu de passer la variable en argument à la fonction, on envoie un message à l'objet en ajoutant un point "." entre la variable et le message (nom fancy pour dire une fonction)

```
    String r = s.toUpperCase();
```


Cette différence dans la syntaxe vous mélangera surement au début... mais vous vous y ferez et saurez laquelle utiliser selon le type de la variable. 

## Console
Nous avons aussi utilisé le type d'objet Console. 

Avez-vous remarqué qu'on ne déclare jamais de variable de type Console, mais qu'on appel les fonctions directement avec Console?

Pour l'instant nous n'irons pas en détails sur cette "bizarrerie", mais ne vous en faites pas, vous comprendrez la nuances en deuxième session. 