# Introduction à Java

## Qu'est-ce que la programmation ?

### Définitions

Programmation Informatique

> Ensemble des **activités techniques** reliées à l'élaboration d'un **programme informatique**.

[GDT](https://vitrinelinguistique.oqlf.gouv.qc.ca/)

Programme informatique

> **Suite d'instructions** écrites sous une forme que l'**ordinateur peut comprendre** pour traiter un **problème** ou pour effectuer une **tâche**.

[GDT]

Langage

> Ensemble organisé de symboles, de **mots-clés**, de caractères et de **règles** (instructions et syntaxe) utilisé pour adresser des **commandes** à **l'ordinateur** et assurer la communication avec la machine.

[GDT]

Compilation

> Action de **traduire automatiquement**, à l'aide d'un compilateur, un programme écrit en **langage de haut niveau** en un programme équivalent en **langage machine**. 

[GDT]



> NOTE :
>
> Un programmeur écrit des instructions SIMPLES et PRÉCISES dans un langage restreint en respectant des règles précises dans le but de résoudre un problème grâce à un programme informatique.
>
> Exemple
>
> * Votre ordinateur est votre calculatrice
> * Vous devez résoudre 1+1
> * Qu'allez-vous faire ?
> * Vous allez écrire 1 + 1 =
> * Et la calculatrice vous répondra 2
> * Vous avez écrit dans le langage d'une calculatrice des instructions simples et précises dans un langage restreint en respectant des règles précises
> * Vous n'auriez pas pu écrire + 1 1 = ou bien 1 plus 1 égal
>
> Il est important de respecter les règles et la syntaxe


Que ce passe-t-il quand on écrit un programme? (version super simplifiée)

* le code est dans l'éditeur (l'éditeur est lui aussi un programme)
* il est "compilé" (le compilateur est lui aussi un programme)
* le code compilé prend de l'espace en mémoire RAM (que vous verrez dans le cours d'init à l'ordi)
* le processeur exécute les instructions (pas tout à fait vrai pour Java, mais gardons cela simple)
* de la mémoire est utilisée pour les variables. 

## Types d'instructions

Tout ce qu'un ordinateur peut faire se résume en trois types d'opérations :

1. assignation: mettre une valeur dans l'ordinateur
2. comparaison: comparer cette valeur avec une autre valeur
3. branchement conditionnel: selon le résultat de la comparaison, sauter ou non à un autre endroit dans le code. 

Vous verrez plus en détails ce que cela représente lorsque vous suivrez un cours d'assembleur qui est le langage que comprend réellement l'ordinateur. 

Dans les langages de plus haut niveau tel que Java, ces mêmes trois opérations peuvent être converties en 4 types que nous étudierons durant la session:

1. assignation: mettre une valeur dans une variable
2. comparaison: comparer une variable avec une valeur et exécuter ou non une séquence d'opération (IF)
3. boucle: faire une séquence d'opération tant qu'une condition est vrai (FOR, ou WHILE)
4. appel de fonction: saut à un autre endroit dans le code.

## Langage JAVA

Il existe énormément de langage de programmation.

Dans la technique, vous allez en voir plusieurs :

* Java
* HTML
* CSS
* Javascript  <<<< ne pas mélanger avec Java
* Typescript
* C#
* SQL
* XAML
* ...

Chacun de ces langages a ses forces et ses faiblesses

Dans ce cours, nous allons voir JAVA

Langage de programmation inventé par **James Gosling en 1991**.

Développé par la compagnie **Sun Microsystems** qui s’est fait **racheté par Oracle Corporation en 2009**.

Java est un langage surtout utilisé pour :

* les **applications de bureau** (sur votre ordinateur)
* pour les applications **Android**.

C’est un des langages :

* les plus populaires
* les plus documentés
* ayant une énorme communauté qui s’est formé à travers les années
* qui utilise la programmation orientée objet
* multi-plateforme

## Structure du Hello World en Java

Nous avons déjà fait notre premier programme en Java: Hello World. 

Le voici plus en détails.

```java
public class Programme {
    public static void main(String[] args) {
        Console.afficher("Hello World !");
    }
}
```

`public static void main(String[] args) {}`

* Point d'entrée de l'application
* Lorsque le programme est exécuté, il cherche la fonction `main`
* Une fonction regroupe une suite d'instructions
* Vous devez l'écrire exactement comme cela
* Java est sensible à la casse (majuscule et minuscule). Il faut donc y faire attention. 

`public class Programme`

* Une classe regroupe un ensemble de fonctions
* On doit absolument mettre la fonction `main` dans une classe
* Nous y reviendrons

`Console.afficher()`

* Permet d'afficher du texte dans la console
* Nous y reviendrons

`"Hello World !"`

* Chaîne de caractères à afficher
* Notez les guillemets
* Nous y reviendrons

`;`

* Note la fin d'une instruction
* Chaque instruction en Java doit se terminer par un `;`
* Notez que les autres lignes ne sont pas des instructions
* Une instruction se termine soit par un ;  soit par } . S'il y a une }, alors pas besoin de ; 



