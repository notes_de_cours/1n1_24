# Paramètre d'une fonction en référence vs par valeur

Lorsque vous envoyez une variable de type primitif ou une String à une fonction, la **valeur** de cette variable est envoyée à la fonction. Ce qui fait en sorte que si vous modifiez la variable à l'intérieur de la fonction, celle-ci ne sera pas modifiée dans l'appelant.

```java
public class Programme {
    public static void main(String[] args) {
        int a = 2;
        incremente(a);
        System.out.println(a); // 2
    }
    
    public static void incremente(int n) {
        n = n + 1;
    }
}
```

Par contre, si vous envoyez autre chose qu'une variable de type primitif (c-à-d un objet ou un tableau), celle-ci sera envoyée par **référence** (son adresse en mémoire). Ce qui signifie que si vous modifiez la variable à l'intérieur de la fonction, celle-ci sera **aussi** modifiée dans l'appelant.

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        int[] a = {2};
        incremente(a);
        System.out.println(Arrays.toString(a)); // [3]
    }

    public static void incremente(int[] n) {
        n[0] = n[0] + 1;
    }
}
```
:::info   
Nous allons voir Arrays dans la prochaine leçon
:::

Pour régler ce problème, la solution ici aussi est de cloner le tableau. 

```java
package ca.cegepdrummond;

import java.util.Arrays;

public class Programme {
    public static void main(String[] args) {
        int[] a = {2};
        incremente(a.clone); // [!code highlight]
        System.out.println(Arrays.toString(a)); // [2]
    }

    public static void incremente(int[] n) {
        n[0] = n[0] + 1;
    }
}
```
## Exercice

* Écrire un programme qui demande un certains nombres de valeurs positives à l'usager et les met dans un tableau (vous venez de faire un programme qui fait l'entrée de nombre et les met dans un tableau, réutilisez une partie de ce code )
* Envoyez ce tableau  à une fonction.
* Dans un premier temps, cette fonction doit faire une boucle qui remplace chaque valeur du  tableau par celle-ci multipliée par 2.
* Dans un deuxième temps, la fonction affiche le résultat de la multiplication par 2 que vous venez de faire (utiliser une boucle séparée de celle qui sert à la multiplication)
* De retour dans la fonction main(), envoyez le tableau à une fonction qui trouve le plus grand et qui l'affiche (vous venez de faire un exercice qui trouvait la valeur la plus grande, vous pouvez réutiliser une partie de ce code). 

:::details

```java:line-numbers
package ca.cegepdrummond;
public class Programme {
    public static void main(String[] args) {
        int longueur;
        int tableau[];
        Console.afficher("Combien d'éléments ?");
        longueur = Console.demanderEntier();
        tableau = new int[longueur];
        for(int i=0; i<longueur;i++) {
            boolean bon = false;
            while(!bon) {
                Console.afficher("Quelle est la valeur de l'élement " + i);
                int nombre = Console.demanderEntier();
                if (nombre >= 0) {
                    bon = true;
                    tableau[i] = nombre;
                }
            }
        }
        multiplierPar2(tableau);
        trouverPlusGrand(tableau);
    }

    public  static void multiplierPar2(int[] tableau) {
        for(int i = 0; i<tableau.length; i++) {
            tableau[i] *= 2;
        }
        for(int nombre : tableau) {
            Console.afficher(nombre);
        }
    }

    public static void trouverPlusGrand(int[]tableau) {
        int plusGrand = tableau[0]; // ca prend une valeur de départ, on présume que le premier est le plus petit.
        for(int i = 0; i< tableau.length; i++) {
            if(tableau[i]>plusGrand) {
                plusGrand = tableau[i];
            }
        }
        Console.afficher("Le plus grand des "+tableau.length+" nombres demandés est : "+plusGrand);
    }
}

```

:::

Que remarquez-vous à propos de la valeur la plus grande ?

:::details
c'est la plus grande valeur multipliée par 2
:::

Pourquoi?

:::details
Le tableau est passé par référence. Toute modification faite à ses valeurs affectera aussi les valeurs dans le tableau original. (à vrai dire, c'est le même tableau)
:::

Comment résoudre ce problème?

:::details
au lieu de passer le tableau à la première fonction, passer un clone. 
:::

Que signifie *= à la ligne 26?
:::details
Nous avons déjà vue `i++` qui est l'équivalent de `i = i+1`

`var *= 2` est l'équivalent de `var = var * 2`
:::