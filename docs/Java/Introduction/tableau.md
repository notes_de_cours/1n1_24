# Tableaux

## Qu'est-ce qu'un tableau ?

En programmation, un tableau permet de stocker une liste d'éléments du même type dans une seule variable. On peut ensuite accéder aux différents éléments grâce à un index.

Les Strings sont un tableau de caractères.

Voici une représentation d'un tableau d'entiers

| Index | Élément |
| ----- | ------- |
| 0     | 30      |
| 1     | 40      |
| 2     | 20      |
| 3     | 60      |
| ...   | ...     |

## Tableau en Java (Array)

En Java, on utilise `[ ]` (les crochets carrés) pour indiquer qu'on veut un tableau d'éléments d'un certain type.

On peut déclarer un tableau de la façon suivante :

```java
double[] resultatsExamen;
```

Donc, on spécifie le type des éléments du tableau suivi des `[]`.

:::info
Il est d'usage de donner un nom *pluriel* à une variable de type tableau. En effet, ce tableau contient **des** résultat**S** d'examen. 

Si vous avez un tableau qui contient des noms, alors le tableau s'appelera **noms**. 
:::

## Spécifier les éléments d'un tableau

Pour spécifier les éléments d'un tableau, on peut les définir d'un seul coup.

```java
resultatsExamen = new double[] {75.5, 99.0, 80.0};
```
ou

```java
double[] resultatsExamen = {75.5, 99.0, 80.0};
```


On peut aussi instancier le tableau et le remplir élément par élément par la suite.

```java
resultatsExamen = new double[3]; // équivalent de {0, 0, 0} étant la valeur par défaut du int
resultatsExamen[0] = 75.5;
resultatsExamen[1] = 99.0;
resultatsExamen[2] = 80.0;
```

:::warning
Il n'est pas possible de supprimer ou d'ajouter un élément dans un tableau en Java. Il faut donc refaire un autre tableau. (Nous verrons plus tard une alternative)
:::

:::warning
On peut toujours modifier un élément d'un tableau, mais on ne peut pas modifier le caractère d'une String de la même façon, puisque la String est immuable. Il faut utiliser les fonctionnalités de la classe String comme : replace(), insert(), etc.
:::

## Accéder aux éléments d'un tableau

Pour accéder aux éléments d'un tableau il suffit d'indiquer l'index de l'élément voulu entre `[]`. 

:::tip
Comme pour les Strings, l'index commence toujours à 0.
:::

```java
System.out.println("La première note est :" + resultatsExamen[0]); // 75.5
```

## Propriété d'un tableau

La seule propriété d'un tableau est `length` qui permet d'obtenir le nombre d'éléments dans le tableau.

:::tip
Remarquez qu'il n'y a pas de () après length, car c'est une propriété et non une méthode. 
:::

```java
int nbElements = resultatsExamen.length; // 3
```

## Itération dans un tableau

L'avantage d'un tableau, c'est qu'il est facile de le parcourir (itérer)

```java
System.out.println("Voici les résultats de l'examen:");
for (int i = 0; i < resultatsExamen.length; i++) {
    int resultatExamen = resultatsExamen[i]; // [!code highlight]
    System.out.println(resultatExamen + "%");
}
```

:::info
Notez ici qu'il est courant d'utiliser le nom du tableau au singulier pour le nom de la variable utilisée pour *extraire* une valeur de ce tableau.
:::

## Exercice 1

* Créez un programme qui demande un entier à l'usager. 
* Si l'usager entre un nombre plus petit que 1, vous devez le redemander.
* Ensuite créez un tableau d'entier ayant pour taille cet entier. 
* Faites une boucle pour la taille du tableau et qui demande un nombre entier positif et le met dans le tableau. Si le nombre entré est négatif, ne pas le mettre dans le tableau,  ne pas avancer dans le tableau, et redemander cet entier. 
* Une fois tous les nombres entrés, faites une boucle qui trouve le plus grand des nombres entrés dans le tableau.
* Affichez ce nombre. 

:::details Algorithme

![tableauplusgrand](/tableauplusgrand.png)

:::
:::details

```java
package ca.cegepdrummond;
public class Programme {
    public static void main(String[] args) {
        int longueur;
        int tableau[];
        do {
            Console.afficher("Combien d'éléments ?");
            longueur = Console.demanderEntier();
        } while (longueur < 1);

        tableau = new int[longueur];
        for(int i=0; i<longueur;i++) {
            int nombre;
            do  {
                Console.afficher("Quelle est la valeur de l'élement " + (i+1));
                nombre = Console.demanderEntier();
                if (nombre >= 0) {
                    tableau[i] = nombre;
                }
            } while( nombre < 0);
        }
        int plusGrand = tableau[0]; // ca prend une valeur de départ, on présume que le premier est le plus petit.
        for(int i = 0; i< longueur; i++) {
            if(tableau[i]>plusGrand) {
                plusGrand = tableau[i];
            }
        }
        Console.afficher("Le plus grand des "+longueur+" nombres demandés est : "+plusGrand);
    }
}

```

:::



## Assignation d'un tableau à un autre tableau

Si j'ai un int `a` et que je veux le mettre égal à un autre int `b`, je n'ai qu'à faire . 

```java
int a;
int b = 1;
a = b; // [!code highlight]
b = 42;
System.out.println(a) //affiche 1
```
Si je change `b`, la valeur de `a` ne change pas.


Pour un tableau, cette façon de faire est dangereuse.

:::tip
Vous comprendrez mieux la raison de ce problème dans le cours Native 1 lorsque vous manipulerez des pointeurs. 
:::

```java
int[] a;
int[] b = {1,2,3};
a = b;
b[0] = 42;
System.out.println(a[0]); // affiche 42 et non 1. // [!code error]
```

Pour régler ce problème, il faut **clone**" le tableau b. 

Le clone copie toutes les valeurs de `b` dans `a`. Si on change une valeur dans `b` cela n'affecte pas `a` 

```java
int[] a;
int[] b = {1,2,3};
a = b.clone(); // [!code highlight]
b[0] = 42;
System.out.println(a[0]); // affiche 1 comme on s'y attend.
```

Il y a d'autres façons de le faire: https://www.tutorialspoint.com/Array-Copy-in-Java

## Split et Join

La méthode `split()` de String permet de convertir une chaine de caractères (String) en tableau

```java

String texte = "allo comment ça va";
String[] morceaux = texte.split(" "); // morceaux => ["allo", "comment", "ça", "va"]
for(int i=0;i<morceaux.length; i++) {
    Console.afficher(morceaux[i]);
}

```

On peut spécifier n'importe quoi comme séparateur :

```java
String[] morceaux = texte.split("a"); ]

```

Pour faire l'inverse, on utilise `join()`

```java
String texte2 = String.join(" ", morceaux);
Console.afficher(texte2);
```




