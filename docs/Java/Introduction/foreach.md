# For each

Il est possible de parcourir un tableau avec une variante de la boucle `for` que l'on nomme `for each`

```java
//création
int[][] t = new int[3][4];

//assignation de valeurs
t[0][1] = 3;
t[2][3] = 1;

for(int[] ligne : t ) {
	for(int valeur : ligne ) {
		System.out.print(valeur);
	}
	System.out.println();
}
```

Cette syntaxe s'adapte très bien aux tableaux qui n'ont pas le même nombre d'éléments dans chaque ligne

```java
//création
int[][] t = {
	{0,3},
	{},
	{},
	{0,0,0,1}
};

for(int[] ligne : t ) {
	for(int valeur : ligne ) {
		System.out.print(valeur);
	}
	System.out.println();
}
```

Remarquez que le `foreach` est le même pour les deux exemples. (le `for` de l'exemple précédent aurait aussi été le même étant donné qu'on utilise `length`)


:::info
Comme indiqué précédemment, il est courant d'utiliser une variable ayant le nom du tableau au singulier afin d'entreposer un élément du tableau. 

Il est donc courant de voir:

```java
for(int resultatExamen : resultatsExamen) { ... }
```

Notez le singulier et le pluriel

:::