# Décomposition d'un problème

En programmation, le pseudo-code, également appelé LDA (pour Langage de Description d'Algorithmes) est une façon de décrire un algorithme en langage presque naturel, sans référence à un langage de programmation en particulier.

L'écriture en pseudo-code permet souvent de bien prendre toute la mesure de la difficulté de la mise en œuvre de l'algorithme, et de développer une démarche structurée dans la construction de celui-ci. En effet, son aspect descriptif permet de décrire avec plus ou moins de détail l'algorithme, permettant de ce fait de commencer par une vision très large et de passer outre temporairement certains aspects complexes, ce que n'offre pas la programmation directe. [wikipedia](https://fr.wikipedia.org/wiki/Pseudo-code)

## Décomposition du problème en sous-problèmes

- décomposer le problème en petits morceaux
  - premièrement, définir le "quoi", c.-à-d.: que doit faire le code:
    - On se base habituellement sur une spécification du problème. Dans la "vraie vie", cette spécification aura été définie conjointement avec le client. 
  - ensuite, définir le "comment", c.-à-d.: quel est le meilleur "algorithme" pour résoudre ce problème. 
- deux approches:
  - descendante (top-down):  partir du plus général vers le plus spécifique 
    - définir les grandes lignes et les raffiner
  - ascendante (bottom-up): partir des détails et les combiner pour résoudre le problème plus complexe. 
  - ici nous prendront l'approche descendante/top-down, mais libre à vous d'utiliser celle qui vous convient. 
- s'attaquer à une seule partie à la fois, ne pas essayer de résoudre tout le problème en même temps
- il est possible de se tromper dans notre décomposition. Ne pas hésiter à changer d'idée si on a une "meilleure" idée. Ça peut prendre plusieurs itérations avant d'avoir une solution qui a du sens. 

## Pseudo-code

- ÉCRIRE la solution en français: pas juste y penser... vraiment l'écrire afin de se libérer l'esprit et pouvoir peaufiner les détails.
  - évite de se heurter à la syntaxe du langage informatique. 
- ne pas penser trop aux détails: commencer par les grandes lignes et y revenir une fois qu'on a une bonne idée. 
- pour un système plus complexe, on s'attardera souvent aux parties difficiles à comprendre, ou pour lesquels nous n'avons pas d'expérience. 


### Exemple de pseudo-code

Le pseudo-code permet de décrire un processus. Pas nécessairement un processus informatique.

- Décrivez votre routine du matin. Commencez par les grandes lignes, pour ensuite les décrires plus en détails. 
- Exemple: 
  - Haut niveau: ai-je un cours ce matin ? Si oui: lequel? Si non: je me recouche. 
  - Plus de détails: pour ce cours, ai-je un travail à remettre? Si oui, l'ai-je fait?, Si non: no problemo. 
  
## Processus itératif

Trouver la solution à un problème n'est pas un processus linéaire. Il arrive souvent qu'on ait une idée qui pourrait peut-être potentiellement dans le meilleur des cas résoudre le problème. 

Il faut ensuite améliorer cette solution en évaluant tous les cas possibles. 

Voici quelques règles pour y arriver:

- trouver une solution qui fonctionne, et ensuite la raffiner
- Penser à la logique en premier, et créer les variables nécessaires au fur et à mesure. 
  - ne pas penser à optimiser le code en partant
    - qu'est-ce que du code optimal ?  Optimal en rapport à la vitesse, à la mémoire?  Le plus important c'est que le code fonctionne, soit clair, simple, bien écrit, bien documenté, et surtout facile à lire (on écrit le code une fois, on le lit et devons le comprendre plusieurs fois). 
- Résoudre une partie du problème, et ensuite l'intégrer dans la partie du niveau supérieur et voir si cette solution s'intègre. 
  - il arrive souvent que la solution ne s'intègre pas tout à fait. Il faut alors la modifier.
  - est-ce la nouvelle partie qui doit être modifiée, ou le code déjà existant? ça dépend 
    - ex.: on trouve une solution qui retourne un double, mais la solution du niveau supérieur s'attend à avoir un entier. 
      - doit-on arrondir le double pour avoir un entier
      - ou modifier le reste du code pour utiliser un double 
      - ça dépend... 
- plus le problème est complexe, plus il y aura de possibilités de raffinement en trouvant des "bouts de code" qui sont communs et qui pourraient être mis dans des fonctions.



## Exercice 1 Rechercher le plus grand nombre dans un tableau

Voici le problème que vous devez résoudre: on vous donne un tableau de nombre et vous devez trouver la position du nombre ayant la valeur la plus élevée. 

Décrivez comment vous vous y prendriez pour résoudre ce problème. 

:::details Une solution 
- Le tableau est dans la variable T  
- La plus grande valeur sera mise dans la variable PG initialisée à 0 (zéro)  
- La position de la plus grande valeur sera mise dans la variable PPG initialisée à 0 (zéro)  
- Itérer sur toutes les valeurs de T. Mettre la position du tableau dans P  
  - Si la valeur de T à la position P est plus grande que PG, mettre cette valeur dans PG
  et mettre la valeur de P dans PPG

Afficher la valeur de PPG et PG
:::

Cette solution fonctionne ... sauf s'il n'y a que des nombres négatifs dans le tableau, ou si le tableau est vide. 

De plus, si la même valeur se retrouve plusieurs fois dans le tableau, cet algorithme retourne la position de la dernière. 

Il faut souvent se poser des questions sur les données à traiter, ou sur les détails du problème à résoudre. Savoir poser des questions est une des grandes qualités d'un programmeur!

Si nous reprenons le premier problème (Il n'y a QUE des nombres négatifs dans le tableau) comment le résoudre? 

Cette solution ne fonctionne pas à cause du fait que PG est initialisé à 0. Cette valeur sera toujours plus grande que les valeurs négatives du tableau. Le programme indiquera donc que c'est la valeur 0 à la position 0 qui est la plus grande, alors qu'elle n'est même pas dans le tableau. 

Il y a deux solutions: 
1) trouver un nombre qui est toujours le plus petit. En Java il existe un tel nombre MIN_VALUE. Mais ce n'est pas vrai pour tous les langages. 
2) aller chercher la première valeur (T[0]) du tableau pour initialiser PG. De cette facon, soit que T[0] est la plus grande valeur, soit qu'une autre valeur sera plus grande. 

Si nous reprenons le deuxième problème (plusieurs fois la même valeur), comment le résoudre?

Dans ce cas, il n'y a qu'une solution: demander à l'usager. 

En effet, ce n'est pas au programmeur de prendre ce genre de décision. C'est à l'usage de nous dire s'il désire la première position, la dernière position, toutes les positions. 

Il peut y avoir plusieurs solutions à un problème. Voici une autre solution:

:::details Une autre solution
- Le tableau est dans la variable T  
- Utiliser la fonctionnalité de Arrays pour trier un tableau. 
- La plus grande valeur est dans T[longueur de T -1]

... avec de l'expérience, d'autres solutions apparaissent.

:::danger Attention
Cette solution, bien que plus *simple* à écrire, requiert beaucoup plus de calculs. En effet, trier un tableau est une opération assez couteuse en terme de nombres de calculs. La première solution est baucoup plus efficace pour régler ce problème. 
:::

## Exercice 2 Trier un tableau

Voici le problème: j'ai un tableau de nombre positif, et je dois les trier. 

Vous n'avez pas vue les algorithmes de tri encore, mais imaginez comment cela pourrait se faire. 

:::details Une solution

- Le tableau est dans T
- (1) Parcourir le tableau T du début à la fin-1 . L'indice de la position est P
  - Si T[P] est plus grand que T[P+1], les échanger et noter qu'il a eu un échange
- S'il y a eu un échange retourner à l'étape (1)
- Sinon, le tableau est trié. 

Cet algorithme très simple s'appel un tri à bulles. Il n'est pas très efficace. 

Il y a beaucoup d'[autres solutions](https://fr.wikipedia.org/wiki/Algorithme_de_tri) à ce problème
:::

## Refactoring

- une fois qu'on a du code fonctionnel et qu'on a démontré qu'il y a une solution (proof of concept), alors on peut penser à améliorer le code
- pour aider, il y a des options pour renommer une fonction ou une variable ou les paramètres dans intellij. 
- des techniques plus avancées peuvent aider au refactoring en s'assurant qu'une fois qu'on a du code fonctionnel, il continue à être fonctionnel plus tard (tests d'intégration, TDD... sera vu plus tard dans votre DEC)