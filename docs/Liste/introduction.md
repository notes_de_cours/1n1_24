# Listes

Jusqu'à maintenant, lorsque nous avions une série de nombres ou de String à manipuler, nous utilisions un `tableau` (ex : int[]) pouvant être manipuler avec la classe `Arrays`. 

Le désavantage d'un tableau est le fait qu'on ne peut modifier sa taille une fois créé. Si on doit augmenter sa taille, on doit créer un autre tableau plus grand et transférer les données d'un tableau à l'autre. 

Pour remédier à ce problème, Java propose des `Collections`. 

:::warning
Les listes sont fréquemment utilisées dans Greenfoot.

Par exemple:
* la liste des acteurs qui sont près d'un autre acteur.
* la liste des acteurs qui sont dans le jeu présentement.

Vous n'aurez pas souvent à faire des manipulations complexes avec ces listes, mais il est important de comprendre leur utilisation. 

:::

## Collection

- La bibliothèque de classes standard Java comprend un ensemble de conteneurs appelés `collections` permettant de regrouper des éléments dans une seule unité.
- Les collections sont plus sophistiquées et flexibles que les Array.
  - Tout d'abord, ils sont redimensionnables:
    - Vous pouvez ajouter un nombre quelconque d'éléments à une collection.
  - Une collection gére automatiquement la suppression d'un élément de n'importe quelle position.
- Il existe plusieurs types de collections avec une structure de stockage interne différente.
- Vous pouvez choisir le type de collection le mieux adapté à votre problème afin que vos opérations les plus fréquentes soient pratiques et efficaces.

```mermaid
graph BT
	List["#lt;#lt;interface#gt;#gt; List"] --> Collection["#lt;#lt;interface#gt;#gt; Collection"]
	ArrayList --> List
	LinkedList --> List
	Set["#lt;#lt;interface#gt;#gt; Set"] --> Collection
	autre[...] --> Collection
	autre2[...] --> Set
	autre3[...] --> List
```

- Il existe une restriction: les collections ne peuvent pas stocker de valeurs primitives.
  - Au lieu de cela, vous devez utiliser l’une des classes de wrapper (`Integer`, `Long`, `Character`, `Double` ou autre).


Exemple:

```java
package ca.cegepdrummond;

import java.util.ArrayList;
import java.util.List;

public class Programme {
    public static void main(String[] args) {
        List<String> proverbes = new ArrayList<String>();
        proverbes.add("a beau mentir qui vient de loin");
        proverbes.add("aide-toi, le ciel t’aidera");
        proverbes.add("après la pluie vient le beau temps");

        System.out.println("Voici les proverbes :");

        for (String proverbe : proverbes) {
            System.out.println(proverbe);
        }
    }
}
```

À noter:

* Le type de ce qui sera entreposé dans la collection est indiqué entre `< >` . Cette collection ne pourra qu'entreposer des éléments de ce type. 
* `ArrayList` est une classe. c'est pour cela qu'on utilise son constructeur `ArrayList<type>()` avec le new. 
* La taille n'a pas à être spécifiée lors de la création de la collection. 

Exemple avec un type primitif:

```java
package ca.cegepdrummond;

import java.util.ArrayList;
import java.util.List;

public class Programme {
    public static void main(String[] args) {
        List<Integer> notes = new ArrayList<Integer>();
        notes.add(10); // convertion automatique de type primitif int à type wrapper Integer
        notes.add(22);
        notes.add(23);
        int noteDeGeorge = 21;
        notes.add(noteDeGeorge);

        for (Integer note : notes) { 
          // note est du type wrapper Integer
            System.out.println(note);
        }

        for (Integer note : notes) {
            int lNote = note;
            // la conversion de wrapper à primitif est automatique
            System.out.println(lNote);
        }

    }
}


```

À noter:

* Pour entreposer des types primitifs, on doit utiliser un `wrapper`. 

int -> Integer  
long -> Long  
char -> Character  
float -> Float  
double -> Double   
boolean -> Boolean
* Lors de l'ajout, on peut utiliser un type primitif. La conversion dans un wrapper sera faite automatiquement.
* Lors de l'extraction, c'est un valeur du type wrapper qui est extraite. Mais il est possible de la convertir automatiquement en type primitif. 



## Manipulation d'une List (et de ses sous-classe ArrayList ou LinkedList)

* Une liste est une collection **ordonnée** d'éléments.
* Vous pouvez parcourir séquentiellement les éléments d'une liste ou vous référer à l'index.

* En un sens, une liste fonctionne un peu comme les Array, sauf que sa taille peut être modifiée dynamiquement.

- En Java, l'interface `List<E>` représente un type de données **abstrait**. Il hérite de l'interface `Collection<E>`.

- Les méthodes de l'interface les plus couramment utilisées sont les suivantes:
  - `E get(int index)` 
    - Renvoie l'élément à la position spécifiée dans cette liste
  - `int size()`
    - Renvoie le nombre d'éléments de cette collection.
  - `boolean isEmpty ()`
    - Renvoie `true` si cette collection ne contient aucun élément
  - `boolean contains(Object o)`
    - Renvoie `true` si cette collection contient l'élément spécifié
  - `boolean add(E e)`
    - Ajoute l'élément spécifié à la fin de la collection
  - `boolean remove(E element)`
    - Supprime l'élément spécifié de la collection
  - `void clear()`
    - Supprime tous les éléments de cette collection
  - `void add(int index, E element)`
    - Insère l'élément spécifié à la position spécifiée dans cette liste
  - `E remove(int index)`
    - Supprime l'élément à la position spécifiée dans cette liste et le renvoie
  - `E set(int index, E element)`
    - Remplace l'élément à la position spécifiée dans cette liste par l'élément spécifié
  - `int indexOf(E element)`
    - Renvoie l'index de la première occurrence de l'élément spécifié dans cette liste, ou -1
  - `int lastIndexOf(E element)`
    - Renvoie l'index de la dernière occurrence de l'élément spécifié dans cette liste, ou -1

Exemple

Ajoutez ce code à la fin de l'exemple précédent:


```java
        System.out.println("index 2");
        System.out.println(notes.get(2));
        System.out.println(notes.size());

        System.out.println("remove index 1");
        System.out.println(notes.contains(10));
        notes.remove(0);
        System.out.println(notes.contains(10));
        System.out.println(notes.size());

        System.out.println("remove valeur 22");
        System.out.println(notes.contains(22));
        notes.remove((Integer) 22);
        System.out.println(notes.contains(22));
        System.out.println(notes.size());

        System.out.println("insertion de 33");
        notes.add(1, 33);
        for (Integer note : notes) {
            System.out.println(note);
        }
```


* Vous ne pouvez pas créer une instance de l'interface List, mais vous pouvez créer une instance de l'une de ses implémentations: `ArrayList` ou `LinkedList`.
  * Ces implémentations ont des mécanismes internes différents, mais l'interface List vous permet de travailler avec les deux de la même manière.


* Un ArrayList est construite sur un Array Java standard et est étendue à un ensemble d'opérations pratiques.



* Une seule restriction existe:
  * Puisqu'un ArrayList est une Collection, on ne peut pas stocker des valeurs primitives, on doit utiliser les Wrappers (Integer, Long, Character, Double ou autre).

### Création d'une instance de ArrayList

* Pour utiliser cette classe, effectuez l'importation suivante:

```java
import java.util.ArrayList;
```

Le moyen le plus simple pour créer un ArrayList consiste à utiliser un constructeur sans argument.

```java
List<String> liste = new ArrayList<>();
```

* La liste créée est vide, mais sa capacité initiale est de 10 (par défaut).

Nous pouvons également spécifier la capacité initiale de celui-ci:

```java
List<String> liste = new ArrayList<>(50);
```

* Cette liste est vide, mais sa capacité initiale est définie sur 50.

Vous pouvez construire une ArrayList composée d'éléments d'une autre liste:

```java
List<String> liste = new ArrayList<>(uneAutreListe);
```

Quelle que soit la manière dont vous créez une instance de ArrayList, sa taille pourra changer de manière dynamique.

### Quelques exemples

```java
List<String> noms = new ArrayList<>(); // liste vide de String

System.out.println(noms.size()); // 0

noms.add("Stéphane"); // [Stéphane]
noms.add("Evely"); // [Stéphane, Evely]
noms.add("Livia"); // [Stéphane, Evely, Livia]

System.out.println(noms); // [Stéphane, Evely, Livia]
System.out.println(noms.size()); // 3

System.out.println(noms.get(0)); // le premier élément est "Stéphane"
System.out.println(noms.get(2)); // le dernier élément est "Livia"

noms.add(0,"Vanessa"); // [Vanessa, Stéphane, Evely, Livia]

noms.remove("Stéphane"); // [Vanessa, Evely, Livia]
noms.clear(); // []

System.out.println(noms.size ()); // 0
```

## For-each

On peut utiliser le for-each aussi pour itérer sur les listes.

```java
List<Long> puissancesDe10 = new ArrayList<>();

for(int i = 0; i < 5; i++) {
    long puissance = (long) Math.pow(10, i);
    puissancesDe10.add(puissance);
}

for(Long valeur : puissancesDe10) {
    System.out.print(valeur + " ");
}
```

Le code imprime ce qui suit:

```java
1 10 100 1000 10000
```
