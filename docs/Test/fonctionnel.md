# Tests fonctionnels


<!-- est ce que fit avec ce qui est enseigné en gestion de projet --> 


[définition](https://vitrinelinguistique.oqlf.gouv.qc.ca/fiche-gdt/fiche/8391118/test-fonctionnel)

Les tests fonctionnels sont une approche de test qui vise à garantir que chaque fonctionnalité de l’application s'éxécute conformément aux exigences du logiciel. Chaque élément du logiciel sera évalué en fonction des besoins (les requis) afin de vérifier que les résultats sont conformes aux attentes de l’utilisateur final.

Le but des tests fonctionnels est de tester les fonctionnalités de notre application en **envoyant différentes valeurs à l'application et en vérifiant le résultat**. En d'autres mots, on teste ce que le système **doit faire**. 

Ces tests peuvent être fait manuellement ou de façon automatisée. Dans le cadre du cours nous allons nous concentrer sur les tests manuels. 

Un exemple de tests automatisés se trouve dans le TP1. Une série de tests ont été écrits afin de vérifier si votre solution donnait le bon résultat. Notez que ces tests ne peuvent prédire **comment** vous avez écrit le code. Ils ne font que tester que le résultat est bon. Ce genre de tests est fréquemment appelé **boite noire** car aucune connaissance de l'algorithme utilisé n'est nécessaire. Il existe un autre type de tests qui valident l'algorithme. Dans ce cas, on parle de tests **boite blanche**. 

## Contenu d'un test

Un test doit décrire les étapes à faire, ainsi que les résultats attendus. 

Dans tous les cas, le programme ne devrait pas "planter" lors d'un test. Par contre, il est possible que le comportement ne soit pas celui auquel on s'attend. Dans ce cas, on devra analyser le résultat et localiser le problème. Il est possible que ca soit le test qui soit en erreur, par exemple suite à un changement dans les spécifications. Dans ce cas, il faut mettre à jour le test. Si c'est le code qui est erroné, il faut noter l'erreur afin de la corriger. 

Un test doit aussi être le plus simple possible et ne tester qu'une chose. Il est possible qu'une "action" provoque plusieurs résultats. Vous pouvez alors indiquer tous les résultats attendus. Mais il serait mieux d'essayer d'isoler un résultat à l'aide d'un test spécifique, mais ce n'est pas toujours possible. 

Un test ne devrait pas retester ce qu'un autre test à déjà valider. Il est bon de faire une liste des choses à tester et de les cocher dès qu'un test a été fait pour cette chose. Un tableau croisé est parfois une bonne solution. 


Voici quelques exemples de tests fonctionnels tirés du jeu Yam/Yatzee selon les règlements du site https://www.regles-de-jeux.com/regle-du-yams/ et l'implémentation faite dans le TP2:

:::tip
(T) indique ce que le test doit faire
(R) indique le résultat attendu
:::

* (T) Démarrer le jeu
  * (R) 5 dés ayant une valeur entre 1 et 6(inclusivement) sont générés et affichés
  * (R) le jeu propose de garde des dés
* (T) garder le dé #1 après le premier lancé
  * (R) le jeu indique le dé gardé
  * (R) le jeu relance 4 dés et les affiche. 
* (T) garder le dé #1 après le deuxième lancé 
  * (R) le deuxième dé gardé est en deuxième position dans la liste des dés gardés
  * (R) le jeu relance 3 dés et les affiche
  * (R) les 3 dés générés sont affichés après les 2 qui étaient gardés
  * (R) le jeu affiche les possibilités et demande de choisir. 
* (T) entrer une valeur négative pour la position des dés à conserver
  * (R) la question est reposée. 
* (T) entrer une valeur trop grande pour la position des dés à conserver
  * (R) la question est reposée. 
* (T) entrer du texte pour la position des dés à conserver
  * (R) la question est reposée. 





## Attention à la "testabilité" du test

Certains aspects peuvent être difficiles à tester en mode "boite noire". 
Par exemple, je pourrais écrire un test vérifiant si "les nombres générés sont toujours entre 1 et 6 inclusivement". Mais comment le tester? Les dés sont une valeur aléatoire. Donc sans regarder le code, il est difficile de **prouver** que c'est le cas. On peut démontrer uniquement que statistiquement le jeu ne semble pas générer autre chose. 


## Ne tester que ce qui a été spécifié

Notez qu'on ne peut pas faire un test fonctionnel sur quelque chose qui n'a pas été spécifiée. 

Par exemple, je ne peux pas faire ces tests:

* jouer avec 4 dés seulement;
* que le temps de réponse est moins que x millisecondes. 


Ces détails n'ont jamais été spécifiés, le programmeur (ou le testeur) ne peut décider arbitrairement quelles valeurs utiliser. 
:::warning
C'est au client de décider des valeurs à tester. Toujours s'y référer, ne jamais prendre de décision arbitraire.
:::

Plus les spécifications sont précises, plus il est facile de tester (souvenez vous de ça en troisième année quand vous aurez à écrire les spécifications d'un système et ensuite d'y faire des tests). 

Par exemple, ces détails n'étaient pas dans la spécification:

*  utiliser 0 ou un autre code pour indiquer de ne garder aucun dés;
*  est-il possible de quitter la partie en plein milieu;
* combien d'essaies est il possible de faire pour indiquer quelle position des dés garder avant que le jeu arrête


De plus, les tests fonctionnels ne portent pas sur les aspects *non fonctionnels* de l'application. Exemple:

* quel ensemble de caractères utiliser;
* le jeu doit être facile à contrôller;
* le jeu doit être amusant. 

Ces éléments peuvent être testés, mais ce sont des tests **non fonctionnels** qui les couvriront (ce que nous ne ferons pas dans le cadre de ce cours). 

[Pour en savoir plus](https://www.all4test.fr/blog-du-testeur/quest-ce-test-fonctionnel-tutoriel-complet-exemples/#:~:text=Les%20tests%20fonctionnels%20sont%20une,attentes%20de%20l'utilisateur%20final)  
[Pour en savoir encore plus](https://fr.wikipedia.org/wiki/Test_(informatique))

## Comment tester

Les tests doivent couvrir:

* les bonnes valeurs
* les mauvaises valeurs
* les cas limites


Une bonne pratique est de **tester le cas valide de chacune des fonctionnalités** en premier. Le but est d'entrer des informations correspondantes aux critères spécifiés afin de voir si minimalement le cas valide fonctionne.

Exemple:
* (T) entrer 1 comme position des dés à garder
  * (R) le premier dé est conservé et affiché
* (T) entrer 5 comme position des  dés à garder. 
  * (R) le dernier dé est conservé et affiché.



Ensuite, nous allons faire des **tests aux limites**.

* (T) entrer 1 et 5 comme position des dés à garder
  * (R) c'est accepté  
* (T) entrer 0 
  * (R) tous les dés sont conservés
* (T) entrer 6 comme position des dés à garder
  * (R) la message est réaffiché
  
  (Imaginez que le code est le suivant: if(valeur >0 && valeur < 6)... Peut-etre que par erreur ca serait if(valeur >=0 && valeur<= 6)... c'est ca un bug!)


Ensuite, on peut **tester les erreurs possibles** du programme en entrant des valeurs invalides.

* (T) entrer une lettre 
  * (R) le message est réaffiché
* (T) entrer 9
  * (R) le message est réaffiché


  

Voici quelques types de tests qui s'appliquent pratiquement tout le temps:

* Entrer un type différent de ce qui est demandé (ex: une chaine de caractères pour un champ numérique, et vice-versa)
* Provoquer une division par 0 (ex: peut être difficile à faire, mais fréquemment utilisés dans les tests boite-blanche)
* Demander un élément en dehors des limites d'un tableau ou d'une liste
* Il peut aussi être intéressant de tester plusieurs combinaisons pour des champs qui sont reliés (ex: un champ pour une date. Qu'arrive-t-il si j'entre le 29 février? Ca va dépendre de l'année)
* Faire du *monkey testing*, c.à.d. taper n'importe quoi, n'importe où. 

> Les programmeurs ont tendance à être trop gentils. N'ayez pas peur de briser votre code, mieux vaut que vous trouviez le bug plutôt que le client!

Les problèmes trouvés devront ensuite être corrigés, et les mêmes tests devront être ré-exécutés afin de démontrer que l'erreur est vraiment corrigée (d'où l'avantage des tests automatisés). 

> Il est donc important de bien noter comment l'erreur a été produite afin de pouvoir la reproduire (ce qui n'est pas toujours le cas)

## Exemple

Voici une spécification, suivie de tests pouvant valider si le code effectuerait la bonne chose

Spécification:

Le programme doit:

* demander deux nombres entiers
* si le premier nombre est un nombre premier, afficher "nombre premier", sinon "quelconque"
* si le deuxième nombre est pair, afficher "pair",sinon "impair"
* calculer et afficher le PPCM (Plus Petit Commun Multiple) des 2 nombres. 
  * Si les nombres ont un signe inverse, afficher "PPCM introuvable"
  * S'il n'y a pas de PPCM < que minInt ou > que maxInt, afficher "PPCM  introuvable"
  * Si un des nombres est 0, afficher "PPCM introuvable"
  
Notez que Console sera utilisé pour faire l'entrée des données. Vous n'avez donc pas à vérifier si la valeur entrée est vraiment un entier. 
 
Plan de tests (ce qu'il faut tester):

1. la validation de nombre premier est faite sur le premier nombre
2. la validation de pair/impaire est faite sur le 2e nombre
3. le PPCM est basé sur les 2 nombres
4. la validation de nombre premier fonctionne
5. la validation de pair/impair fonctionne
6. 2 nombres sont demandés
7. il faut que les 2 nombres aient le même signe pour le ppcm
8. 0 n'est pas bon pour le ppcm 
9. si le PPCM est hors des valeurs acceptables pour un entier

 Les tests:

1. Entrer 2 et 7 (R) affiche nombre premier (valide 6, 4)
1. Entrer 2 et 7 (R) affiche impair   (valide 2, 5 pour impaire (et 6 mais c'est déjà validé))(tel qu'indiqué, un test  doit généralement valider une seule chose) 
1. Entrer 2 et 7 (R) le PPCM est 14  (valide 3)
1. Entrer 2 et 2 (R) affiche nombre premier (valide 1 avec un cas limite (2 est le plus petit nombre premier))
1. Entrer 2 et 2 (R) affiche pair (valide 5 pour pair)
1. Entrer 2 et 2 (R) le ppcm est 2 (valide 3 pour un cas limite)
1. Entrer 7 et 2 (R) affiche nombre premier (valide 4) (notez que jusqu'à maintenant, on ne sait toujours pas si le test de nombre premier est fait sur le premier ou 2e nombre) (est-ce que ce test ajouter qqchose ? pas vraiment)
1. Entrer 7 et 2 (R) affiche pair (valide 2 et 5 pour pair ) (ici aussi, est-ce que test ajoute qqchose?)
1. Entrer 4 et 2 (R) affiche quelconque (valide 1 ) (là on sait que le test de premier est fait sur le premier nombre)
1. Entrer minInt et maxInt (R) affiche PPCM introuvable (valide 7)
2. Entrer 0 pour un des 2 nombres (R) affiche PPCM introuvable (valide 8)
3. Entrer maxInt et maxInt-1 (R) affiche PPCM introuvable (valide 9)
   
   
Comme on peut le remarquer, la gestion de la *qualité* des tests peut rapidement devenir complexe. Je ne vous en demanderai pas autant. 



