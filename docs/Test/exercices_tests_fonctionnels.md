# Tests fonctionnels: Exercices


## Exercice 1

Voici la spécification d'un programme. À partir de cette spécifications, nous pourrons écrire une série de tests.

Spécification:

Le programme doit 
* demander le nom de la personne
* demander la date de naissance de la personne
* le nom doit avoir entre 1 et 50 (inclusivement) caractères
  * le msg "nom invalide (1 à 50 caractères)" doit être affiché en cas d'erreur
* la date est sous le format AAAAMMJJ
  * le msg "date invalide (AAAAMMJJ)" doit être affiché en cas d'erreur
* le programme calcule et affiche l'âge de la personne basée sur la date de naissance
  

Exemples de tests possibles. Ne regardez pas les exemples, nous allons y penser ensemble.

:::details exemples de tests

* (T) Entrer un nom valide (entre 1 et 50 caractères) (ex Benoit Desrosiers). (R) la valeur est acceptée
* (T) Entrer un nom vide. (R) le programme devrait afficher que le nom est invalide  "nom invalide (1 à 50 caractères)"
* (T) Entrer un nom trop long (51 caractère). (R) le programme devrait afficher le msg d'erreur approprié "nom invalide (1 à 50 caractères)"
* (T) Entrer un nom limite (49, 50 caractères). (R) la valeur est acceptée
* (T) Entrer un nom contenant uniquement des espaces. (R) hmmmm, est-ce bon?
* (T) Entrer une date valide. Ex: 20200101, 20230131, 19761231, 00010101, 99991231 (il n'y a pas de limite à la date), 20200229. (R) la valeur est acceptée
* (T) Entrer une date invalide. Ex: 20201301, 20210229 (pas bisextile), 20200132, 20230431, 01012023. (R) le msg date invalide (AAAMMJJ) est affiché
* (T) Entrer une date avec du texte. ex: 2020jan01. (R) le msg d'erreur  "date invalide (AAAAMMJJ)" est affiché
* (T) Entrer une date dans le futur (R) pas spécifié, mais c'est une date de naissance, ca ne devrait donc pas être dans le futur!!!
* (T) Entrer une date valide et calculer l'age manuellement. (R) l'âge affiché devrait correspondre au calcul manuel (l'âge dépend de la journée et du mois, pas juste de l'année). (Il faut faire le calcul à l'aide d'une autre source fiable.)   
* ...
:::



## Exercice 2

Voici la spécification d'un programme. À partir de celle-ci, écrivez une série de tests fonctionnels. N'oubliez pas de tester les cas limites!

Vous trouverez ci-bas le code correspondant à cette spécification. 

:::warning
Ne le regardez pas avant d'avoir terminé l'écriture de vos tests. C'est un exercice de test boite noire. Vous ne devez pas connaitre le code pour faire vos tests.
:::

Malheureusement, ce programmeur n'est pas très méticuleux et n'a pas étudié au Cégep de Drummondville. Son code est donc plein d'erreurs. 

Est-ce que vos tests auraient permis de trouver tous les bugs?  

### La spécification

* A) Le programme doit prendre 2 entiers en paramètres
* B) Le programme doit calculer la factoriel du premier
* C) Le programme doit élever le premier à la puissance du deuxième. Le résultat doit être un entier. 
* D) Le programme doit diviser le premier par le deuxième et vice-versa, le résultat doit avoir des décimales. 
* E) Le programme doit écrire tous les nombres entre le plus petit des nombres et le plus grand des nombres inclusivement sur une même ligne,  peu importe si le premier est plus petit ou plus grand que le deuxième

Notez que Console sera utilisé pour faire l'entrée des données. Les entiers seront donc validés. 

Voici un exemple d'un test. Vous pouvez utiliser ce format

* (T) entrer 2 entiers positifs (ex: 5 et 6) 
  * (R B) la factoriel donne le bon résultat (ex: 120)
  * (R C) 15625 
  * (R D) 0.8333 et 1.2
  * (R E) 5 6


:::details Exemples de tests

* (T) entrer des lettres pour les 2 entiers
  * (R) ca ne devrait pas être accepté, et ca devrait redemander d'entrer les valeurs
  * Bien qu'il soit indiqué que le programme utilise Console, il peut quand même être utile de faire un test simple comme celui-ci afin de s'assurer que le programme utilise bien Console. 
* (T) entrer 2 entiers négatifs (ex: -5 et -6) 
  * (R B) erreur
  * (R C) 0, et non pas 0.000064 car le résultat doit être un entier
  * (R D) 0.8333 et 1.2 
  * (R D) -6 -5
* (T) entrer 1 entier positif et un négatif (ex 3 et -4)
  * (R B) 6
  * (R C) 0 
  * (R D) -0.75, -1.333
  * (R E) -4 -3 -2 -1 0 1 2 3 
*  (T) entrer un grand nombre entier positif et 0 (ex: 1000000 et 0)
   *  (R B) la plus grand valeur pour calculer une factoriel dans un entier est 12. 
   *  (R C) 1  (un nombre à la puissance 0 donne 1 )
   *  (R D) erreur, et 0
   *  (R E) 0 1 2 3 .... (ca va surement donne qqchose de bizz)
....

:::

:::details Le programme


```java

package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Console.afficher("Premier nombre :");
        int lEntier1 = Console.demanderEntier(); // l'avantage de prendre une fonction comme demanderEntier est que la
        // validation est faite.
        Console.afficher("Deuxieme nombre :");
        int lEntier2 = Integer.parseInt(Console.demanderTexte()); //bug si on entre du texte. il est possible de vraiment pas savoir
        // comment utiliser une fonctionnalité. Un bon testeur vérifie tout quand même.
        Console.afficher("la factoriel de "+lEntier1+" est: "+calculerFactoriel(lEntier2)); //bug, j'ai pris le 2e
        Console.afficher(lEntier1 + " élevé à la puissance "+ lEntier2 +" = "+ calculerPuissance(lEntier1, lEntier2)); //oups, c'est un double
        Console.afficher(lEntier1 + " divisé par "+ lEntier2 +" = "+ calculerDivision(lEntier1, lEntier2));
        Console.afficher("Les nombres entre "+lEntier1+ " et "+lEntier1+ " sont :"); // bug subtile, l'intervalle affiché n'est pas le bon. Ce genre de bug peut être difficile à tester et demande de faire très attention à ce qui est affiché.
        calculerIntervale(lEntier1, lEntier2);
    }

    public static int calculerFactoriel(int aNombre) {
        // bug si aNombre <1
        // bug si aNombre est très grand (la factoriel de 12 est la plus grande valeur qui entre dans un entier)
        int resultat = 1;
        for (int i = 1; i < aNombre; i++) { // bug < au lieu de <= 
            resultat *= i;
        }
        return resultat;
    }

    public static double calculerPuissance(int aNombre, int aPuissance) {
        return Math.pow(aNombre,aPuissance); // bug si les valeurs sont trop grandes
        // bug aussi si on passe un négatif dans aPuissance car on a demandé que la réponse soit un entier, et élever
        // un nombre à une puissance négative peut donner des décimales.
    }

    public static float calculerDivision(int aNumerateur, int aDenominateur )  {
        return aNumerateur/aDenominateur; // bug pour aDenominateur = 0;
        // bug les paramètres étant des entiers, ca devient une division entière. Bien qu'on retourne un float,
        // la valeur après le . sera toujours 0. 
        // ici, même si la division était faite correctement avec des floats, la division d'un entier par un autre ne
        // peut dépasser la valeur maximale d'un float. Mais dépendamment du type des paramètres, certains tests
        // seraient nécessaires éventuellement.
    }

    public static void calculerIntervale(int aPremier, int aDeuxieme) {
        for (int i = aPremier; i < aDeuxieme + 1; i++) {  // bug étant donné que c'est < +1, si on utilise maxInt, ca ne fonctionnera pas.
            // bug, si aPremier <= aDeuxieme, ca ne fait rien
            System.out.print(i);
        }
        System.out.println();
    }

}
```

:::