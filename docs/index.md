---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Techniques de l'informatique"
  text: "420-1N1 "
  tagline:  "Introduction à la programmation"
  

features:
  - title: Premier cours
    details: Les outils dont tu auras besoin durant la session.
    link: intro/introduction
  - title: Algorithmie
    details: Introduction aux algorithmes.
    link: Flowgorithm/introduction
  - title: Java
    details: Introduction à la programmation avec le langage Java
    link: Java/Introduction/programmation
  - title: Greenfoot
    details: Introduction à Greenfoot, un environnement graphique de programmation en Java
    link: Greenfoot/introduction
  - title: Orienté Objet
    details: Introduction à l'orienté objet
    link: OO/introduction
  - title: Listes
    details: Mieux que Arrays
    link: Liste/introduction
  - title: TP3
    details: Outils nécessaires pour le TP3
    link: Demo_TP3/introduction
  - title: Révision
    details: Des exercices pour te préparer à l'examen final
    link: Revision/exercices
  - title: TPs
    details: Les Travaux Pratiques
    link: TPs/
---

