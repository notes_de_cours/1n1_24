# Le mode commande

On vous demandera souvent d'entrer des commandes texte dans le **cmd**. C'est quoi ca le cmd?

Le cmd, c'est la console texte de windows. Il en existe 2 principales: le cmd classique, et le powershell. 

Dans le cadre du cours, nous allons surtout utiliser le cmd classique. Si vous désirez apprendre le powershell, libre à vous, mais certaines commandes ne sont pas disponible dans ce dernier (ou ont une syntaxe différente). 

Pour ouvrir le cmd, vous pouvez ouvrir le menu **démarrer** de windows (en bas à gauche complètement) et inscrire **cmd**

Si vous l'ouvrez de cette façon, vous vous retrouverez dans un répertoire quelconque, selon votre configuration. 

Vous pouvez aussi ouvrir un navigateur de répertoires, vous déplacer dans un répertoire, et de là taper cmd dans la barre d'adresse se trouvant en haut. 

Quelques commandes utiles:

* cd
    * cd ..
* dir
* mkdir

## Exercices
 
Pour vous pratiquer, nous allons créer un répertoire dans lequel vous pourrez mettre tous vos projets. 

Ouvrez un navigateur de répertoire, et déplacez-vous dans **C:\\** (vous pouvez aller dans **Ce PC**, ensuite **Disque local (C:)** ).

Créez un répertoire **Projets**  (clic droit, nouveau dossier)

Faites un clic dans la barre d'adresse en haut de la fenêtre (Ca devrait être indiqué **Ce PC > Disque local (C:)** ) (le texte devrait changer pour **C:\\** ) et inscrivez **cmd**

Cela devrait ouvrir un **cmd** sur **C:\\>**

Déplacez-vous dans le répertoire **Projets** que vous venez de créer en inscrivant la commande **cd Projets**

Créez un nouveau répertoire **1n1** en inscrivant **mkdir 1n1**

Entrez la commande **dir** afin de vérifier que le répertoire est bien créé.

À partir de maintenant, vous devriez mettre tous vos projets de ce cours dans ce répertoire. 

Il est fortement recommandé de créer un répertoire pour vos autres cours. 

:::warning IMPORTANT 
Ne jamais mettre d'accent ni d'espace dans un nom de fichier ou de répertoire. Bien que ce soit accepté, vous vous éviterez bien des problèmes en suivant cette règle simple.
:::

