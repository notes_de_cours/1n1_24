# Récupération des exercices du TP1

Ce premier TP (Travail Pratique) consiste à compléter une série d'exercices afin d'approfondir les notions vues en cours.

:::warning IMPORTANT 
Le but de faire ces exercices n'est pas de trouver la bonne réponse, c'est de comprendre le fonctionnement des éléments de programmation que nous vous enseignons. Il est donc important de ne pas faire ces exercices "bêtement", en cherchant juste à les terminer le plus vite possible. Ces bases vous serviront durant les 3 prochaines années, et durant toute votre carrière de programmeur. Il est donc important de bien les assimiler. 
:::


## Accéder au GitLab du département 

:::warning IMPORTANT
Si vous êtes à l'extérieur du cégep, avant d'accéder au serveur du cégep, vous devez utiliser le VPN. Voir [VPN du cégep](installation_logiciels.html#vpn-du-cégep)
:::

:::info
Le nom **gitlab** représente 2 choses, voici la description offerte par ChatGTP:

GitLab est à la fois une plateforme de gestion de projets et un outil de gestion de code source basé sur Git. Voici une description détaillée pour chacun :

GitLab (le site web) :

> GitLab.com est un service web offert par GitLab Inc. qui permet aux développeurs de collaborer sur des projets logiciels en utilisant Git comme système de contrôle de version. C'est une plateforme complète de gestion du cycle de vie des applications (ALM) qui inclut des fonctionnalités telles que le suivi des problèmes, la gestion des tests, l'intégration continue, la livraison continue (CI/CD), la gestion des déploiements, et plus encore. GitLab.com propose une version SaaS (Software as a Service) où les utilisateurs peuvent héberger leurs projets en ligne sans avoir à gérer leur propre infrastructure.

GitLab (le logiciel) :

> GitLab est également un logiciel open source disponible sous licence MIT, qui peut être installé localement sur les serveurs de l'entreprise ou sur un cloud privé pour gérer le développement de logiciels de manière collaborative. Il offre toutes les fonctionnalités disponibles sur GitLab.com, permettant aux équipes de développeurs de gérer l'ensemble du processus de développement logiciel de manière efficace et intégrée. Cela inclut la gestion du code source, le suivi des problèmes, la gestion des versions, l'intégration continue, la livraison continue, et la sécurité des applications.

En résumé, GitLab combine les fonctionnalités de gestion de code source de Git avec des outils complets de gestion de projet et d'automatisation du développement, que ce soit en tant que service cloud avec GitLab.com ou en tant que logiciel à déployer localement avec GitLab CE (Community Edition) ou GitLab EE (Enterprise Edition).

Ce logiciel a été installé sur le serveur départemental. Il crée une version locale similaire à ce qu'on retrouve sur gitlab.com. Le site que nous gérons au département est accessible à l'adresse https://info420.cegepdrummond.ca/tigit . 

:::
### Premier accès 

Un compte a été créé pour chaque étudiant sur le serveur git du département. Pour y accéder vous devez commencer par changer votre mot de passe. Un courriel vous a été envoyé sur votre outlook du cégep. 

Pour accéder à votre courriel outlook du cégep:
* Allez sur https://outlook.office.com/mail/
  * nom d'utilisateur : prenom.nom@etu.cegepdrummond.ca
  * mot de passe : le même que votre mot de passe pour vous connecter au portail
* Vous allez avoir un courriel nommé : **Account was created for you** de **GitLab**

![image-20220815142434851](/image-20220815142434851.png) 

* Dans le courriel, cliquez sur le lien **Click here to set your password**
* Inscrivez le mot de passe que vous voullez utiliser pour le serveur GitLab du cégep
* En vous connectant, vous devriez avoir une page comme celle-ci

![image-20220818074336450](/image-20220818074336450.png) 

## Copier le projet du TP1 (fork)

Une fois votre mot de passe changé, vous pouvez accéder au projet en allant sur le lien suivant : https://info420.cegepdrummond.ca/tigit/benoitd/420-1n1-tp1-exercices.git



:::warning IMPORTANT 
dans les exemples qui suivent, remplacez **janvier** par **benoitd**.­
:::

Pour faire le TP1 de votre côté, vous devrez copier (fork) le projet.
* Cliquez sur le bouton **Fork**

<img src="/image-20220815143033793.png" alt="image-20220815143033793" style="zoom:180%;" /> 

* Sélectionnez votre utilisateur comme **namespace** (dans l'exemple ici c'est "Étudiant 420"), mettrez le projet **Private** pour que vos collègues ne puissent pas vous copier et appuyez sur **Fork project**

 ![image-20220818074539191](/image-20220818074539191.png)

* Une fois le **fork** terminé, allez dans le menu **Project information / Members**

 ![image-20220818074633201](/image-20220818074633201.png) 

* Sélectionnez l'option **Invite members**

 ![img](/invitemembers.png)

* Dans **Invite member**
  * mettez mon nom "Benoit Desrosiers"
  * mettez mon rôle **Developer** (ou **Maintainer**)
  * Sans date d'expiration
* Cliquez sur **Invite**

 ![image-20220818074735558](/image-20220818074735558.png) 

* Maintenant tout est prêt pour récupérer le projet sur votre ordinateur

## Récupérer le projet par IntelliJ

* Ouvrez IntelliJ et revenez à la fenêtre d'accueil
  * Si vous avez un projet d'ouvert aller dans **File / Close Project**
* Dans la fenêtre d'accueil, cliquez sur **Get from VCS**

 ![image-20220815144206266](/image-20220815144206266.png)

* Remplissez la configuration
  * Version control : Git
  * URL : https://info420.cegepdrummond.ca/tigit/0000000/420-1n1-tp1-exercices.git
    * Remplacez 0000000 par votre numéro de DA
  * Directory : C:\Projets\1N1\420-1n1-tp1-exercices
* Cliquez sur **Clone**
* Une fenêtre apparaître pour savoir si vous faites confiance au projet. Cliquez sur **Trust Project**

![image-20220815144518422](/image-20220815144518422.png) 



* Patientez pendant que le projet se charge...
  * Peut prendre du temps
  * Il n'y aura plus de barre de progression en bas à droite

## Première modification

* Dans le projet, allez dans **src/main/java/ca.cegepdrummond/Serie1_Introduction**

![image-20220815144754934](/image-20220815144754934.png) 

* Allez à la fonction **print1** et inscrivez le code suivant :

```java
public static void print1() {
    Console.afficher("Bonjour");
}
```

* Mettez la configuration de départ à **Serie1_IntroductionTest** et appuyez sur le bouton *play* (la flèche en vert)

![image-20220815144928344](/image-20220815144928344.png)

* Patientez pendant l'exécution des tests (la première fois est plus longue)
* Assurez-vous d'avoir le crochet de coché dans les résultats du test pour voir les tests qui ont fonctionné

 ![image-20220815145124989](/image-20220815145124989.png)

## Envoi de la modification

Maintenant que nous avons modifié le code. Nous pouvons le publier pour ne pas la perdre et pour que l'enseignant puisse voir votre progression.

Les étapes suivantes sont à faire seulement la première fois. Les prochaines fois, vous aurez seulement à faire un **commit / push**.

* Allez dans **File / Settings**
* Allez dans **Version Control / Commit**

![image-20220815145536082](/image-20220815145536082.png) 

* Cochez **use non-modal commit interface**

![image-20220815145557180](/image-20220815145557180.png) 

* Allez dans **Version Control / Git**

![image-20220815145813346](/image-20220815145813346.png) 

* Décochez **Show Push dialog for Commit and Push**

![image-20220815145845425](/image-20220815145845425.png) 

* Décochez **Use credential helper**

![image-20220815150457654](/image-20220815150457654.png) 

* Faites **Apply** et **OK**
* Vous devriez avoir un onglet **Commit** en-dessous de **Project**

![image-20220815145636573](/image-20220815145636573.png) 

* Allez dans l'onglet *Commit*, cochez tous les changements (vous pouvez simplement cocher la première ligne **Changes** ), inscrivez un message significatif indiquant ce que vous avez modifié (par exemple: Serie 1, print1) et cliquez sur **Commit and Push...**

<img src="/image-20220815145912597.png" alt="image-20220815145912597" style="zoom:150%;" /> 

* Si vous n'avez jamais utilisé git, vous devrez mettre votre nom et votre courriel pour identifier qui a fait la modification.
  * Name : Votre nom complet
    * Il est important de mettre votre vrai nom car il permettra à l'enseignant de savoir qui a fait les modifications.
  * E-mail: prenom.nom@etu.cegepdrummond.ca
    * Il est important de mettre votre courriel du cégep.
* Exemple avec l'utilisateur "Étudiant 420"

![image-20220815150228825](/image-20220815150228825.png) 

* Appuyez sur **Set and Commit**
* Vous devez maintenant vous authentifier sur GitLab
  * Username : 0000000
    * Remplacez 0000000 par votre numéro de DA
  * Mot de passe : mot de passe que vous avez configuré sur GitLab
* Voilà! Votre modification est publiée

## Envoyer votre code les prochaines fois

* Pour les prochaines fois, vous n'avez qu'à aller dans l'onglet **Commit**, cocher vos modifications, écrire un message significatif et cliquer sur **commit and Push...** en étant sur le VPN du cégep
* N'oubliez pas d'exécuter les tests pour vérifier vos réponses

## Édition du code

Le code que vous devez modifier se trouve dans les séries 1 à 13. Vous ne devez pas modifier les fichiers **Console**, **Menu**, **ItemMenu**, et **Programme**. 

Dans les séries, vous trouverez des fonctions devant être modifiées. 

Une fonction est un petit bout de code commencant par les mots **public static void** et suivit du nom de la fonction suivit de parenthèses **()**. 

Par exemple, dans Serie1_Introduction, vous trouverez la fonction **print3**

![print3](/fonctionprint3.png)

Les lignes se trouvant juste avant la fonction contiennent les instructions pour cet exercice. Ici, il faut modifier le code afin que la fonction affiche "Bonjour" (le but de ce numéro est de vous faire remarquer que **console** est écrit avec une minuscule, mais il doit s'écrire avec une majuscule... n'oubliez pas: Java fait la différence entre les majuscules et le minuscules)

Certaines lignes sont présentes afin de permettre que le code compile. Elle permette de mettre des bouts de code en commentaires afin qu'ils ne soient pas compilés. Nous en avons un exemple ici avec les ligne commencant par /* et se terminant par */. 
Vous devez enlever ces 2 lignes. Vous verrez alors que le mot **console** sera affiché en rouge, vous indiquant qu'il y a une erreur. Corrigez cette erreur en mettant un C et vous pourrez tester le code. 

En général, le code à modifier se retrouvera entre ce type de commentaire. Vous retrouverez aussi des lignes avec des **??**. Cela indique qu'il faut remplacer les ?? par quelquechose. 

Si vous ne réussisez pas à trouver la solution à un exercice et que le programme ne compile pas parce que votre code contient des erreurs, il vous est possible de mettre tout le code de la fonction en commentaire. Vous ne devez pas mettre toute la fonction en commentaire, seulement son contenu (tout ce qui est entre les accolades {} )


## Exécuter votre code dans la console

Vous pouvez choisir la configuration de départ selon ce que vous voulez faire (exécuter le code, ou le tester). Vous devez ensuite appuyer sur le bouton **play** en vert pour démarrer cette configuration.

![image-20220815205750808](/image-20220815205750808.png) 

* **Programme** : Exécute le programme pour le tester manuellement
* **SerieX_...Test** : Permet de tester automatiquement le code d'une série d'exercices à la fois
  * Si vous avez des crochets verts sur tous les tests, c'est que vous avez probablement terminé la série d'exercices
* **Tests** : Permet d'exécuter tous les tests automatisés de toutes les séries d'exercices

Si vous utilisez **Programme**, l'application s'exécutera en mode console. Cette application consiste en une série de menus vous permettant d'exécuter chaque fonction individuellement. 

Par exemple, nous avons modifié la fonction **print1** précédemment et nous avons exécuté le test automatique. Mais si vous désirez voir ce que produit cette fonction, démarrez **Programme**. Après quelques secondes, vous verrez un menu s'afficher:

![programme_serie1](/Programme_serie1.png)

Entrez **1** et un sous-menu pour la série 1 sera affiché:

![serie1](/Serie1.png)

Entrez **2** pour choisir **1b (print1)** et le texte **Bonjour** sera affiché, suivit du menu pour la série 1. 

À tout moment, vous pouvez entrez **0** pour revenir au menu précédent, ou sortir de l'application si vous êtes au menu principal. 

## Tester votre code

Lorsque vous exécutez les configurations se terminant par Test, le programme démarre en mode automatique et exécute une série de tests sur votre code. 

Ces tests vont fournir des données en entrée à votre code, et vérifier si le résultat est ce qui est attendu. 

Vous verrez dans un futur cours comment écrire de tels tests. Pour l'instant cela demeure en dehors de ce que nous allons vous apprendre dans ce cours. 

Lors de l'exécution de ces tests, il est possible que vous n'ayez pas fait la bonne chose. Dans ce cas, le test aura un X orange comme print2 et print3 dans l'exemple ci-dessous 

![testerreur](/TestErreur.png)

Vous devez alors cliquer sur la fonction en erreur, et dans la fenêtre d'affichage de l'exécution, vous pouvez dérouler jusqu'à ce que vous voyez du texte en rouge. Ce texte vous indiquera ce qui était désiré et ce qui a été affiché. Par exemple, pour print2, le message d'erreur était le suivant

![erreurprint2](/ErreurPrint2.png)

Car mon code était le suivant:

![fonctionprint2mauvaise.png](/fonctionprint2mauvaise.png) 

Les instructions indique qu'il faut afficher Bonjour sur une ligne, et "tout le monde" sur une autre ligne. Je me suis trompé et j'ai affiché "Bonjour" deux fois. Il faut donc que je corrige la  deuxième ligne avec Console pour:

![fonctionprint2fixe.png](/fonctionprint2fixe.png) 

Et si vous réexécutez les tests, print2 sera maintenant en vert. 

