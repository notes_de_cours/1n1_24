# Configuration initiale d'un projet dans Intelij

Afin de vous simplifier un peu le codage, nous vous fournissons 2 utilitaires: un système de menu, et une simplification de l'affichage et de l'entrée de données. 

Afin de les utiliser, vous devez importer un modèle de projet. Télécharger ce [fichier zip](/Intellij_project_template.zip) contenant le modèle pour vos projets.

:::tip
Le fichier devrait être dans le répertoire **téléchargement** ou **Downloads** et porter le nom **Intellij_project_template.zip**
:::

Une fois téléchargé, démarrez Intellij. 

Si vous n'avez pas de projet ouvert, vous devriez être sur cet écran. 

![customizesettings](/customizesettings.png)

Sélectionnez **Customize** et ensuite **Import Settings...**.

Si vous aviez un projet d'ouvert, ouvrez le menu **File/Manage IDE settings/import settings**

Importez le fichier **Intellij_project_template.zip** que vous venez de télécharger (il devrait être dans votre répertoire **Téléchargements  ou Downloads**). 

Faites OK sur cette fenêtre

![confirmtemplate](/confirmtemplate.png)

Redémarrez Intellij. 

Fermez le projet que vous aviez créé à l'étape précédente ( File/Close project ). 

Créez un nouveau projet en utilisation **Configuration1N1**. Appelez ce projet **helloworld**. 

![ouvriravectemplate](/nouveau_avec_template.png)

Vous devriez maintenant avoir un projet comportant les aides.

![projetavecaide](/projet_avec_aides.png)


À partir de maintenant, quand vous créez un projet pour ce cours, utilisez toujours **Configuration1N1**

Nous verrons à quoi servent Console et Menu d'ici peu.

## hello world

Il est classique de tester un environnement de programmation en écrivant un programme qui affichera **Hello world!**

Dans le projet **helloworld** que vous venez de créer, ouvrez la classe **Programme**

![helloworld_programme](/helloworldprogramme.png)

Ce qui devrait vous donner ceci:
![programmejava](/Programme.png)

Nous allons expliquer plus en détails ce code au courant des prochaines semaines. 

Pour l'instant, notez que:

* du code Java doit toujours être dans une classe (concept que vous étudierez surtout dans le cours Programmation orientée objet I). Ici nous avons la classe **Programme**. Une classe sert à structurer le code. 
* dans une classe nous retrouvons des fonctions (concept que nous allons couvrir dans les prochains cours). Ici, nous avons la fonction **main()** qui est le point de départ pour un programme Java. 
* Le code, en Java, est toujours entre **{ }**

Ajoutez le code suivant entre les { } de main():

```java
Console.afficher("Hello World!");
```

Appuyez sur le triangle vert en haut de l'écran pour démarrer le programme.

![helloworld](/helloworld.png)

Une console devrait s'ouvrir et vous y verrez Hello World!

Voila, vous venez de réaliser votre premier programme en Java!!!