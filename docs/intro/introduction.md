# 420-1N1-DM Initiation à la programmation


> Automne 2024  
> Benoit Desrosiers et Jonathan Gareau

[comment]: #  (la source pour ce cours se trouve sous  https://gitlab.com/notes_de_cours/1N1.git)


Au terme de ce cours, l’étudiant.e sera capable d’analyser un problème simple afin de réaliser des programmes simples en traduisant un algorithme dans un langage de programmation. Il ou elle sera aussi capable de faire le débogage d’un problème simple et d’appliquer un plan de test simple.

Ce cours n’a pas de préalables. Il est préalable à 420-2N2-DM Développement d’application native I, et est préalable relatif à: 420-2W1-DM Développement d’application Web II, 420-2N3-DM Outils de développement et 420-2N1-DM Programmation Orientée Objet I.
Ce cours se donne à la première session. Il sert à introduire les concepts de la programmation.
Il permet de faire :  
* l’introduction aux concepts de base de la programmation, ex : les variables, l’assignation, les alternatives conditionnelles, les boucles; 
* l’introduction des concepts de la programmation orientée objet : classes, méthodes, attributs. Les concepts plus avancés de la programmation orientée objet (ex : héritage, polymorphisme) seront enseignés dans les cours de programmation orientée objet, les cours de programmation native, ainsi que les cours de programmation web.


:::warning IMPORTANT 
Vous verrez beaucoup de nouveaux concepts dans ce cours. La seule façon de bien les comprendre est de faire les exercices demandés, de poser des questions aux profs et aux autres étudiant.e.s, et de fouiller quand on ne comprend pas un concept.  
:::

Veuillez ouvrir le plan de cours se trouvant sur Léa. 

 


