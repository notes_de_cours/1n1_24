# Installation des logiciels

## JDK

Pour programmer en Java, nous aurons besoin du JDK (Java Development Kit) (nous rentrerons en détail sur cela plus tard dans la session).

* Téléchargez le JDK à l'adresse suivante : https://adoptium.net/temurin/releases/?version=21

:::warning
 Il est important de prendre la version 21 pour le cours.

Dans les notes de cours, vous verrez 16, mais utilisez **21**. 
:::

* Si vous avez Windows 10, choisissez les options suivantes :

![image-20220810144445041](/image-20220810144445041.png)

* Cliquez sur le bouton ".msi" pour télécharger l'installateur

* Dans l'installateur, choisissez les options suivantes :

  <img src="/unknown-16601573165946.png" alt="img" style="zoom:120%;" />

  * Mettez toutes les options
    * Ajouter au PATH pour pouvoir faire les commandes Java de n'importe où
    * Associer les .jar pour démarrer les programmes compilés
    * Définir la variable JAVA_HOME pour utiliser différents outils en Java
    * Clés de registre JavaSoft est optionnel
  * Mettez le JDK dans un répertoire court et facile d'accès
    * C:\JDKs\21\
  * Pour le reste, laissez les options par défaut

* Une fois installé, essayez de faire la commande `java --version` dans l'invite de commande ou dans powershell

![image-20220810145257233](/image-20220810145257233.png) 



## IntelliJ

### Installation

Dans le cadre de ce cours, nous allons utiliser l'**IDE** (Integrated Development Environment) **Intellij** de Jetbrains. 

* Téléchargez IntelliJ Ultimate sur : https://www.jetbrains.com/idea/download 

  * "Ultimate", puisque vous allez avoir quelques options supplémentaires.
* Lors de l'installation, vous pouvez cocher toutes les options.

<img src="/unknown-16601594700479.png" alt="img" style="zoom:110%;" />

* Vous devez redémarrer votre ordinateur à la fin de l'installation
  
### License 
  * Vous devez obtenir une licence académique : https://www.jetbrains.com/shop/eform/students
  * Postulez en tant qu'universitaire et utilisez votre courriel du cégep : prenom.nom@etu.cegepdrummond.ca
    * Pour consulter vos courriels : https://outlook.office.com/mail/
    * Utilisez votre courriel comme nom d'utilisateur.


### Localisation des projets

Par défaut, Intellij sauvegarde les projets dans le répertoire **IdeaProjects** dans votre répertoire personnel. 

Pour utiliser le répertoire **c:/Projets/1N1** que nous venons de créer, suivez la procédure suivante:

1) ouvrez **File/Settings**
2) dans** Appearance & Behavior**, allez dans **System Settings**
3) dans la section **Project**, changez le répertoire par défaut. 



### Langue (optionnel)

Avec IntelliJ, il est possible d'avoir un outil pour vérifier le français. Par défaut, IntelliJ ne vérifie que l'anglais.

Pour ajouter la langue française :



soit:

* Ouvrez le panneau de configuration d'IntelliJ:

![img](/intellijCustomize.png)

![img](/allsetting.png)


ou :
* Allez dans `File -> Settings...`

![img](/unknown-166016154471242.png)

* Ouvrez `Editor` et  `Natural Languages`

![img](/unknown-166016158798945.png)

* Appuyez sur le `+` pour ajouter `français`

![img](/unknown-166016161139348.png)

* Faites "Apply"
* Allez dans "Grammar and Style" et dans l'onglet "Scope"
* Cochez tout

![img](/unknown-166016165144451.png)

* Faites  "Apply"

Ce n'est pas parfait, mais vous devriez faire un peu moins de fautes de français.

## 7zip

Permet de compresser un ou plusieurs fichiers en 7z ou en ZIP. Ce sera utile pour remettre des travaux et décompresser certains fichiers que je vais vous envoyer.

* Téléchargez 7zip sur https://www.7-zip.org/

  ![image-20220810145922087](/image-20220810145922087.png)

* Pour Windows 10, prenez la version 64-bit x64

* Installez 7zip avec les options par défaut

## Git

Git est un gestionnaire de version permettant la collaboration entre plusieurs développeurs. Nous allons utiliser cet outil pour que je puisse vous partager du code.

Pour ceux qui ont Windows :

* Téléchargez Git sur : https://gitforwindows.org/
* Faites l'installation avec les options par défaut

## (Optionnel) Notepad++ 

Vous avez probablement déjà utilisé **Bloc-note** pour éditer un fichier texte. Cet éditeur n'est pas très puissant et pourrait vous causer certains problèmes de conversion de fichiers. Un remplacant très souvent utilisé est **notepad++**

Vous pouvez le télécharger sur https://notepad-plus-plus.org/downloads/


## VPN du cégep

Pour accéder au serveur de la technique informatique de l'extérieur du cégep, vous devrez installer un VPN.

Celui-ci vous permettra d'accéder au code du premier TP et à répondre au sondage sur votre ressenti par rapport à vos cours.

Pour télécharger le VPN, veuillez suivre les instructions de ce site: https://notes-2n3.robinhoodsj.org/_media/vpn.pdf

:::tip
Notez que le VPN n'est pas nécessaire si vous êtes connecté au réseau du cégep. 
:::

## Discord

Discord est un logiciel de chat surtout utilisé par les *gamers*. Nous l'utilisons afin de permettre une communication instantanée entre les étudiant.e.s, les enseignant.e.s, et même les anciens étudiant.e.s. 

Vous pouvez le télécharger sur https://discord.com et rejoindre notre serveur `420DM`.


<!-- 

## (Optionnel) Script de démarrage

Si vous désirez être capable de démarrer votre programme sans ouvrir IntelliJ, vous pouvez faire un script Powershell de démarrage.

* Créez un nouveau fichier dans le dossier du projet nommé `DEMARRER.ps1`

![img](/unknown-166016041739115.png)

* Mettez le contenu suivant dans le fichier `DEMARRER.ps1` : 

```powershell
$OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding =
                    New-Object System.Text.UTF8Encoding
./gradlew run -q --console plain
```

* Ouvrez powershell (de la même façon que vous ouvrez cmd, mais en inscrivant powershell)
* Pour démarrer le programme dans powershell, inscrivez la commande suivante : `./DEMARRER.ps1`
* Le script est en powershell pour avoir la possibilité de spécifier l'encodage des caractères (nous en reparlerons plus tard)

-->
