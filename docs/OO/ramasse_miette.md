# Durée de vie d'un objet (ramasse-miette)

Une instance est créée par le mot `new`. 

Mais comment est-elle effacée de la mémoire ?

Réponse: En Java (et dans la majorité des langages OO), un mécanisme, s'appelant le `ramasse-miette` (`garbage collector`) s'occupe de libérer l'espace mémoire occupé par un objet dès que cet objet n'est plus référé. 

En d'autres mots: si aucun autre objet n'a de référence à cet objet, il est purgé. 

Exemple: 

Dans la classe Programme, ajoutez ce code:

```java
    personnages[0] = new Personnage("Georges",1,1,1,1,1,1);
        for (Personnage personnage : personnages) {
            personnage.afficher();
        }
```

Ce code remplace le premier Personnage dans le tableau par un nouveau. Donc à partir de ce moment, l'ancien Personnage n'est plus référencé et pourra être effacé par le ramasse-miette. 


## Référence à un objet null

Une des erreurs les plus courantes en programmation orientée-objet est l'appel d'une méthode sur un objet null.

Il est fréquent de vouloir détruire un objet en le mettant égal à `null` afin que le ramasse-miette s'en occupe. 

Voici un exemple un peu simpliste. Nous en verrons des plus réalistes avec GreenFoot. 

```java
Personnage toto = new Personnage("Toto",1,1,1,1,1,1);
toto.afficher();
toto = null;
toto.afficher();
```

