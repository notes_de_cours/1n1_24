# Héritage

En programmation orientée-objet, une classe peut hériter d'une autre classe. Elle hérite des attributs et des méthodes de la classe. Dans la signature de la classe, on peut spécifier un parent avec l'instruction `extends`.

Voici un exemple non-exhaustif de ce que l'on peut faire avec l'héritage en modifiant le code fait dans les sections précédentes :

Créez la classe `Humain` :

```java
package ca.cegepdrummond;
public class Humain extends Personnage {
    public Humain(String nom) {
        super(nom, 10, 10, 10, 10, 10, 10);
    }

    public void afficher() {
        System.out.println("Je suis un humain!");
        super.afficher();
    }
}
```

Créez la classe `Elfe` :

```java
package ca.cegepdrummond;
public class Elfe extends Personnage {
    public Elfe(String nom) {
        super(nom, 10, 10, 8, 12, 10, 10);
    }

    public void afficher() {
        System.out.println("Je suis un elfe!");
        super.afficher();
    }
}
```

Dans la classe `Main` :

```java
package ca.cegepdrummond;
public class Programme {
    public static void main(String[] args) {
        Personnage[] personnages = {
                new Humain("Bob"),
                new Humain("Randall"),
                new Elfe("Lyonel"),
        };

        System.out.println("Voici les fiches des personnages :");

        for (Personnage personnage : personnages) {
            personnage.setForce(personnage.getForce() + 2);
            personnage.afficher();
        }
    }
}

```

Notez de l'utilisation de l'instruction `super` qui permet de faire la même chose que `this`, mais pour une classe parente.

https://docs.oracle.com/javase/tutorial/java/IandI/super.html

On utilise le mot réservé `super` pour appeler une méthode définie dans une classe dont on hérite (noter qu'en Java, on hérite de plusieurs classes en ligne droite, pas d'héritage multiple). On doit donc appeler un constructeur d'une superclasse en utilisant `super()` au lieu de `this()`. 

Notez aussi que je peux avoir une liste de personnages de différents types, puisque ce sont tous des personnages.

Notez aussi que je peux redéfinir des fonctionnalités (`afficher()`) ou utiliser des fonctionnalités du parent sans la redéfinir dans l'enfant (`setForce()` et `getForce()`)

