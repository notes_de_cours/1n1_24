# Constructeur

Le constructeur est une méthode spéciale. Il est appelé lors du `new`. Il permet de créer une instance de cette classe. Il sert habituellement à mettre des valeurs dans les attributs. 

:::info Définition
Le constructeur se distingue par le fait que la **méthode porte le nom de la classe** et aussi par le fait que la méthode n'a **pas de type de retour** (même pas void). 
:::

:::warning
Nous allons fréquemment parler du constructeur ... rappelez-vous de cette définition
:::

On peut donc définir un constructeur pour définir les arguments lors du `new`.

Par défaut, il existe un constructeur sans argument qui ne fait rien à part mettre des valeurs par défaut dans les attributs. Si on définit un nouveau constructeur, celui par défaut n'est plus utilisable. 

Nous allons définir un constructeur pour Personnage qui prendra le nom de la personne. 

Dans la classe **Personnage** :

<pre>
public class Personnage {
    public String nom;
</pre>

```java    
    public Personnage(String aNom) { // #1
        nom = aNom;
    }
```

Étant donné que nous avons définit un constructeur qui requiert le nom du personnage, le constructeur par défaut n'est plus valide. Il faut donc mettre à jour la classe Programme. 

Dans la classe **Programme** remplacez la création des 2 personnages par le code suivant :

<pre>
public class Programme {
    public static void main(String[] args) {
</pre>

<div class="no-copy">
 
:::info enlevez ces 4 lignes
``` 
    Personnage personnage1 = new Personnage();
    Personnage personnage2 = new Personnage();
    personnage1.nom = "Bob";
    personnage2.nom = "Randall";
```
:::
</div>

:::info remplacez les par celles-ci
```java 
        Personnage personnage1 = new Personnage("Bob");
        Personnage personnage2 = new Personnage("Randall");
```
:::

<pre>
        personnage1.afficher();
        personnage2.afficher();
    }
}
</pre>


## Multiple constructeurs

En Java, il est possible d'avoir plusieurs constructeurs avec des `signatures` différentes (c.-à-d. la liste des arguments de la fonction). 

Nous allons en ajouter un qui ne prend aucun argument.


<pre>
public class Personnage {
    public String nom;
    
    public Personnage(String aNom) { // #1
        nom = aNom;
    }
</pre>

```java    
    public Personnage() {  // #2
        this("nom inconnu");
    }
```



Ainsi, je peux décider de créer un `Personnage` sans spécifier son nom. 

Notez l'utilisation de `this` pour appeler un constructeur déjà fait. La `signature` (liste d'arguments) de l'appel sert à déterminer quelle constructeur appeler. 

Ici, le constructeur #2 appel le constructeur #1 puisqu'il passe en paramètre une String. 

Dans la classe `Programme` ajoutons un exemple d'utilisation de ce second constructeur:

<pre>
public class Programme {
    public static void main(String[] args) {
        Personnage personnage1 = new Personnage("Bob");// constructeur #1
        Personnage personnage2 = new Personnage("Randall"); // constructeur #1
</pre>
```java
        Personnage personnage3 = new Personnage(); // appel le constructeur #2
```
<pre>
        personnage1.afficher();
        personnage2.afficher();
</pre>
```java
        personnage3.afficher();
```
        
<pre>
    }
}
</pre>

Il est possible d'avoir autant de constructeur que l'on veut... tant qu'ils ont une signature différente.

Par exemple, ajoutons un autre attribut à la classe:

<pre>
public class Personnage {
    public String nom;
</pre>
```java
    public int force;
```

Remplacez les 2 constructeurs que nous avons par ceux-ci:


```java
    public Personnage(String aNom) { // #1
        this(aNom, 0); // appel #4
    }

    public Personnage() {  // #2
        this("nom inconnu"); // appel #1
    }
    public Personnage(int aForce) { // #3
        this("nom inconnu avec force", aForce); // appel #4
    }

    public Personnage(String aNom, int aForce) { // #4
        nom = aNom;
        force = aForce;
    }

```

Comme un peut le voir, les constructeurs s'appelent en cascade, du plus simple (#2) au plus complet (#4)
