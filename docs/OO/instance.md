# Instance

Si une classe est une usine ou un moule à objet, alors un objet est le produit d'une classe.

Lorsqu'on fait **Personnage toto = new Personnage()** on obtient une `instance` de la classe Personnage qui s'appelle toto. 


## Attributs

Un attribut est une variable qui n'existe qu'à l'intérieur d'une instance (à l'exception des variables static, voir la section plus loin) 

Les attributs sont aussi appelés `champs`

Un attribut appartient à une instance. Chaque fois qu'on fait un `new`, et donc qu'on crée une instance, de l'espace mémoire est réservé pour chacun des attributs de la classe. 

Le fait de changer la valeur d'un attribut dans une instance n'a aucun effet sur les attributs des autres instances. (à part pour un attribut static, mais on ne l'utilisera pas dans le cadre du cours ... et ce n'est pas recommandé de le faire)

Les instances ont leurs propres valeurs pour les attributs. Dans l'exemple, le seul attribut de Personnage est son `nom`. Chaque instance (personnage1 et personnage2) ont leur propre valeur pour le nom. On dit que la valeur de ses attributs détermine  l'`état` d'un objet . 

 
## Méthodes

Dans une classe je peux avoir des données (`attributs`) et des fonctionnalités (`méthodes`).

Par exemple, je peux définir comment un personnage doit s'afficher.

Dans la classe **Personnage** , ajoutez cette fonction :

<pre>
public class Personnage {  
     public String nom;
</pre>


```java
    public void afficher() {
        System.out.println("Nom: " + nom);
    }
```

Dans la classe `Programme` : 

**REMPLACEZ** les 2 `System.out.println()` de la fonction `main()` par ce code. 

<pre>
public class Programme {  
    public static void main(String[] args) {  
        Personnage personnage1 = new Personnage();   
        Personnage personnage2 = new Personnage();   
        personnage1.nom = "Bob";  
        personnage2.nom = "Randall";  
</pre>

```java
        personnage1.afficher();
        personnage2.afficher();
    
```

Ici, chaque personnage affiche son propre nom.

Même si la même méthode est appelée 2 fois sans paramètre, 2 résultats différents seront produits puisque ce sont 2 instances différentes qui ont chacune leur ensemble d'attributs (leur **état**).

Les méthodes sont communes pour toutes les instances d'une classe. On dit que les méthodes déterminent le **comportement** de l'objet. Ce comportement dépend de l'état de l'objet. 

Exemple: Si un personnage n'a plus de force, on ne peut la diminuer. Par contre, on peut diminuer la force de ceux qui en ont encore.


## This

Le mot réservé `this` représente l'instance elle-même. On utilise `this` dans les 3 circonstances suivantes:

https://docs.oracle.com/javase/tutorial/java/javaOO/thiskey.html

1. La méthode qu'on veut appeler prend un objet en paramètre, et l'objet que l'on veut passer en paramètre est l'objet qui s'exécute présentement. (Nous en avons vue un exemple dans la méthode `verifierPourEnlever()` de Asteroide en Greenfoot ) 

2. Un paramètre a le même nom qu'une variable d'instance. Pour différencier les deux, on utilise `this.nomDeLaVariable = nomDeLaVariable`  (voir l'exemple ci-dessous)

3. Pour appeler en cascade un autre constructeur (que nous verrons plus loin) de la même classe.


### exemple d'utilisation inutile

Vous trouverez probablement souvent du code comme celui-ci:

```java
    public void afficher() {
        System.out.println("Nom: " + this.nom);
    }
```

Ici, l'utilisation de `this.nom` est totalement superflue. Vous pouvez la remplacer par `nom` tout simplement. 

### exemple d'utilisation pouvant être remplacée

Voici un autre exemple pour lequel l'utilisation du mot réservé `this` semble nécessaire. 

Supposons que nous avons ce code:

```java
Personnage toto = new Personnage();
toto.nom = "Bob";

```
et qu'un peu plus loin, on veuille vérifier si le nom de toto est "Georges". 

```java
if(toto.seNomme("Georges"))
```

Et que la fonction `seNomme` est définie comme suit:

```java
    public boolean seNomme(String nom) {
        return this.nom.equals(nom);
    }
}
```

Ici, `this.nom` fait référence à l'attribut `public String nom` de toto.

Le `nom` dans le `equals` fait référence à l'argument `nom` qui a été passé en paramètre.

L'utilisation de `this` semble donc nécessaire afin de différencier ces 2 noms!!!

Mais bien entendu, nous préférons l'utilisation des préfixes. Ce code peut donc être réécrit comme suit:

```java
    public boolean seNomme(String aNom) {
        return nom.equals(aNom);
    }
}
```

Éliminant ainsi toute ambiguité. 

:::danger 
Dans le cadre de ce cours, vous **devez** utiliser le préfixe `a`. Vous ne devriez donc pas avoir souvent besoin de `this`.  
:warning: Vous perdrez des points si vous n'utilisez pas le préfixe. :warning:
:::

Le mot réservé `this` ne devrait donc être utilisé uniquement dans un constructeur (comme nous allons le voir), ou lors de l'appel d'une fonction nécessitant l'instance qui est en court d'exécution (que nous utiliserons avec Greenfoot) ou dans les setters générés par Intellij (que nous verrons plus tard)


## Résumé 

:::info Instance
En [programmation orientée objet](https://fr.wikipedia.org/wiki/Programmation_orientée_objet), on appelle **instance** d'une [classe](https://fr.wikipedia.org/wiki/Classe_(informatique)), un [objet](https://fr.wikipedia.org/wiki/Objet_(informatique)) avec un comportement et un état, tous deux définis par la classe. Il s'agit donc d'un objet constituant un exemplaire de la classe. 
:::
:::info Instanciation 
L'action d'instancier, de créer un objet à partir d'un modèle. Elle est réalisée par la composition de deux opérations : l'allocation et l'initialisation. L'allocation consiste à réserver un espace mémoire au nouvel objet. L'initialisation consiste à fixer l'état du nouvel objet. Cette opération fait par exemple appel à l'un des [constructeurs](https://fr.wikipedia.org/wiki/Constructeur_(programmation_informatique)) de la classe de l'objet à créer.

En programmation orientée classe, l'instanciation est la création d'un objet à partir d'une [classe](https://fr.wikipedia.org/wiki/Classe_(informatique)). En [programmation orientée prototype](https://fr.wikipedia.org/wiki/Programmation_orientée_prototype), l'instanciation consiste à créer un nouvel objet à partir d'un objet existant (clonage).

*- Wikipédia : https://fr.wikipedia.org/wiki/Instance_(programmation)*

Donc chaque instance a un état (valeurs des attributs). Lorsque j'appelle des fonctionnalités (méthodes) d'une instance, celles-ci peuvent interagir avec l'état de l'instance.

L'allocation c'est `Personnage personnage1`

L'initialisation c'est `= new Personnage("Bob")`
:::


