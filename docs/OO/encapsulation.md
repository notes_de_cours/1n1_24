# Encapsulation

## définition

L'un des 4 principes fondamentaux de la programmation orientée-objet est l'encapsulation. 

En programmation, l’encapsulation désigne le regroupement de données avec un ensemble de routines qui en permettent la lecture et la manipulation. Ce principe est souvent accompagné du masquage de ces données brutes afin de s’assurer que l’utilisateur ne contourne pas l’interface qui lui est destinée. L’ensemble se considère alors comme une boîte noire ayant un comportement et des propriétés spécifiés.

L’encapsulation est un pilier de la programmation orientée objet, où chaque classe définit des méthodes ou des propriétés pour interagir avec les données membres.

Avantages:

* L'encapsulation permet de modifier les structures de données internes sans modifier l’interface de celle-ci et donc sans pénaliser les utilisateurs. Cette situation arrive fréquemment lorsque l’on veut améliorer l’efficacité (rapidité de traitement) d’une classe ou d’un module, il faut souvent modifier les structures de données en conséquence.
* L'encapsulation permet d’ajouter aisément des règles de validation et des contraintes d’intégrité, par exemple limiter le domaine des valeurs qu’une variable peut prendre (validité) ou vérifier que cette valeur n’entre pas en conflit avec les valeurs d’autres variables (intégrité).
  

[source wikipedia](https://fr.wikipedia.org/wiki/Encapsulation_(programmation))


(les 3 autres étant: l'abstraction, le polymorphisme, et l'héritage)

En java, l'encapsulation est réalisée grace au modificateurs d'accès. Les fonctions ou les attributs sont déclarés `private` afin de restreindre leur accès.

## Encapsulation de Personnage 

Nous allons ajouter les attributs nécessaires à notre classe. 

Notez qu'ils sont `private`. 

Nous verrons comment accéder aux attributs private dans la section suivante.

REMPLACEZ la classe `Personnage` par le code suivant :

```java
package ca.cegepdrummond;

public class Personnage {
    private String nom;
    private int force;
    private int agilite;
    private int constitution;
    private int intelligence;
    private int sagesse;
    private int charisme;

    public Personnage(String aNom, int aForce, int aAgilite, int aConstitution, int aIntelligence, int aSagesse, int aCharisme) {
        nom = aNom;
        force = aForce;
        agilite = aAgilite;
        constitution = aConstitution;
        intelligence = aIntelligence;
        sagesse = aSagesse;
        charisme = aCharisme;
    }

    public void afficher() {
        System.out.println("Nom : " + nom);
        System.out.println("STR : " + force);
        System.out.println("DEX : " + agilite);
        System.out.println("CON : " + constitution);
        System.out.println("INT : " + intelligence);
        System.out.println("WIS : " + sagesse);
        System.out.println("CHA : " + charisme);
    }
}
```

Et REMPLACEZ la classe `Programme` par le code suivant:

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Personnage[] personnages = {
            new Personnage("Bob", 16, 14, 14, 10, 10, 14),
            new Personnage("Randall", 12, 16, 14, 10, 14, 12),
            new Personnage("Lyonel", 10, 14, 8, 18, 12, 14),
        };

        System.out.println("Voici les fiches des personnages :");

        for (Personnage personnage : personnages) {
            personnage.afficher();
        }
    }
}
```


Ici, la boucle `for` est une boucle `for each`. 

:::warning
Notez ici l'utilisation du pluriel dans le nom de la variable `personnageS`. Il est fréquent de mettre le nom d'un tablau au pluriel car il contient `des personnages`. 
:::

Si vous essayez d'afficher directement le nom d'un personnage dans Programme, vous obtiendrez une erreur de compilation.

Essayez ce code dans Programme (ATTENTION IL NE FONCTIONNE PAS ET VOUS DEVREZ L'ENLEVER):

```java
System.out.println(personnages[0].nom);
```

Alors comment accéder aux attributs???

La solution: Getter et Setter...

## Getter et Setter

En Java (et dans la majorité des langages OO), des méthodes spéciales appelées `getter` et `setter` sont utiliser afin de manipuler les attributs `private`.

La méthode `get` **retournera** la valeur de l'attribut. 

La méthode `set` **prendra** une valeur en paramètre et mettra (habituellement) cette valeur dans l'attribut. 

Dans la classe `Personnage` ajoutez les fonctions suivantes à la fin de la classe :

```java
    public String getNom() {
        return nom;
    }

    public void setNom(String aNom) {
        nom = aNom;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int aForce) {
        force = aForce;
    }

    public int getAgilite() {
        return agilite;
    }

    public void setAgilite(int aAgilite) {
        agilite = aAgilite;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int aConstitution) {
        constitution = aConstitution;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int aIntelligence) {
        intelligence = aIntelligence;
    }

    public int getSagesse() {
        return sagesse;
    }

    public void setSagesse(int aSagesse) {
        sagesse = aSagesse;
    }


```

### Pourquoi faire ca?

- Pour garder le contrôle sur les attributs. 
  - pour le get, on peut par exemple retourner une valeur modifiée de l'attribut. 
  - pour le set, on peut vouloir valider que la valeur passée en paramètre est valide. 
- On peut décider qu'une variable est seulement en lecture ou seulement en écriture en ne fournissant que le getter ou le setter

REMPLACEZ setForce par le code suivant:

```java
public void setForce(int aForce) {
    if (aForce > 0 && aForce < 50) {
        force = aForce;
    }
}
```

On peut  mettre un getter, mais pas un setter ou vice-versa.

```java
public String getNom() {
    return nom;
}

/* public void setNom(String aNom) {
    nom = aNom;
}*/
```

:::info

Dans IntelliJ, pour générer les getters et les setters : 

* Mettez votre curseur sur le nom de la classe. 
* Faites Shift, Shift
* Écrivez `Getter and Setter`
* Sélectionnez les attributs sur lesquelles vous voulez générer des getters et des setters
  * ils ne sont présentement pas créés pour le `charisme`. Ajoutez les. 
* Notez que les setter utiliseront la syntaxe avec `this`. Ce cas est accepté. Par contre, si vous modifiez le setter, vous devez préfixer le nom de l'argument, et enlever l'utilisation de `this` pour ce setter. 

Dans Greenfoot, il n'y a pas d'automatisation de la génération des getter / setter :sob:
:::

Il est maintenant possible d'obtenir la valeur du champs `nom` grace au getter. 

Essayez ce code dans main():

```java
Console.afficher(personnages[0].getNom());
```
