# Exercice de création d'une classe en OO

Pour cet exercice, vous devez créer un classe qui répond à la spécification suivante:

Vous devez avoir une classe Chien
* Un chien a: 
  * un nom (String), 
  * un sexe ('M' ou 'F'), 
  * une espèce (String), 
  * et un prix (int pour ne pas avoir d'erreur d'arrondissement)
* Il est possible de créer un chien en spécifiant tous les paramètres.
* Il est aussi possible de créer un chien en ne spécifiant que le nom et le sexe. Dans ce cas, l'espèce sera "batard" et le prix sera 0. 
* Pour le prix, lors de la création, la fonction appelante spécifie un montant en dollar et cents (double 150.99). Mais dans la classe, ce prix doit être entreposé dans un nombre entier. Vous devez donc convertir un double en integer en le multipliant par 100. 
* Une fois créé, il n'est plus possible de changer l'espèce ni le sexe. 
* La classe chien fournie les services suivants:
  * afficher toutes les informations sur ce chien
  * modifier le nom
  * retourner seulement le nom (non modifiable)
  * retourner le sexe
  * retourner seulement le prix en double (non modifiable)
  * retourner seulement l'espèce (non modifiable)
  * modifier le prix (recoit un double)
* le sexe est validé. Seul les valeurs 'M' ou 'm' ou 'F' ou 'f' sont valides. Si une autre valeur est spécifiée, alors c'est 'M' qui sera utilisé. La valeur est entreposée en majuscule. 



Le programme principal doit 

* Créer 3 chiens et les afficher
* Afficher le prix total des 3 chiens en allant chercher le prix pour chaque chien. 
* Changer le prix du premier chien. 



:::details Solution classe Chien

```java
package ca.cegepdrummond;

public class Chien {
    private String nom;
    private char sexe;
    private String espece;
    private int prix;

    public Chien(String aNom, char aSexe, String aEspece, double aPrix) {
        nom = aNom;
        aSexe = Character.toUpperCase(aSexe);
        if( aSexe == 'F') {
            sexe = 'F';
        } else {
            sexe = 'M';
        }
        espece = aEspece;
        prix =(int) (aPrix*100);
    }

    public Chien(String aNom, char aSexe) {
        this(aNom, aSexe, "batard", 0);
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String aNom) {
        nom = aNom;
    }

    public void afficher() {
        System.out.println("nom: "+nom);
        System.out.println("sexe: "+sexe);
        System.out.println("espece: "+espece);
        System.out.println("prix: "+(prix/100.0));

    }
    public char getSexe() {
        return sexe;
    }
    public double getPrix() {
        return prix/100.0;
    }
    public void setPrix(double aPrix) {
        prix =(int) (aPrix*100);
    }
    public String getEspece() {
        return espece;
    }
}

```

:::

:::details Solution Programme

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Chien[] chiens = {
                new Chien("papoute", 'M', "cocker", 0.1),
                new Chien("Chouka", 'z'),
                new Chien("Romy", 'f', "labrador", 1000000)
        };

        chiens[0].setPrix(44.99);

        for(Chien pitou: chiens) {
            pitou.afficher();
        }
        double total = 0;
        for(Chien pitou:chiens) {
            total+= pitou.getPrix();
        }
        System.out.println(total);
    }
}

```

:::
