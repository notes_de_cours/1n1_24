# Classe

Une classe permet de définir un nouveau type. Le but étant de définir les données pouvant être enregistrées dans ce type et quelles seront les fonctionnalités offertes par ce type.

## Création d'une classe

Créez un nouveau projet 

Créez une nouvelle classe à côté du fichier  `Programme` et nommez cette classe `Personnage`

 ![image-20211103161213232](/image-20211103161213232.png)

 ![image-20211103161243666](/image-20211103161243666.png)

Vous pouvez maintenant naviguer entre les 2 classes en utilisant l'arborescence de votre projet.

 ![image-20211103161332461](/image-20211103161332461.png)

:::warning
Le nom des classes doit être en PascalCase, c'est-à-dire, que la première lettre de chaque mot doit être en majuscule.
:::

En programmation orientée-objet, la déclaration d'une classe regroupe des méthodes (fonctions) et propriétés (attributs, variables) communes à un ensemble d'objets. On utilise parfois le terme **membres** afin de parler de ces méthodes et propriétés.

La classe déclare, d'une part, des attributs représentant l'état des objets et, d'autre part, des méthodes représentants leur comportement.
:::info Définition
Une classe représente donc une **catégorie d'objets**. Elle apparaît aussi comme un **moule** ou une **usine** à partir de laquelle il est possible de **créer des objets** ; c'est en quelque sorte une « boîte à outils » qui permet de fabriquer un objet. On parle alors d'un objet en tant **qu'instance** d'une classe (création d'un objet ayant les propriétés de la classe). 

[https://fr.wikipedia.org/wiki/Classe_(informatique) ]
:::

## première utilisation

Remplacez le code de la classe `Personnage` :

```java
package ca.cegepdrummond;

public class Personnage {
    public String nom;
}

```


Ajoutez ce code dans la classe `Programme`  : 


<pre>
public class Programme {  
    public static void main(String[] args) {
</pre>
```java
        Personnage personnage1 = new Personnage();
        Personnage personnage2 = new Personnage();
        personnage1.nom = "Bob";
        personnage2.nom = "Randall";

        System.out.println(personnage1.nom);
        System.out.println(personnage2.nom);
```


Essayez ce code...

Comme vous pouvez le voir, chaque personnage a son propre nom.

De plus, comme pour le `Scanner` ou `String`, je dois faire un `new` pour créer un nouveau personnage.

