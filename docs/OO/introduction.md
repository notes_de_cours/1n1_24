# Objets en Java

Ce cours se veut une introduction rapide à l'Orienté Objet (OO). Plusieurs concepts seront vus très rapidement. Ne vous en faites donc pas si vous ne comprenez pas tout. Vous aurez plusieurs autres cours qui se concentreront sur ces concepts. 

## Introduction

:::info Paradigme de programmation : 

Un **paradigme de programmation** est une façon d'approcher la [programmation informatique](https://fr.wikipedia.org/wiki/Programmation_informatique) et de traiter les solutions aux problèmes et leur formulation dans un [langage de programmation](https://fr.wikipedia.org/wiki/Langage_de_programmation) approprié. Il s'oppose à la [méthodologie](https://fr.wikipedia.org/wiki/Méthodologie), qui est une manière d'organiser la solution des problèmes spécifiques du [génie logiciel](https://fr.wikipedia.org/wiki/Génie_logiciel).

*- Wikipédia : https://fr.wikipedia.org/wiki/Paradigme_(programmation)*
:::

Il existe donc plusieurs paradigmes en programmation : 

* Programmation impérative (par instructions séquentielles)
  * Structurée (sans goto : if else, while, for, etc.)
  * Procédurale (contenant des procédures (fonction void))
  * Orientée objet (séparation du code en objets)
    * Orientée prototype (permet de créer des objets sans faire de classe (ex.: Javascript))
    * Orientée classe (un objet doit respecter la définition d'une classe (ex.: Java))
* Programmation déclarative (je dis ce que je veux, et ça le fait)
  * Fonctionnelle (déclaration de fonctions mathématiques pouvant être exécutées pour obtenir un résultat (ex.: Lisp ou Javascript))
  * Descriptive (ex.: HTML)
* Métaprogrammation
  * Macros (ex.: les macros d'Excel)
* Etc.






La programmation orientée-objet est donc une façon de structurer notre programmation afin de rendre notre programme plus lisible, facile à développer et facile à maintenir. La programmation orientée-objet consiste à séparer notre programme en plusieurs objets.



Voici un exemple d'un programme qui possède une liste de personnages avec des noms et des abilités sans utiliser la programmation orientée-objet : 

```java
public class Main {
    public static void main(String[] args) {
        String[] noms = {"Bob", "Randall", "Lyonel"};
        int[] forces = {16, 12, 10};
        int[] agilites = {14, 16, 14};
        int[] constitutions = {14, 14, 8};
        int[] intelligences = {10, 10, 18};
        int[] sagesses = {10, 14, 12};
        int[] charismes = {14, 12, 14};

        System.out.println("Voici les fiches des personnages :");

        for (int i = 0; i < noms.length; i++) {
            System.out.println("Nom : " + noms[i]);
            System.out.println("STR : " + forces[i]);
            System.out.println("DEX : " + agilites[i]);
            System.out.println("CON : " + constitutions[i]);
            System.out.println("INT : " + intelligences[i]);
            System.out.println("WIS : " + sagesses[i]);
            System.out.println("CHA : " + charismes[i]);
        }
    }
}
```

Voici le même exemple en utilisant la programmation orientée-objet (Ne le copiez pas pour l'instant, nous allons le reconstruire étape par étape) :

Personnage.java

```java
public class Personnage {
    private String nom;
    private int force;
    private int agilite;
    private int constitution;
    private int intelligence;
    private int sagesse;
    private int charisme;

    public Personnage(String aNom, int aForce, int aAgilite, int aConstitution, int aIntelligence, int aSagesse, int aCharisme) {
        nom = aNom;
        force = aForce;
        agilite = aAgilite;
        constitution = aConstitution;
        intelligence = aIntelligence;
        sagesse = aSagesse;
        charisme = aCharisme;
    }

    public void afficher() {
        System.out.println("Nom : " + nom);
        System.out.println("STR : " + force);
        System.out.println("DEX : " + agilite);
        System.out.println("CON : " + constitution);
        System.out.println("INT : " + intelligence);
        System.out.println("WIS : " + sagesse);
        System.out.println("CHA : " + charisme);
    }
}
```

Programme.java

```java
public class Programme {
    public static void main(String[] args) {
        Personnage[] personnages = {
            new Personnage("Bob", 16, 14, 14, 10, 10, 14),
            new Personnage("Randall", 12, 16, 14, 10, 14, 12),
            new Personnage("Lyonel", 10, 14, 8, 18, 12, 14),
        };

        System.out.println("Voici les fiches des personnages :");

        for (Personnage personnage : personnages) {
            personnage.afficher();
        }
    }
}
```








