# Modificateur d'accès

On peut spécifier des modificateurs d'accès pour contrôler l'accès aux membres (attribut ou méthode) d'une classe.

* `public`
  * accessible par l'utilisateur de la classe (dans Programme par exemple)
* `protected`
  * accessible par les enfants de la classe ou les classes du package (on va voir ça plus tard)
* `private`
  * accessible seulement à l'intérieur de la classe

Comme nous le verrons,  `private` sera favorisé devant les attributs. Des méthodes seront utilisées pour manipuler ces attributs private. (Voir Getter et Setter)

De plus, on peut vouloir cacher des méthodes qui servent à la mécanique interne de la classe, mais pas à l'utilisateur de la classe. Dans ce cas, les méthodes seront `protected` ou `private`. Nous n'utiliserons pas cette technique dans le cadre du cours. Mais vous le verrez plus en détails dans les cours d'OO. 



## Modificateur supplémentaire

Les modificateur que nous venons de voir permettre de restreindre l'accès aux fonctions. 

Le modificateur `static` permet d'indiquer que le membre (fonction ou attribut) appartient à la classe et non à l'instance. 

Il peut être utilisé pour créer des fonctions qui ne sont pas dépendantes des attributs d'instance. On ne peut donc pas utiliser les attributes d'instance dans une fonction static. Cela fait du sens puisque ces attributs sont associés à une instance en particulier. On peut par contre utiliser des attributs qui ont eux même été définis static. Mais dans le cadre du cours, vous ne devriez pas vous en servir (c'est pratiquement une variable globale, et ca peut facilement causer des problèmes)

Notez que `static` va après les autres modificateurs 

Ex:

```java
public static int calculerQQchose() {}

```
