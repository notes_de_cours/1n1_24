# Exercice de création d'une classe en OO

Pour cet exercice, vous devez créer un classe qui répond à la spécification suivante:

## La classe Chien
1) Un chien a: 
    * un nom (String), 
    * un sexe ('M' ou 'F'), 
    * une espèce (String), 
    * et un prix (int pour ne pas avoir d'erreur d'arrondissement)
1) Il y a deux constructeurs:
    * Il est possible de créer un chien en spécifiant tous les paramètres.
        * le sexe est validé. Seul les valeurs 'M' ou 'm' ou 'F' ou 'f' sont valides. Si une autre valeur est spécifiée, alors c'est 'M' qui sera utilisé. La valeur est entreposée en majuscule. 
        * Pour le prix, lors de la création, la fonction appelante spécifie un montant en dollar et cents (double 150.99). Mais dans la classe, ce prix doit être entreposé dans un nombre entier. Vous devez donc convertir un double en integer en le multipliant par 100. 
    * Il est aussi possible de créer un chien en ne spécifiant que le nom et le sexe. Dans ce cas, l'espèce sera "batard" et le prix sera 0. 
1) La classe chien fournie les services suivants:
    * afficher toutes les informations sur ce chien
    * modifier le nom
    * retourner le nom 
    * retourner le sexe (non modifiable)
    * retourner le prix en double 
    * modifier le prix (recoit un double)
    * retourner l'espèce (non modifiable)



## Le programme principal

* Créer 3 chiens et les afficher
* Changer le prix du premier chien. 
* Afficher le prix total des 3 chiens en allant chercher le prix pour chaque chien. 


## Étape 1: les attributs

Créez un nouveau projet. 

Ajoutez la classe `Chien`

Ajoutez les attributs

:::details solution
```java
package ca.cegepdrummond;

public class Chien {
    private String nom;
    private char sexe;
    private String espece;
    private int prix;
}
```
:::

## Étape 2: les constructeurs

### le construteur complet

Il doit permettre de spécifier tous les paramètres.

Dans un premier temps, assignez directement les arguments aux attributs associés.

:::details Constructeur 1
```java
public Chien(String aNom, char aSexe, String aEspece, double aPrix) {
    nom = aNom;
    sexe = aSexe;
    espece = aEspece;
    prix = aPrix; // donnera une erreur que nous allons corriger
}
```
:::

#### la validation du sexe

Il faut modifier l'assignation du sexe pour répondre à la spécification.

:::details Constructeur 1 modifié pour sexe
```java
public Chien(String aNom, char aSexe, String aEspece, double aPrix) {
    nom = aNom;
    aSexe = Character.toUpperCase(aSexe); // [!code highlight:6]
        if( aSexe == 'F') {
            sexe = 'F';
        } else {
            sexe = 'M';
        }
    espece = aEspece;
    prix = aPrix;
}
```
:::


#### conversion du prix
Le prix est spécifié en `double`, mais nous désirons l'entreposer en `int`

:::details Constructeur 1 modifié pour le prix
```java
public Chien(String aNom, char aSexe, String aEspece, double aPrix) {
        nom = aNom;
        aSexe = Character.toUpperCase(aSexe);
        if( aSexe == 'F') {
            sexe = 'F';
        } else {
            sexe = 'M';
        }
        espece = aEspece;
        prix =(int) (aPrix*100); // [!code highlight]
    }
```
:::


### le construteur partiel

Ce deuxième constructeur ne prend que le nom et le sexe en paramètre et fourni des valeurs par défaut au constructeur complet.

:::details Constructeur partiel
```java
public Chien(String aNom, char aSexe) {
    this(aNom, aSexe, "batard", 0);
}
```
:::

## Les getter et setter

Il faut maintenant fournir des services via des méthodes et des getter et des setter. 

### affichage de toute l'information

Ajoutez une méthode qui affichera tous les attributs d'un chien.

:::details afficher()
```java
public void afficher() {
    System.out.println("nom: "+nom);
    System.out.println("sexe: "+sexe);
    System.out.println("espece: "+espece);
    System.out.println("prix: "+(prix/100.0));

}
```
:::

### le nom

Besoin d'un getter et setter

:::details nom getter et setter
```java
public String getNom() {
    return nom;
}
public void setNom(String aNom) {
    nom = aNom;
}
```
:::

### le sexe 

Le sexe ne peut être modifié une fois le chien créé. 

:::details sexe getter
```java
public char getSexe() {
    return sexe;
}
```
:::


### le prix

Le prix peut être affiché et modifié. Un valeur `double` est fournie lors de la modification, mais doit être entreposer en `int` 

:::details prix getter et setter
```java
public double getPrix() {
    return prix/100.0;
}
public void setPrix(double aPrix) {
    prix =(int) (aPrix*100);
}
```
:::


## Le programme principal

Codez le programme principal afin de réponde aux requis ci-haut. 

La solution est dans la solution complète.



## Solution complète
:::details Solution classe Chien

```java
package ca.cegepdrummond;

public class Chien {
    private String nom;
    private char sexe;
    private String espece;
    private int prix;

    public Chien(String aNom, char aSexe, String aEspece, double aPrix) {
        nom = aNom;
        aSexe = Character.toUpperCase(aSexe);
        if( aSexe == 'F') {
            sexe = 'F';
        } else {
            sexe = 'M';
        }
        espece = aEspece;
        prix =(int) (aPrix*100);
    }

    public Chien(String aNom, char aSexe) {
        this(aNom, aSexe, "batard", 0);
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String aNom) {
        nom = aNom;
    }

    public void afficher() {
        System.out.println("nom: "+nom);
        System.out.println("sexe: "+sexe);
        System.out.println("espece: "+espece);
        System.out.println("prix: "+(prix/100.0));

    }
    public char getSexe() {
        return sexe;
    }
    public double getPrix() {
        return prix/100.0;
    }
    public void setPrix(double aPrix) {
        prix =(int) (aPrix*100);
    }
    public String getEspece() {
        return espece;
    }
}

```

:::

:::details Solution Programme

```java
package ca.cegepdrummond;

public class Programme {
    public static void main(String[] args) {
        Chien[] chiens = {
                new Chien("papoute", 'M', "cocker", 0.1),
                new Chien("Chouka", 'z'),
                new Chien("Romy", 'f', "labrador", 1000000)
        };

        chiens[0].setPrix(44.99);

        for(Chien pitou: chiens) {
            pitou.afficher();
        }
        double total = 0;
        for(Chien pitou:chiens) {
            total+= pitou.getPrix();
        }
        System.out.println(total);
    }
}

```

:::
