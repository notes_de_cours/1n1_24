package ca.cegepdrummond;

// #region commentaires
// Ce programme est votre premier exemple de code Java 
// Il vous permettra de vous familiariser avec la terminologie

/*
   Par exemple, vous voyez ici la différence entre des commentaires de fin de ligne (ci-haut),
    et ce commentaires multilignes
 */
// #endregion commentaires

public class Programme {
    // #region commentaire_fonction
    /**
     * La fonction principale du programme 
     * Ceci est un exemple de commentaire de fonction
     * @param args Arguments envoyés à l'application
     */
    // #endregion commentaire_fonction

    public static void main(String[] args) {
        System.out.println("Premier programme");

        // #region declaration_variable
        int a; // une variable de type entier, ayant un nom non-significatif
        int nombreDeCaracteres; // une variable de type entier, ayant un nom significatif
        char premierCaractere; // une variable de type caractère
        String nomDuProf; // une variable de type chaine de caractères
        // #endregion declaration_variable

        // #region declaration_constante
        final String UNE_CONSTANTE = "cette valeur ne changera pas durant l'exécution du programme";
        // #endregion declaration_constante

        // #region assignation_variable
        a = 10;
        nomDuProf = "Benoit";
        premierCaractere = nomDuProf.charAt(0); //utilisation d'une fonction qui retourne le premier caractère
        nombreDeCaracteres = nomDuProf.length(); //et une autre fonction qui calcule la longueur de la String
        // #endregion assignation_variable


        // #region utilisation_variable
        System.out.println("Affichage avec System.out.println()");
        System.out.print("La valeur de a: ");
        System.out.println(a);

        System.out.print("Le premier caractère de nomDuProf: ");
        System.out.println(premierCaractere);

        System.out.print("Le nombre de caractères de nomDuProf: ");
        System.out.println(nombreDeCaracteres);

        System.out.print("Le contenu de nomDuProf: ");
        System.out.println(nomDuProf);

        System.out.print("Et la constante: ");
        System.out.println(UNE_CONSTANTE);
        // #endregion utilisation_variable

        // #region intro_console
        Console.afficher("Affichage avec Console.afficher()");
        Console.afficher("La valeur de a: " + a);
        Console.afficher("Le premier caractère de nomDuProf: " + premierCaractere);

        Console.afficher("Le nombre de caractères de nomDuProf: " + nombreDeCaracteres);

        Console.afficher("Le contenu de nomDuProf: " + nomDuProf);

        Console.afficher("Et la constante: " + UNE_CONSTANTE);
        // #endregion intro_console

    }
}