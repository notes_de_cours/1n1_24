package ca.cegepdrummond;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de créer un menu en console plus rapidement et facilement
 * <p>
 * Ex. :
 * <pre>{@code
 * public class Programme {
 *     public static void main(String args[]){
 *         Menu menu = new Menu("Menu principal");
 *         menu.ajouterOption("Option 1", Programme::option1);
 *         menu.ajouterOption("Option 2", Programme::option2);
 *         menu.executer();
 *     }
 *
 *     private static void option1() {
 *         Console.afficher("Ceci est l'option1");
 *     }
 *
 *     private static void option2() {
 *         Console.afficher("Ceci est l'option2");
 *     }
 * }
 * }</pre>
 */
public class Menu {
    private final List<ItemMenu> menu;
    private final String titre;
    private final String texteQuitter;

    /**
     * Permet de créer un menu en spécifiant le titre, mais aussi en spécifiant
     * le texte pour l'option 0. Utile si nous faisons un sous-menu pour écrire
     * "Retour" au lieu de texte par défaut "Quitter".
     * @param titre Titre du menu
     * @param texteQuitter Texte pour l'option 0
     */
    public Menu(String titre, String texteQuitter) {
        this.titre = titre;
        this.menu = new ArrayList<>();
        this.texteQuitter = texteQuitter;
    }

    /**
     * Permet de créer un menu (habituellement le menu principal), puisque
     * le texte pour l'option 0 est "Quitter").
     * @param titre Titre du menu
     */
    public Menu(String titre) {
        this(titre, "Quitter");
    }

    /**
     * Permet d'ajouter une option au menu en commençant par l'option 1.
     * @param titre Titre de l'option à afficher dans le menu
     * @param fonction Adresse de la fonction à exécuter lorsque l'option est sélectionnée
     */
    public  void ajouterOption(String titre, Runnable fonction) {
        menu.add(new ItemMenu(titre, fonction));
    }

    /**
     * Une fois que le menu est configuré (options ajoutées), vous pouvez l'exécuter
     */
    public void executer() {
        int choix = -1;
        while (choix != 0) {
            afficher();
            choix = Console.demanderEntier();
            if (choix < 0 || choix > menu.size()) {
                Console.afficher("Choix invalide!");
            }
            else if (choix > 0) {
                menu.get(choix-1).getFonction().run();
            }
        }
    }

    private void afficher() {
        Console.afficher("=== " + titre + " ===");
        for (int i = 0; i < menu.size(); i++) {
            Console.afficher((i+1) + " - " + menu.get(i).getTitre());
        }
        Console.afficher("0 - " + texteQuitter);
        System.out.print("Votre choix : ");
    }
}
