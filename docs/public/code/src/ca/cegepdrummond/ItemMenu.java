package ca.cegepdrummond;

/**
 * Permet de créer une option dans un menu
 * @see Menu
 */
public class ItemMenu {
    private String titre;
    private Runnable fonction;

    /**
     * Créer un item dans le menu avec son titre et la fonction qu'il doit
     * exécuter lorsque l'option est sélectionnée
     * @param titre Titre de l'option
     * @param fonction Fonction à exécuter
     */
    public ItemMenu(String titre, Runnable fonction) {
        this.titre = titre;
        this.fonction = fonction;
    }

    /**
     * Obtient le titre de l'option
     * @return le titre de l'option
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Configure le titre de l'option
     * @param titre le titre de l'option
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Obtient la fonction à exécuter lorsque l'option est sélectionnée
     * @return la fonction à exécuter lorsque l'option est sélectionnée
     */
    public Runnable getFonction() {
        return fonction;
    }

    /**
     * Configure la fonction à exécuter lorsque l'option est sélectionnée
     * @param fonction la fonction à exécuter lorsque l'option est sélectionnée
     */
    public void setFonction(Runnable fonction) {
        this.fonction = fonction;
    }
}