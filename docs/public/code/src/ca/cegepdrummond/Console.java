package ca.cegepdrummond;

import java.util.Scanner;

/**
 * Classe permettant d'utiliser la console plus facilement.
 * <p>
 * Remplace l'utilisation de System.out.println pour afficher
 * du texte en console.
 * <p>
 * Remplace l'utilisation de java.util.Scanner pour lire
 * les entrées de l'utilisateur sur la console.
 */
public class Console {
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Permet d'afficher une chaîne de caractères
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher("Hello World!");
     * }
     * }</pre>
     * @param message chaîne de caractères à afficher
     */
    public static void afficher(String message) {
        System.out.println(message);
    }

    /**
     * Permet d'afficher un nombre entier
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher(1);
     * }
     * }</pre>
     * @param nombre nombre entier à afficher
     */
    public static void afficher(int nombre) {
        System.out.println(nombre);
    }

    /**
     * Permet d'afficher un nombre entier long
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher(1L);
     * }
     * }</pre>
     * @param nombre nombre entier long à afficher
     */
    public static void afficher(long nombre) {
        System.out.println(nombre);
    }

    /**
     * Permet d'afficher un nombre à virgule
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher(1.0);
     * }
     * }</pre>
     * @param nombreAVirgule nombre à virgule à afficher
     */
    public static void afficher(double nombreAVirgule) {
        System.out.println(nombreAVirgule);
    }

    /**
     * Permet d'afficher un caractère
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher('a');
     * }
     * }</pre>
     * @param caractere caractère à afficher
     */
    public static void afficher(char caractere) {
        System.out.println(caractere);
    }

    /**
     * Permet d'afficher un booléen (true ou false)
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher(1 < 2); // affiche true
     * }
     * }</pre>
     * @param booleen booléen à afficher
     */
    public static void afficher(boolean booleen) {
        System.out.println(booleen);
    }

    /**
     * Permet de demander du texte à l'utilisateur
     * <p>
     * Le code va s'arrêter jusqu'à ce que l'utilisateur
     * entre un texte et appuie sur "ENTER".
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher("Quel est votre nom?");
     *     String nom = Console.demanderTexte();
     *     Console.afficher("Votre nom est : " + nom);
     * }
     * }</pre>
     * @return Le texte entré par l'utilisateur sans le saut de ligne
     */
    public static String demanderTexte() {
        return scanner.nextLine();
    }

    /**
     * Permet de demander un nombre entier à l'utilisateur
     * <p>
     * Le code va s'arrêter jusqu'à ce que l'utilisateur
     * entre un texte et appuie sur "ENTER".
     * <p>
     * Si le texte entré n'est pas un nombre valide,
     * la console affichera "Nombre invalide!" et l'utilisateur
     * devra réessayer jusqu'à entrer un nombre valide.
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher("Quel est votre âge?");
     *     int age = Console.demanderEntier();
     *     Console.afficher("Votre âge est : " + age);
     * }
     * }</pre>
     * @return Le nombre valide entré par l'utilisateur
     */
    public static int demanderEntier() {
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Nombre invalide!");
            return demanderEntier();
        }
    }

    /**
     * Permet de demander un nombre à virgule à l'utilisateur
     * <p>
     * Le code va s'arrêter jusqu'à ce que l'utilisateur
     * entre un texte et appuie sur "ENTER".
     * <p>
     * Si le texte entré n'est pas un nombre valide,
     * la console affichera "Nombre invalide!" et l'utilisateur
     * devra réessayer jusqu'à entrer un nombre valide.
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher("Quel est votre poids en lbs?");
     *     double poids = Console.demanderNombreAVirgule();
     *     Console.afficher("Votre poids est : " + poids);
     * }
     * }</pre>
     * @return Le nombre valide entré par l'utilisateur
     */
    public static double demanderNombreAVirgule() {
        try {
            return Double.parseDouble(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Nombre invalide!");
            return demanderNombreAVirgule();
        }
    }

    /**
     * Permet de demander oui ou non à l'utilisateur
     * <p>
     * Le code va s'arrêter jusqu'à ce que l'utilisateur
     * entre un texte et appuie sur "ENTER".
     * <p>
     * Si le texte entré ne commence pas par "o", par "y" ou par "n"
     * (majuscule ou minuscule), la console affichera "Réponse invalide!"
     * et l'utilisateur devra réessayer jusqu'à entrer une réponse valide.
     * <p>
     * Ex. :
     * <pre>{@code
     * public static void main(String args[]){
     *     Console.afficher("Avez-vous faim?");
     *     boolean aFaim = Console.demanderOuiNon();
     *     if(aFaim) {
     *         Console.afficher("Voilà de la nourriture!");
     *     } else {
     *         Console.afficher("Je garde la nourriture pour moi!");
     *     }
     * }
     * }</pre>
     * @return Vrai, si le texte commence par un "o" ou un "y".
     * Faux, si le texte commence par "n"
     */
    public static boolean demanderOuiNon() {
        String reponse = scanner.nextLine().toLowerCase();
        if (reponse.startsWith("o") || reponse.startsWith("y")) {
            return true;
        } else if (reponse.startsWith("n")) {
            return false;
        } else {
            System.out.println("Réponse invalide !");
            return demanderOuiNon();
        }
    }

    /**
     * Permet d'afficher le texte "Appuyez sur une touche pour continuer..."
     * et d'attendre que l'utilisateur appuie sur "ENTER" pour continuer le code.
     * <pre>{@code
     * public static void main(String args[]){
     *     for(int i = 10; i > 1; i--) {
     *         Console.afficher(i);
     *         Console.appuyezSurUneTouchePourContinuer();
     *     }
     *     Console.afficher("Bonne année!");
     * }
     * }</pre>
     */
    public static void appuyezSurUneTouchePourContinuer() {
        System.out.print("Appuyez sur une touche pour continuer...");
        scanner.nextLine();
    }
}