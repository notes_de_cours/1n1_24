# Greenfoot démo #1: la Fusée

Nous allons construire un jeu à l'aide de Greenfoot afin de vous familiariser avec l'outil. 

Les concepts vous seront expliqués par la suite. Ne vous en faites pas si certains d'entre eux vous sont inconnus. 

Continuons à partir du démo de la fusée créé précédemment. 

## But du démo

Avant d'entreprendre un projet, il est important de savoir ce qu'il doit faire. Voici une spécification de ce que ce démo doit avoir comme fonctionnalités:

* Le but du jeu est d'avoir une fusée qui se déplace librement en suivant les mouvements de la souris;
* Des astéroides apparaissent à la droite de l'écran et se déplacent de façon aléatoire vers la gauche et de haut en bas;
* Les astéroides ne sortent pas en haut ou en bas de l'écran; ils y rebondissent;
* Si un astéroide touche la fusée, la partie se termine;
* Si un astéroide se rend complètement à la gauche de l'écran, il disparait;
* Un pointage s'affiche à l'écran. Ce pointage augmente tant que la partie n'est pas terminée;
* Le nombre d'astéroides augmente progressivement avec le temps;
* Quand la partie se termine, le pointage final est affiché. 


## Déplacement de la fusée

Nous allons changer le mode de déplacement de la fusée. 

Remplacez le `act()` de `Fusee` par  ce code: 


```java
    public void act()
    {
        MouseInfo mi = Greenfoot.getMouseInfo();
        if( mi != null ) {  
            setLocation(mi.getX(), mi.getY());
        }    
    }
```


Faites un Reset, et Run. 

La fusée devrait suivre votre souris. 

## Les astéroides

Créez une nouvelle classe sous Actor, appelez la `Asteroide` et prenez l'image de la roche dans la catégorie Nature. 

Dans la fonction `MyWorld()` (aussi appelé le `constructeur`) de la classe MyWorld, ajoutez le code suivant:

<div class="no-copy">

:::info contexte, ne pas copier
``` 
public class MyWorld extends World
{
    private Fusee fusee;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        super(600, 400, 1); 
        fusee= new Fusee();
        addObject(fusee,112,174);
```
:::

</div>  



```java 
        Asteroide asteroide1 = new Asteroide();
        addObject(asteroide1,590,174+Greenfoot.getRandomNumber(60));

        Asteroide asteroide2 = new Asteroide();
        addObject(asteroide2,590,174-Greenfoot.getRandomNumber(60));
```

Pour qu'ils se déplacent, ajoutez ce code dans le `act()` d'`Asteroide`:

```java
this.setLocation(getX()-Greenfoot.getRandomNumber(3), 
        this.getY()+Greenfoot.getRandomNumber(9)-4);
turn(Greenfoot.getRandomNumber(5));
```

Si vous exécutez le code, vous verrez que les astéroides se déplacent de droite à gauche, mais qu'ils restent coincés à gauche. 

Il faut détecter que l'astéroide est rendu à gauche et l'enlever du jeu. Ajoutez cette fonction dans **Asteroide**:

```java
private void verifierPourEnlever() {
    World lMonde = getWorld();
    if( getX() < 1) { 
        lMonde.removeObject(this);
    }
}
```

Et cette ligne à la fin du `act()` d'**Asteroide**:

```java
verifierPourEnlever();
```

## Détection de collisions

Pour détecter que la fusée est entrée en collision avec un astéroide, ajouter cette fonction `act()` dans `MyWorld`:

```java
public void act() {
    if(fusee!=null && fusee.estEnCollision()) {
            removeObject(fusee);
            fusee = null;
    }
}
```

:::tip
Notez ici l'utilisation du `&&`. Si la fusee est null, on ne devrait pas pouvoir appeler la fonction **estEnCollision()** (on ne peut appeler une méthode d'un objet null). Ici, si la partie gauche du `&&` est fausse, la partie de droite ne sera pas évaluée. C'est un exemple d'un raccourcis de l'opérateur `&&` tel qu'expliqué dans la section sur les **Booléen et Conditions/Raccourcis**
:::

:::tip
Il est normal que `estEnCollision` soit souligné en rouge car la fonction n'est pas définie encore. Prenez l'habitude de comprendre pourquoi du code est souligné en rouge. Cela vous indique une erreur, et contrairement à une erreur dans Word, celles indiqué dans un IDE ne peuvent être ignorées (vous ne devriez pas ignorer les erreurs dans Word non plus... mais ca c'est une autre histoire)
:::

Et, finalement, ajoutez la fonction `estEnCollision()` dans `Fusee`:

```java
public boolean estEnCollision() {
    Actor lCollision = getOneIntersectingObject(Asteroide.class);
    return lCollision != null;
} 
```

## Tableau de pointage

Ajoutez la classe `Pointage` en tant que sous-classe de `Actor`. Pas besoin de choisir une image, nous allons la dessiner. 

Rapidement, voici le code complet pour la classe `Pointage`:

:::tip
Copiez le code ci-dessous, et faites `ctrl-a` suivit de `ctrl-v` dans l'éditeur de Pointage afin de remplacer tout le code par celui-ci
:::

```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Pointage here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Pointage extends Actor
{
    private  int point;
    
    public Pointage() {
        GreenfootImage lImage = new GreenfootImage(100, 20);
        lImage.setColor(Color.GREEN);
        lImage.fill();
        lImage.setColor(Color.RED);
        lImage.drawString("0",45, 15);
        
        setImage(lImage);
        
        point =0;
        
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
    
    public void augmentePointage(int aIncrement) {
        point += aIncrement;
        GreenfootImage lImage = getImage();
        lImage.setColor(Color.GREEN);
        lImage.fill();
        lImage.setColor(Color.RED);
        lImage.drawString(String.valueOf(point),45, 15);
    }
    
    public int getPoint() {
        return point  ;
    }
    
    public void setPoint(int aPoint) {
        point = aPoint;
    }
}

```

Il faut maintenant ajouter un pointage à `MyWorld`:

Ajoutez cette variable au début de la classe `MyWorld`

<div class="no-copy">
 
:::info contexte, ne pas copier
```
public class MyWorld extends World
{
    private Fusee fusee;
```
:::
</div>

```java
    private Pointage tableau;
```

et ce code à la fin la fonction `MyWorld()` après les Asteroides:

```java
tableau = new Pointage();
addObject(tableau, this.getWidth()/2, 20);
```

Dans le `act()` de `MyWorld`, ajoutez ce `else` au `if` pour détecter la collision:

<div class="no-copy">
 
:::info contexte, ne pas copier
```
if(fusee.estEnCollision()) {
    removeObject(fusee);
    fusee = null;
}
```
:::
</div>

```java
else {
    tableau.augmentePointage(1);
}
```

Run. 

## Correction du pointage qui augmente sans arrêt

Lorsque la fusée est détruite, le pointage continue à être incrémenté. 

Ce bug est dû au fait que l'augmentation du pointage est fait dans le `else` qui est exécuté si la fusée est `null`. 

Pour le corriger, changer le code pour celui-ci:

```java
if(fusee!=null) {
    if( fusee.estEnCollision()) {
        removeObject(fusee);
        EcranFin lFin = new EcranFin(tableau.getPoint());
        Greenfoot.setWorld(lFin);
    } else {
        tableau.augmentePointage(1);
    }
}
```

## Ajout d'astéroides. 

Le jeu est un peu ennuyeux, ajoutons des niveaux de difficultés.

Ajoutez une variable pour déterminer le niveau dans MyWorld.

<div class="no-copy">
 
:::info contexte, ne pas copier
```
public class MyWorld extends World
{
    private Fusee fusee;
    private Pointage tableau;
```
:::
</div>

```java
    private int niveau = 0;
```

Augmentez le niveau de difficulté à tous les 500 points en ajoutant ce code à la fin du `act()` de `MyWorld`:

```java
if (tableau.getPoint() % 500 == 0) {
    niveau++;
}
```

Et ajoutez ce code aussi à la fin du `act()` de `MyWorld` afin d'ajouter des astéroides de façon aléatoire, et de plus en plus selon le niveau:

```java
if( Greenfoot.getRandomNumber(1000) < 10*niveau ) {
    Asteroide lAsteroide = new Asteroide();
    addObject(lAsteroide, 590, Greenfoot.getRandomNumber(getHeight()-50)+10);
}
```

Run.

Woooooooo ca devient fou !!!

Mais comment on termine le jeu quand il y a eu une collision???

## Changement d'écran 

Dans Greenfoot, pour changer d'écran, on change le monde.

Créez un nouveau `World`. Appelez le `EcranFin` et choisissez un background. 

Voici le code pour cette classe:

```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EcranFin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EcranFin extends World
{
    
    public EcranFin(int pointage) {
        super(600, 400, 1);  
        showText("C'est finiiii!!!", getWidth()/2, getHeight()/2);
        showText("Pointage : "+pointage, getWidth()/2, getHeight()/2+20);
    }

    public EcranFin()
    {    
        this(0);
    }
}

```

Dans le `act()` de `MyWorld`, **remplacez** la ligne `fusee = null;` par celles-ci:


```java
    EcranFin lFin = new EcranFin(tableau.getPoint());
    Greenfoot.setWorld(lFin);
```

Notez que la ligne `if(fusee!=null)` n'est plus requise puisque nous changeons de monde.


**Et voila, vous avez le jeu de l'année en votre possession !!!!**
