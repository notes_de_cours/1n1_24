# Introduction à Greenfoot	

Greenfoot est un environnement de développement intégré en Java conçu principalement pour l'enseignement au niveau secondaire supérieur et universitaire. Il permet de développer facilement des applications en 2D telles que des jeux ou des simulations.

Greenfoot est développé et maintenu à l'Université du Kent et l'Université de La Trobe, avec le soutien de Oracle. C'est un logiciel libre distribué sous la licence GPL. Greenfoot est disponible pour Microsoft Windows, Mac OS X, Linux, Sun Solaris, et n'importe quelle machine virtuelle Java récente. 

[https://fr.wikipedia.org/wiki/Greenfoot]

Pour le télécharger: https://www.greenfoot.org/download

## Création d'un projet

Comme premier projet, nous allons créer une petite démo très simple.

* Démarrez Greenfoot

* Scenario/new java scenario

* Changez le répertoire de destination pour votre répertoire de projets pour 1n1.

* Nommez le projet  **demofusee**


Une fois le projet créé, ouvrez l'éditeur pour  `MyWorld`


Changez l'image de fond de `MyWorld`. Pour ce faire, faites un clic de droit sur `MyWorld` et choisissez `Set image` (prenez le fond noir avec les étoiles)

Nous allons maintenant créer notre premier acteur:

Faites un clic de droit sur `Actor` et choisissez  `new subclass`

Nommez la nouvelle classe `Fusee` (F majuscule, sans accent)

:::warning
Il est important de respecter les majuscules/minuscules en java. 

Et ne mettez jamais d'accents dans les noms de variables ou de classes. Bien que ce soit permis, ne le faite pas, vous vous sauverez beaucoup de problèmes. 
:::

Prenez l'image de la fusée dans la section transport

Ouvrez l'éditeur de `Fusee` et ajoutez cette ligne dans le `act()`:


```java
setLocation(this.getX()+3, this.getY());
```

Dans la classe `MyWorld` ajoutez cette variable au début de la classe.

<div class="no-copy">

:::info contexte, ne pas copier
``` 
public class MyWorld extends World
{
```
:::

</div> 

```java
private Fusee fusee;
```

et ajouter ce code à la fin de la méthode MyWorld()

<div class="no-copy">

:::info contexte, ne pas copier
```
public MyWorld()
    {    
        super(600, 400, 1); 
```
:::
</div>

```java
fusee= new Fusee();
addObject(fusee,112,174);
```


Appuyez sur le bouton  `Run` au bas de l'écran... décollage.. 


## Tour rapide de l'interface:

* Bouton act
  * Permet d'exécuter une séquence seulement

* Bouton run/pause
  * Permet d'exécuter le programme normalement où le mettre en pause

* Bouton reset
  * Permet de remettre les éléments à leur position de départ

* Ouvrir le code
  * Double-cliquez sur la classe

* Vous pouvez mettre des breakpoint et utiliser le débogueur 

:::warning
il arrive parfois que le débuggeur gèle. La façon la plus simple de s'en sortir est de quitter Greenfoot)
:::

## Ouverture d'un projet

Pour ouvrir un projet à partir du navigateur de fichiers, vous n'avez qu'à ouvrir le fichier `project.greenfoot` dans le répertoire du projet

## Formatage du code

À tout moment vous pouvez aller dans `Edit` et prendre l'option `auto-layout` afin de reformater correctement votre code. 

:::warning
Il est possible que l'enseignant refuse de vous aider si votre code est mal formaté. Apprenez à bien présenter votre code. Il est très facile de le reformater avec auto-layout.
:::

## Documentation

Dans le menu `Help`, prendre l'option `Greenfoot class documentation`. 