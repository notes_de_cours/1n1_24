# Tests fonctionnels

## Retour sur ce que sont les tests.

Nous avons déjà parlé des tests fonctionnels. Nous y revenons avec des exemples tirés du jeu de la fusée, et en profitons pour rappeler certains concepts importants

Voici quelques exemples de tests fonctionnels :

* La fusée suit la souris.
* Les astéroides disparaissent à la gauche de l'écran.
* La partie se termine si un astéroide frappe la fusée.
* Le pointage augmente avec le temps.
* ....

Notez qu'on ne peut pas faire un test fonctionnel sur quelquechose qui n'a pas été spécifié. 

Par exemple, je ne peux pas faire ces tests:

* le pointage augmente de 10 points chaque seconde;
* un astéroide touche la fusée dès qu'il est à 20 pixels de la fusée;

Ces détails n'ont jamais été spécifiés, le programmeur (ou le testeur) ne peut décider arbitrairement quelles valeurs utiliser. 

:::warning
C'est au client de décider des valeurs à tester. Toujours s'y référer, ne jamais prendre de décision arbitraire.
:::

Plus les spécifications sont précises, plus il est facile de tester (souvenez vous de ça en troisième année quand vous aurez à écrire les spécifications d'un système et ensuite d'y faire des tests). Par exemple, la spécification du jeu de fusée manquait de détails:

* quelle est la taille min et max de la fusée et des astéroides?
* quelle est la vitesse des astéroides?
* combien d'astéroides peuvent être à l'écran à la fois?

Par contre, certains aspect peuvent être laissés libres car ils ne dérangent pas la **fonctionnalité** de l'application. 

* est-ce que le pointage doit être en haut, en bas, .. ??
* quelle couleur utiliser ?
* doit-il y avoir une image dans le fond ?

De plus, les tests fonctionnels ne portent pas sur les aspects *non fonctionnels* de l'application. Exemple:

* la fusée doit être belle;
* le jeu doit être facile à controller;
* le jeu doit être amusant. 

Ces éléments peuvent être testés, mais ce sont des tests **non fonctionnels** qui les couvriront (ce que nous ne ferons pas dans le cadre de ce cours). 

[Pour en savoir plus](https://www.all4test.fr/blog-du-testeur/quest-ce-test-fonctionnel-tutoriel-complet-exemples/#:~:text=Les%20tests%20fonctionnels%20sont%20une,attentes%20de%20l'utilisateur%20final)  
[Pour en savoir encore plus](https://fr.wikipedia.org/wiki/Test_(informatique))

## Comment écrire un test

Un test doit décrire avec autant de détails que possibles:

* les étapes de ce qui est à tester
* les résultats attendus. 

Si nous reprenons les premiers tests vus ci-haut

* Test: Déplacer la souris dans le jeu
  * Résultats attendus: la fusée se déplace en suivant la souris. Il peut y avoir un certain "lag", mais la fusée doit rejoindre la souris lorsque celle-ci s'arrête.
* Test: Attendre qu'un astéroide arrive à la bordure gauche du jeu
  * Résultats attendus: L'astéroide disparait dès qu'il frappe la bordure gauche de l'écran. 
  
## Comment tester

Les tests doivent couvrir:

* les bonnes valeurs
* les mauvaises valeurs
* les cas limites


Une bonne pratique est de **tester le cas valide de chacune des fonctionnalités** en premier. Le but est d'entrer des informations correspondantes aux critères spécifiés afin de voir si minimalement le cas valide fonctionne.

Exemple:
* Le test ci-haut pour le déplacement de la souris dans le jeu
* Test: démarrer le jeu et attendre quelques secondes
  * Résultats attendus: un astéroide apparait 
* Test: Attendre l'apparition d'un astéroide
  * Résultats: L'astéroide se déplace vers la gauche dans un mouvement aléatoire
* Test: Faire brasser un dés
  * Résultats: la valeur du dé est 1-2-3-4-5-6 pour un dé à 6 faces.


Ensuite, nous allons faire des **tests aux limites**.

* (T) déplacer la souris sur les bordures du jeu, mais juste à l'intérieur (1 pixel)
  * (R) la fusée suit la souris jusqu'aux bords de l'écran

* (T) déplacer la souris très rapidement dans le jeu
  * (R) la fusée fini par rejoindre la souris quand celle-ci s'arrête
* (T) entrer la valeur limite supérieur et inférieur lorsque le programme demande d'entrer une valeur 
  * (R) la valeur est acceptée


Ensuite, on peut **tester les erreurs possibles** du programme en entrant des valeurs invalides.à

* (T) entrer la valeur limite supérieur +1 et inférieur -1 
  * (R) la valeur est refusée
* (T) déplacer la souris sur les bordures du jeu, mais juste à l'extérieur (1 pixel)
  * (R) la fusée reste immobile. 
* (T) entrer le 32 pour un mois de janvier
  * (R) la valeur est refusée



:::tip
Les programmeurs ont tendance à être trop gentils. N'ayez pas peur de briser votre code, mieux vaut que vous trouviez le bug plutôt que le client!.
:::
Les problèmes trouvés devront ensuite être corrigés, et les mêmes tests devront être ré-exécutés afin de démontrer que l'erreur est vraiment corrigée (d'où l'avantage des tests automatisés). 

:::tip
Il est donc important de bien noter comment l'erreur a été produite afin de pouvoir la reproduire (ce qui n'est pas toujours le cas)
:::
