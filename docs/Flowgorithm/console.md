# Console

Afin de simuler les fonctionnalités de Console ajoutées dans Intellij, vous pouvez charger ce fichier de Flowgorithm. 

Téléchargez ce fichier et décompressé le dans votre répertoire de projets que nous venons de créer (c:\projets) :

[console.zip](/console.zip)

Si vous désirez utiliser ces fonctions dans votre algorithme, vous n'avez qu'à ouvrir le fichier **console.fprg**. Je vous conseille de sauvegarder le nouveau projet sous un autre nom une fois ouvert afin de ne pas écraser console.fprg.
 
Vous y trouverez les fonctions suivantes:

* ConsoleAfficher(Chaine texte) //affiche le texte
* ConsoleAfficherEntier(Entier nombre) //affiche l'entier
* ConsoleAfficherLong(Entier nombre) //affiche l'entier (long n'existe pas dans Flowgorithm)
* ConsoleAfficherNombreAVirgule(Réel nombre) //affiche le réel. 
* ConsoleAfficherBoolean(Booléen bool) //affiche le booléen (vrai ou faux)
* Chaine ConsoleDemanderTexte() //demande une chaine et la retourne
* Entier ConsoleDemanderEntier() //demande un entier et le retourne
* Réel ConsoleDemanderNombreAVirgule() //demande un réel et le retourne
* Booléen ConsoleDemanderOuiNon() //demande un texte, si ce texte commence par "o" retourne vrai, si commence par "n" retourne faux; sinon redemande


:::info
Pour des raisons techniques, les fonctions suivantes ne sont pas simulées:

* l'affichage d'un caractère
* appuyezSurUneTouchePourContinuer()
:::


:::info
Vous pouvez utiliser ces fonctions dans les exercices afin de mieux correspondre avec le code Java; mais vous pouvez aussi utiliser simplement Entrée et Sortie. 

:::
