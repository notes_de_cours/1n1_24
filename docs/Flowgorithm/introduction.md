# Flowgorithm



Avant même de faire de la programmation, il est important de comprendre ce qu'est un algorithme. 

Un algorithme est une suite finie et non ambiguë d'instructions et d’opérations permettant de résoudre une classe de problèmes.  [wikipedia](https://fr.wikipedia.org/wiki/Algorithme) 

Afin de nous familiariser avec les algorithmes nous allons utiliser le logiciel Flowgorithm pouvant être téléchargé à cette adresse http://flowgorithm.org/download/index.html

:::info
Malheureusement, ce logiciel est disponible uniquement sur Windows. 

Si vous avez une autre plateforme, veuillez vous mettre en équipe avec une autre étudiant. 

Il n'y aura aucune évaluation basée sur Flowgorithm
:::

Une fois téléchargé et installé, veuillez le démarrer afin de vous familiariser avec l'interface. 

Nous allons rapidement voir plusieurs concepts qui seront revus en détails lors des prochains cours. Ne vous inquietez pas si vous ne comprenez pas certains de ceux-ci immédiatement; nous les reverrons plusieurs fois durant la session. Nous n'expérimenterons pas avec toutes les variations possibles des algorithmes, expressions, et opérateurs. Nous allons garder ca pour plus tard. 

## Premier contact

Sur le diagramme, cliquez sur la flèche entre les 2 bulles. Cette fenêtre s'ouvrira:

![outils](/outils_flowgorithm.png) 

Vous avez sur cette image tous les concepts de base de la programmation. Le reste de la session servira à approfondir ces concepts. 

## Notion 1 : entrée/sortie

Le *problème* que nous allons résoudre est de demander le nom à l'usager. 

### Étape 1: afficher la question

Pour demander une question, il faut afficher du texte. 

Cliquez sur la flèche entre Principale et Fin, sélectionnez l'icone **Sortie**.

Double cliquez sur l'icone Sortie qui a été inséré dans le diagramme. Une fenêtre s'ouvrira vous demandant qu'elle est le texte à afficher. Inscrivez 
```
"Quel est votre nom ?"
```

:::warning Attention
Les guillemets doubles sont obligatoires au début et à la fin. Nous verrons pourquoi plus tard
:::

### Étape 2: demander la réponse
Maintenant que nous avons posez la question, il faut mettre la réponse à quelque part. Cliquez sur la flèche entre la **Sortie** et la **Fin**. Choisissez **Entrée**. 

Double cliquez sur l'icone d'**Entrée**. Une fenêtre s'ouvrira vous demandant le nom de variable dans laquelle mettre la réponse. Une variable est un espace qu'on réserve dans la mémoire de l'ordinateur afin d'y entreposer de l'information. Entrez **nom**  (sans guillemet cette fois-ci)

Essayons l'algorithme. Assurez-vous que la console est ouverte (**Fenêtre/Afficher la console**... F10 )

Vous pouvez régler la vitesse d'exécution (**Programme/Vitesse d'exécution**) à **Lente** afin de voir les étapes s'exécuter une à une. 

Appuyez sur la flèche verte.

### Étape 3: déclaration de la variable

Oups, une erreur indiquant que la variable `nom` n'est pas déclarée. Comme dans plusieurs langages, il faut **déclarer** une variable avant de s'en servir. 

Notez qu'habituellement, on fait la déclaration dès qu'on sait qu'on aura besoin d'une variable. 

Cliquez sur la flèche sortant de **Principale** et sélectionnez **Déclaration**.

Dans la fenêtre, inscrivez **nom** et choisissez **Chaine** pour le type. 

Nous venons de **déclarer** la variable **nom**, donc de réserver de l'espace mémoire, et d'indiquer qu'elle contiendra une **chaine de caractères**. Nous pouvons maintenant **utiliser** cette variable comme nous l'avons fait dans la case de sortie. 

La déclaration doit se faire avant l'utilisation. 

Re-exécutez le programme, cette fois-ci il n'y aura aucune erreur, mais ce code ne fait pas grand chose. 

Ajoutez une étape juste avant la fin. Choississez **Sortie** et inscrivez:

```
"Votre nom est : " & nom
```

Dans ce petit algorithme, nous venons de voir plusieurs concepts:

* variable
* déclaration d'une variable
* utilisation d'une variable
* affichage de texte
* entrée d'information
* affichage de texte suivit d'une variable à l'aide de l'opérateur **&** (concaténation)

## Notion 2 : addition

Pour ce deuxième exemple, nous allons demander 2 nombres et les additionner. 

Commençons par remettre le programme à l'état de départ: **Fichier/ Nouveau** (si vous désirez conserver votre premier algorithme en souvenir, vous pouvez faire Fichier/enregistrer avant de l'effacer).

### Étape 1 : demander la première valeur

Ici, nous allons faire plusieurs sous-étapes en rafale. 

Si nous désirons demander un nombre à l'usager, nous devons l'entreposer en mémoire afin de pouvoir le traiter par la suite. Comme déjà vue, l'entreposage se fait dans une variable. 

Nous devons donc déclarer la variable, ensuite poser la question, et conserver la réponse dans cette variable.

Ajoutez une déclaration et nommez la variable **valeur1** de type **entier**

Ajoutez la question en ajoutant une sortie dans laquelle vous inscrirez 
```
"Entrez la première valeur entière :"
```

Finalement, ajoutez une entrée pour **valeur1**. 

### Étape 2 : demander la deuxième valeur

Ajoutez les 3 mêmes étapes, mais utilisez **valeur2** cette fois-ci, et demandez la deuxième valeur entière. 

### Étape 3 : additionner les deux valeurs

Étant donné que nous allons entreposer le résultat en mémoire, nous avons besoin d'une troisième variable. 

Ajoutez la déclaration de la variable **resultat** de type entier aussi. 

:::info 
Il est fortement recommandé de ne pas mettre d'accent dans les noms des variables. C'est donc **resultat** et non **résultat**
:::
Ajouter une étape d'**affectation**.

Inscrivez **resultat** dans la section Variable. 

Inscrivez **valeur1 + valeur2** dans la section Expression. 

### Étape 4 : afficher le résultat

Ajouter une sortie dans laquelle vous incrirez:

```
"Le résultat est : " & resultat
``` 

Vous pouvez maintenant tester votre algorithme. 

Dans cet exemple, nous venons d'apprendre les concepts suivants:

* l'affectation (mettre le résultat d'un calcul dans une variable)
* l'addition

:::details Solution
![addition](/addition.png)
:::

## Notion 3 : boucle Pour

Il arrive fréquemment que l'on veuille faire une opération à répétition. 

Ici, le problème à résoudre est: afficher les nombres de 1 à 10. 

Commencez par effacer le projet actuel.

### étape 1 : déclaration du compteur

Afin de compter, nous allons utiliser une variable. Il faut donc la déclarer. 

Ajoutez une déclaration pour la variable **i**

:::info
Les variables i, j, k sont souvent utilisées pour compter
:::

### étape 2 : Choisir le type de boucle

Il existe 3 types de boucles: **tant que**, **pour**, et **répétition**. Nous verrons plus tard leurs caractéristiques. Pour l'instant, insérez une boule **pour**

Dans la case **Pour** inscrivez les valeurs suivantes:

* i pour la variable
* 1 pour la valeur de départ
* 10 pour la valeur de fin
* 1 pour l'incrément
* et cochez En Augmentant

Cliquez sur la flèche **Suivant**, et insérez une **Sortie**

Dans la sortie, inscrivez:
```
"La valeur de i est : " & i
```

Vous pouvez maintenant essayer le code. 

Dans cet exemple, nous venons d'apprendre les concepts suivants:

* la boucle **Pour** qui est utilisée quand on veut faire un nombre fixé d'itérations. 

:::details Solution
![for1](/for1_10.png)
:::

:::tip Note
Notez ici que la boucle affiche 10. La valeur de fin est donc incluse. 

Nous verrons que cela correspond à une boucle ou la condition de fin est <= 10. 

Dans certains cas nous ne voudrons pas que la valeur 10 soit incluse, ce qui correspond a < 10. Ce cas ne peut être représenté dans Flowgorithm. 

:::
## Notion 3 : tableau de multiplications

Le problème : afficher la table de multiplication de 1 à 9 pour un nombre donnée

N'oubliez pas d'effacer la solution actuelle. 

### étape 1 : demander pour quel nombre faire la table de multiplication

Nous avons déjà vu comment demander un nombre à l'usager. Je vous laisse écrire ce bout

### étape 2 : afficher la table de multiplication

Nous allons devoir faire une boucle de 1 à 9. Vous savez déjà comment faire ca.

Ensuite, dans cette boucle, nous devons multiplier l'indice de la boucle (la variable i) par la valeur entrée par l'usager, et ensuite afficher ce résultat. Nous avons fait des opérations très similaires précédemment. 

:::details 
![tableau multiplication](/tableau_multiplication.png) 

Pour l'affichage, voici la formule magique

```
i & " * " & valeur1 & " = " & resultat
```

Il est possible de concaténer plusieurs éléments. 

:::

Dans cet exemple, nous venons d'apprendre les concepts suivants:

* l'utilisation d'une boucle pour faire des calculs
* la concaténation de plusieurs éléments. 

## Notion 4 : condition

Il est fréquent que l'on doive vérifier une valeur et changer le cheminement de l'algorithme selon cette valeur. 

Pour se faire, nous allons utiliser des **conditions** (vous entendrez souvent le terme **booleens**).

Le problème que nous désirons résoudre: afficher si un nombre entré est plus grand que 10. 

### étape 1 : demander le nombre

Ca nous savons comment le faire ... utilisez la variable **valeur1** encore. 

### étape 2 : vérifier si le nombre est plus grand que 10

Après la saisie de valeur1, ajouter un **Si**

Cliquez sur le **Si** et entrez l'expression :
```
valeur1 > 10
```

Sur la branche **Vrai** ajoutez une sortie contenant le texte **plus grand**.

Sur la branche **Faux** ajoutez une sortie contenant le texte **plus petit**. 

Essayez la solution. 

### étape 3 : quoi faire pour la valeur 10

Si vous avez entrez la valeur 10, le texte **plus petit** est affiché. En effet notre condition vérifie si le nombre entré est **plus grand** que 10. La valeur 10 n'étant pas plus grande, elle est donc catégorisée comme plus petite. 

Pour corriger ce **bug**, ajoutez un **Si** sur la branche **faux**.

Dans ce **Si** ajoutez la condition 

```
valeur1 = 10
```

Faites une clic de droite sur l'affichage de **plus petit** et faite **couper**. Cliqué sur le **faux** du 2e **Si** et faite **coller** (dans le presse papier). Vous venez de déplacer un bout de logique. 

Maintenant dans la branche **Vrai** ajoutez la sortie 
```
"C'est 10"
```

Ré-essayez la solution avec la valeur 10, ainsi qu'une valeur plus petite que 10. 

:::details
![pluspetit10](/pluspetit10.png) 

:::


Dans cet exemple, nous venons d'apprendre les concepts suivants:
* les conditions
* la correction d'un bug 



## Notion 5 : boucle Répétition
La boucle **Pour** s'exécute un certain nombre de fois. 

Mais parfois, on ne sait pas combien d'itérations seront nécessaires car nous désirons boucler tant qu'une condition est vrai. 

Le problème que nous désirons résoudre: demander un nombre à l'usager tant que ce nombre est plus petit que 10. 

L'usager doit donc entrer un nombre plus grand ou égal à 10 pour terminer le programme. 

Commencons par effacer la solution actuelle. 

### étape 1 : déclaration de la variable

Nous allons avoir besoin d'une variable pour y mettre le nombre entré par l'usager. Déclarez la variable **nombre** (pour faire changement)

### étape 2 : le choix de la boucle

Ici nous allons ajouter une boucle **Répétition**. Elle a la particularité d'être exécutée au moins une fois (nous y reviendrons plus tard). 

Sur la branche qui va vers la droite à partir du point, demandez le nombre. 

Dans la case Répétition, entrez la formule :

```
nombre < 10 
```

Vous pouvez essayer la solution 

:::details 
![dowhilepluspetit10](/dowhilepluspetit10.png) 

:::



Dans cet exemple, nous venons d'apprendre les concepts suivants:
* un nouveau type de boucle

## Notion 6 : boucle tant que

Le dernier type de boucle que nous utiliserons peut remplacer tous les autres. Nous verrons plus tard comment faire. 

Ce type de boucle ressemble beaucoup à la boucle de répétition. La différence majeure est qu'il est possible de ne jamais effectuer une itération. 

Nous allons refaire le derner exercice mais à l'aide d'une boucle **Tant que**

Faites un clic de droit sur la **Répétition** et effacez la. 

Après la case de déclaration ajoutez une **Affectation** et mettez la variable **nombre** à la valeur **0**.

Ajoutez un **Tant Que** et entrez y l'expression 
```
nombre < 10 
```

Sur la branche **Vrai** ajoutez la demande du nombre. 

Essayez la solution 


:::details 
![whilepluspetit10](/whilepluspetit10.png) 
:::

Dans cet exemple, nous venons d'apprendre les concepts suivants:
* la boucle tant que, qui est le format de boucle le plus général. 


## Notion 7 : fonction

Lorsque la complexité du code augmente, la solution est de diviser le code en plusieurs petits morceaux qu'on appel **fonction**.

Nous allons modifier le dernier exercice en déplacant une partie du code dans une fonction. 

### étape 1 : création de la fonction

En haut de l'écran, si vous sélectionnez **Principale**, un menu s'ouvrira avec l'option d'ajouter une fonction (new function ... il semble que la traduction ne soit pas complète :smile: ). 

Dans la fenêtre qui s'ouvre, inscrivez:

* **demanderNombre** pour le nom
* ne rien mettre dans les paramètres. 
* sélectionner **Entier** comme type de retour
* **valeur** dans **Variable de Retour** qui est apparu quand vous avez sélectionné **Entier**. 

Un nouvel algorithme apparaitra. N'ayez pas peur, vous n'avez pas perdu le précédent. Il suffit de sélectionner **demanderNombre** en haut de l'écran pour voir la liste des fonctions disponibles, et **Principale** est toujours là. 

Dans **demanderNombre** après la déclaration de **valeur** qui a été ajouté automatiquement, ajouter la sortie et l'entrée nécessaire pour demander ce nombre.

La dernière case contient **Retour Entier valeur**, ce qui indique que cette fonction va retourner la valeur entrée dans la variable valeur... mais la retourner ou ?

Affichez la fonction **Principale** 

Enlevez les deux cases de la branche **Vrai**. 

Sur la branche **Vrai** qui est maintenant vide, ajoutez une assignation, et inscrivez:

* nombre pour la variable
* demanderNombre() pour l'expression

Testez. 

:::details 

La fonction Principale
![fonctionPrincipalewhilepluspetit10](/fonctionPrincipalewhilepluspetit10.png) 

La fonction demanderNombre
![fonctionDemanderEntierwhilepluspetit10](/fonctionDemanderEntierwhilepluspetit10.png) 

:::

Dans cet exemple, nous venons d'apprendre les concepts suivants:
* la création d'une fonction
* l'appel d'une fonction 
* l'utilisation de la valeur de retour 
* la portée d'une variable

## Conclusion 

Dans les différents exercices que nous venons de faire, nous avons eu un apercu de plusieurs des concepts qui seront explorés durant le reste de la session. Si vous n'avez pas tout compris, c'est normal. Et ce n'est que la pointe de l'iceberg. Chacune de ces notions doit être revues en détails afin d'en découvrir les subtilités. 

