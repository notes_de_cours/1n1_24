# Les Travaux Pratiques

Vous réaliserez 3 travaux durant ce cours. 

Le premier consiste en une série d'exercices qui vous permettrons de vous approprier les différents concepts vues durant le cours. 

[Voir l'énoncé](./TP1.md)

Le deuxième consiste en la programmation d'un jeu en mode console à partir d'un squellette de ce jeu écris en pseudocode. Vous approfondirez l'écriture du code sans avoir à vous en faire avec l'algorithme à suivre. 

[Voir l'énoncé](./TP2.md)

Le troisième consiste en la programmation d'un jeu en mode graphique à l'aide de l'environnement Greenfoot. 

[Voir l'énoncé](./TP3.md)
