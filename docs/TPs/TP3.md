# TP3: Jeu graphique à l'aide de Greenfoot

## Date de remise

Ce TP est à remettre au dernier jours de la session:12 décembre. 

Vous devez remettre le gfar du projet. (bouton `share` dans Greenfoot, et bouton `export` dans la fenêtre qui s'ouvre )

## Énoncé

Pour ce travail vous devez créer un jeu dans lequel un personnage réalisera une quête. Il rencontrera des ennemis qui essaieront de le tuer ou tout simplement l'empêcher de passer, ainsi que des énigmes à résoudre. 

Le jeu sera constitué de plusieurs tableaux ayant des difficultés différentes. Pour passer d'un niveau à l'autre, le personnage devra accomplir une action lui permettant de terminer le niveau. 

Une fois tous les niveaux terminés, un tableau de pointage sera affiché. 

Pour vous donner de l'inspiration, vous pouvez regarder un jeu comme Zelda: https://arcadespot.com/game/the-legend-of-zelda/

## Ce qui sera à réaliser 

* Le jeu doit avoir minimalement 2 niveaux + le tableau de pointage final:
    * les tableaux doivent être facilement modifiables (ex: utiliser la technique du générateur, être totalement aléatoire, ... );
    * il doit y avoir un moyen de passer d'un niveau à l'autre (ex: une porte, un téléporteur, un ascenceur, ...)
    * il doit y avoir un but final au jeu permettant d'afficher le tableau de fin (ex: ramasser assez d'argent, avoir tuer tous les ennemis,... );
    * le tableau de fin doit présenter le pointage obtenu par le personnage avant de mourir ou lorsqu'il a terminé tous les niveaux;
* Vous devez avoir 5 types d'éléments parmis ceux-ci, minimalement dans 3 catégories: 
    1) des objets empêchant le personnage de passer (ex: mur, ...)
    1) des object auquels le personnage ne doit pas toucher (ex: lave, ...)
    1) un objet permettant de récuper la solution d'une énigme, et un autre objet réagissant à cette solution lorsque touché (ex: un coffre contenant une clé nécessaire pour ouvrir une porte, ... );
    1) des ennemis qui vous poursuivent (ex: des zombies, des soldats, des dragons, ... )
    1) des objets qui se déplacent et qui sont dangereux pour le personnage (ex: des balles, des missiles, des boules de feux, ...);
    1) des objets qu'il faut détruire (ex: un mur qui doit être détruit en y mettant une bombe, ou en tirant un certain nombre de projectiles);
    1) des objets qui donnent des pouvoirs ou des capacités au personnage (ex: accélération, survie à des attaques, invisibilité, ... );
    1) des objets qui se transforment (ex: une porte dans un mur qui se transforme en mur une fois franchie, ...)
    1) autres: demandez au professeur si 1) c'est réalisable; 2) si ca compte;



* Votre personnage doit avoir minimalement les capacités suivantes:
    * se déplacer à l'aide du clavier ou de la souris;
    * être capable de se défendre ou d'attaquer de 2 façons (ex: tirer des projectiles, poser des bombes, se battre avec une épée, ... );
    * avoir un capacité qui descend soit avec le temps, soit parce qu'un ennemi l'a touché (ex: un niveau d'oxygène qui descend, un niveau de vie qui descend quand il recoit un projectile, ... );

* Vous devez utiliser minimalement 1 son. 
    * par exemple, lors d'une explosion. 

* Vous devez écrire 5 tests fonctionnels
    * les écrire en commentaires au début du code de la classe MyWorld. 
    * utilisez le format utilisé en cours (T) et (R)

:::info 
Pour les 5 types d'éléments, vous pouvez par exemple avoir 2 éléments dans la catégorie 1, et 2 dans la catégorie 2, et 1 dans la catégorie 3. Mais les éléments dans une même catégorie doivent avoir un comportement assez différent. (voir l'enseignant si vous n'êtes pas certain) 

Par exemple, dans la catégorie 3, vous pourriez avoir une clé pour ouvrir une porte, ou de l'argent pour être assez riche pour acheter votre sortie. 
:::

:::info
Pour le son, nous n'en avons pas parlé... à vous de lire la documentation
:::

## Critères d'évaluation

:::danger AVERTISSEMENT
Vous devez remettre du code qui peut être exécuté. 

**AUCUN EFFORT** ne sera fait pour faire fonctionner votre programme. 

Si vous remettez un programme qui ne peut pas être exécuté, l'enseignant vous demandera de corriger le code (minimalement enlever les bouts de code qui causent l'erreur) et une pénalité de retard de 10% s'appliquera pour chaque tranche de 24h suivant l'avis. 

:::

Ce TP compte pour 30% de la note finale.

L'évaluation sera fait selon les critères suivants:

| Critères | Note |
|---|---|
| avoir 2 tableaux de jeu facilement modifiables  | 1 |
| avoir un écran de fin qui est affiché lorsque le personnage meurt ou qu'il complète les tableaux. Cet écran doit afficher le pointage | 2 |
| avoir les 5 types d'éléments de jeu fonctionnels (2 points chacun) | 10 |
| avoir un personnage qui se déplace | 2 |
| avoir les 2 modes d'attaque ou de défense | 2 |
| avoir une capacité qui descend | 2 |
| utiliser un son | 2 |
| avoir 5 tests fonctionnels | 5 |
| Documentation du code (fonctions et code complexe)  | 4 |
| Français: correction négative<br>-1 point par faute jusqu’à 3 points (10%)  |  |
| Respect des normes de présentation (qualité et présentation du code)<br>jusqu'à 2 points (5%)  |  |
| Total | 30 |

:::info
Pour la documentation, vous devez documenter toutes les fonctions correctement:
* ce que la fonction réalise
* à quoi servent les arguments
* ce qui est retourné
:::
## Respect des normes de présentations

Tel qu'indiqué dans le plan de cours:
:::info Normes de présentation
Pour les travaux pratiques de programmation, le code doit respecter les normes de présentations de l’enseignant ou de la technologie utilisée. Un maximum de 5 % de la note totale peut être retranché pour la présentation d’un travail pratique inadéquat.

Conformément à la PIEA, le personnel enseignant peut exiger qu’un travail soit refait s’il ne répond pas aux règles de présentation matérielle. Les pénalités de retard s’appliquent 24 heures après que la personne enseignante communique avec la personne étudiante. Le contenu ne doit pas être modifié lors de la 2e remise.
:::

## Utilisation de l'IA pour ce travail. 

Tel qu'indiqué dans le plan de cours:

:::info Niveau 2
L'étudiant rédige un texte, du code ou produit un travail sans consulter l'IA.
:::

## Fiche à remplir pour aider la correction

Afin de nous aider à faire la correction, veuillez copier  ce texte dans un fichier AideCorrectionTP3.txt que vous ajouterez dans le répertoire principal de votre projet:

```
Quelles sont les règles de votre jeu:
	-Comment se déplacer
	-Comment se défendre (2 façons) (les touches et ce quelles font)
	-Comment terminer un niveau
	-Comment terminer la partie
	
-Quels sont les 5 types d'éléments que vous avez utilisé:
	-Indiquez la classe et le type d'élément

-Quel est la capacité qui descend:
	-Indiquez la classe et comment elle réagit
```

