# TP1: Exercices en java     

## Énoncé

Pour ce travail, vous devez réaliser plusieurs séries d’exercices en Java. Veuillez consulter la section [Récupération du TP1](../intro/recuperationTP1)
des notes de cours afin de récupérer et remettre le travail. 

Vous devez vous assurez que tous les exercices passent les tests automatisés. 

De plus, vous devez comprendre ce que vous faites et produire du code de qualité qui correspond à ce qui est demandé.


## Modalités de remise

•	Votre projet devra être remis sur GitLab selon la procédure dans la section [Envoyer votre code les prochaines fois](../intro/recuperationTP1#envoyer-votre-code-les-prochaines-fois).  

•	La partie 1 du travail devra être remise avant **23:59:59 le jour du dernier cours de la semaine 3**

•	La partie 2 du travail devra être remise avant **23:59:59 le jour du dernier cours de la semaine 6**

## Critères d’évaluation

### Partie 1

|      **Critères**     |      **Pondération**     |
|---|---|
|     Série 1 - Introduction    |     3    |
|     Série 2 - Variables    |     4    |
|     Série 3 – Opérateurs et   conditions    |     9    |
|     Série 4 – Entrées clavier    |     5    |
|     Série 5 – Opérateurs booléens    |     12    |
|     Série 6 - String    |     6    |
|     Normes de présentation: correction négative<br>jusqu'à 2 points (5%)| |
|     Total d’évaluation    |     39    |
|     Total pondéré    |     7    |

### Partie 2

|      Critères     |      Pondération     |
|---|---|
|     Série 7 – Boucles    |     14    |
|     Série 8 – Entrées/Sorties   avancées    |     8    |
|     Série 9 – Math    |     4    |
|     Série 10 – Fonctions    |     18    |
|     Série 11 – Tableaux    |     12    |
|     Série 12 – Pseudo-code    |     8    |
|     Série 13 – Révision    |     12    |
|     Normes de présentation: correction négative<br>jusqu'à 4 points (5%)| |
|     Total d’évaluation    |     76    |
|     Total pondéré    |     13    |

Ce TP vaut donc 7+13 = 20 points sur la note finale. 


## Respect des normes de présentations

Tel qu'indiqué dans le plan de cours:
:::info Normes de présentation
Pour les travaux pratiques de programmation, le code doit respecter les normes de présentations de l’enseignant ou de la technologie utilisée. Un maximum de 5 % de la note totale peut être retranché pour la présentation d’un travail pratique inadéquat.

Conformément à la PIEA, le personnel enseignant peut exiger qu’un travail soit refait s’il ne répond pas aux règles de présentation matérielle. Les pénalités de retard s’appliquent 24 heures après que la personne enseignante communique avec la personne étudiante. Le contenu ne doit pas être modifié lors de la 2e remise.
:::

## Utilisation de l'IA pour ce travail. 

Tel qu'indiqué dans le plan de cours:

:::info Niveau 2
L'étudiant rédige un texte, du code ou produit un travail sans consulter l'IA.
:::