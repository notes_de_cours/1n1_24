# TP2 : jeu en mode console

## Énoncé

Pour ce travail, vous devez réaliser un jeu de Yam/Yahtzee/Yum.

Vous pouvez aller voir la description du jeu sur https://www.regles-de-jeux.com/regle-du-yams ou https://fr.wikipedia.org/wiki/Yahtzee


Vous trouverez un squelette d'application ci-dessous.

Créez un nouveau projet et copiez ce code dans **Programme.java**.
:::details Code à copier

```java
package ca.cegepdrummond;

import java.util.Random;

public class Programme {

    /**
     * Jeu de Yam's  (aussi connu sous les noms Yatzee, Yum.
     *
     * Vous pouvez trouver les règlements sur https://www.regles-de-jeux.com/regle-du-yams/
     *      ou https://fr.wikipedia.org/wiki/Yahtzee
     *
     *
     * @param args
     */
    public static void main(String[] args) {

        final int NOMBRE_DE_DES = 5; // le nombre de dés à brasser
        final int NOMBRE_DE_FACES = 6; // le nombre de faces sur les dés
        final int NOMBRE_DE_TOURS = 3; // le nombre de tours dans une partie
        final int NOMBRE_DE_LANCERS_PAR_TOUR = 3; // le nombre de lancers dans un tour


        int nombreDeDesABrasser; // le nombre de dés restants à brasser pour ce tour.
        int compteurDeLancer;  // le numéro du lancer actuel. Au dernier lancer, on ramasse tous les dés générés
        int[] desGardesPourTour; // les dés que l'usager veut conserver pour un tour.
        int[] desGardesPourLancer; // les dés gardés pour un lancer
        int[] desLances; // les dés générés pour un lancer
        int[] points = new int[NOMBRE_DE_FACES]; // le tableau de pointage
        int[] combinaisons; // les combinaisons pouvant être choisies pour ce tour.
        int combinaisonChoisie; // l'indice de la combinaison choisie pour ce tour.

        //TODO 13 boucler NOMBRE_DE_TOURS de fois
            nombreDeDesABrasser = NOMBRE_DE_DES;  // au début d'un tour, on brasse NOMBRE_DE_DES dés.
            compteurDeLancer = 1; // au début d'un tour, réinitialisation du compteur de lancer.
            desGardesPourTour = new int[NOMBRE_DE_DES]; // au début d'un tour, réinitialisation du tableau des dés choisis pour le tour.

            // TODO 7 tant qu'il reste des dés à brasser et qu'il reste des dés à
            //         lancer pour ce tour (le nombre de dés restant à brasser est plus grand que 0),
            //         et que le nombre de lancers pour le tour est plus petit ou égal au nombre de lancer maximum par tour
                // TODO 1 appeler lancerDes en lui passant le nombre de dés à brasser ainsi que le nombre de faces par dés,
                //  et conserver le résultat dans desLances

                // TODO 2 afficher un message indiquant que ce qui suit sont les dés brassés,
                //  et appeler afficherDes pour afficher desLances.

                // TODO 3a si on n'est pas au dernier lancer de ce tour.  (est-ce que le compteur de lancer est plus petit que le nombre de lancer pour 1 tour?)
                    // TODO 3 appeler choisirDesAGarder en lui passant les dés lancés et conserver le résultat dans desGardesPourLancer

                // TODO 3b sinon
                    //     le dernier coup on garde tous les des de ce lancer: mettre les desLances dans desGarderPourLancer
                    //     ATTENTION: ici vous avez besoin de copier un tableau dans un autre .. voir les notes de cours.

                compteurDeLancer++; // augmenter le compteur du nombre de lancers
                // TODO 4  appeler concatTableaux en lui envoyant les dés du tour et les dés du lancer, et conserver le résultat dans desGarderPourTour

                // TODO 5 afficher un message indiquant que ce qui suit sont les dés gardés pour ce tour, et appeler afficherDes pour afficher les dés gardés pour le tour.

                // TODO 6 calculer le nombre de dés à brasser pour le prochain tour et conserver le résultat dans nombreDeDesABrasser
                //   indice: (nombre de dés à brasser) - (nombre de dés gardés pour ce lancer)

            // TODO 8 appeler genererCombinaisons en lui passant les dés gardés pour ce tour, le tableau de points, et NOMBRE_DE_FACES
            //         conserver le résultat dans combinaisons.

            // TODO 9 appeler afficherCombinaisonsUtilisables pour afficher les combinaisons pouvant être choisies

            // TODO 10 appeler choisirCombinaisons en passant les combinaisons valides,
            //  et conserver le résultat dans combinaisonChoisie

            // TODO 11 calculer le pointage selon la combinaison choisie
            //   indice: Dans le cas simple ou il n'y a que les combinaisons de 1 à 6, il suffit de multiplier
            //           le numéro de la combinaison par la valeur de cette combinaison (le nombre de fois que cette valeur
            //           de dé est sortie).
            //   indice: n'oubliez pas que combinaisonChoisie démarre à 1, mais que les indices de tableaux partent à 0

            // TODO 12 afficher le pointage.

    }

    /**
     * Génère une série de dés de façon aléatoire*
     * @param nombreDeDes Le nombre de dés à générer
     * @param nombreDeFaces Le nombre de faces des dés
     * @return un tableau d'entiers représentant les dés.
     */
    private static int[] lancerDes(int nombreDeDes, int nombreDeFaces) {
        Random r = new Random();
        int[] resultats = new int[nombreDeDes];
        // TODO 1.1 générer nombreDeDes nombres aléatoire de 1 à nombreDeFaces (inclusivement) et les mettre dans resultats.

        return resultats;
    }

    /**
     * Affiche les dés sur une seule ligne, suivit d'un retour de chariot.
     * @param des les dés à afficher.
     */
    private static void afficherDes(int[] des) {
        // TODO 2.1 faire une boucle pour transférer les entiers de "des" dans une chaine de caractère
        //    aide: faire de la concaténation
        //    Ajouter le dé seulement s'il n'a pas la valeur 0.

        // TODO 2.2 afficher la chaine de caractères.
    }

    /**
     * Permet de choisir quels dés garder dans une série de dés
     * Si l'usager ne veut rien garder, il indique 0.
     * C'est la position du dé et non sa valeur qu'il faut utiliser.
     *
     * @param des un tableau de dés qui permettra de choisir les dés à conserver
     * @return les dés choisis.
     */
    private static int[] choisirDesAGarder(int[] des) {
        String positionDesAGarder = "";
        int[] desAGarder = new int[0];
        boolean valide = false;
        // TODO 3.1 tant que ce n'est pas valide
            // TODO 3.2 demander la position des dés à conserver (0 pour n'en conserver aucun)

            // TODO 3.3 appeler validerIndices et conserver le résultat dans valide.
            //          indice: pour la valeurMax de la fonction validerIndices vous devez utiliser le nombre de dés (c.à.d. la longueur du tableau de dés)

        // TODO 3.4 si l'usager n'a pas entré "0", appeler extraireDes
        //               et conserver le résultat dans desAgarder.
        //     notez que desAgarder est déjà initialisé avec un tableau vide, ce qui correspond à ce qu'on veut retourner
        //           dans l'éventualité où l'usager entre "0".

        return desAGarder;
    }

    /**
     * Valide que la chaine contenant les indices ne contient que des chiffres entre 1 et valeurMax
     * La valeur 0 est aussi acceptée
     *
     * @param indices chaine de caractère qui ne devrait contenir que des nombres entre 0 et valeurMax inclusivement.
     * @param valeurMax la valeur maximum pouvant être utilisé comme indice.
     * @return vrai si les indices sont bons, faux sinon.
     */
    private static boolean validerIndices(String indices, int valeurMax) {
        boolean valide = false;
        // TODO 3.3.1 valeurMax doit être > 0, sinon ce n'est pas valide

            // TODO 3.3.2 si indices est vide, ce n'est pas valide

            // TODO 3.3.3 sinon, si indices egal "0" c'est valide.

            // TODO 3.3.4 sinon si la longueur de indices est plus petite ou égale à valeurMax
            //                  faire une boucle pour vérifier que chacune des valeurs de indices
            //                         est un nombre entre 1 et valeurMax.
            //                     Si toutes les valeurs sont bonnes: c'est valide
            //                     Si une des valeurs n'est pas bonne: c'est invalide.
            //                  indice: présumer que c'est valide, et si une valeur est invalide, alors c'est invalide


        return valide;
    }

    /**
     * Extrait de "des" les dés aux indices contenus dans "positions"
     * @param des un tableau contenant les dés à extraire
     * @param positions les positions à extraire.
     * @return les dés extraits.
     */
    private static int[] extraireDes(int[] des, String positions) {
        int[] desExtraits = new int[positions.length()];
        // TODO 3.4.1 si positions n'égale pas "0", boucler pour transférer les dés aux positions requises.
        //  indice: "positions" est une chaine de caractères, il faut convertir chaque caractère en sa valeur numérique
        //          ensuite, il faut aller chercher dans "des", la valeur à cet indice-1.
        //          Pour convertir un caractère en sa valeur numérique, on peut utiliser
        //                   Character.getNumericValue(leCaractère)


        return desExtraits;
    }

    /**
     * Concatène les 2 tableaux d'entiers en ne conservant que les valeurs non égales à 0.
     * Le premier tableau peut contenir des 0. Il doit être le plus grand des deux, et le nombre
     * de valeurs non-égales à 0 plus tableau2.length ne doit pas dépasser tableau1.length
     * Il est possible que le tableau résultant contienne des 0 si le nombre d'éléments non-égal à 0
     * plus tableau2.length soit plus petit que tableau1.length.
     *
     * @param tableau1 un tableau d'entier duquel sont extraites les valeurs non-égales à 0.
     * @param tableau2 un deuxième tableau d'entier duquel sont extraites toutes les valeurs.
     * @return un tableau contenant les valeurs des deux tableaux.
     *
     */
    private static int[] concatTableaux( int[] tableau1, int[] tableau2 ) {
        int[] resultat = new int[tableau1.length];
        int pos = 0;
        // TODO 4.1 parcourir tableau1 et mettre les valeurs != 0 dans resultat[pos]
        //      la variable pos est incrémentée chaque fois qu'un élément est inséré dans resulat.

        // TODO 4.2 parcourir tableau2 et mettre toutes les valeurs dans resultat.
        //         indice: continuer à vous servir de "pos"

        return resultat;
    }


    /**
     * Calcule les combinaisons valides selon la valeur des dés et des combinaisons déjà utilisées.
     * Pour cette version simple, seules les combinaisons correspondantes à la valeur du dé sont utilisées.
     * 1) nombre de 1
     * 2) nombre de 2
     * 3) nombre de 3
     * ...
     * N) nombre de N
     * <p>
     * Une combinaison est valide si ces 2 conditions sont vraies :
     * -elle n'a pas déjà été utilisée (c'est pour cela qu'on doit avoir le tableau de points jusqu'à maintenant).
     * -les dés permettent de remplir les conditions pour cette combinaison.
     * <p>
     * Pour les combinaisons 1 à N, on met dans le tableau retourné le nombre de dés de cette valeur.
     * Ex : si la combinaison des 1 a déjà été utilisée et que les dés sont 12223, le résultat sera
     * 031000, indiquant par 0 que la combinaison des 1 ne peut être utilisée, car elle l'a déjà été, qu'il y a trois 2,
     * et un 3, et aucun 4, 5, 6.
     * ---------------
     * <p>
     * Les combinaisons suivantes ne sont pas utilisées pour cette version simple du jeu
     * <p>
     * N+1) brelan: 3 dés de même valeur (3 fois la valeur des dés identiques. ex: 444xx = 4*3=12)
     * N+2) carré: 4 dés de même valeur (4 fois la valeur des dés identiques)
     * N+3) full: 3 dés de même valeur + 2 dés de même valeur (25 points)
     * N+4) petite suite: 4 dés qui se suivent (30 points)
     * N+5) grande suite: 5 dés qui se suivent (40 points)
     * N+6) Yams: 5 dés de même valeur (50 points)
     * N+7) chance: somme des dés.  (somme des dés)
     *
     * @param des Les dés brassés
     * @param combinaisonsDejaUtilisees Tableau des points déjà utilisés
     * @param nombreDeFaces  Le nombre de faces des dés
     * @return un tableau contenant 0 à la position de la combinaison si celle-ci n'est pas valide, et le nombre
     * de fois que cette valeur du dé est sortie si la combinaison est valide.
     */
    private static int[] genererCombinaisons(int[] des, int[] combinaisonsDejaUtilisees, int nombreDeFaces) {
        int[] combinaisonsUtilisables = new int[nombreDeFaces];
        // TODO 8.1 parcourir les dés
		//          Si la valeur du dé n'est pas 0
		//          	Vérifier si la valeur du dé n'a pas été utilisée précédemment (combinaisonDejaUtilisees sera à 0 à cette position)
		//              	Si c'est 0, ajouter 1 dans le tableau des combinaisonsUtilisables à la position de la valeur du dé.
		//          	aide: pour savoir si le tableau des combinaisonsDejaUtilisees à la position de la valeur du dé-1 est égal à 0:
		//                          combinaisonsDejaUtilisees[(des[i]-1)] == 0 (rappelez moi de vous l'expliquer en cours)
		//                  	Vous pouvez appliquer la même logique pour augmenter combinaisons.
		//
        //          Ex : si les dés sont 11232 et que combinaisonsDejaUtilisees contient 0,0,6,0,0,0 indiquant que
        //                  la combinaison de 3 a déjà été utilisée (la valeur 6 indiquant qu'il y avait 2*3 précédemment)
        //               alors combinaisonsUtilisables devrait contenir 2,2,0,0,0,0 indiquant qu'il y a 2*1, 2*2, que le 3 ne
        //               peut être réutilisé, et qu'il n'y a pas de 4, 5, 6.

        return combinaisonsUtilisables;
    }

    /**
     * affiche les combinaisons valides (
     * @param combinaisons les combinaisons à afficher
     */
    private static void afficherCombinaisonsUtilisables(int[] combinaisons) {
        // TODO 9.1 faire une boucle pour afficher les combinaisons qui sont valides (qui ont une valeur > 0)
        // Dans la version simple, seule les combinaisons pour une valeur de 1 à 6 sont utilisées
        // Vous devez donc afficher
        // "1) Total des 1"
        // "2) Total des 2"
        // et ainsi de suite jusqu'à la fin du tableau
        // pour chacune des combinaisons > 0.


    }

    /**
     * sélection d'une combinaison parmi celle qui sont valides ( > 0 )
     * @param combinaisons tableau d'entier. Une valeur >0 indique une combinaison valide
     * @return le numéro de la combinaison choisie.
     */
    private static int choisirCombinaison(int[] combinaisons) {
        boolean bonneOption = false;
        int combinaisonChoisi = 0;
        // TODO 10.1 Tant que l'usager n'a pas choisi une bonneOption
        //   TODO 10.1.1 afficher un message pour indiquer qu'il faut choisir une combinaison
        //   TODO 10.1.2 demander la combinaison
        //   TODO 10.1.3 vérifier que le nombre entré est >0 et  < que la longueur de combinaison
        //   TODO 10.1.4 vérifier que cette combinaisons est valide dans les combinaisons passées en paramètre
        //                c-à-d: qu'elle n'est pas égale à 0.

        return combinaisonChoisi;
    }

    private static void afficherPointage(int[] points) {
        // TODO 12 faire une boucle pour afficher le pointage de chacune des combinaisons
        //         et cumuler les points pour afficher le grand total à la fin
        int grandTotal = 0;

    }
}

```
:::


Vous devez suivre la séquence des TODOs proposée en commencant par TODO 1, suivit de TODO 1.1. Si le TODO indique d'appeler une fonction, cette fonction sera la prochaine étape à réaliser. Ex: le TODO 1 indique d'appeler la fonction lancerDes. Cette fonction contient le TODO 1.1. 

Chacune de ces étapes est indépendante. Vous pouvez les coder séparément et vérifier qu'elles font vraiment ce qu'elles doivent faire en utilisant le débogueur afin de voir la valeur des variables durant l'exécution du programme.

Le pseudocode vous est aussi fourni pour chacune des étapes. Il est possible que vous pensiez à d'autres façons de résoudre le problème, mais dans une première étape veuillez suivre les instructions afin de simplifier la correction. ( 45 corrections à 10min = 6-8 heures ... merci de nous simplifier la vie :) )


## Modalités de remise

• La remise doit être faite sur sur Léa. 

:::danger Attention
Ne remettre que le fichier **Programme.java**
:::

• Ce TP est à remettre avant **23:59:59 le jour du dernier cours de la semaine 9**

## Critères d'évaluation

| Critères | Note |
|---|---|
| TODO* 1 lancerDes<br>TODO 2 afficherDes | 4 |
| TODO 3 choisirDesAGarder<br>Incluant 3A, 3B, et tous les sous-TODO de 3 | 12 |
| TODO 4 concatTableaux | 6 |
| TODO 5 affichage des dés gardés pour le tour<br>TODO 6 calcul du nombre de dés à brasser<br>TODO 7 boucle de tour | 6 |
| TODO 8 genererCombinaisons <br>TODO 9 afficherCombinaisons | 6 |
| TODO 10 choisirCombinaison<br>TODO 11 calculer le pointage<br>TODO 12 afficher le pointage<br>TODO 13 boucle de tour | 6 |
| Français: correction négative<br>-0,4 point par faute jusqu’à 6 points (15%)  |  |
| Normes de présentation: correction négative<br>jusqu'à 2 points (5 %)
| Total | 40 |

:::info
Ce TP vaut 15% de la note. La note sera donc divisée pour donner 15%
:::

## Respect des normes de présentations

Tel qu'indiqué dans le plan de cours:
:::info Normes de présentation
Pour les travaux pratiques de programmation, le code doit respecter les normes de présentations de l’enseignant ou de la technologie utilisée. Un maximum de 5 % de la note totale peut être retranché pour la présentation d’un travail pratique inadéquat.

Conformément à la PIEA, le personnel enseignant peut exiger qu’un travail soit refait s’il ne répond pas aux règles de présentation matérielle. Les pénalités de retard s’appliquent 24 heures après que la personne enseignante communique avec la personne étudiante. Le contenu ne doit pas être modifié lors de la 2e remise.
:::

## Utilisation de l'IA pour ce travail. 

Tel qu'indiqué dans le plan de cours:

:::info Niveau 2
L'étudiant rédige un texte, du code ou produit un travail sans consulter l'IA.
:::