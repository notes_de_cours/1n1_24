# Tir de missile

Dans cette leçon, nous allons ajouter le tir de missile par le vaisseau ennemi ajouté à la leçon précédente

## Ajout de la classe Missile

Ajoutez une sous-classe de `Element` appelée `Missile` et prenez le rond rouge et noir dans la catégorie `Other` des images. 

Nous allons ajouter un déplacement très simple dans le `act()`

```java
move(-10);
if( getX() < 0 ) { 
    getWorld().removeObject(this);
} 
```

Notez qu'il serait aussi possible de faire une rotation de 180 et de faire un move() positif. Mais ce missile est rond, ca ne change donc rien. 

## Tir du missile
Nous allons maintenant ajouter un tir de facon aléatoire dans `Ennemi`.

Dans `Ennemi.act()`, ajoutez le code suivant:

```java
if(Greenfoot.getRandomNumber(50) == 1) {
    getWorld().addObject(new Missile(), getX(), getY());
} 
```
Vous pouvez modifier la formule à votre guise, mais elle permet un tir quand même assez *réaliste*.

## Collision des missiles. 

Si un missile atteint un météore ou notre personnage, il faudrait enlever le météore ou la fusée, ainsi que le missile. 

Commençons par détecter une collision. Ajoutez donc le code suivant dans `Missile.act()` à la suite du `if()` :
```java
else {
    Personnage p = (Personnage)getOneIntersectingObject(Personnage.class);
    if(p!=null) {
        ((MyWorld)getWorld()).tuerPersonnage();
        getWorld().removeObject(this);
    } else {
        Meteore m  = (Meteore)getOneIntersectingObject(Meteore.class);
        if(m!=null) {
            ((MyWorld)getWorld()).detruireMeteore(m);
            getWorld().removeObject(this);
        }
    }
} 
```

et ajoutez les 2 nouvelles fonctions dans `MyWorld`
```java
public void tuerPersonnage() {
    removeObject(joueur);
    // C'est ici que le changement de tableau pourrait se faire
    // ou vous pourriez avoir un nombre de vies qui serait décrémenté ici. 
}

public void detruireMeteore(Meteore aMeteore) {
    removeObject(aMeteore);
}
```

Testez... 

Est-ce que le missile disparait s'il frappe qqchose ?
Est-ce que les météores disparaisent s'ils sont frappés par un missile ?
Est-ce que la fusée disparait si elle est frappée par un missile ?

## Animation de la destruction des météores

Pour réaliser l'explosion, nous allons utiliser un gif animé. 

Téléchargez cette image https://tenor.com/fr/view/explosion-deltarune-deltarune-explosion-boom-explode-gif-16548223447993760048

Sauvegardez l'image dans le répertoire **images** du projet greenfoot. Vous devez faire un clic de droit sur l'image pour la sauvegarder.  

Nous devons maintenant ajouter une classe pour gérer ce gif animé. Dans le menu **Edit/Import class**, importez la classe `GifImage`.

Ajoutez une classe `Explosion` sous `Element`. Vous n'avez pas à choisir d'image car nous allons la changer par programmation.

Ajoutez ce code dans Explosion:

```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Animation d'une explosion.
 * 
 * L'image de l'explosion provient de https://tenor.com/fr/view/explosion-deltarune-deltarune-explosion-boom-explode-gif-16548223447993760048
 * 
 * 
 */
public class Explosion extends Actor
{
    GifImage image; //L'image en format gif animé
    int compteurCycle; //le nombre cycle dans l'animation du gif

    /**
     * Anime l'explosion
     */
    public void act()
    {
        setImage(image.getCurrentImage());
        getImage().scale(40, 40);
        super.act();
        if(compteurCycle-- == 0) {
            ((MyWorld)getWorld()).removeObject(this);
        }
    }
    
    public void addedToWorld(World aWorld) {
        super.addedToWorld(aWorld);
        image = new GifImage("explosion-deltarune.gif");
        compteurCycle = image.getImages().size();
    }
}

```

Il reste maintenant à activer l'explosion. Ajoutez cette ligne au début de `MyWorld.detruireMeteore()`:
```java
addObject(new Explosion(), aMeteore.getX(), aMeteore.getY());
```

Cette ligne ajoute une explosion là ou se trouvait le météore, donnant l'impression que le météore c'est transformé en explosion. 

Testez...

Essayez de capturer quelques météores avec la fusée et de vous en servir comme bouclier. 
wouuu cool!!!


## Conclusion 

Dans cette leçon nous avons appris:

* comment tirer un missile;
* comment animer une explosion;
* comment remplacer un objet par un autre (missile -> explosion)

## Code partiel cette étape

Seules les classes ayant changées sont fournies. Pour les autres, allez à la leçon précédente. 

:::details Missile
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Missile here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Missile extends Element
{
    /**
     * Act - do whatever the Missile wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        move(-10);
        if( getX() < 0 ) { 
            getWorld().removeObject(this);
        } else {
            Personnage p = (Personnage)getOneIntersectingObject(Personnage.class);
            if(p!=null) {
                ((MyWorld)getWorld()).tuerPersonnage();
                getWorld().removeObject(this);
            } else {
                Meteore m  = (Meteore)getOneIntersectingObject(Meteore.class);
                if(m!=null) {
                    ((MyWorld)getWorld()).detruireMeteore(m);
                    getWorld().removeObject(this);
                }
            }
        }
    }
}
```
:::

:::details Ennemi
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Ennemi here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ennemi extends Element
{
    private int directionDeplacement;
    private SimpleTimer minuterieDeplacement = new SimpleTimer();

    public Ennemi() {
        minuterieDeplacement.mark();
    }

    /**
     * Act - do whatever the Ennemi wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        if(Greenfoot.getRandomNumber(50) == 1) {
            getWorld().addObject(new Missile(), getX(), getY());
        }
        if(minuterieDeplacement.millisElapsed()>1000) {
            directionDeplacement  = Greenfoot.getRandomNumber(3)-1;
            minuterieDeplacement.mark();
        }
        setLocation(getX(), Math.max(20,Math.min(getWorld().getHeight()-20,
                    getY()+directionDeplacement)));
    }
}
```
:::

:::details MyWorld
```java
import greenfoot.*;

public class MyWorld extends World
{

    Personnage joueur;  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 

        int[][] niveau1 = new NiveauBD().getTableauAIndice(0);
        GenerateurNiveaux gn = new GenerateurNiveaux(40,40,niveau1);
        gn.genererNiveau(this);

        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 
    }

    public void tuerPersonnage() {
        removeObject(joueur);
        // C'est ici que le changement de tableau pourrait se faire
        // ou vous pourriez avoir un nombre de vies qui serait décrémenté ici. 
    }

    public void detruireMeteore(Meteore aMeteore) {
        addObject(new Explosion(), aMeteore.getX(), aMeteore.getY());
        removeObject(aMeteore);
    }
}
```
:::

:::details Explosion
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Animation d'une explosion.
 * 
 * L'image de l'explosion provient de https://tenor.com/fr/view/explosion-deltarune-deltarune-explosion-boom-explode-gif-16548223447993760048
 * 
 * 
 */
public class Explosion extends Actor
{
    GifImage image; //L'image en format gif animé
    int compteurCycle; //le nombre cycle dans l'animation du gif

    /**
     * Anime l'explosion
     */
    public void act()
    {
        setImage(image.getCurrentImage());
        getImage().scale(40, 40);
        super.act();
        if(compteurCycle-- == 0) {
            ((MyWorld)getWorld()).removeObject(this);
        }
    }
    
    public void addedToWorld(World aWorld) {
        super.addedToWorld(aWorld);
        image = new GifImage("explosion-deltarune.gif");
        compteurCycle = image.getImages().size();
    }
}
```
:::

Vous devez aussi inclure la classe `GifImage`.

