# Gestion des collisions. 

Dans cette leçon vous allez apprendre comment détecter qu'un acteur touche à un autre. 

## Ajout des météores

:::info Connaissance générale
Quelle est la différence entre un météore et une météorite?

Un météore est un roche qui entre dans l'atmosphère. 

Une météorite est un météore qui a survécu et qui s'écrase sur la planète
:::

Nous allons placer des météores dans le jeu et faire en sorte que le personnage ne puisse les traverser. 

Commençons par créer une sous-classe de Actor que nous allons appeler `Meteore`. Utilisez la roche de la section `nature` comme image. 

Nous allons maintenant ajouter un `Meteore` dans le jeu à l'aide de `MyWorld`. 

Ajoutez le code suivant après le `super()` dans `MyWorld()`:
```java
Meteore unMeteore = new Meteore();
addObject(unMeteore, 300,100);
```

Faites reset du jeu. Voila, vous avez un météore dans le jeu. 

Si vous déplacez votre personnage, vous verrez qu'il passe au travers du météore. 

:::warning Attention
Votre personnage passe par dessus ou sous le météore dépendamment de l'ordre dans lequels vous les avez créés. 

Par défaut, le dernier objet créé est sur le dessus. Il est possible de changer l'ordre à l'aide de` setPaintOrder()` de `World`.
:::

## Rendre le météore impassable

Afin d'empêcher le personnage de passer à travers le météore, il faut que le personnage détecte qu'il est entré en collision. 

Il existe plusieurs fonctions permettant de détecter la présence d'un autre `Actor` à proximité. Allez dans le menu **Help/Greenfoot Class Documentation**, et sur cette page, cliquez sur `Actor` afin de voir la documentation de la classe. 

Les méthodes `getIntersectingObjects, getNeighbours, getObjectAtOffset, getObjectInRange, getOneIntersectingObject, getOneObjectAtOffset, intersects, isTouching`, sont toutes des fonctions vous donnant de l'information sur les objets qui sont à proximité. À vous d'expérimenter pour trouver celle qui répondra à vos besoins. 

Présentement, le besoin que nous avons est de savoir si en déplacant le personnage nous le ferons rencontrer un météore. Il faut donc faire l'évaluation avant de le déplacer. 

Nous avons présentement du code pour calculer la distance de déplacement. Il faudra donc vérifier si ce déplacement est bon avant de le faire.

Allons chercher la liste des objets se trouvant à cette distance. Nous allons utiliser `getObjectsAtOffset()`. 

Cette fonction cherche pour des objets d'une classe spécifique se trouvant à une distance X et Y de notre personnage. Elle va retourner une `List` (revoir la section sur les Listes dans les notes de cours)
Si cette liste n'est pas vide, c'est que le personnage entrerait dans un météore s'il se déplacait dans cette direction. 

Remplacez la ligne `setLocation` de `Personnage` par ce bloc de code:

```java
List<Meteore> elements = getObjectsAtOffset(distX, distY,  Meteore.class);    
if(elements.isEmpty()) { //le personnage ne frappe pas un meteore
    setLocation(getX()+distX, getY()+distY);
}
```
:::warning Attention
Vous aurez une erreur sur List. Faites un clic de droit et sélectionnez **Fix:import class java.util.List**
:::
:::tip Info
Ici nous avons utilisez getObjectsAtOffset(), mais nous aurions aussi pu utiliser getOneObjectAtOffset() pour obtenir le même résultat. Il y a souvent plusieurs façons de faire la même chose
:::

:::info Théorie
La fonction `getObjectsAtOffset()` retourne une `List`. Il est donc possible de parcourir cette liste ou d'utiliser toutes les fonctionnalités vues dans la section **Listes** des notes de cours. 
:::


Testez...


Comme vous pouvez le voir, le personnage entre à moitié dans le météore. Il faut se rappeler que les coordonnés d'un `actor` sont **en son centre**. 

Il est possible de corriger cette erreur en ajoutant la moitié de la largeur de l'image lors des calculs. De plus, il faut prendre en considération la direction afin d'additionner ou soustraire cette demi largeur. 

Ajoutez ces 2 lignes après le calcul de distX et distY 
```java
int distX_corrigee= distX + (getImage().getWidth()*Integer.signum(distX)/2);
int distY_corrigee= distY +  (getImage().getWidth()*Integer.signum(distY)/2);
```

:::info Théorie
La fonction [Integer.signum()](https://www.geeksforgeeks.org/integer-signum-method-in-java/) retourne -1,0,1 selon la valeur de l'entier. 
Cette valeur nous permet donc de savoir la direction d'approche du personnage pas rapport avec le météore.
:::

et d'utiliser cette distance corrigée dans getObjectsAtOffset(). 

```java
List<Meteore> elements = getObjectsAtOffset(distX_corrigee, distY_corrigee,  Meteore.class);    
```

Question: pourquoi utilisons-nous getWidth() pour le Y ?

:::details Réponse
Quand le personnage se déplace verticalement, l'image est tournée, c'est donc sa longueur dont nous avons besoin et non de sa hauteur. 
:::

Notez que ce n'est pas parfait. Il est encore possible de passer un peu dans l'image du météore. Mais c'est assez pour nos besoins. 

## Correction de la taille du météore.

Le météore est un peu gros. Nous allons donc modifier la taille de son image. 

Nous allons ajouter un constructeur à la classe `Meteore` afin de modifier la taille de l'image lors de sa création. 

:::info Théorie
Le constructeur est une méthode spéciale. Il est appelé lors du `new`. Il permet de créer une instance de cette classe. Il sert habituellement à initialiser les attributs.

Le constructeur se distingue par le fait que la **méthode porte le nom de la classe** et aussi par le fait que la **méthode n'a pas de type de retour** (même pas void).

Il est possible d'avoir plusieurs constructeurs. Ils doivent avoir une signature différente. La signature est la liste des arguments. Cette liste doit se différencier d'un constructeur à un autre soit en ayant un nombre d'argument différent des autres constructeurs, soit en ayant des d'arguments de types différents des autres constructeurs.
:::

Ajoutez le code suivant dans la classe `Meteore`:
```java
public Meteore() {
    GreenfootImage im = getImage();
    if(im != null) {        
        im.scale(40,40);
        setImage(im);
    }
}
```
Testez...

:::warning Attention
Nous verrons plus tard que ce n'est pas la meilleure place pour mettre ce code. Nous y reviendrons.
:::


## Déplacement du météore.

Nous allons reprendre la technique utilisée dans le jeu de la fusée afin de déplacer notre météore. 

Ajoutez ce code dans `Meteore.act()`:

```java
int distX = Greenfoot.getRandomNumber(3);
int distY = Greenfoot.getRandomNumber(9)-4;
setLocation(getX()-distX, getY()+distY);

turn(Greenfoot.getRandomNumber(5));

if( getX() == 0 ) { 
    getWorld().removeObject(this);
}
```

:::info Théorie
Les météores sont ajoutés lors du `new`. Mais tant qu'ils existent, ils prennent de la mémoire. Après un certain temps, la mémoire se remplirait et votre ordi cesserait de fonctionner. 

En POO, un mécanisme appelé `ramasse miette` (*garbage collector*) est utilisé afin de détruire les objets qui ne sont plus utilisés. 

Dans le code ci-haut, le fait de faire `removeObjet` fait en sorte qu'aucun objet ne *référence* le météore. Celui-ci sera automagiquement enlevé de la mémoire. 

:::

Testez...

Oups, si vous foncez dans la roche avec la fusée, celle-ci ne peut plus avancer, mais le météore continue. Il faudrait donc que le météore arrête de bouger s'il croise notre fusée. 

Remplacez la ligne du `setLocation` dans `Meteore.act()` par ce code:
```java
List<Personnage> elements = getObjectsAtOffset(distX, distY, Personnage.class);    
if(elements.isEmpty()) { 
    setLocation(getX()-distX, getY()+distY);
}
```

## Conclusion

Dans cette leçon nous avons appris:

* comment détecter une collision;
* comment empêcher un objet de se déplacer;
* comment changer la taille de l'image d'un actor;
* comment faire un déplacement aléatoire

Nous verrons aussi que certaines parties du code ne sont pas placées de façon optimale. Il est fréquent de réécrire une partie du code lorsqu'on s'apercoit qu'il est réutilisé ailleur et qu'il faudrait le placer plus intelligemment afin d'éviter de la redondance de code. Par exemple, ici nous avons changé la taille du météore. Mais nous allons avoir d'autres objets à placer dans le jeu. Allons nous mettre ce changement de taille dans chaque classe ? ou allons nous plutôt généraliser cette fonctionnalité?    ... à suivre...


## Code pour cette étape

:::details MyWorld
```java
import greenfoot.*;

public class MyWorld extends World
{

    Personnage joueur;  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 

        Meteore unMeteore = new Meteore();
        addObject(unMeteore, 300,100);

        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 
    }
}
```
::::

:::details Personnage
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Le personnage controlé par le joueur. 
 * 
 * @author Benoit Desrosiers
 * @version 20240916
 */
public class Personnage extends Actor
{
    private final String TOUCHE_DROITE = "right"; 
    private final String TOUCHE_GAUCHE = "left";
    private final String TOUCHE_BAS = "down";
    private final String TOUCHE_HAUT = "up";

    private final int ANGLE_HAUT = 270;
    private final int ANGLE_DROITE = 0;
    private final int ANGLE_BAS = 90;
    private final int ANGLE_GAUCHE = 180;

    private final int VITESSE = 4;

    /**
     * Act - do whatever the Personnage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; 
        boolean touchePressee = false;

        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
            touchePressee = true; 
        } 

        if(touchePressee) {
            tourner(rotation, ancienneRotation);
            int distX = (int) Math.cos(Math.toRadians(rotation))*VITESSE;
            int distY = (int) Math.sin(Math.toRadians(rotation))*VITESSE;
            int distX_corrigee= distX + (getImage().getWidth()*Integer.signum(distX)/2);
            int distY_corrigee= distY +  (getImage().getWidth()*Integer.signum(distY)/2);
            List<Meteore> elements = getObjectsAtOffset(distX_corrigee, distY_corrigee,  Meteore.class);
            if(elements.isEmpty()) { //le personnage ne frappe pas un meteore
                setLocation(getX()+distX, getY()+distY);
            }      
        }
    }

    /**
     * Change la rotation du personnage
     * Si le personnage doit pointer vers la gauche, son image est pivotée
     * afin qu'il n'ait pas la tête en bas. Si on retourne vers la droite, 
     * il est pivoté de nouveau
     * 
     * @param aRotation la rotation demandée
     * @param aAncienneRotation l'ancienne rotation
     */
    public void tourner(int aRotation, int aAncienneRotation) {
        if(aRotation != aAncienneRotation) {
            if(aAncienneRotation == ANGLE_GAUCHE || aRotation == ANGLE_GAUCHE) {
                getImage().mirrorVertically();
            }
        }
        setRotation(aRotation);
    }
}
```
:::

:::details Meteore
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Meteore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Meteore extends Actor
{
    public Meteore() {
        GreenfootImage im = getImage();
        if(im != null) {        
            im.scale(40,40);
            setImage(im);
        }
    }

    /**
     * Act - do whatever the Meteore wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int distX = Greenfoot.getRandomNumber(3);
        int distY = Greenfoot.getRandomNumber(9)-4;
        List<Personnage> elements = getObjectsAtOffset(distX, distY, Personnage.class);    
        if(elements.isEmpty()) { 
            setLocation(getX()-distX, getY()+distY);
        }

        turn(Greenfoot.getRandomNumber(5));

        if( getX() == 0 ) { 
            getWorld().removeObject(this);
        }
    }
}

```
:::
