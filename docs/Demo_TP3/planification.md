# Planification de la réalisation du TP3

Le TP3 est votre premier programme fait en solo, sans filet. 

Voici donc quelques idées pour faire la planification de votre travail.

## 1) Quel sera le concept du jeu?

Avant de vous lancer dans la programmation du jeu, il serait bon d'avoir une idée de la logique de celui-ci (le *gameplay*)

* quel sera le thème ? (un jeu de zombie, un jeu de combat, un jeu de logique...)
* quels seront les difficultés ? (des ennemis qui poursuivent le joueur, des énigmes à résoudre...)
* quel sera le thème graphique ? (chateaux, monde futuriste, monde apocalyptique, champs avec des brebis et des moutons... )
* qu'est ce qui fait que le joueur meurt ?
* comment ramasse-t-il des points ? 


## 2) Quel sera la séquence des tableaux ?

Il faut au moins 2 tableaux. 

* quel sera le mécanisme pour passer de l'un à l'autre ?
* peut-on revenir au premier ?
* qu'est ce qui fait qu'on termine un tableau ? (nombre de point, une porte, une énigme résolue...)
* qu'est ce qui fait que la partie est terminée (gagnée ou perdue) ? (nombre de points, tous les ennemis tués, ennemi final tué...)

## 3) Comment seront intégrés les éléments obligatoires ?

Il faut 5 éléments de jeu. Lesquels choisir?

Passez en revue la liste des types d'éléments et notez des idées. 

Selon les choix du thème du jeu, certains éléments seront plus *évidents* à intégrer. 

Avez-vous une petite idée comment ces éléments pourraient se coder ? Basez vous sur les notions apprisent pour le TP3 dans les notes de cours. 

Nous avons vue comment:
* créer une grille (tableau) d'objets
* déplacer le joueur avec le clavier (et avec la souris dans le démo de la fusée);
* gérer les collisions, ce qui permet de détruire un objet, ou de le ramasser; 
* tirer un projectile;
* tuer notre joueur;
* transformer un objet en un autre;
* déplacer des objets (ennemis) aléatoirement, ou selon une trajectoire;
* afficher des compteurs (peuvent servir pour compter le nombre d'objets ramassés) 

## 4) Dans quel ordre écrire le programme

Nous vous donnons ici une séquence, vous n'êtes pas obligé de la suivre, mais c'est une séquence *logique* d'étapes :

1) faire déplacer votre joueur;
1) concevoir le premier tableau (générateur) avec 1 type d'objet (mur);
1) faire réagir le joueur selon les objets du tableau;
1) ajouter un autre type d'objet;
1) faire réagir le joueur selon ce nouvel objet;
1) ajouter un objet qui *attaque* le joueur et permettre au joueur de se défendre (tirer un balle, mettre une bombe/trappe, ...);
1) faire mourir le joueur;
1) ajouter la capacité qui descend;
1) faire le changement de tableau;
1) ajouter un son

Une fois que vous aurez tout ca de fait, vous aurez une bonne compréhension de Greenfoot. Ajouter d'autres éléments sera beaucoup plus rapide. 

Au fur et à mesure que vous écrivez ce code, pensez à des tests à faire. Vous en avez 5 à écrire. 


## Et finalement: Demander de l'aide

N'hésitez pas à demander de l'aide à votre enseignant si vous êtes bloqué.e sur un concept ou sur un bout de code. 

