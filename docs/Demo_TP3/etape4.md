# Création d'un ennemi. 

Dans cette leçon nous allons ajouter une vaisseau ennemi qui va tirer des ogives nucléaires capables de détuire les météores ... et notre fusée. Il faudra donc éviter ces missiles.

## Génération du vaisseau ennemi

Ajoutez une nouvelle classe sous `Actor` et appelez la `Ennemi`. Prenez l'avion comme image. 

Il faut maintenant ajouter cet ennemi dans le jeu. Nous avons déjà un outil pour le faire... le générateur de niveaux. 

Dans le tableau décrivant le niveau 1 dans `NiveauBD`, remplacez un 0 ou un 1 par un 2 dans une colonne à droite. 

Ensuite, dans `GenerateurNiveaux`, remplacez le case(2) par:
```java
case(2):
    Ennemi e = new Ennemi();
    aWorld.addObject(e,posX,posY);
    e.setRotation(180);
    break;
```
Testez...

Ca fonctionne, mais l'avion est un peu grosse. 

Nous pourrions faire comme avec le météore et changer le constructeur d'Ennemi pour changer la taille de son image. Mais en faisant cela, on se trouve à dupliquer du code :angry:.

Nous allons profiter de cette occasion pour créer une classe qui représentera les éléments qu'on ajoute sur le tableau et qui réalisera les comportements communs. 

Ajoutez une classe sous `Actor` et appelez la `Element`. Cette classe n'aura pas d'image. 

Ensuite, changez le `extends` dans `Meteore` et dans `Ennemi` pour remplacer `Actor` par `Element`. 

Vous devriez vous retrouver avec la hierarchie suivante:

![hierarchie1](/hierarchie1.png)

:::info Théorie
En Java, et dans plusieurs langage OO, on appel `héritage` le fait qu'une classe puisse acquérir les attributs et les méthodes d'une autre classe. 

Dans notre scénario, un `Ennemi` peut faire tout ce qu'un `Element` peut faire. Toutes les méthodes définies dans Element sont donc utilisable par un Ennemi.

:::

:::info Théorie 
Pour être plus précis, seul les attributs et méthodes `public` et `protected` sont disponibles. 

Il y a 3 modificateurs d'accès. 

* public
    * accessible par **l'utilisateur** de la classe (dans Programme par exemple)
* protected
    * accessible par les **enfants** de la classe ou les classes du package (on va voir ça plus tard)
* private
    * accessible seulement à l'**intérieur** de la classe

protected et private sont à favoriser. Vous verrez les raisons dans les cours de POO. 
:::

Dans `Element`, ajoutez la fonction suivante:

```java
protected void addedToWorld(World aWorld) {
    int taille = 40;
    GreenfootImage im = getImage();
    if(im != null) {        
        im.scale(taille,taille);
        setImage(im);
    }
}
```

et enlevez le constructeur dans `Meteore`.

La méthode `addedToWorld`, bien que définie dans Element, est aussi accessible dans Ennemi et Meteore grace à l'héritage.


Testez... l'avion devrait avoir une meilleure taille, et les météores sont toujours à la même taille. 

:::info 
La méthode `addedToWorld()` est appelée automatiquement **après** que l'objet ait été ajouté dans le monde. 

Elle permet donc d'effectuer certaines opérations qui n'aurait pas été possibles avant que l'objet n'ai été ajouté dans le World. 

:::

:::tip Utilisation de Element
Comme indiqué, la classe `Element` servira à mettre des comportements communs à tous les éléments du jeu. 
Si vous vous apercevez que vous dupliquez du code dans plusieurs classes, n'hésitez pas à les regrouper comme sous-classe d'une classe commune. 
:::

## Déplacement de l'ennemi

Notre vaisseau ennemi aura simplement un déplacement vertical. 

Ajoutez cette variable dans la classe `Ennemi`:
```java    
private int directionDeplacement;
```
Ajoutez ce code dans `Ennemi.act()`:

```java
directionDeplacement = Greenfoot.getRandomNumber(3)-1;
setLocation(getX(), Math.max(20,Math.min(getWorld().getHeight()-20,
            getY()+directionDeplacement)));
```

:::info
Le random génère les valeurs suivantes: 0, 1 ou 2. En soustrayant 1, on obtient -1, 0, 1. Ce qui signifie que le déplacement ce fera d'une unité vers le haut ou le bas, ou pas du tout. 
:::

Le `setLocation` utilise la même technique que `Personnage` afin de s'assurer de ne pas sortir du jeu. Ici, il n'ira pas plus loin que 20 pixels du bord. 

Testez ... 

Hmmm pas très concluant. Notre vaisseau semble ne pas être certain de sa direction. 

Afin d'améliorer le mouvement, nous allons faire en sorte qu'il se déplacera pendant un certain temps dans une direction avant de changer.

Ajoutez cette variable dans la classe `Ennemi`:

```java
private SimpleTimer minuterieDeplacement = new SimpleTimer();
```

Oups la classe `SimpleTimer` n'existe pas. 

Pour l'installer, allez dans le menu **Edit** du jeu (pas de l'éditeur) et choisissez **Import class**. Sélectionnez `SimpleTimer` et OK.

Ajoutez un constructeur pour `Ennemi` afin de démarrer la minuterie :
```java
public Ennemi() {
    minuterieDeplacement.mark();
}
```

Changez l'assignation de `directionDeplacement` par le code suivant:
```java
if(minuterieDeplacement.millisElapsed()>1000) {
    directionDeplacement  = Greenfoot.getRandomNumber(3)-1;
    minuterieDeplacement.mark();
}
```

Testez... voila l'avion se promène de haut en bas de façon aléatoire. 

# Conclusion 

Dans cette leçon nous avons appris:

* comment ajouter un nouvel élement dans le générateur de tableau;
* comment regrouper certaines fonctionnalités dans une super-classe;
* comment utiliser une minuterie. 


## Code pour cette étape

:::details MyWorld
```java
import greenfoot.*;

public class MyWorld extends World
{

    Personnage joueur;  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 

        int[][] niveau1 = new NiveauBD().getTableauAIndice(0);
        GenerateurNiveaux gn = new GenerateurNiveaux(40,40,niveau1);
        gn.genererNiveau(this);

        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 
    }
}
```
::::

:::details Personnage
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Le personnage controlé par le joueur. 
 * 
 * @author Benoit Desrosiers
 * @version 20240916
 */
public class Personnage extends Actor
{
    private final String TOUCHE_DROITE = "right"; 
    private final String TOUCHE_GAUCHE = "left";
    private final String TOUCHE_BAS = "down";
    private final String TOUCHE_HAUT = "up";

    private final int ANGLE_HAUT = 270;
    private final int ANGLE_DROITE = 0;
    private final int ANGLE_BAS = 90;
    private final int ANGLE_GAUCHE = 180;

    private final int VITESSE = 4;

    /**
     * Act - do whatever the Personnage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; 
        boolean touchePressee = false;

        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
            touchePressee = true; 
        } 

        if(touchePressee) {
            tourner(rotation, ancienneRotation);
            int distX = (int) Math.cos(Math.toRadians(rotation))*VITESSE;
            int distY = (int) Math.sin(Math.toRadians(rotation))*VITESSE;
            int distX_corrigee= distX + (getImage().getWidth()*Integer.signum(distX)/2);
            int distY_corrigee= distY +  (getImage().getWidth()*Integer.signum(distY)/2);
            List<Meteore> elements = getObjectsAtOffset(distX_corrigee, distY_corrigee,  Meteore.class);
            if(elements.isEmpty()) { //le personnage ne frappe pas un meteore
                setLocation(Math.max(20,Math.min(getWorld().getWidth()-20, getX()+distX)), 
                    Math.max(20,Math.min(getWorld().getHeight()-20,getY()+distY)));
            }      
        }
    }

    /**
     * Change la rotation du personnage
     * Si le personnage doit pointer vers la gauche, son image est pivotée
     * afin qu'il n'ait pas la tête en bas. Si on retourne vers la droite, 
     * il est pivoté de nouveau
     * 
     * @param aRotation la rotation demandée
     * @param aAncienneRotation l'ancienne rotation
     */
    public void tourner(int aRotation, int aAncienneRotation) {
        if(aRotation != aAncienneRotation) {
            if(aAncienneRotation == ANGLE_GAUCHE || aRotation == ANGLE_GAUCHE) {
                getImage().mirrorVertically();
            }
        }
        setRotation(aRotation);
    }
}
```
:::

:::details Meteore
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Meteore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Meteore extends Element
{
    /**
     * Act - do whatever the Meteore wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int distX = Greenfoot.getRandomNumber(3);
        int distY = Greenfoot.getRandomNumber(9)-4;
        List<Personnage> elements = getObjectsAtOffset(distX, distY, Personnage.class);    
        if(elements.isEmpty()) { 
            setLocation(getX()-distX, getY()+distY);
        }

        turn(Greenfoot.getRandomNumber(5));

        if( getX() == 0 ) { 
            getWorld().removeObject(this);
        }
    }
}


```
:::


:::details NiveauBD
```java
/**
 * Write a description of class NiveauBD here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NiveauBD  
{
    private int[][][] elements = { 
            //niveau 0
            {    
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,2,1},
                {1,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            },
        };

    /**
     * retourne un tableau bidimensionel 
     * @param indice le numéro du tableau à retourner (commence à 0)
     */
    public int[][] getTableauAIndice(int indice) {
        return elements[indice];
    }
}

```
:::

:::details GenerateurNiveaux
```java
import greenfoot.*;  
import java.util.*;

/**
 * Génération d'un niveau basé sur un matrice contenant le numéro des éléments à ajouter
 *
 */
public class GenerateurNiveaux  
{

    private int[][] elements;
    private  int largeur;
    private  int hauteur;

    /** 
     * Constructeur 
     * @param aLargeur la largeur en pixels des éléments 
     * @param aHauteur la hauteur en pixels des éléments
     * @param aNiveau le tableau à 2 dimensions contenant les id des éléments à ajouter
     * 
     * 0) aucun élément
     * 1) un mur
     */
    public GenerateurNiveaux(int aLargeur, int aHauteur, int [][] aNiveau) {
        largeur = aLargeur;
        hauteur = aHauteur;
        elements = aNiveau;
    }

    /**
     * Génère le niveau 
     * @param aWorld le World appelant 
     */
    public void genererNiveau(MyWorld aWorld) {
        for(int i = 0; i<elements.length; i++) {
            int posY = i*hauteur+hauteur/2;
            for(int j = 0; j<elements[i].length; j++) {
                int posX = j*largeur+largeur/2;
                switch(elements[i][j]) {
                        case(0):
                        break;
                        case(1): 
                        Meteore m1 = new Meteore();
                        aWorld.addObject(m1, posX+aWorld.getWidth(), posY);        
                        break;
                        case(2):
                        Ennemi e = new Ennemi();
                        aWorld.addObject(e,posX,posY);
                        e.setRotation(180);
                        break;
                }
            }
        }  
    }

    /**
     * La largeur en pixel du niveau
     */
    public int getLargeurNiveau() {
        return largeur * elements[0].length;
    }

    /**
     * La hauteur en pixel du niveau
     */
    public int getHauteurNiveau() {
        return hauteur * elements.length;
    }
}
```
:::

:::details Element
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Element here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Element extends Actor
{
    /**
     * Act - do whatever the Element wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        // Add your action code here.
    }

    protected void addedToWorld(World aWorld) {
        int taille = 40;
        GreenfootImage im = getImage();
        if(im != null) {        
            im.scale(taille,taille);
            setImage(im);
        }
    }
}
```
:::

:::details Ennemi
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Ennemi here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ennemi extends Element
{
    private int directionDeplacement;
    private SimpleTimer minuterieDeplacement = new SimpleTimer();

    public Ennemi() {
        minuterieDeplacement.mark();
    }

    /**
     * Act - do whatever the Ennemi wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        if(minuterieDeplacement.millisElapsed()>1000) {
            directionDeplacement  = Greenfoot.getRandomNumber(3)-1;
            minuterieDeplacement.mark();
        }
        setLocation(getX(), Math.max(20,Math.min(getWorld().getHeight()-20,
                    getY()+directionDeplacement)));
    }
}
```
:::

Vous devez aussi inclure la classe `Simpletimer`.