# Algoritmes qui vous seront utiles

À partir des techniques illustrées dans ce démo, et avec un peu d'imagination, il est possible de créer les éléments nécessaires pour le TP3. Il suffit d'extraire certains bouts de code et de les appliquer aux bons endroits. 


Par exemple pour le TP3, vous pourriez créer les éléments suivants:

* un coffre qui contient une clé permettant d'accéder à une porte;
* des zombies qui vous poursuivent;
* des portes qui vous permettent d'accéder aux autres niveaux;
* des canons qui vous tirent dessus;
* des murs pouvant être détruits par une bombe ou un balle;
* des articles divers pouvant être ajoutés à un inventaire;
* des tuiles qui ralentissent ou accélèrent le personnage;
* ... utilisez votre imagination!


Cette section contient des idées sur comment réaliser certains composants du TP3. 

Vous pouvez récupérer le gfar du projet démo réalisé en classe. Notez que ce projet a été augmenté de quelques fonctionnalités intéressantes. De plus, il est documenté comme vous devriez le faire pour le TP3. 

[demo pour le TP3](/demo2024.zip)

Pour l'exécuter, vous devez extraire le fichier gfar du fichier zip. Ensuite quand vous cliquez sur le gfar, l'archive sera extraite et Greenfoot s'ouvrira sur ce projet. Par la suite, vous ne devez **plus** utiliser le `gfar`, vous **devez** utiliser le fichier `project.greenfoot` qui se trouve dans le répertoire 

## Gestion du jeu

La gestion des actions du jeu devrait se faire majoritairement dans MyWorld. 

Par exemple, ce n'est pas à un projectile de savoir ce qui devrait se passer si le personnage meurt. Le projectile devrait indiquer à MyWorld qu'il vient de percuter le personnage et donc que celui-ci devrait mourir. Le monde s'occupera de diminuer le nombre de vie et de replacer le jeu en conséquence, ou de rediriger vers le tableau de fin. Ce même comportement (tuer le personnage) pourrait être demander par un ennemi. Il ne faut donc pas dupliquer la logique. 

Exemple: Missile appel tuerPersonnage() ou detruireMeteore. 

## Tirer un projectile

Commencez par créer la classe du projectile. 

Exemple: Missile

Le `act()` de cette classe déplacera l'acteur. 

Lors de la création du projectile, donnez lui une rotation. Elle devrait normalement etre celle dans laquelle était le personnage quand il a tiré. 

La fonction `move()` permet de déplacer un acteur d'un certain nombre de pixel selon sa rotation.

Lorsque la balle frappe qqchose, elle doit faire une action:
* un mur: elle est détruite
* le personnage: il meurt
* un ennemi: il meurt et donne potentiellement des points
* autre: à vous de décider. 

La balle doit être enlevée du jeu lorsqu'elle frappe qqchose, ou si elle sort du jeu (si le jeu n'est pas entouré de murs).

Exemple: Missile.act()

Lorsqu'un ennemi ou le personnage meurt, c'est le monde qui devrait faire l'action. Ce n'est pas à la balle de connaitre la logique à appliquer pour se débarrasser d'un zombie!

## Déplacer les ennemis

Vous pouvez déplacer les ennemis de façon aggressive, ou stupide. 

Pour les déplacer aggressivement, vous pouvez chercher le personnage dans un certain rayon à l'aide de `getObjectsInRange()`. Si cette fonctionne retourne le personnage, vous pouvez tourner l'ennemi dans sa direction à l'aide de `turnTowards()` et le déplacer à l'aide de `move()`.

Pour les déplacer stupidement, vous n'avez qu'à calculer un angle aléatoire et tourner l'ennemi dans cette rotation. 

Afin que le déplacement aléatoire ne soit pas trop sacadé, vous pouvez mettre un compteur qui décrémente ou une minuterie, et lorsqu'il tombe à zéro vous recalculez une nouvelle direction et repartez le compteur. 

Exemple: Ennemi

Si l'ennemi touche au personnage, vous devez enlever l'ennemi, et enlever une vie au personnage. 

Ici encore, ce comportement devrait être géré par le MyWorld. Ce n'est pas au zombie de savoir quoi faire lorsque le personnage meurt.

## Gestion de comportement commun

Vous vous retrouverez avec plusieurs classes d'élément de jeu. Certains auront des caractéristiques communes. 

Comme exemple, nous avons déjà parlé du fait qu'il faudra enlever tous les éléments du jeu sauf le personnage et les compteurs lors d'un changement de niveau. 

Afin de ne pas enlever les éléments qui doivent rester, il serait possible de mettre ceux qui doivent disparaitre comme étant des sous-classes d'une sous-classe de Actor. Par exemple, une classe `ElementGenere` indiquant que les sous-classes sont des élements qui sont ajoutés par le générateur. 

Exemple: Element

Cette classe `ElementGenere` sera aussi un bon endroit pour mettre des caractéristiques commune tel que le fait que l'élément peut par exemple:
* être traversé (s'il y a plusieurs types de murs)
* être tué par balle (s'il y a plusieurs types de zombies)
* valoir de l'argent (pour les choses à ramasser)
* etc. 

Il suffira de mettre une fonction retournant un boolean ayant une valeur par défaut dans la classe `ElementGenere` et de redéfinir cette fonction pour retourner une autre valeur dans les sous-classes qui en ont besoin. 

## Utilisation de minuteur. 

Il est fréquent de vouloir réaliser une opération après un certain temps. Par exemple, votre personnage dépose une bombe. Vous ne voulez pas qu'elle lui explose dans le visage. Il est donc nécessaire de mettre un minuteur. 

Heureusement, Greenfoot contient une classe pour faire un minuterie: dans **Edit/Import Class**, vous y trouverez `SimpleTimer`. 

Exemple: Ennemi

## Affichage de pointage

Ici aussi, Greenfoot fournit une classe pour gérer un pointage: dans **Edit/Import Class**, utilisez `Counter`, ou `Label`. 

Exemple: MyWorld


## Affichage de texte

Il y a 2 façons de le faire:

Greenfoot fournit la classe `Label` permettant d'afficher un texte.

World fournit la méthode `showText()` permettant aussi d'afficher un texte. 


Exemple: Fin pour les deux. 


## Demander de l'info au joueur

La classe Greenfoot a une fonction appelée `ask()` permettant de demander une String à l'usager (l'équivalent de Console.demanderTexte()).

Lorsque cette fonction est appelée, le jeu est arrêté tant que l'usager n'appuie pas sur OK. 

Vous pourriez vous en servir afin de demander à l'usager quoi faire dans tel situation. 
