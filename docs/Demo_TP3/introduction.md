# Techniques pour le TP3

Dans cette leçon vous allez apprendre quelques techniques utiles pour réaliser le TP3. 

Vous apprendrez des techniques générales. Il vous faudra un peu d'imagination pour réappliquer celles-ci dans la réalisation du TP3. 

Il est possible que vous pensiez à d'autres façons de réaliser ce qui est demandé dans le TP3. N'hésitez pas à expérimenter... il y a souvent plusieurs façons de faire la même chose. 

Par contre, si vous expérimentez, assurez-vous de bien comprendre ce que vous faites. Si l'enseignant voit du code qui est radicalement différent de ce qui a été enseigné, il est possible que vous ayez à l'expliquer en détails. Si vous n'en êtes pas capable, l'enseignant devra présumer que vous n'êtes pas l'auteur de ce code ... = plagiat. 

## Le jeu

Cette année, le projet sera de réaliser un jeu de tableau dans lequel un personnage se promènera et réalisera des quêtes afin de gagner la partie. Durant ces quêtes, il devra combattre des ennemis, ramasser des biens, et résoudre des énigmes. 


Voici un exemple de ce que pourrait avoir l'air le jeu.

![demoTP3_1](/demoTP3_1.png)

Vous serez le créateur du jeu. Nous allons simplement vous montrer quelques techniques de base. Vous devrez appliquer ces techniques afin de réaliser les tableaux et construire la logique du jeu. Chaque jeu sera donc unique. 

Nous allons donc vous expliquer comment:

* faire déplacer un personnage à l'aide du clavier;
* créer des ennemis de différentes facons (par un générateur tabulaire, et aléatoirement);
* détecter des éléments sur le tableau de jeu;
* créer et enlever des éléments dans le jeu;
* utiliser des minuteries pour faire des actions; 
* afficher des compteurs qui serviront pour les points, le nombre de vies, etc.;
* faire des animations simples;

Pour ce faire, nous allons reprendre certains éléments du jeu de la fusée et les améliorer. 

Ouvrez Greenfoot, et créez un nouveau scénario Java appeler DemoTP3. 


:::info
Le code complet de chaque étape sera donné à la fin de l'étape. 
Si votre programme ne fonctionne plus durant la présentation, vous n'avez qu'à copier ce code. 
:::

