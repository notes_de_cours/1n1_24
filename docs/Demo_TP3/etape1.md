# Création du personnage

Dans cette leçon vous allez apprendre comment déplacer un personnage à l'écran à l'aide du clavier. 

Afin de séparer la logique de chaque élément du jeu, une classe sera créée pour chacun d'eux. 

:::info Définition
Une classe représente une **catégorie d'objets**. Elle apparaît aussi comme un **moule** ou une **usine** à partir de laquelle il est possible de **créer des objets** ; c'est en quelque sorte une « boîte à outils » qui permet de fabriquer un objet. On parle alors d'un objet en tant **qu'instance** d'une classe (création d'un objet ayant les propriétés de la classe). 

[https://fr.wikipedia.org/wiki/Classe_(informatique) ]
:::

## La classe Personnage

Commençons par ajouter une sous-classe de `Actor` nommée `Personnage` utilisant l'image de la fusée. 

:::info Théorie
Le nom d'une classe est en PascalCase, c'est-à-dire: la première lettre de chaque mot est en majuscule
:::

Ajoutez un personnage dans MyWorld.

```java

import greenfoot.*;

public class MyWorld extends World
{

    Personnage joueur;  // [!code highlight]
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        
        joueur = new Personnage(); // [!code highlight]
        addObject(joueur, 60, getHeight()/2); // [!code highlight]
    }
}
```

:::info Théorie
Une variable définie dans une classe est nommée `attribut` ou `champs`. 

Les `attributs` appartiennent à une instance. Chaque fois qu'on fait un `new` afin de créer une instance, de l'espace mémoire est réservé pour chacun des attributs de la classe. 

On dit que la valeur de ses attributs détermine l'`état` d'un objet .
:::

## Déplacement du personnage

Pour déplacer le personnage, nous allons utiliser le clavier. Nous allons donc commencer par définir les touches à l'aide de constantes. 

Et ensuite réagir à l'aide de `Greenfoot.isKeyDown()`

Commencons par simplement faire tourner notre personnage dans la bonne direction. Nous allons changer l'angle de la rotation. 

Remplacez le code de la classe `Personnage` par celui-ci:
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Le personnage controlé par le joueur. 
 * 
 * @author Benoit Desrosiers
 * @version 20240916
 */
public class Personnage extends Actor
{
    private final String TOUCHE_DROITE = "right"; 
    private final String TOUCHE_GAUCHE = "left";
    private final String TOUCHE_BAS = "down";
    private final String TOUCHE_HAUT = "up";
    
    private final int ANGLE_HAUT = 270;
    private final int ANGLE_DROITE = 0;
    private final int ANGLE_BAS = 90;
    private final int ANGLE_GAUCHE = 180;

    /**
     * Act - do whatever the Personnage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int rotation = getRotation(); 
        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
        } 
        
        setRotation(rotation);
    }
}
```

Essayons ce code!

Comme vous pouvez le voir, ca fonctionne, mais c'est un peu bizarre. Lorsqu'on va vers la gauche, le personnage se retrouve la tête en bas (regardez l'aileron). 

Comment corriger ce problème ?

Avez-vous des suggestions ?

## Diriger le personnage vers la gauche

Afin de régler ce problème, nous allons utiliser un mirroir vertical. 

Mais pour se faire, nous devons connaitre l'ancienne rotation du personnage afin de savoir l'état précédent. 

Remplacez Personnage.act() par ce code:
```java
public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; // [!code highlight]
         if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
        } 
        // [!code highlight:4]
        if(ancienneRotation == ANGLE_GAUCHE || rotation == ANGLE_GAUCHE) {
            getImage().mirrorVertically();
        }
        setRotation(rotation);
    }
```

On essai. 

Oups!!! pourquoi ca flip ???? 

La raison est que l'ordinateur est très rapide. Il détecte qu'on a le doigt sur la touche plusieurs fois par seconde. L'ancienne rotation est à 180, donc on flip l'image. Le coup suivant, c'est rotation qui est aussi à 180, donc on flip.  Il faut donc vérifier si on a changé de direction avant de faire le flip. 

Changez le code pour le suivant:
```java
if(ancienneRotation != rotation) {// [!code highlight]
    if(ancienneRotation == ANGLE_GAUCHE || rotation == ANGLE_GAUCHE) {
        getImage().mirrorVertically();
    }
    setRotation(rotation);
}// [!code highlight]
```

:::warning Notez
Vous voyez l'importance de tester le code fréquemment. Il est fréquent de trouver de petits bugs de ce genre. Ils sont beaucoup plus faciles à localiser en testant fréquemment. 
:::

## Création d'une fonction pour tourner

:::info Théorie
Une fonction, qu'on appel aussi `méthode`, définie dans une classe, est accessible par toutes les instances de cette classe.
:::
Nous avons un beau bout de code qui a un rôle évident: faire tourner le personnage. Nous allons donc l'envoyer dans une fonction. 

Ajoutez la fonction suivante:
```java
   /**
     * Change la rotation du personnage
     * Si le personnage doit pointer vers la gauche, son image est pivotée
     * afin qu'il n'ait pas la tête en bas. Si on retourne vers la droite, 
     * il est pivoté de nouveau
     * 
     * @param aRotation la rotation demandée
     * @param aAncienneRotation l'ancienne rotation
     */
    public void tourner(int aRotation, int aAncienneRotation) {
        if(aRotation != aAncienneRotation) {
            if(aAncienneRotation == ANGLE_GAUCHE || aRotation == ANGLE_GAUCHE) {
                getImage().mirrorVertically();
            }
        }
        setRotation(aRotation);
    }
    
```

Remplacer le if que nous venons d'ajouter par l'appel de cette fonction 
```java
        tourner(rotation, ancienneRotation);
```

## Déplacement du personnage

Maintenant qu'on peut tourner le personnage, comment faire pour le faire avancer?

Nous allons détecter que la touche est pressée constamment afin de faire avancer le personnage. 

:::warning Attention
Cette technique n'est pas parfaite. Comme vous pourrez le voir, il sera difficle de faire tourner le personnage sans que celui-ci avance un peu. 

Mais si vous regardez certains vieux jeux, ils ont le même problème :D

https://arcadespot.com/game/the-legend-of-zelda/
:::

Ajoutez un boolean pour savoir si une touche a été pressée durant ce cycle, et mettre ce boolean à vrai quand une touche est pressée. 

Ensuite, si une touche a été pressée, tournez et déplacez le personnage. 

Voici le nouveau act():

```java
public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; 
        boolean touchePressee = false; // [!code highlight]
        
        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
            touchePressee = true;  // [!code highlight]
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
            touchePressee = true; // [!code highlight]
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
            touchePressee = true; // [!code highlight]
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
            touchePressee = true; // [!code highlight]
        } 
        
        if(touchePressee) {// [!code highlight]
            tourner(rotation, ancienneRotation);
            int distX = (int) Math.cos(Math.toRadians(rotation));// [!code highlight]
            int distY = (int) Math.sin(Math.toRadians(rotation));// [!code highlight]
            setLocation(getX()+distX, getY()+distY);// [!code highlight]
        }
    }
```

Wouuu, des cos, des radians ... Oui, parfois des notions de trigonométries sont utiles. 
Cette formule permet de calculer le déplacement à faire en X et en Y selon l'angle de la rotation. Ici, l'angle est toujours un multiple de 90. Le déplacement sera donc -1, 0, +1. 

Comme on peut voir, notre personnage est un peu lent. 

Pour l'accélérer, nous allons lui donner une vitesse de croissière. 

Ajoutez une constante, et multiplié le saut distX et distY par cette constante. 

```java
    private final int VITESSE = 4;
```

```java
    int distX = (int) Math.cos(Math.toRadians(rotation))*VITESSE;
    int distY = (int) Math.sin(Math.toRadians(rotation))*VITESSE;
```

## Limitation du déplacement. 

Par défaut, Greenfoot ne laissera pas les objets sortir de l'écran. C'est pour cela que le personnage ne sort pas lorsqu'on arrive sur le bord du jeu. 

Il serait possible de changer ce comportement en ajoutant un argument lors de l'appel à `super()` dans `MyWorld()`. Nous le ferons un peu plus tard. 

## Conclusion 

Dans cette leçon nous avons appris:

* comment utiliser les touches du clavier;
* comment faire tourner une image;
* comment faire avancer le personnage;


## Code pour cette étape

:::details MyWorld
```java

import greenfoot.*;

public class MyWorld extends World
{

    Personnage joueur;  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        
        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 
    }
}
```
::::

:::details Personnage
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Le personnage controlé par le joueur. 
 * 
 * @author Benoit Desrosiers
 * @version 20240916
 */
public class Personnage extends Actor
{
    private final String TOUCHE_DROITE = "right"; 
    private final String TOUCHE_GAUCHE = "left";
    private final String TOUCHE_BAS = "down";
    private final String TOUCHE_HAUT = "up";

    private final int ANGLE_HAUT = 270;
    private final int ANGLE_DROITE = 0;
    private final int ANGLE_BAS = 90;
    private final int ANGLE_GAUCHE = 180;

    private final int VITESSE = 4;

    /**
     * Act - do whatever the Personnage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; 
        boolean touchePressee = false;

        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
            touchePressee = true; 
        } 

        if(touchePressee) {
            tourner(rotation, ancienneRotation);
            int distX = (int) Math.cos(Math.toRadians(rotation))*VITESSE;
            int distY = (int) Math.sin(Math.toRadians(rotation))*VITESSE;
            setLocation(getX()+distX, getY()+distY);
        }
    }

    /**
     * Change la rotation du personnage
     * Si le personnage doit pointer vers la gauche, son image est pivotée
     * afin qu'il n'ait pas la tête en bas. Si on retourne vers la droite, 
     * il est pivoté de nouveau
     * 
     * @param aRotation la rotation demandée
     * @param aAncienneRotation l'ancienne rotation
     */
    public void tourner(int aRotation, int aAncienneRotation) {
        if(aRotation != aAncienneRotation) {
            if(aAncienneRotation == ANGLE_GAUCHE || aRotation == ANGLE_GAUCHE) {
                getImage().mirrorVertically();
            }
        }
        setRotation(aRotation);
    }
}
```
:::
