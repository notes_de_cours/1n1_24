# Création d'un niveau

Dans le premier démo de la fusée nous avons vu comment changer de World lorsque la fusée était détruite. 

Dans le TP3, il sera aussi nécessaire de rediriger vers un écran de fin lorsque le joueur n'aura plus de vies. Mais il sera aussi intéressant de faire des changements de niveaux lorsque le joueur termine le niveau ou traverse une porte pour changer de niveau. 

Afin de vous introduire à la construction d'un niveau, nous allons construire un générateur de niveaux. 

:::warning Pourquoi ne pas simplement changer de niveau comme dans la fusée ?
Il ne serait pas utile de changer de World pour changer de niveau pour les raisons suivantes:
* il faudrait s'assurer que les Worlds aient tous les mêmes capacités. Nous verrons que le World contient les *règles* du jeu. Il serait possible de réaliser cela avec de l'héritage, mais l'héritage n'est pas la réponse à tous les problèmes.
* il faudrait communiquer beaucoup d'information entre les Worlds: le joueur, le pointage, la taille des éléments ...  
:::

## Les niveaux

Si vous observez l'image du prototype du TP3, vous remarquerez qu'il pourrait être représenté par un tableau à 2 dimensions. 

Nous allons donc créer une classe qui lira un tableau 2 dimensions et qui y placera les objets.

Nous allons mettre ce tableau dans une classe ne contenant que des tableaux. Elle servira donc de *base de données*


Créez la classe `NiveauBD` (BD pour base de données). Cette classe ne descend d'aucune autre classe.

Enlevez les variables et les fonctions prédéfinies. 

Créez un tableau 2 dimensions:

```java 
private int[][][] elements = { 
    //niveau 0
    {    
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        },
    };
```

Jusqu'à présent, nous avons redimensionné les images des éléments à 40x40. Ce tableau est constitué de 10 lignes de 15 éléments, ce qui donne 10 * 40->400 de haut, et 15 * 40->600 de large, soit exactement les dimensions de notre jeu. Bien entendu, si vous désirez faire un jeu d'une autre dimension, il vous faudra ajuster cette taille en conséquence. 

Ajoutez le code suivant à la fin de `NiveauBD`:
```java
/**
 * retourne un tableau bidimensionel 
 * @param indice le numéro du tableau à retourner (commence à 0)
 */
public int[][] getTableauAIndice(int indice) {
        return elements[indice];
}
```

:::info Théorie
Il est courant et recommandé d'utiliser le modificateur d'accès `private` pour les attributs (et certaines méthodes) d'une classe.

Un attribut private n'est accessible que part la classe dans laquelle il est défini. Afin de permettre à d'autre classe de l'utiliser, un `getter` est ajouté (getTableauAIndice() ).

L'utilisation des getters (et des setters) est une technique courante et recommandée. 
:::
**Question**: Avez-vous remarqué ... ce tableau est à 3 dimensions. Pourquoi 3?

:::details réponse
Parce que nous aurons plusieurs niveaux, donc plusieurs tableaux 2 dimensions. Nous les mettons donc eux-mêmes dans un tableau. 
:::

## Le générateur de niveaux

Nous allons maintenant créer une classe qui lira ce tableau et qui générera différents types d'objets selon la valeur de chacune des cases. 

:::info Théorie
Pourquoi ne pas mettre les tableaux dans la même classe que le générateur ?

Réponse: Afin de pouvoir réutiliser le générateur avec différents tableaux. 

Si on veux changer les tableaux du jeu, il suffit de changer la *base de données*. (Tant que les types d'éléments de jeu demeurent les mêmes)
:::

Créez une classe appelée `GenerateurNiveaux`. Celle-ci aussi n'est descendante d'aucune classe. 

Remplacez le code par celui-ci:

```java
import greenfoot.*;  
import java.util.*;

/**
 * Génération d'un niveau basé sur un matrice contenant le numéro des éléments à ajouter
 *
 */
public class GenerateurNiveaux  
{
    
    private int[][] elements;
    private  int largeur;
    private  int hauteur;

    /** 
     * Constructeur 
     * @param aLargeur la largeur en pixels des éléments 
     * @param aHauteur la hauteur en pixels des éléments
     * @param aNiveau le tableau à 2 dimensions contenant les id des éléments à ajouter
     * 
     * 0) aucun élément
     * 1) un mur
     */
    public GenerateurNiveaux(int aLargeur, int aHauteur, int [][] aNiveau) {
        largeur = aLargeur;
        hauteur = aHauteur;
        elements = aNiveau;
    }
    
    /**
     * Génère le niveau 
     * @param aWorld le World appelant 
     */
    public void genererNiveau(MyWorld aWorld) {
        for(int i = 0; i<elements.length; i++) {
            int posY = i*hauteur+hauteur/2;
            for(int j = 0; j<elements[i].length; j++) {
                int posX = j*largeur+largeur/2;
                switch(elements[i][j]) {
                        case(0):
                        break;
                        case(1): 
                            Meteore m1 = new Meteore();
                            aWorld.addObject(m1, posX, posY);
                            break;
                        case(2):
                
                            break;
                }
            }
        }  
    }
    
    /**
     * La largeur en pixel du niveau
     */
    public int getLargeurNiveau() {
        return largeur * elements[0].length;
    }
    
    /**
     * La hauteur en pixel du niveau
     */
    public int getHauteurNiveau() {
        return hauteur * elements.length;
    }
}

```

Le constructeur recoit la dimension en pixels des éléments qui seront générés, ainsi que le tableau bidimensionel provenant de la bd.

La fonction `genereNiveau` parcours le tableau et génère les éléments selon leur identificateur. 

:::info
Cet identificateur n'a aucune signification particulière. J'ai décidé que 0 était une case vide, que 1 était un météore. Mais ca pourrait être 42. L'important est que le créateur de la BD et le créateur du générateur s'entendent sur la signification d'une valeur.
:::

Il ne reste qu'à appeler le générateur dans MyWorld. 

Enlevez la ligne générant le météore dans `Myworld` et ajoutez ces lignes:
```java
int[][] niveau1 = new NiveauBD().getTableauAIndice(0);
GenerateurNiveaux gn = new GenerateurNiveaux(40,40,niveau1);
gn.genererNiveau(this);
```

Faites un reset ... vous devriez avoir des météores un peu partout. 

:::warning Pourquoi créer des météores en bloc comme ca ?
La réponse et simplement que cette technique peut être réutilisée dans le TP3. 

Si vous remplacez les météores par des murs, il devient évident qu'on peut ainsi rapidement générer des tableaux de jeu. 

Nous verrons plus tard qu'il sera facile d'ajouter d'autres types d'éléments dans le jeu à l'aide de cette technique. 

Et pour changer de niveaux, il vous faudra simplement effacer tous les éléments qui sont déjà dans ce monde, et rappeler le générateur avec un autre niveau. Nous y reviendrons.
:::

Afin de rendre la création des météores un peu plus intéressante, nous allons déplacer tout le tableau à l'extérieur de l'écran et laisser les météores apparaitre les uns après les autre. 

Modifiez, dans `GenerateurNiveaux` la ligne du addObject (ligne 44) pour celle-ci:
```java
aWorld.addObject(m1, posX+aWorld.getWidth(), posY);
```

Testez... 

oups, les météores sont tous allignés sur le bord du jeu... pourquoi ?

:::details Réponse
Lors de l'initialisation de MyWorld, nous avons utilisez le super constructeur qui ne permet pas que les objets sortent de l'écran. 

Changez donc la première ligne dans MyWorld() pour celle-ci:
```java
super(600, 400, 1,false); 
```
:::

Testez...

Ca fonctionne, les météores apparaissent selon leur position dans le tableau. 

Sauf que maintenant notre fusée peut sortir du jeu... 

Il est maintenant nécessaire de limiter les déplacements de la fusée. Pour ce faire, nous allons utiliser une petite passe-passe mathématique. 

Remplacez la ligne du `setLocation` dans `Personnage.act()` (ligne 56) par celle-ci:
```java
setLocation(Math.max(20,Math.min(getWorld().getWidth()-20, getX()+distX)), 
            Math.max(20,Math.min(getWorld().getHeight()-20,getY()+distY)));
```




## Conclusion

Dans cette leçon nous avons appris:
* comment générer des niveaux;
* comment empêcher un objet de sortir du jeu si le jeu est ouvert. 



:::tip Réutilisation de ce code
Pour le TP3, vous pouvez réutiliser la majorité de ce code. 

Certaines classes comme le `GenerateurNiveaux` et `NiveauBD` peuvent être réutilisées telles quelles.
:::


## Code pour cette étape

:::details MyWorld
```java
import greenfoot.*;

public class MyWorld extends World
{

    Personnage joueur;  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 

        int[][] niveau1 = new NiveauBD().getTableauAIndice(0);
        GenerateurNiveaux gn = new GenerateurNiveaux(40,40,niveau1);
        gn.genererNiveau(this);

        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 
    }
}
```
::::

:::details Personnage
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Le personnage controlé par le joueur. 
 * 
 * @author Benoit Desrosiers
 * @version 20240916
 */
public class Personnage extends Actor
{
    private final String TOUCHE_DROITE = "right"; 
    private final String TOUCHE_GAUCHE = "left";
    private final String TOUCHE_BAS = "down";
    private final String TOUCHE_HAUT = "up";

    private final int ANGLE_HAUT = 270;
    private final int ANGLE_DROITE = 0;
    private final int ANGLE_BAS = 90;
    private final int ANGLE_GAUCHE = 180;

    private final int VITESSE = 4;

    /**
     * Act - do whatever the Personnage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; 
        boolean touchePressee = false;

        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
            touchePressee = true; 
        } 

        if(touchePressee) {
            tourner(rotation, ancienneRotation);
            int distX = (int) Math.cos(Math.toRadians(rotation))*VITESSE;
            int distY = (int) Math.sin(Math.toRadians(rotation))*VITESSE;
            int distX_corrigee= distX + (getImage().getWidth()*Integer.signum(distX)/2);
            int distY_corrigee= distY +  (getImage().getWidth()*Integer.signum(distY)/2);
            List<Meteore> elements = getObjectsAtOffset(distX_corrigee, distY_corrigee,  Meteore.class);
            if(elements.isEmpty()) { //le personnage ne frappe pas un meteore
                setLocation(Math.max(20,Math.min(getWorld().getWidth()-20, getX()+distX)), 
                    Math.max(20,Math.min(getWorld().getHeight()-20,getY()+distY)));
            }      
        }
    }

    /**
     * Change la rotation du personnage
     * Si le personnage doit pointer vers la gauche, son image est pivotée
     * afin qu'il n'ait pas la tête en bas. Si on retourne vers la droite, 
     * il est pivoté de nouveau
     * 
     * @param aRotation la rotation demandée
     * @param aAncienneRotation l'ancienne rotation
     */
    public void tourner(int aRotation, int aAncienneRotation) {
        if(aRotation != aAncienneRotation) {
            if(aAncienneRotation == ANGLE_GAUCHE || aRotation == ANGLE_GAUCHE) {
                getImage().mirrorVertically();
            }
        }
        setRotation(aRotation);
    }
}
```
:::

:::details Meteore
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Meteore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Meteore extends Actor
{
    public Meteore() {
        GreenfootImage im = getImage();
        if(im != null) {        
            im.scale(40,40);
            setImage(im);
        }
    }

    /**
     * Act - do whatever the Meteore wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int distX = Greenfoot.getRandomNumber(3);
        int distY = Greenfoot.getRandomNumber(9)-4;
        List<Personnage> elements = getObjectsAtOffset(distX, distY, Personnage.class);    
        if(elements.isEmpty()) { 
            setLocation(getX()-distX, getY()+distY);
        }

        turn(Greenfoot.getRandomNumber(5));

        if( getX() == 0 ) { 
            getWorld().removeObject(this);
        }
    }
}

```
:::


:::details NiveauBD
```java
/**
 * Write a description of class NiveauBD here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NiveauBD  
{
    private int[][][] elements = { 
            //niveau 0
            {    
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            },
        };

    /**
     * retourne un tableau bidimensionel 
     * @param indice le numéro du tableau à retourner (commence à 0)
     */
    public int[][] getTableauAIndice(int indice) {
        return elements[indice];
    }
}
```
:::

:::details GenerateurNiveaux
```java
import greenfoot.*;  
import java.util.*;

/**
 * Génération d'un niveau basé sur un matrice contenant le numéro des éléments à ajouter
 *
 */
public class GenerateurNiveaux  
{

    private int[][] elements;
    private  int largeur;
    private  int hauteur;

    /** 
     * Constructeur 
     * @param aLargeur la largeur en pixels des éléments 
     * @param aHauteur la hauteur en pixels des éléments
     * @param aNiveau le tableau à 2 dimensions contenant les id des éléments à ajouter
     * 
     * 0) aucun élément
     * 1) un mur
     */
    public GenerateurNiveaux(int aLargeur, int aHauteur, int [][] aNiveau) {
        largeur = aLargeur;
        hauteur = aHauteur;
        elements = aNiveau;
    }

    /**
     * Génère le niveau 
     * @param aWorld le World appelant 
     */
    public void genererNiveau(MyWorld aWorld) {
        for(int i = 0; i<elements.length; i++) {
            int posY = i*hauteur+hauteur/2;
            for(int j = 0; j<elements[i].length; j++) {
                int posX = j*largeur+largeur/2;
                switch(elements[i][j]) {
                        case(0):
                        break;
                        case(1): 
                        Meteore m1 = new Meteore();
                        aWorld.addObject(m1, posX+aWorld.getWidth(), posY);        
                        break;
                        case(2):

                        break;
                }
            }
        }  
    }

    /**
     * La largeur en pixel du niveau
     */
    public int getLargeurNiveau() {
        return largeur * elements[0].length;
    }

    /**
     * La hauteur en pixel du niveau
     */
    public int getHauteurNiveau() {
        return hauteur * elements.length;
    }
}
```
:::
