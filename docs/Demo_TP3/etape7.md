# Affichage des points

Il serait intéressant d'avoir un compteur affichant le pointage. 

Ici le pointage sera uniquement le nombre de cycle depuis le début de la partie. 

## Ajout de l'affichage.

Greenfoot fournit un affichage pour un compteur dans le menu **Edit/Import class**, importez `Counter`

Ajoutez cette variable dans `MyWorld`:

```java 
private Counter affichagePointage;
```

et ce code dans le constructeur de `MyWorld()`:
```java
affichagePointage = new Counter();
addObject(affichagePointage, this.getWidth()/2, 20);
```

et finalement ce code à la fin de `MyWorld.act()`:
```java
affichagePointage.add(1);
```

La valeur de ce compteur pourrait être transféré à l'écran de fin lorsque la fusée est détruite. 

## Conclusion 

Dans cette leçon nous avons appris:

* comment ajouter un affichage. 

## Code partiel cette étape

Seules les classes ayant changées sont fournies. Pour les autres, allez à la leçon précédente. 

:::details MyWorld
```java
import greenfoot.*;
import java.util.List;

public class MyWorld extends World
{

    Personnage joueur; 
    private Counter affichagePointage;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 

        int[][] niveau1 = new NiveauBD().getTableauAIndice(0);
        GenerateurNiveaux gn = new GenerateurNiveaux(40,40,niveau1);
        gn.genererNiveau(this);

        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 

        affichagePointage = new Counter();
        addObject(affichagePointage, this.getWidth()/2, 20);
    }

    public void act() {
        if( Greenfoot.getRandomNumber(50) == 1) {
            Meteore m = new Meteore();
            addObject(m, getWidth(), Greenfoot.getRandomNumber(getHeight()-50)+10);
        }
        affichagePointage.add(1);
    }

    public void tuerPersonnage() {
        removeObject(joueur);
        // C'est ici que le changement de tableau pourrait se faire
        // ou vous pourriez avoir un nombre de vies qui serait décrémenté ici. 
    }

    public void detruireMeteore(Meteore aMeteore) {
        addObject(new Explosion(), aMeteore.getX(), aMeteore.getY());
        removeObject(aMeteore);
    }

    public void detruireTout() {
        List<Meteore> meteores = getObjects(Meteore.class);
        for(Meteore m  : meteores) {
            detruireMeteore(m);
        }
    }
}
```
:::

Vous devez aussi inclure la classe `Counter`.
