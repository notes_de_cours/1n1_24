# Capture d'un objet

Nous allons ajouter une mégabombe qui permettra au joueur de faire exploser tous les météores. 

## Ajout de météores

Commencons par ajouter la génération de météores aléatoirement. 

Ajoutez la méthode `MyWorld.act()`:
```java
public void act() {
    if( Greenfoot.getRandomNumber(50) == 1) {
        Meteore m = new Meteore();
        addObject(m, getWidth(), Greenfoot.getRandomNumber(getHeight()-50)+10);
    }
}
```

Testez...

Après la génération initiale des méthéores, il devrait continuer d'y avoir des météores qui apparaissent aléatoirement. 

## Ajout de la mégabombe

Ajoutez la classe `Megabombe` sous `Element` et prenez l'image de la bombe. 

Ajoutez la génération de la mégabombe dans le générateur de tableaux. 

* Ajoutez la valeur 3 en haut de la 4e colonne de droite du niveau 1 dans `NiveauBD`.

* Ajoutez le cas 3 dans GenerateurNiveaux:

```java
case(3):
    aWorld.addObject(new Megabombe(), posX,posY);
    break;
```

## Capture de la mégabombe

Ajoutez le codes suivant à la fin de `Personnage.act()`:
```java
Megabombe mb  = (Megabombe)getOneIntersectingObject(Megabombe.class);
if(mb != null) {
    ((MyWorld)getWorld()).detruireTout();
}
```

et ajoutez MyWorld.detruireTout():
```java
public void detruireTout() {
    List<Meteore> meteores = getObjects(Meteore.class);
    for(Meteore m  : meteores) {
        detruireMeteore(m);
    }
}
```

Vous aurez à inclure List dans MyWorld. 

Testez... essayez de vous rendre à la bombe... tous les météores devraient exploser. 


## Conclusion 

Dans cette leçon nous avons appris:

* comment faire une action lorsqu'on *capture* un objet;
* comment ajouter des objets aléatoirement.

## Code partiel cette étape

Seules les classes ayant changées sont fournies. Pour les autres, allez à la leçon précédente. 

:::details MyWorld
```java
import greenfoot.*;
import java.util.List;

public class MyWorld extends World
{

    Personnage joueur;  
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 

        int[][] niveau1 = new NiveauBD().getTableauAIndice(0);
        GenerateurNiveaux gn = new GenerateurNiveaux(40,40,niveau1);
        gn.genererNiveau(this);

        joueur = new Personnage(); 
        addObject(joueur, 60, getHeight()/2); 
    }

    public void act() {
        if( Greenfoot.getRandomNumber(50) == 1) {
            Meteore m = new Meteore();
            addObject(m, getWidth(), Greenfoot.getRandomNumber(getHeight()-50)+10);
        }
    }

    public void tuerPersonnage() {
        removeObject(joueur);
        // C'est ici que le changement de tableau pourrait se faire
        // ou vous pourriez avoir un nombre de vies qui serait décrémenté ici. 
    }

    public void detruireMeteore(Meteore aMeteore) {
        addObject(new Explosion(), aMeteore.getX(), aMeteore.getY());
        removeObject(aMeteore);
    }

    public void detruireTout() {
        List<Meteore> meteores = getObjects(Meteore.class);
        for(Meteore m  : meteores) {
            detruireMeteore(m);
        }
    }
}
```
:::

:::details Personnage
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Le personnage controlé par le joueur. 
 * 
 * @author Benoit Desrosiers
 * @version 20240916
 */
public class Personnage extends Actor
{
    private final String TOUCHE_DROITE = "right"; 
    private final String TOUCHE_GAUCHE = "left";
    private final String TOUCHE_BAS = "down";
    private final String TOUCHE_HAUT = "up";

    private final int ANGLE_HAUT = 270;
    private final int ANGLE_DROITE = 0;
    private final int ANGLE_BAS = 90;
    private final int ANGLE_GAUCHE = 180;

    private final int VITESSE = 4;

    /**
     * Act - do whatever the Personnage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        int rotation = getRotation(); 
        int ancienneRotation = rotation; 
        boolean touchePressee = false;

        if (Greenfoot.isKeyDown(TOUCHE_BAS)) {
            rotation = ANGLE_BAS;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_HAUT)) {
            rotation = ANGLE_HAUT;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_DROITE)) {
            rotation = ANGLE_DROITE;
            touchePressee = true; 
        } else if (Greenfoot.isKeyDown(TOUCHE_GAUCHE)) {
            rotation = ANGLE_GAUCHE; 
            touchePressee = true; 
        } 

        if(touchePressee) {
            tourner(rotation, ancienneRotation);
            int distX = (int) Math.cos(Math.toRadians(rotation))*VITESSE;
            int distY = (int) Math.sin(Math.toRadians(rotation))*VITESSE;
            int distX_corrigee= distX + (getImage().getWidth()*Integer.signum(distX)/2);
            int distY_corrigee= distY +  (getImage().getWidth()*Integer.signum(distY)/2);
            List<Meteore> elements = getObjectsAtOffset(distX_corrigee, distY_corrigee,  Meteore.class);
            if(elements.isEmpty()) { //le personnage ne frappe pas un meteore
                setLocation(Math.max(20,Math.min(getWorld().getWidth()-20, getX()+distX)), 
                    Math.max(20,Math.min(getWorld().getHeight()-20,getY()+distY)));
            }      
        }

        Megabombe mb  = (Megabombe)getOneIntersectingObject(Megabombe.class);
        if(mb != null) {
            ((MyWorld)getWorld()).detruireTout();
        }
    }

    /**
     * Change la rotation du personnage
     * Si le personnage doit pointer vers la gauche, son image est pivotée
     * afin qu'il n'ait pas la tête en bas. Si on retourne vers la droite, 
     * il est pivoté de nouveau
     * 
     * @param aRotation la rotation demandée
     * @param aAncienneRotation l'ancienne rotation
     */
    public void tourner(int aRotation, int aAncienneRotation) {
        if(aRotation != aAncienneRotation) {
            if(aAncienneRotation == ANGLE_GAUCHE || aRotation == ANGLE_GAUCHE) {
                getImage().mirrorVertically();
            }
        }
        setRotation(aRotation);
    }
}
```
:::

:::details NiveauBD
```java
/**
 * Write a description of class NiveauBD here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NiveauBD  
{
    private int[][][] elements = { 
            //niveau 0
            {    
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,1,0,0,0,0,0,0,3,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,2,1},
                {1,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,1,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            },
        };

    /**
     * retourne un tableau bidimensionel 
     * @param indice le numéro du tableau à retourner (commence à 0)
     */
    public int[][] getTableauAIndice(int indice) {
        return elements[indice];
    }
}
```
:::

:::details GenerateurNiveaux
```java
import greenfoot.*;  
import java.util.*;

/**
 * Génération d'un niveau basé sur un matrice contenant le numéro des éléments à ajouter
 *
 */
public class GenerateurNiveaux  
{

    private int[][] elements;
    private  int largeur;
    private  int hauteur;

    /** 
     * Constructeur 
     * @param aLargeur la largeur en pixels des éléments 
     * @param aHauteur la hauteur en pixels des éléments
     * @param aNiveau le tableau à 2 dimensions contenant les id des éléments à ajouter
     * 
     * 0) aucun élément
     * 1) un mur
     */
    public GenerateurNiveaux(int aLargeur, int aHauteur, int [][] aNiveau) {
        largeur = aLargeur;
        hauteur = aHauteur;
        elements = aNiveau;
    }

    /**
     * Génère le niveau 
     * @param aWorld le World appelant 
     */
    public void genererNiveau(MyWorld aWorld) {
        for(int i = 0; i<elements.length; i++) {
            int posY = i*hauteur+hauteur/2;
            for(int j = 0; j<elements[i].length; j++) {
                int posX = j*largeur+largeur/2;
                switch(elements[i][j]) {
                        case(0):
                        break;
                        case(1): 
                        Meteore m1 = new Meteore();
                        aWorld.addObject(m1, posX+aWorld.getWidth(), posY);        
                        break;
                        case(2):
                        Ennemi e = new Ennemi();
                        aWorld.addObject(e,posX,posY);
                        e.setRotation(180);
                        break;
                        case(3):
                        aWorld.addObject(new Megabombe(), posX,posY);
                        break;
                }
            }
        }  
    }

    /**
     * La largeur en pixel du niveau
     */
    public int getLargeurNiveau() {
        return largeur * elements[0].length;
    }

    /**
     * La hauteur en pixel du niveau
     */
    public int getHauteurNiveau() {
        return hauteur * elements.length;
    }
}
```
:::

:::details Megabombe
```java
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Megabombe here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Megabombe extends Element
{
    /**
     * Act - do whatever the Megabombe wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        // Add your action code here.
    }
}
```
:::
